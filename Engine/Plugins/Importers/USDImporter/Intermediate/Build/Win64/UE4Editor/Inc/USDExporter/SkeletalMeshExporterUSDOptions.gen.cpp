// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDExporter/Public/SkeletalMeshExporterUSDOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSkeletalMeshExporterUSDOptions() {}
// Cross Module References
	USDEXPORTER_API UScriptStruct* Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions();
	UPackage* Z_Construct_UPackage__Script_USDExporter();
	USDCLASSES_API UScriptStruct* Z_Construct_UScriptStruct_FUsdStageOptions();
	USDEXPORTER_API UClass* Z_Construct_UClass_USkeletalMeshExporterUSDOptions_NoRegister();
	USDEXPORTER_API UClass* Z_Construct_UClass_USkeletalMeshExporterUSDOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
// End Cross Module References
class UScriptStruct* FSkeletalMeshExporterUSDInnerOptions::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern USDEXPORTER_API uint32 Get_Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions, Z_Construct_UPackage__Script_USDExporter(), TEXT("SkeletalMeshExporterUSDInnerOptions"), sizeof(FSkeletalMeshExporterUSDInnerOptions), Get_Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Hash());
	}
	return Singleton;
}
template<> USDEXPORTER_API UScriptStruct* StaticStruct<FSkeletalMeshExporterUSDInnerOptions>()
{
	return FSkeletalMeshExporterUSDInnerOptions::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions(FSkeletalMeshExporterUSDInnerOptions::StaticStruct, TEXT("/Script/USDExporter"), TEXT("SkeletalMeshExporterUSDInnerOptions"), false, nullptr, nullptr);
static struct FScriptStruct_USDExporter_StaticRegisterNativesFSkeletalMeshExporterUSDInnerOptions
{
	FScriptStruct_USDExporter_StaticRegisterNativesFSkeletalMeshExporterUSDInnerOptions()
	{
		UScriptStruct::DeferCppStructOps<FSkeletalMeshExporterUSDInnerOptions>(FName(TEXT("SkeletalMeshExporterUSDInnerOptions")));
	}
} ScriptStruct_USDExporter_StaticRegisterNativesFSkeletalMeshExporterUSDInnerOptions;
	struct Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StageOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StageOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUsePayload_MetaData[];
#endif
		static void NewProp_bUsePayload_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUsePayload;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PayloadFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PayloadFormat;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Separate struct inner class so that it can be reused by UAnimSequenceExporterUSDOptions without needing\n * a details customization\n */" },
		{ "ModuleRelativePath", "Public/SkeletalMeshExporterUSDOptions.h" },
		{ "ToolTip", "Separate struct inner class so that it can be reused by UAnimSequenceExporterUSDOptions without needing\na details customization" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSkeletalMeshExporterUSDInnerOptions>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_StageOptions_MetaData[] = {
		{ "Category", "USDSettings" },
		{ "ModuleRelativePath", "Public/SkeletalMeshExporterUSDOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_StageOptions = { "StageOptions", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshExporterUSDInnerOptions, StageOptions), Z_Construct_UScriptStruct_FUsdStageOptions, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_StageOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_StageOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_bUsePayload_MetaData[] = {
		{ "Category", "USDSettings" },
		{ "Comment", "/** If true, the mesh data is exported to yet another \"payload\" file, and referenced via a payload composition arc */" },
		{ "ModuleRelativePath", "Public/SkeletalMeshExporterUSDOptions.h" },
		{ "ToolTip", "If true, the mesh data is exported to yet another \"payload\" file, and referenced via a payload composition arc" },
	};
#endif
	void Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_bUsePayload_SetBit(void* Obj)
	{
		((FSkeletalMeshExporterUSDInnerOptions*)Obj)->bUsePayload = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_bUsePayload = { "bUsePayload", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FSkeletalMeshExporterUSDInnerOptions), &Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_bUsePayload_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_bUsePayload_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_bUsePayload_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_PayloadFormat_MetaData[] = {
		{ "Category", "USDSettings" },
		{ "Comment", "/** USD format to use for exported payload files */" },
		{ "EditCondition", "bUsePayload" },
		{ "GetOptions", "USDExporter.LevelExporterUSDOptions.GetUsdExtensions" },
		{ "ModuleRelativePath", "Public/SkeletalMeshExporterUSDOptions.h" },
		{ "ToolTip", "USD format to use for exported payload files" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_PayloadFormat = { "PayloadFormat", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSkeletalMeshExporterUSDInnerOptions, PayloadFormat), METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_PayloadFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_PayloadFormat_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_StageOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_bUsePayload,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::NewProp_PayloadFormat,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_USDExporter,
		nullptr,
		&NewStructOps,
		"SkeletalMeshExporterUSDInnerOptions",
		sizeof(FSkeletalMeshExporterUSDInnerOptions),
		alignof(FSkeletalMeshExporterUSDInnerOptions),
		Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_USDExporter();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SkeletalMeshExporterUSDInnerOptions"), sizeof(FSkeletalMeshExporterUSDInnerOptions), Get_Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions_Hash() { return 2038769577U; }
	void USkeletalMeshExporterUSDOptions::StaticRegisterNativesUSkeletalMeshExporterUSDOptions()
	{
	}
	UClass* Z_Construct_UClass_USkeletalMeshExporterUSDOptions_NoRegister()
	{
		return USkeletalMeshExporterUSDOptions::StaticClass();
	}
	struct Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Inner;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_USDExporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Options for exporting skeletal meshes to USD format.\n */" },
		{ "IncludePath", "SkeletalMeshExporterUSDOptions.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/SkeletalMeshExporterUSDOptions.h" },
		{ "ToolTip", "Options for exporting skeletal meshes to USD format." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics::NewProp_Inner_MetaData[] = {
		{ "Category", "USDSettings" },
		{ "ModuleRelativePath", "Public/SkeletalMeshExporterUSDOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics::NewProp_Inner = { "Inner", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USkeletalMeshExporterUSDOptions, Inner), Z_Construct_UScriptStruct_FSkeletalMeshExporterUSDInnerOptions, METADATA_PARAMS(Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics::NewProp_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics::NewProp_Inner_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics::NewProp_Inner,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USkeletalMeshExporterUSDOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics::ClassParams = {
		&USkeletalMeshExporterUSDOptions::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USkeletalMeshExporterUSDOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USkeletalMeshExporterUSDOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USkeletalMeshExporterUSDOptions, 3186099384);
	template<> USDEXPORTER_API UClass* StaticClass<USkeletalMeshExporterUSDOptions>()
	{
		return USkeletalMeshExporterUSDOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USkeletalMeshExporterUSDOptions(Z_Construct_UClass_USkeletalMeshExporterUSDOptions, &USkeletalMeshExporterUSDOptions::StaticClass, TEXT("/Script/USDExporter"), TEXT("USkeletalMeshExporterUSDOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USkeletalMeshExporterUSDOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
