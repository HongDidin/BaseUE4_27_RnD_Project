// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDSTAGE_USDTransactor_generated_h
#error "USDTransactor.generated.h already included, missing '#pragma once' in USDTransactor.h"
#endif
#define USDSTAGE_USDTransactor_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UUsdTransactor, NO_API)


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUsdTransactor(); \
	friend struct Z_Construct_UClass_UUsdTransactor_Statics; \
public: \
	DECLARE_CLASS(UUsdTransactor, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/USDStage"), NO_API) \
	DECLARE_SERIALIZER(UUsdTransactor) \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_ARCHIVESERIALIZER


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_INCLASS \
private: \
	static void StaticRegisterNativesUUsdTransactor(); \
	friend struct Z_Construct_UClass_UUsdTransactor_Statics; \
public: \
	DECLARE_CLASS(UUsdTransactor, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/USDStage"), NO_API) \
	DECLARE_SERIALIZER(UUsdTransactor) \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_ARCHIVESERIALIZER


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUsdTransactor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdTransactor) \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdTransactor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUsdTransactor(UUsdTransactor&&); \
	NO_API UUsdTransactor(const UUsdTransactor&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUsdTransactor(UUsdTransactor&&); \
	NO_API UUsdTransactor(const UUsdTransactor&); \
public: \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdTransactor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UUsdTransactor)


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_40_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h_43_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDSTAGE_API UClass* StaticClass<class UUsdTransactor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDStage_Public_USDTransactor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
