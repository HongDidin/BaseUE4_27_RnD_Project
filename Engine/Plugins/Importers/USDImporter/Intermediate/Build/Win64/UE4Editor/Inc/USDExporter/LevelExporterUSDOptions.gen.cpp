// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDExporter/Public/LevelExporterUSDOptions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLevelExporterUSDOptions() {}
// Cross Module References
	USDEXPORTER_API UClass* Z_Construct_UClass_ULevelExporterUSDOptions_NoRegister();
	USDEXPORTER_API UClass* Z_Construct_UClass_ULevelExporterUSDOptions();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_USDExporter();
	USDCLASSES_API UScriptStruct* Z_Construct_UScriptStruct_FUsdStageOptions();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FIntPoint();
	ENGINE_API UClass* Z_Construct_UClass_UAssetExportTask_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(ULevelExporterUSDOptions::execGetUsdExtensions)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FString>*)Z_Param__Result=ULevelExporterUSDOptions::GetUsdExtensions();
		P_NATIVE_END;
	}
	void ULevelExporterUSDOptions::StaticRegisterNativesULevelExporterUSDOptions()
	{
		UClass* Class = ULevelExporterUSDOptions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetUsdExtensions", &ULevelExporterUSDOptions::execGetUsdExtensions },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ULevelExporterUSDOptions_GetUsdExtensions_Statics
	{
		struct LevelExporterUSDOptions_eventGetUsdExtensions_Parms
		{
			TArray<FString> ReturnValue;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_ULevelExporterUSDOptions_GetUsdExtensions_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULevelExporterUSDOptions_GetUsdExtensions_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelExporterUSDOptions_eventGetUsdExtensions_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelExporterUSDOptions_GetUsdExtensions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelExporterUSDOptions_GetUsdExtensions_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelExporterUSDOptions_GetUsdExtensions_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelExporterUSDOptions_GetUsdExtensions_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelExporterUSDOptions_GetUsdExtensions_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelExporterUSDOptions, nullptr, "GetUsdExtensions", nullptr, nullptr, sizeof(LevelExporterUSDOptions_eventGetUsdExtensions_Parms), Z_Construct_UFunction_ULevelExporterUSDOptions_GetUsdExtensions_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelExporterUSDOptions_GetUsdExtensions_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00042401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelExporterUSDOptions_GetUsdExtensions_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelExporterUSDOptions_GetUsdExtensions_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelExporterUSDOptions_GetUsdExtensions()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelExporterUSDOptions_GetUsdExtensions_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULevelExporterUSDOptions_NoRegister()
	{
		return ULevelExporterUSDOptions::StaticClass();
	}
	struct Z_Construct_UClass_ULevelExporterUSDOptions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StageOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_StageOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartTimeCode_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StartTimeCode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndTimeCode_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_EndTimeCode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSelectionOnly_MetaData[];
#endif
		static void NewProp_bSelectionOnly_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSelectionOnly;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bBakeMaterials_MetaData[];
#endif
		static void NewProp_bBakeMaterials_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bBakeMaterials;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BakeResolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BakeResolution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRemoveUnrealMaterials_MetaData[];
#endif
		static void NewProp_bRemoveUnrealMaterials_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRemoveUnrealMaterials;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bUsePayload_MetaData[];
#endif
		static void NewProp_bUsePayload_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bUsePayload;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PayloadFormat_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PayloadFormat;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExportActorFolders_MetaData[];
#endif
		static void NewProp_bExportActorFolders_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExportActorFolders;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LowestLandscapeLOD_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LowestLandscapeLOD;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HighestLandscapeLOD_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_HighestLandscapeLOD;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LandscapeBakeResolution_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_LandscapeBakeResolution;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bExportSublayers_MetaData[];
#endif
		static void NewProp_bExportSublayers_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bExportSublayers;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_LevelsToIgnore_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LevelsToIgnore_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_LevelsToIgnore;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentTask_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentTask;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULevelExporterUSDOptions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_USDExporter,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ULevelExporterUSDOptions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ULevelExporterUSDOptions_GetUsdExtensions, "GetUsdExtensions" }, // 3635262567
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Options for exporting levels to USD format.\n */" },
		{ "HideCategories", "Hidden" },
		{ "IncludePath", "LevelExporterUSDOptions.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ToolTip", "Options for exporting levels to USD format." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_StageOptions_MetaData[] = {
		{ "Category", "Stage options" },
		{ "Comment", "/** Basic options about the stage to export */" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ShowOnlyInnerProperties", "" },
		{ "ToolTip", "Basic options about the stage to export" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_StageOptions = { "StageOptions", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelExporterUSDOptions, StageOptions), Z_Construct_UScriptStruct_FUsdStageOptions, METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_StageOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_StageOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_StartTimeCode_MetaData[] = {
		{ "Category", "Stage options" },
		{ "Comment", "/** StartTimeCode to be used for all exported layers */" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ToolTip", "StartTimeCode to be used for all exported layers" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_StartTimeCode = { "StartTimeCode", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelExporterUSDOptions, StartTimeCode), METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_StartTimeCode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_StartTimeCode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_EndTimeCode_MetaData[] = {
		{ "Category", "Stage options" },
		{ "Comment", "/** EndTimeCode to be used for all exported layers */" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ToolTip", "EndTimeCode to be used for all exported layers" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_EndTimeCode = { "EndTimeCode", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelExporterUSDOptions, EndTimeCode), METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_EndTimeCode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_EndTimeCode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bSelectionOnly_MetaData[] = {
		{ "Category", "Export settings" },
		{ "Comment", "/** Whether to export only the selected actors, and assets used by them */" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ToolTip", "Whether to export only the selected actors, and assets used by them" },
	};
#endif
	void Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bSelectionOnly_SetBit(void* Obj)
	{
		((ULevelExporterUSDOptions*)Obj)->bSelectionOnly = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bSelectionOnly = { "bSelectionOnly", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULevelExporterUSDOptions), &Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bSelectionOnly_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bSelectionOnly_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bSelectionOnly_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bBakeMaterials_MetaData[] = {
		{ "Category", "Export settings" },
		{ "Comment", "/** Whether to bake UE materials and add material bindings to the baked assets */" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ToolTip", "Whether to bake UE materials and add material bindings to the baked assets" },
	};
#endif
	void Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bBakeMaterials_SetBit(void* Obj)
	{
		((ULevelExporterUSDOptions*)Obj)->bBakeMaterials = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bBakeMaterials = { "bBakeMaterials", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULevelExporterUSDOptions), &Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bBakeMaterials_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bBakeMaterials_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bBakeMaterials_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_BakeResolution_MetaData[] = {
		{ "Category", "Export settings" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Resolution to use when baking materials into textures */" },
		{ "EditCondition", "bBakeMaterials" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ToolTip", "Resolution to use when baking materials into textures" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_BakeResolution = { "BakeResolution", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelExporterUSDOptions, BakeResolution), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_BakeResolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_BakeResolution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bRemoveUnrealMaterials_MetaData[] = {
		{ "Category", "Export settings" },
		{ "Comment", "/** Whether to remove the 'unrealMaterial' attribute after binding the corresponding baked material */" },
		{ "EditCondition", "bBakeMaterials" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ToolTip", "Whether to remove the 'unrealMaterial' attribute after binding the corresponding baked material" },
	};
#endif
	void Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bRemoveUnrealMaterials_SetBit(void* Obj)
	{
		((ULevelExporterUSDOptions*)Obj)->bRemoveUnrealMaterials = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bRemoveUnrealMaterials = { "bRemoveUnrealMaterials", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULevelExporterUSDOptions), &Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bRemoveUnrealMaterials_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bRemoveUnrealMaterials_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bRemoveUnrealMaterials_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bUsePayload_MetaData[] = {
		{ "Category", "Export settings" },
		{ "Comment", "/** If true, the actual static/skeletal mesh data is exported in \"payload\" files, and referenced via the payload composition arc */" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ToolTip", "If true, the actual static/skeletal mesh data is exported in \"payload\" files, and referenced via the payload composition arc" },
	};
#endif
	void Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bUsePayload_SetBit(void* Obj)
	{
		((ULevelExporterUSDOptions*)Obj)->bUsePayload = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bUsePayload = { "bUsePayload", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULevelExporterUSDOptions), &Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bUsePayload_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bUsePayload_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bUsePayload_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_PayloadFormat_MetaData[] = {
		{ "Category", "Export settings" },
		{ "Comment", "/** USD format to use for exported payload files */" },
		{ "EditCondition", "bUsePayload" },
		{ "GetOptions", "GetUsdExtensions" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ToolTip", "USD format to use for exported payload files" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_PayloadFormat = { "PayloadFormat", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelExporterUSDOptions, PayloadFormat), METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_PayloadFormat_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_PayloadFormat_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bExportActorFolders_MetaData[] = {
		{ "Category", "Export settings" },
		{ "Comment", "/** Whether to use UE actor folders as empty prims */" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ToolTip", "Whether to use UE actor folders as empty prims" },
	};
#endif
	void Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bExportActorFolders_SetBit(void* Obj)
	{
		((ULevelExporterUSDOptions*)Obj)->bExportActorFolders = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bExportActorFolders = { "bExportActorFolders", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULevelExporterUSDOptions), &Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bExportActorFolders_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bExportActorFolders_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bExportActorFolders_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LowestLandscapeLOD_MetaData[] = {
		{ "Category", "Export settings" },
		{ "ClampMin", "0" },
		{ "Comment", "/** Lowest of the LOD indices to export landscapes with (use 0 for full resolution) */" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ToolTip", "Lowest of the LOD indices to export landscapes with (use 0 for full resolution)" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LowestLandscapeLOD = { "LowestLandscapeLOD", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelExporterUSDOptions, LowestLandscapeLOD), METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LowestLandscapeLOD_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LowestLandscapeLOD_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_HighestLandscapeLOD_MetaData[] = {
		{ "Category", "Export settings" },
		{ "ClampMin", "0" },
		{ "Comment", "/**\n\x09 * Highest of the LOD indices to export landscapes with. Each value above 0 halves resolution.\n\x09 * The max value depends on the number of components and sections per component of each landscape, and may be clamped.\n\x09 */" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ToolTip", "Highest of the LOD indices to export landscapes with. Each value above 0 halves resolution.\nThe max value depends on the number of components and sections per component of each landscape, and may be clamped." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_HighestLandscapeLOD = { "HighestLandscapeLOD", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelExporterUSDOptions, HighestLandscapeLOD), METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_HighestLandscapeLOD_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_HighestLandscapeLOD_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LandscapeBakeResolution_MetaData[] = {
		{ "Category", "Export settings" },
		{ "ClampMin", "1" },
		{ "Comment", "/** Resolution to use when baking landscape materials into textures  */" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ToolTip", "Resolution to use when baking landscape materials into textures" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LandscapeBakeResolution = { "LandscapeBakeResolution", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelExporterUSDOptions, LandscapeBakeResolution), Z_Construct_UScriptStruct_FIntPoint, METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LandscapeBakeResolution_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LandscapeBakeResolution_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bExportSublayers_MetaData[] = {
		{ "Category", "Sublayers" },
		{ "Comment", "/** If true, will export sub-levels as separate layers (referenced as sublayers). If false, will collapse all sub-levels in a single exported root layer */" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ToolTip", "If true, will export sub-levels as separate layers (referenced as sublayers). If false, will collapse all sub-levels in a single exported root layer" },
	};
#endif
	void Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bExportSublayers_SetBit(void* Obj)
	{
		((ULevelExporterUSDOptions*)Obj)->bExportSublayers = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bExportSublayers = { "bExportSublayers", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULevelExporterUSDOptions), &Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bExportSublayers_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bExportSublayers_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bExportSublayers_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LevelsToIgnore_ElementProp = { "LevelsToIgnore", nullptr, (EPropertyFlags)0x0000000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LevelsToIgnore_MetaData[] = {
		{ "Category", "Sublayers" },
		{ "Comment", "/** Names of levels that should be ignored when collecting actors to export (e.g. \"Persistent Level\", \"Level1\", \"MySubLevel\", etc.) */" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ToolTip", "Names of levels that should be ignored when collecting actors to export (e.g. \"Persistent Level\", \"Level1\", \"MySubLevel\", etc.)" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LevelsToIgnore = { "LevelsToIgnore", nullptr, (EPropertyFlags)0x0010000000004005, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelExporterUSDOptions, LevelsToIgnore), METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LevelsToIgnore_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LevelsToIgnore_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_CurrentTask_MetaData[] = {
		{ "Category", "Hidden" },
		{ "Comment", "// We temporarily stash our export task here as a way of passing our options down to\n// the Python exporter, that does the actual level exporting\n" },
		{ "ModuleRelativePath", "Public/LevelExporterUSDOptions.h" },
		{ "ToolTip", "We temporarily stash our export task here as a way of passing our options down to\nthe Python exporter, that does the actual level exporting" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_CurrentTask = { "CurrentTask", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelExporterUSDOptions, CurrentTask), Z_Construct_UClass_UAssetExportTask_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_CurrentTask_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_CurrentTask_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULevelExporterUSDOptions_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_StageOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_StartTimeCode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_EndTimeCode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bSelectionOnly,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bBakeMaterials,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_BakeResolution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bRemoveUnrealMaterials,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bUsePayload,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_PayloadFormat,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bExportActorFolders,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LowestLandscapeLOD,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_HighestLandscapeLOD,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LandscapeBakeResolution,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_bExportSublayers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LevelsToIgnore_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_LevelsToIgnore,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelExporterUSDOptions_Statics::NewProp_CurrentTask,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULevelExporterUSDOptions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULevelExporterUSDOptions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULevelExporterUSDOptions_Statics::ClassParams = {
		&ULevelExporterUSDOptions::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ULevelExporterUSDOptions_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelExporterUSDOptions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULevelExporterUSDOptions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULevelExporterUSDOptions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULevelExporterUSDOptions, 349575585);
	template<> USDEXPORTER_API UClass* StaticClass<ULevelExporterUSDOptions>()
	{
		return ULevelExporterUSDOptions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULevelExporterUSDOptions(Z_Construct_UClass_ULevelExporterUSDOptions, &ULevelExporterUSDOptions::StaticClass, TEXT("/Script/USDExporter"), TEXT("ULevelExporterUSDOptions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULevelExporterUSDOptions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
