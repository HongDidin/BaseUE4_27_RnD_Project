// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDStage/Public/USDTransactor.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDTransactor() {}
// Cross Module References
	USDSTAGE_API UClass* Z_Construct_UClass_UUsdTransactor_NoRegister();
	USDSTAGE_API UClass* Z_Construct_UClass_UUsdTransactor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_USDStage();
// End Cross Module References
	void UUsdTransactor::StaticRegisterNativesUUsdTransactor()
	{
	}
	UClass* Z_Construct_UClass_UUsdTransactor_NoRegister()
	{
		return UUsdTransactor::StaticClass();
	}
	struct Z_Construct_UClass_UUsdTransactor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUsdTransactor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_USDStage,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdTransactor_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Class that allows us to log prim attribute changes into the unreal transaction buffer.\n * The AUsdStageActor owns one of these, and whenever a USD notice is fired this class transacts and serializes\n * the notice data with itself. When undo/redoing it applies its values to the AUsdStageActors' current stage.\n *\n * Additionally this class naturally allows multi-user (ConcertSync) support for USD stage interactions, by letting\n * these notice data to be mirrored on other clients.\n */" },
		{ "IncludePath", "USDTransactor.h" },
		{ "ModuleRelativePath", "Public/USDTransactor.h" },
		{ "ToolTip", "Class that allows us to log prim attribute changes into the unreal transaction buffer.\nThe AUsdStageActor owns one of these, and whenever a USD notice is fired this class transacts and serializes\nthe notice data with itself. When undo/redoing it applies its values to the AUsdStageActors' current stage.\n\nAdditionally this class naturally allows multi-user (ConcertSync) support for USD stage interactions, by letting\nthese notice data to be mirrored on other clients." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUsdTransactor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUsdTransactor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUsdTransactor_Statics::ClassParams = {
		&UUsdTransactor::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUsdTransactor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdTransactor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUsdTransactor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUsdTransactor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUsdTransactor, 4094166297);
	template<> USDSTAGE_API UClass* StaticClass<UUsdTransactor>()
	{
		return UUsdTransactor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUsdTransactor(Z_Construct_UClass_UUsdTransactor, &UUsdTransactor::StaticClass, TEXT("/Script/USDStage"), TEXT("UUsdTransactor"), false, nullptr, nullptr, nullptr);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UUsdTransactor)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
