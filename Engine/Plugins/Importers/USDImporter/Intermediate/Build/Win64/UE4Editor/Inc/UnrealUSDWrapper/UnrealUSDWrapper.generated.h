// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UNREALUSDWRAPPER_UnrealUSDWrapper_generated_h
#error "UnrealUSDWrapper.generated.h already included, missing '#pragma once' in UnrealUSDWrapper.h"
#endif
#define UNREALUSDWRAPPER_UnrealUSDWrapper_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_UnrealUSDWrapper_Public_UnrealUSDWrapper_h


#define FOREACH_ENUM_EUSDINITIALLOADSET(op) \
	op(EUsdInitialLoadSet::LoadAll) \
	op(EUsdInitialLoadSet::LoadNone) 

enum class EUsdInitialLoadSet : uint8;
template<> UNREALUSDWRAPPER_API UEnum* StaticEnum<EUsdInitialLoadSet>();

#define FOREACH_ENUM_EUSDPURPOSE(op) \
	op(EUsdPurpose::Default) \
	op(EUsdPurpose::Proxy) \
	op(EUsdPurpose::Render) \
	op(EUsdPurpose::Guide) 

enum class EUsdPurpose : int32;
template<> UNREALUSDWRAPPER_API UEnum* StaticEnum<EUsdPurpose>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
