// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDSTAGEEDITOR_USDStageEditorSettings_generated_h
#error "USDStageEditorSettings.generated.h already included, missing '#pragma once' in USDStageEditorSettings.h"
#endif
#define USDSTAGEEDITOR_USDStageEditorSettings_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUsdStageEditorSettings(); \
	friend struct Z_Construct_UClass_UUsdStageEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UUsdStageEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDStageEditor"), NO_API) \
	DECLARE_SERIALIZER(UUsdStageEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUUsdStageEditorSettings(); \
	friend struct Z_Construct_UClass_UUsdStageEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UUsdStageEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDStageEditor"), NO_API) \
	DECLARE_SERIALIZER(UUsdStageEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUsdStageEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdStageEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUsdStageEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdStageEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUsdStageEditorSettings(UUsdStageEditorSettings&&); \
	NO_API UUsdStageEditorSettings(const UUsdStageEditorSettings&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUsdStageEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUsdStageEditorSettings(UUsdStageEditorSettings&&); \
	NO_API UUsdStageEditorSettings(const UUsdStageEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUsdStageEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdStageEditorSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdStageEditorSettings)


#define Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_12_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDSTAGEEDITOR_API UClass* StaticClass<class UUsdStageEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDStageEditor_Private_USDStageEditorSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
