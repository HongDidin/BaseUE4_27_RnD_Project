// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDStageImporter/Private/USDStageAssetImportFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDStageAssetImportFactory() {}
// Cross Module References
	USDSTAGEIMPORTER_API UClass* Z_Construct_UClass_UUsdStageAssetImportFactory_NoRegister();
	USDSTAGEIMPORTER_API UClass* Z_Construct_UClass_UUsdStageAssetImportFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_USDStageImporter();
	USDSTAGEIMPORTER_API UScriptStruct* Z_Construct_UScriptStruct_FUsdStageImportContext();
// End Cross Module References
	void UUsdStageAssetImportFactory::StaticRegisterNativesUUsdStageAssetImportFactory()
	{
	}
	UClass* Z_Construct_UClass_UUsdStageAssetImportFactory_NoRegister()
	{
		return UUsdStageAssetImportFactory::StaticClass();
	}
	struct Z_Construct_UClass_UUsdStageAssetImportFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportContext_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ImportContext;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUsdStageAssetImportFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_USDStageImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageAssetImportFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Factory to import USD files that gets called when we hit Import in the Content Browser, as well as during reimport */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "USDStageAssetImportFactory.h" },
		{ "ModuleRelativePath", "Private/USDStageAssetImportFactory.h" },
		{ "ToolTip", "Factory to import USD files that gets called when we hit Import in the Content Browser, as well as during reimport" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdStageAssetImportFactory_Statics::NewProp_ImportContext_MetaData[] = {
		{ "ModuleRelativePath", "Private/USDStageAssetImportFactory.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UUsdStageAssetImportFactory_Statics::NewProp_ImportContext = { "ImportContext", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdStageAssetImportFactory, ImportContext), Z_Construct_UScriptStruct_FUsdStageImportContext, METADATA_PARAMS(Z_Construct_UClass_UUsdStageAssetImportFactory_Statics::NewProp_ImportContext_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageAssetImportFactory_Statics::NewProp_ImportContext_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUsdStageAssetImportFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdStageAssetImportFactory_Statics::NewProp_ImportContext,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUsdStageAssetImportFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUsdStageAssetImportFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUsdStageAssetImportFactory_Statics::ClassParams = {
		&UUsdStageAssetImportFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUsdStageAssetImportFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageAssetImportFactory_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUsdStageAssetImportFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdStageAssetImportFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUsdStageAssetImportFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUsdStageAssetImportFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUsdStageAssetImportFactory, 3346496036);
	template<> USDSTAGEIMPORTER_API UClass* StaticClass<UUsdStageAssetImportFactory>()
	{
		return UUsdStageAssetImportFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUsdStageAssetImportFactory(Z_Construct_UClass_UUsdStageAssetImportFactory, &UUsdStageAssetImportFactory::StaticClass, TEXT("/Script/USDStageImporter"), TEXT("UUsdStageAssetImportFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUsdStageAssetImportFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
