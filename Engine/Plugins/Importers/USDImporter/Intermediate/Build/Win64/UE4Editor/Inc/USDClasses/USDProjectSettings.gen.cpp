// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDClasses/Public/USDProjectSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDProjectSettings() {}
// Cross Module References
	USDCLASSES_API UClass* Z_Construct_UClass_UUsdProjectSettings_NoRegister();
	USDCLASSES_API UClass* Z_Construct_UClass_UUsdProjectSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	UPackage* Z_Construct_UPackage__Script_USDClasses();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
// End Cross Module References
	void UUsdProjectSettings::StaticRegisterNativesUUsdProjectSettings()
	{
	}
	UClass* Z_Construct_UClass_UUsdProjectSettings_NoRegister()
	{
		return UUsdProjectSettings::StaticClass();
	}
	struct Z_Construct_UClass_UUsdProjectSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_AdditionalPluginDirectories_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdditionalPluginDirectories_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AdditionalPluginDirectories;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUsdProjectSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_USDClasses,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdProjectSettings_Statics::Class_MetaDataParams[] = {
		{ "DisplayName", "USDImporter" },
		{ "IncludePath", "USDProjectSettings.h" },
		{ "ModuleRelativePath", "Public/USDProjectSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UUsdProjectSettings_Statics::NewProp_AdditionalPluginDirectories_Inner = { "AdditionalPluginDirectories", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdProjectSettings_Statics::NewProp_AdditionalPluginDirectories_MetaData[] = {
		{ "Category", "General" },
		{ "ModuleRelativePath", "Public/USDProjectSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UUsdProjectSettings_Statics::NewProp_AdditionalPluginDirectories = { "AdditionalPluginDirectories", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdProjectSettings, AdditionalPluginDirectories), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UUsdProjectSettings_Statics::NewProp_AdditionalPluginDirectories_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdProjectSettings_Statics::NewProp_AdditionalPluginDirectories_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUsdProjectSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdProjectSettings_Statics::NewProp_AdditionalPluginDirectories_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdProjectSettings_Statics::NewProp_AdditionalPluginDirectories,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUsdProjectSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUsdProjectSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUsdProjectSettings_Statics::ClassParams = {
		&UUsdProjectSettings::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUsdProjectSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUsdProjectSettings_Statics::PropPointers),
		0,
		0x000800A4u,
		METADATA_PARAMS(Z_Construct_UClass_UUsdProjectSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdProjectSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUsdProjectSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUsdProjectSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUsdProjectSettings, 850370452);
	template<> USDCLASSES_API UClass* StaticClass<UUsdProjectSettings>()
	{
		return UUsdProjectSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUsdProjectSettings(Z_Construct_UClass_UUsdProjectSettings, &UUsdProjectSettings::StaticClass, TEXT("/Script/USDClasses"), TEXT("UUsdProjectSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUsdProjectSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
