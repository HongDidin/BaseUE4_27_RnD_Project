// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDEXPORTER_StaticMeshExporterUSD_generated_h
#error "StaticMeshExporterUSD.generated.h already included, missing '#pragma once' in StaticMeshExporterUSD.h"
#endif
#define USDEXPORTER_StaticMeshExporterUSD_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execIsUsdAvailable);


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execIsUsdAvailable);


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUStaticMeshExporterUsd(); \
	friend struct Z_Construct_UClass_UStaticMeshExporterUsd_Statics; \
public: \
	DECLARE_CLASS(UStaticMeshExporterUsd, UExporter, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(UStaticMeshExporterUsd)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUStaticMeshExporterUsd(); \
	friend struct Z_Construct_UClass_UStaticMeshExporterUsd_Statics; \
public: \
	DECLARE_CLASS(UStaticMeshExporterUsd, UExporter, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(UStaticMeshExporterUsd)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UStaticMeshExporterUsd(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UStaticMeshExporterUsd) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStaticMeshExporterUsd); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStaticMeshExporterUsd); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStaticMeshExporterUsd(UStaticMeshExporterUsd&&); \
	NO_API UStaticMeshExporterUsd(const UStaticMeshExporterUsd&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStaticMeshExporterUsd(UStaticMeshExporterUsd&&); \
	NO_API UStaticMeshExporterUsd(const UStaticMeshExporterUsd&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStaticMeshExporterUsd); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStaticMeshExporterUsd); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UStaticMeshExporterUsd)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_9_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDEXPORTER_API UClass* StaticClass<class UStaticMeshExporterUsd>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
