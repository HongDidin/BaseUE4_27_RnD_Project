// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDEXPORTER_MaterialExporterUSD_generated_h
#error "MaterialExporterUSD.generated.h already included, missing '#pragma once' in MaterialExporterUSD.h"
#endif
#define USDEXPORTER_MaterialExporterUSD_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMaterialExporterUsd(); \
	friend struct Z_Construct_UClass_UMaterialExporterUsd_Statics; \
public: \
	DECLARE_CLASS(UMaterialExporterUsd, UExporter, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(UMaterialExporterUsd)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesUMaterialExporterUsd(); \
	friend struct Z_Construct_UClass_UMaterialExporterUsd_Statics; \
public: \
	DECLARE_CLASS(UMaterialExporterUsd, UExporter, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(UMaterialExporterUsd)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMaterialExporterUsd(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMaterialExporterUsd) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMaterialExporterUsd); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMaterialExporterUsd); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMaterialExporterUsd(UMaterialExporterUsd&&); \
	NO_API UMaterialExporterUsd(const UMaterialExporterUsd&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMaterialExporterUsd(UMaterialExporterUsd&&); \
	NO_API UMaterialExporterUsd(const UMaterialExporterUsd&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMaterialExporterUsd); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMaterialExporterUsd); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMaterialExporterUsd)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_9_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDEXPORTER_API UClass* StaticClass<class UMaterialExporterUsd>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
