// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDEXPORTER_LevelExporterUSDOptions_generated_h
#error "LevelExporterUSDOptions.generated.h already included, missing '#pragma once' in LevelExporterUSDOptions.h"
#endif
#define USDEXPORTER_LevelExporterUSDOptions_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetUsdExtensions);


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetUsdExtensions);


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelExporterUSDOptions(); \
	friend struct Z_Construct_UClass_ULevelExporterUSDOptions_Statics; \
public: \
	DECLARE_CLASS(ULevelExporterUSDOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(ULevelExporterUSDOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_INCLASS \
private: \
	static void StaticRegisterNativesULevelExporterUSDOptions(); \
	friend struct Z_Construct_UClass_ULevelExporterUSDOptions_Statics; \
public: \
	DECLARE_CLASS(ULevelExporterUSDOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(ULevelExporterUSDOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelExporterUSDOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelExporterUSDOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelExporterUSDOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelExporterUSDOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelExporterUSDOptions(ULevelExporterUSDOptions&&); \
	NO_API ULevelExporterUSDOptions(const ULevelExporterUSDOptions&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelExporterUSDOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelExporterUSDOptions(ULevelExporterUSDOptions&&); \
	NO_API ULevelExporterUSDOptions(const ULevelExporterUSDOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelExporterUSDOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelExporterUSDOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelExporterUSDOptions)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_17_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDEXPORTER_API UClass* StaticClass<class ULevelExporterUSDOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSDOptions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
