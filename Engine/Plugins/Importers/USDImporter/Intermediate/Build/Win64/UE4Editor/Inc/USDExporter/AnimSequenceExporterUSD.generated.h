// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDEXPORTER_AnimSequenceExporterUSD_generated_h
#error "AnimSequenceExporterUSD.generated.h already included, missing '#pragma once' in AnimSequenceExporterUSD.h"
#endif
#define USDEXPORTER_AnimSequenceExporterUSD_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUAnimSequenceExporterUSD(); \
	friend struct Z_Construct_UClass_UAnimSequenceExporterUSD_Statics; \
public: \
	DECLARE_CLASS(UAnimSequenceExporterUSD, UExporter, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(UAnimSequenceExporterUSD)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUAnimSequenceExporterUSD(); \
	friend struct Z_Construct_UClass_UAnimSequenceExporterUSD_Statics; \
public: \
	DECLARE_CLASS(UAnimSequenceExporterUSD, UExporter, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(UAnimSequenceExporterUSD)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UAnimSequenceExporterUSD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UAnimSequenceExporterUSD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAnimSequenceExporterUSD); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimSequenceExporterUSD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAnimSequenceExporterUSD(UAnimSequenceExporterUSD&&); \
	NO_API UAnimSequenceExporterUSD(const UAnimSequenceExporterUSD&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UAnimSequenceExporterUSD(UAnimSequenceExporterUSD&&); \
	NO_API UAnimSequenceExporterUSD(const UAnimSequenceExporterUSD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UAnimSequenceExporterUSD); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UAnimSequenceExporterUSD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UAnimSequenceExporterUSD)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_12_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDEXPORTER_API UClass* StaticClass<class UAnimSequenceExporterUSD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_AnimSequenceExporterUSD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
