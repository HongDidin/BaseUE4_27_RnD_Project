// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDEXPORTER_LevelExporterUSD_generated_h
#error "LevelExporterUSD.generated.h already included, missing '#pragma once' in LevelExporterUSD.h"
#endif
#define USDEXPORTER_LevelExporterUSD_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelExporterUSD(); \
	friend struct Z_Construct_UClass_ULevelExporterUSD_Statics; \
public: \
	DECLARE_CLASS(ULevelExporterUSD, UExporter, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(ULevelExporterUSD)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_INCLASS \
private: \
	static void StaticRegisterNativesULevelExporterUSD(); \
	friend struct Z_Construct_UClass_ULevelExporterUSD_Statics; \
public: \
	DECLARE_CLASS(ULevelExporterUSD, UExporter, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(ULevelExporterUSD)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelExporterUSD(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelExporterUSD) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelExporterUSD); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelExporterUSD); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelExporterUSD(ULevelExporterUSD&&); \
	NO_API ULevelExporterUSD(const ULevelExporterUSD&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelExporterUSD(ULevelExporterUSD&&); \
	NO_API ULevelExporterUSD(const ULevelExporterUSD&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelExporterUSD); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelExporterUSD); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ULevelExporterUSD)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_9_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDEXPORTER_API UClass* StaticClass<class ULevelExporterUSD>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_LevelExporterUSD_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
