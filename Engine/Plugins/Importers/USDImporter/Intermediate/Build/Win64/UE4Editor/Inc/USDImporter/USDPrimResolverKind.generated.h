// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDIMPORTER_USDPrimResolverKind_generated_h
#error "USDPrimResolverKind.generated.h already included, missing '#pragma once' in USDPrimResolverKind.h"
#endif
#define USDIMPORTER_USDPrimResolverKind_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDPrimResolverKind(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDPrimResolverKind_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDPrimResolverKind, UDEPRECATED_UUSDPrimResolver, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), USDIMPORTER_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDPrimResolverKind)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDPrimResolverKind(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDPrimResolverKind_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDPrimResolverKind, UDEPRECATED_UUSDPrimResolver, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), USDIMPORTER_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDPrimResolverKind)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	USDIMPORTER_API UDEPRECATED_UUSDPrimResolverKind(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDPrimResolverKind) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(USDIMPORTER_API, UDEPRECATED_UUSDPrimResolverKind); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDPrimResolverKind); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	USDIMPORTER_API UDEPRECATED_UUSDPrimResolverKind(UDEPRECATED_UUSDPrimResolverKind&&); \
	USDIMPORTER_API UDEPRECATED_UUSDPrimResolverKind(const UDEPRECATED_UUSDPrimResolverKind&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	USDIMPORTER_API UDEPRECATED_UUSDPrimResolverKind(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	USDIMPORTER_API UDEPRECATED_UUSDPrimResolverKind(UDEPRECATED_UUSDPrimResolverKind&&); \
	USDIMPORTER_API UDEPRECATED_UUSDPrimResolverKind(const UDEPRECATED_UUSDPrimResolverKind&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(USDIMPORTER_API, UDEPRECATED_UUSDPrimResolverKind); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDPrimResolverKind); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDPrimResolverKind)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_15_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDIMPORTER_API UClass* StaticClass<class UDEPRECATED_UUSDPrimResolverKind>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDPrimResolverKind_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
