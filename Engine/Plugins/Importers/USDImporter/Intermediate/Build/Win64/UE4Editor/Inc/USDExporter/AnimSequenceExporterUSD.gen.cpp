// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDExporter/Public/AnimSequenceExporterUSD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeAnimSequenceExporterUSD() {}
// Cross Module References
	USDEXPORTER_API UClass* Z_Construct_UClass_UAnimSequenceExporterUSD_NoRegister();
	USDEXPORTER_API UClass* Z_Construct_UClass_UAnimSequenceExporterUSD();
	ENGINE_API UClass* Z_Construct_UClass_UExporter();
	UPackage* Z_Construct_UPackage__Script_USDExporter();
// End Cross Module References
	void UAnimSequenceExporterUSD::StaticRegisterNativesUAnimSequenceExporterUSD()
	{
	}
	UClass* Z_Construct_UClass_UAnimSequenceExporterUSD_NoRegister()
	{
		return UAnimSequenceExporterUSD::StaticClass();
	}
	struct Z_Construct_UClass_UAnimSequenceExporterUSD_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UAnimSequenceExporterUSD_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UExporter,
		(UObject* (*)())Z_Construct_UPackage__Script_USDExporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UAnimSequenceExporterUSD_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "AnimSequenceExporterUSD.h" },
		{ "ModuleRelativePath", "Public/AnimSequenceExporterUSD.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UAnimSequenceExporterUSD_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UAnimSequenceExporterUSD>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UAnimSequenceExporterUSD_Statics::ClassParams = {
		&UAnimSequenceExporterUSD::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UAnimSequenceExporterUSD_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UAnimSequenceExporterUSD_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UAnimSequenceExporterUSD()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UAnimSequenceExporterUSD_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UAnimSequenceExporterUSD, 2241612601);
	template<> USDEXPORTER_API UClass* StaticClass<UAnimSequenceExporterUSD>()
	{
		return UAnimSequenceExporterUSD::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UAnimSequenceExporterUSD(Z_Construct_UClass_UAnimSequenceExporterUSD, &UAnimSequenceExporterUSD::StaticClass, TEXT("/Script/USDExporter"), TEXT("UAnimSequenceExporterUSD"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UAnimSequenceExporterUSD);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
