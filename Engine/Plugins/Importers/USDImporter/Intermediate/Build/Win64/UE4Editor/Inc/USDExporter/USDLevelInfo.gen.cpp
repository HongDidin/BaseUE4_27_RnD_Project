// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDExporter/Private/USDLevelInfo.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDLevelInfo() {}
// Cross Module References
	USDEXPORTER_API UClass* Z_Construct_UClass_AUSDLevelInfo_NoRegister();
	USDEXPORTER_API UClass* Z_Construct_UClass_AUSDLevelInfo();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_USDExporter();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FFilePath();
// End Cross Module References
	DEFINE_FUNCTION(AUSDLevelInfo::execSaveUSD)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SaveUSD();
		P_NATIVE_END;
	}
	void AUSDLevelInfo::StaticRegisterNativesAUSDLevelInfo()
	{
		UClass* Class = AUSDLevelInfo::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "SaveUSD", &AUSDLevelInfo::execSaveUSD },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AUSDLevelInfo_SaveUSD_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUSDLevelInfo_SaveUSD_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "USD" },
		{ "ModuleRelativePath", "Private/USDLevelInfo.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUSDLevelInfo_SaveUSD_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUSDLevelInfo, nullptr, "SaveUSD", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUSDLevelInfo_SaveUSD_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AUSDLevelInfo_SaveUSD_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUSDLevelInfo_SaveUSD()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUSDLevelInfo_SaveUSD_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AUSDLevelInfo_NoRegister()
	{
		return AUSDLevelInfo::StaticClass();
	}
	struct Z_Construct_UClass_AUSDLevelInfo_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_FilePath;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SubLayerPaths_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubLayerPaths_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SubLayerPaths;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileScale_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_FileScale;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AUSDLevelInfo_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_USDExporter,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AUSDLevelInfo_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AUSDLevelInfo_SaveUSD, "SaveUSD" }, // 1565817487
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUSDLevelInfo_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// This is used by the USDExporter module to dispatch the commands to the Python exporter code\n" },
		{ "IncludePath", "USDLevelInfo.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Private/USDLevelInfo.h" },
		{ "ToolTip", "This is used by the USDExporter module to dispatch the commands to the Python exporter code" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_FilePath_MetaData[] = {
		{ "Category", "USD" },
		{ "ModuleRelativePath", "Private/USDLevelInfo.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_FilePath = { "FilePath", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUSDLevelInfo, FilePath), Z_Construct_UScriptStruct_FFilePath, METADATA_PARAMS(Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_FilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_FilePath_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_SubLayerPaths_Inner = { "SubLayerPaths", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FFilePath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_SubLayerPaths_MetaData[] = {
		{ "Category", "USD" },
		{ "ModuleRelativePath", "Private/USDLevelInfo.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_SubLayerPaths = { "SubLayerPaths", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUSDLevelInfo, SubLayerPaths), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_SubLayerPaths_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_SubLayerPaths_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_FileScale_MetaData[] = {
		{ "Category", "USD" },
		{ "ModuleRelativePath", "Private/USDLevelInfo.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_FileScale = { "FileScale", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUSDLevelInfo, FileScale), METADATA_PARAMS(Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_FileScale_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_FileScale_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AUSDLevelInfo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_FilePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_SubLayerPaths_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_SubLayerPaths,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUSDLevelInfo_Statics::NewProp_FileScale,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AUSDLevelInfo_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AUSDLevelInfo>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AUSDLevelInfo_Statics::ClassParams = {
		&AUSDLevelInfo::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AUSDLevelInfo_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AUSDLevelInfo_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AUSDLevelInfo_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AUSDLevelInfo_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AUSDLevelInfo()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AUSDLevelInfo_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AUSDLevelInfo, 216409907);
	template<> USDEXPORTER_API UClass* StaticClass<AUSDLevelInfo>()
	{
		return AUSDLevelInfo::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AUSDLevelInfo(Z_Construct_UClass_AUSDLevelInfo, &AUSDLevelInfo::StaticClass, TEXT("/Script/USDExporter"), TEXT("AUSDLevelInfo"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AUSDLevelInfo);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
