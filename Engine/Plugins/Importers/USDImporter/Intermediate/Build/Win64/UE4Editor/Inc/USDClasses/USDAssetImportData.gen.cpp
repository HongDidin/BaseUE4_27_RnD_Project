// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDClasses/Public/USDAssetImportData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDAssetImportData() {}
// Cross Module References
	USDCLASSES_API UClass* Z_Construct_UClass_UUsdAssetImportData_NoRegister();
	USDCLASSES_API UClass* Z_Construct_UClass_UUsdAssetImportData();
	ENGINE_API UClass* Z_Construct_UClass_UAssetImportData();
	UPackage* Z_Construct_UPackage__Script_USDClasses();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	void UUsdAssetImportData::StaticRegisterNativesUUsdAssetImportData()
	{
	}
	UClass* Z_Construct_UClass_UUsdAssetImportData_NoRegister()
	{
		return UUsdAssetImportData::StaticClass();
	}
	struct Z_Construct_UClass_UUsdAssetImportData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PrimPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImportOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUsdAssetImportData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAssetImportData,
		(UObject* (*)())Z_Construct_UPackage__Script_USDClasses,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdAssetImportData_Statics::Class_MetaDataParams[] = {
		{ "AutoExpandCategories", "Options" },
		{ "IncludePath", "USDAssetImportData.h" },
		{ "ModuleRelativePath", "Public/USDAssetImportData.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdAssetImportData_Statics::NewProp_PrimPath_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDAssetImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UUsdAssetImportData_Statics::NewProp_PrimPath = { "PrimPath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdAssetImportData, PrimPath), METADATA_PARAMS(Z_Construct_UClass_UUsdAssetImportData_Statics::NewProp_PrimPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdAssetImportData_Statics::NewProp_PrimPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdAssetImportData_Statics::NewProp_ImportOptions_MetaData[] = {
		{ "Comment", "// Likely a UUSDStageImportOptions, but we don't declare it here\n// to prevent an unnecessary module dependency on USDStageImporter\n" },
		{ "ModuleRelativePath", "Public/USDAssetImportData.h" },
		{ "ToolTip", "Likely a UUSDStageImportOptions, but we don't declare it here\nto prevent an unnecessary module dependency on USDStageImporter" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUsdAssetImportData_Statics::NewProp_ImportOptions = { "ImportOptions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdAssetImportData, ImportOptions), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUsdAssetImportData_Statics::NewProp_ImportOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdAssetImportData_Statics::NewProp_ImportOptions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUsdAssetImportData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdAssetImportData_Statics::NewProp_PrimPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdAssetImportData_Statics::NewProp_ImportOptions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUsdAssetImportData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUsdAssetImportData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUsdAssetImportData_Statics::ClassParams = {
		&UUsdAssetImportData::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUsdAssetImportData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUsdAssetImportData_Statics::PropPointers),
		0,
		0x000810A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUsdAssetImportData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdAssetImportData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUsdAssetImportData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUsdAssetImportData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUsdAssetImportData, 1414403009);
	template<> USDCLASSES_API UClass* StaticClass<UUsdAssetImportData>()
	{
		return UUsdAssetImportData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUsdAssetImportData(Z_Construct_UClass_UUsdAssetImportData, &UUsdAssetImportData::StaticClass, TEXT("/Script/USDClasses"), TEXT("UUsdAssetImportData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUsdAssetImportData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
