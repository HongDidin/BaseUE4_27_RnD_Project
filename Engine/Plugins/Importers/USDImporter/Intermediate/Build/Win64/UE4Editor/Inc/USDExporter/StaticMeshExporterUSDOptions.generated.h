// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDEXPORTER_StaticMeshExporterUSDOptions_generated_h
#error "StaticMeshExporterUSDOptions.generated.h already included, missing '#pragma once' in StaticMeshExporterUSDOptions.h"
#endif
#define USDEXPORTER_StaticMeshExporterUSDOptions_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUStaticMeshExporterUSDOptions(); \
	friend struct Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics; \
public: \
	DECLARE_CLASS(UStaticMeshExporterUSDOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(UStaticMeshExporterUSDOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUStaticMeshExporterUSDOptions(); \
	friend struct Z_Construct_UClass_UStaticMeshExporterUSDOptions_Statics; \
public: \
	DECLARE_CLASS(UStaticMeshExporterUSDOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(UStaticMeshExporterUSDOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UStaticMeshExporterUSDOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UStaticMeshExporterUSDOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStaticMeshExporterUSDOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStaticMeshExporterUSDOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStaticMeshExporterUSDOptions(UStaticMeshExporterUSDOptions&&); \
	NO_API UStaticMeshExporterUSDOptions(const UStaticMeshExporterUSDOptions&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UStaticMeshExporterUSDOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UStaticMeshExporterUSDOptions(UStaticMeshExporterUSDOptions&&); \
	NO_API UStaticMeshExporterUSDOptions(const UStaticMeshExporterUSDOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UStaticMeshExporterUSDOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UStaticMeshExporterUSDOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UStaticMeshExporterUSDOptions)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_16_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDEXPORTER_API UClass* StaticClass<class UStaticMeshExporterUSDOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_StaticMeshExporterUSDOptions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
