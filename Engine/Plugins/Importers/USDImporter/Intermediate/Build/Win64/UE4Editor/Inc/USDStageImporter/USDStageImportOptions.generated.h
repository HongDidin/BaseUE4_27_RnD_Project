// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDSTAGEIMPORTER_USDStageImportOptions_generated_h
#error "USDStageImportOptions.generated.h already included, missing '#pragma once' in USDStageImportOptions.h"
#endif
#define USDSTAGEIMPORTER_USDStageImportOptions_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUsdStageImportOptions(); \
	friend struct Z_Construct_UClass_UUsdStageImportOptions_Statics; \
public: \
	DECLARE_CLASS(UUsdStageImportOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDStageImporter"), NO_API) \
	DECLARE_SERIALIZER(UUsdStageImportOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_INCLASS \
private: \
	static void StaticRegisterNativesUUsdStageImportOptions(); \
	friend struct Z_Construct_UClass_UUsdStageImportOptions_Statics; \
public: \
	DECLARE_CLASS(UUsdStageImportOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDStageImporter"), NO_API) \
	DECLARE_SERIALIZER(UUsdStageImportOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUsdStageImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdStageImportOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUsdStageImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdStageImportOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUsdStageImportOptions(UUsdStageImportOptions&&); \
	NO_API UUsdStageImportOptions(const UUsdStageImportOptions&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUsdStageImportOptions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUsdStageImportOptions(UUsdStageImportOptions&&); \
	NO_API UUsdStageImportOptions(const UUsdStageImportOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUsdStageImportOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUsdStageImportOptions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUsdStageImportOptions)


#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_44_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h_47_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class UsdStageImportOptions."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDSTAGEIMPORTER_API UClass* StaticClass<class UUsdStageImportOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDStageImporter_Public_USDStageImportOptions_h


#define FOREACH_ENUM_EREPLACEASSETPOLICY(op) \
	op(EReplaceAssetPolicy::Append) \
	op(EReplaceAssetPolicy::Replace) \
	op(EReplaceAssetPolicy::Ignore) 

enum class EReplaceAssetPolicy : uint8;
template<> USDSTAGEIMPORTER_API UEnum* StaticEnum<EReplaceAssetPolicy>();

#define FOREACH_ENUM_EREPLACEACTORPOLICY(op) \
	op(EReplaceActorPolicy::Append) \
	op(EReplaceActorPolicy::Replace) \
	op(EReplaceActorPolicy::UpdateTransform) \
	op(EReplaceActorPolicy::Ignore) 

enum class EReplaceActorPolicy : uint8;
template<> USDSTAGEIMPORTER_API UEnum* StaticEnum<EReplaceActorPolicy>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
