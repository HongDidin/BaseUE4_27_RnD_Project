// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDIMPORTER_USDSceneImportFactory_generated_h
#error "USDSceneImportFactory.generated.h already included, missing '#pragma once' in USDSceneImportFactory.h"
#endif
#define USDIMPORTER_USDSceneImportFactory_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDSceneImportFactory(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDSceneImportFactory, USceneImportFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDSceneImportFactory)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDSceneImportFactory(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDSceneImportFactory, USceneImportFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDSceneImportFactory)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUSDSceneImportFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDSceneImportFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUSDSceneImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDSceneImportFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUSDSceneImportFactory(UDEPRECATED_UUSDSceneImportFactory&&); \
	NO_API UDEPRECATED_UUSDSceneImportFactory(const UDEPRECATED_UUSDSceneImportFactory&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUSDSceneImportFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUSDSceneImportFactory(UDEPRECATED_UUSDSceneImportFactory&&); \
	NO_API UDEPRECATED_UUSDSceneImportFactory(const UDEPRECATED_UUSDSceneImportFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUSDSceneImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDSceneImportFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDSceneImportFactory)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ImportContext() { return STRUCT_OFFSET(UDEPRECATED_UUSDSceneImportFactory, ImportContext); } \
	FORCEINLINE static uint32 __PPO__ImportOptions_DEPRECATED() { return STRUCT_OFFSET(UDEPRECATED_UUSDSceneImportFactory, ImportOptions_DEPRECATED); }


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_16_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h_19_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class UUSDSceneImportFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDIMPORTER_API UClass* StaticClass<class UDEPRECATED_UUSDSceneImportFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDImporter_Private_USDSceneImportFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
