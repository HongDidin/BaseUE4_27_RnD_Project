// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDIMPORTER_USDPrimResolver_generated_h
#error "USDPrimResolver.generated.h already included, missing '#pragma once' in USDPrimResolver.h"
#endif
#define USDIMPORTER_USDPrimResolver_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDPrimResolver(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDPrimResolver, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), USDIMPORTER_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDPrimResolver)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDPrimResolver(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDPrimResolver, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), USDIMPORTER_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDPrimResolver)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	USDIMPORTER_API UDEPRECATED_UUSDPrimResolver(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDPrimResolver) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(USDIMPORTER_API, UDEPRECATED_UUSDPrimResolver); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDPrimResolver); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	USDIMPORTER_API UDEPRECATED_UUSDPrimResolver(UDEPRECATED_UUSDPrimResolver&&); \
	USDIMPORTER_API UDEPRECATED_UUSDPrimResolver(const UDEPRECATED_UUSDPrimResolver&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	USDIMPORTER_API UDEPRECATED_UUSDPrimResolver(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	USDIMPORTER_API UDEPRECATED_UUSDPrimResolver(UDEPRECATED_UUSDPrimResolver&&); \
	USDIMPORTER_API UDEPRECATED_UUSDPrimResolver(const UDEPRECATED_UUSDPrimResolver&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(USDIMPORTER_API, UDEPRECATED_UUSDPrimResolver); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDPrimResolver); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDPrimResolver)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_54_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h_57_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDIMPORTER_API UClass* StaticClass<class UDEPRECATED_UUSDPrimResolver>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDPrimResolver_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
