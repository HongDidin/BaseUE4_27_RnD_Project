// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "GeometryCacheUSD/Public/GeometryCacheTrackUSD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeGeometryCacheTrackUSD() {}
// Cross Module References
	GEOMETRYCACHEUSD_API UClass* Z_Construct_UClass_UGeometryCacheTrackUsd_NoRegister();
	GEOMETRYCACHEUSD_API UClass* Z_Construct_UClass_UGeometryCacheTrackUsd();
	GEOMETRYCACHE_API UClass* Z_Construct_UClass_UGeometryCacheTrack();
	UPackage* Z_Construct_UPackage__Script_GeometryCacheUSD();
// End Cross Module References
	void UGeometryCacheTrackUsd::StaticRegisterNativesUGeometryCacheTrackUsd()
	{
	}
	UClass* Z_Construct_UClass_UGeometryCacheTrackUsd_NoRegister()
	{
		return UGeometryCacheTrackUsd::StaticClass();
	}
	struct Z_Construct_UClass_UGeometryCacheTrackUsd_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UGeometryCacheTrackUsd_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGeometryCacheTrack,
		(UObject* (*)())Z_Construct_UPackage__Script_GeometryCacheUSD,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UGeometryCacheTrackUsd_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** GeometryCacheTrack for querying USD */" },
		{ "HideCategories", "Object Object" },
		{ "IncludePath", "GeometryCacheTrackUSD.h" },
		{ "ModuleRelativePath", "Public/GeometryCacheTrackUSD.h" },
		{ "ToolTip", "GeometryCacheTrack for querying USD" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UGeometryCacheTrackUsd_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UGeometryCacheTrackUsd>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UGeometryCacheTrackUsd_Statics::ClassParams = {
		&UGeometryCacheTrackUsd::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001020A0u,
		METADATA_PARAMS(Z_Construct_UClass_UGeometryCacheTrackUsd_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UGeometryCacheTrackUsd_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UGeometryCacheTrackUsd()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UGeometryCacheTrackUsd_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UGeometryCacheTrackUsd, 3386234783);
	template<> GEOMETRYCACHEUSD_API UClass* StaticClass<UGeometryCacheTrackUsd>()
	{
		return UGeometryCacheTrackUsd::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UGeometryCacheTrackUsd(Z_Construct_UClass_UGeometryCacheTrackUsd, &UGeometryCacheTrackUsd::StaticClass, TEXT("/Script/GeometryCacheUSD"), TEXT("UGeometryCacheTrackUsd"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UGeometryCacheTrackUsd);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
