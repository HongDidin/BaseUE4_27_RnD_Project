// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDImporter/Private/USDSceneImportFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDSceneImportFactory() {}
// Cross Module References
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory();
	UNREALED_API UClass* Z_Construct_UClass_USceneImportFactory();
	UPackage* Z_Construct_UPackage__Script_USDImporter();
	USDIMPORTER_API UScriptStruct* Z_Construct_UScriptStruct_FUSDSceneImportContext();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_NoRegister();
// End Cross Module References
	void UDEPRECATED_UUSDSceneImportFactory::StaticRegisterNativesUDEPRECATED_UUSDSceneImportFactory()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_NoRegister()
	{
		return UDEPRECATED_UUSDSceneImportFactory::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportContext_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ImportContext;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImportOptions;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USceneImportFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "USDSceneImportFactory.h" },
		{ "ModuleRelativePath", "Private/USDSceneImportFactory.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::NewProp_ImportContext_MetaData[] = {
		{ "ModuleRelativePath", "Private/USDSceneImportFactory.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::NewProp_ImportContext = { "ImportContext", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDSceneImportFactory, ImportContext), Z_Construct_UScriptStruct_FUSDSceneImportContext, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::NewProp_ImportContext_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::NewProp_ImportContext_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::NewProp_ImportOptions_MetaData[] = {
		{ "DeprecatedProperty", "" },
		{ "DeprecationMessage", "Use the new USDStageImporter module instead" },
		{ "ModuleRelativePath", "Private/USDSceneImportFactory.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::NewProp_ImportOptions = { "ImportOptions", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UDEPRECATED_UUSDSceneImportFactory, ImportOptions_DEPRECATED), Z_Construct_UClass_UDEPRECATED_UUSDSceneImportOptions_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::NewProp_ImportOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::NewProp_ImportOptions_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::NewProp_ImportContext,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::NewProp_ImportOptions,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_UUSDSceneImportFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::ClassParams = {
		&UDEPRECATED_UUSDSceneImportFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::PropPointers),
		0,
		0x020002A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_UUSDSceneImportFactory, 1656324149);
	template<> USDIMPORTER_API UClass* StaticClass<UDEPRECATED_UUSDSceneImportFactory>()
	{
		return UDEPRECATED_UUSDSceneImportFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_UUSDSceneImportFactory(Z_Construct_UClass_UDEPRECATED_UUSDSceneImportFactory, &UDEPRECATED_UUSDSceneImportFactory::StaticClass, TEXT("/Script/USDImporter"), TEXT("UDEPRECATED_UUSDSceneImportFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_UUSDSceneImportFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
