// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDImporter/Public/USDPrimResolver.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDPrimResolver() {}
// Cross Module References
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_USDImporter();
// End Cross Module References
	void UDEPRECATED_UUSDPrimResolver::StaticRegisterNativesUDEPRECATED_UUSDPrimResolver()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver_NoRegister()
	{
		return UDEPRECATED_UUSDPrimResolver::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Base class for all evaluation of prims for geometry and actors */" },
		{ "IncludePath", "USDPrimResolver.h" },
		{ "ModuleRelativePath", "Public/USDPrimResolver.h" },
		{ "ToolTip", "Base class for all evaluation of prims for geometry and actors" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_UUSDPrimResolver>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver_Statics::ClassParams = {
		&UDEPRECATED_UUSDPrimResolver::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x020802A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_UUSDPrimResolver, 1598277868);
	template<> USDIMPORTER_API UClass* StaticClass<UDEPRECATED_UUSDPrimResolver>()
	{
		return UDEPRECATED_UUSDPrimResolver::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_UUSDPrimResolver(Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver, &UDEPRECATED_UUSDPrimResolver::StaticClass, TEXT("/Script/USDImporter"), TEXT("UDEPRECATED_UUSDPrimResolver"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_UUSDPrimResolver);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
