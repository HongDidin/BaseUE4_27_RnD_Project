// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDExporter/Public/MaterialExporterUSD.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMaterialExporterUSD() {}
// Cross Module References
	USDEXPORTER_API UClass* Z_Construct_UClass_UMaterialExporterUsd_NoRegister();
	USDEXPORTER_API UClass* Z_Construct_UClass_UMaterialExporterUsd();
	ENGINE_API UClass* Z_Construct_UClass_UExporter();
	UPackage* Z_Construct_UPackage__Script_USDExporter();
// End Cross Module References
	void UMaterialExporterUsd::StaticRegisterNativesUMaterialExporterUsd()
	{
	}
	UClass* Z_Construct_UClass_UMaterialExporterUsd_NoRegister()
	{
		return UMaterialExporterUsd::StaticClass();
	}
	struct Z_Construct_UClass_UMaterialExporterUsd_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMaterialExporterUsd_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UExporter,
		(UObject* (*)())Z_Construct_UPackage__Script_USDExporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMaterialExporterUsd_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MaterialExporterUSD.h" },
		{ "ModuleRelativePath", "Public/MaterialExporterUSD.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMaterialExporterUsd_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMaterialExporterUsd>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMaterialExporterUsd_Statics::ClassParams = {
		&UMaterialExporterUsd::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMaterialExporterUsd_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMaterialExporterUsd_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMaterialExporterUsd()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMaterialExporterUsd_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMaterialExporterUsd, 4017664459);
	template<> USDEXPORTER_API UClass* StaticClass<UMaterialExporterUsd>()
	{
		return UMaterialExporterUsd::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMaterialExporterUsd(Z_Construct_UClass_UMaterialExporterUsd, &UMaterialExporterUsd::StaticClass, TEXT("/Script/USDExporter"), TEXT("UMaterialExporterUsd"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMaterialExporterUsd);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
