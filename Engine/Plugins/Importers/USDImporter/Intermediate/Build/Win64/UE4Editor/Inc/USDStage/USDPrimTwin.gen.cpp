// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDStage/Public/USDPrimTwin.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDPrimTwin() {}
// Cross Module References
	USDSTAGE_API UClass* Z_Construct_UClass_UUsdPrimTwin_NoRegister();
	USDSTAGE_API UClass* Z_Construct_UClass_UUsdPrimTwin();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_USDStage();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
// End Cross Module References
	void UUsdPrimTwin::StaticRegisterNativesUUsdPrimTwin()
	{
	}
	UClass* Z_Construct_UClass_UUsdPrimTwin_NoRegister()
	{
		return UUsdPrimTwin::StaticClass();
	}
	struct Z_Construct_UClass_UUsdPrimTwin_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PrimPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpawnedActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_SpawnedActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_SceneComponent;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Children_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Children_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Children_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Children;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Parent_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_Parent;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUsdPrimTwin_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_USDStage,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdPrimTwin_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** The Unreal equivalent (twin) of a USD prim */" },
		{ "IncludePath", "USDPrimTwin.h" },
		{ "ModuleRelativePath", "Public/USDPrimTwin.h" },
		{ "ToolTip", "The Unreal equivalent (twin) of a USD prim" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_PrimPath_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDPrimTwin.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_PrimPath = { "PrimPath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdPrimTwin, PrimPath), METADATA_PARAMS(Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_PrimPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_PrimPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_SpawnedActor_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDPrimTwin.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_SpawnedActor = { "SpawnedActor", nullptr, (EPropertyFlags)0x0014000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdPrimTwin, SpawnedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_SpawnedActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_SpawnedActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_SceneComponent_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDPrimTwin.h" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_SceneComponent = { "SceneComponent", nullptr, (EPropertyFlags)0x0014000000080008, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdPrimTwin, SceneComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_SceneComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_SceneComponent_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_Children_ValueProp = { "Children", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_UUsdPrimTwin_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_Children_Key_KeyProp = { "Children_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_Children_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDPrimTwin.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_Children = { "Children", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdPrimTwin, Children), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_Children_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_Children_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_Parent_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDPrimTwin.h" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_Parent = { "Parent", nullptr, (EPropertyFlags)0x0044000000002000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUsdPrimTwin, Parent), Z_Construct_UClass_UUsdPrimTwin_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_Parent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_Parent_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUsdPrimTwin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_PrimPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_SpawnedActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_SceneComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_Children_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_Children_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_Children,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUsdPrimTwin_Statics::NewProp_Parent,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUsdPrimTwin_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUsdPrimTwin>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUsdPrimTwin_Statics::ClassParams = {
		&UUsdPrimTwin::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUsdPrimTwin_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUsdPrimTwin_Statics::PropPointers),
		0,
		0x008800A8u,
		METADATA_PARAMS(Z_Construct_UClass_UUsdPrimTwin_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdPrimTwin_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUsdPrimTwin()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUsdPrimTwin_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUsdPrimTwin, 4087478228);
	template<> USDSTAGE_API UClass* StaticClass<UUsdPrimTwin>()
	{
		return UUsdPrimTwin::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUsdPrimTwin(Z_Construct_UClass_UUsdPrimTwin, &UUsdPrimTwin::StaticClass, TEXT("/Script/USDStage"), TEXT("UUsdPrimTwin"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUsdPrimTwin);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
