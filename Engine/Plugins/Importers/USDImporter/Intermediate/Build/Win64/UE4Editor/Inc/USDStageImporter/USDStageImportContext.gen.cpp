// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDStageImporter/Public/USDStageImportContext.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDStageImportContext() {}
// Cross Module References
	USDSTAGEIMPORTER_API UScriptStruct* Z_Construct_UScriptStruct_FUsdStageImportContext();
	UPackage* Z_Construct_UPackage__Script_USDStageImporter();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	USDSTAGEIMPORTER_API UClass* Z_Construct_UClass_UUsdStageImportOptions_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	USDCLASSES_API UClass* Z_Construct_UClass_UUsdAssetCache_NoRegister();
// End Cross Module References
class UScriptStruct* FUsdStageImportContext::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern USDSTAGEIMPORTER_API uint32 Get_Z_Construct_UScriptStruct_FUsdStageImportContext_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FUsdStageImportContext, Z_Construct_UPackage__Script_USDStageImporter(), TEXT("UsdStageImportContext"), sizeof(FUsdStageImportContext), Get_Z_Construct_UScriptStruct_FUsdStageImportContext_Hash());
	}
	return Singleton;
}
template<> USDSTAGEIMPORTER_API UScriptStruct* StaticStruct<FUsdStageImportContext>()
{
	return FUsdStageImportContext::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FUsdStageImportContext(FUsdStageImportContext::StaticStruct, TEXT("/Script/USDStageImporter"), TEXT("UsdStageImportContext"), false, nullptr, nullptr);
static struct FScriptStruct_USDStageImporter_StaticRegisterNativesFUsdStageImportContext
{
	FScriptStruct_USDStageImporter_StaticRegisterNativesFUsdStageImportContext()
	{
		UScriptStruct::DeferCppStructOps<FUsdStageImportContext>(FName(TEXT("UsdStageImportContext")));
	}
} ScriptStruct_USDStageImporter_StaticRegisterNativesFUsdStageImportContext;
	struct Z_Construct_UScriptStruct_FUsdStageImportContext_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ObjectName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PackagePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PackagePath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FilePath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportOptions_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImportOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportedAsset_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImportedAsset;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetCache_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AssetCache;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/USDStageImportContext.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FUsdStageImportContext>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_SceneActor_MetaData[] = {
		{ "Comment", "/** Spawned actor that contains the imported scene as a child hierarchy */" },
		{ "ModuleRelativePath", "Public/USDStageImportContext.h" },
		{ "ToolTip", "Spawned actor that contains the imported scene as a child hierarchy" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_SceneActor = { "SceneActor", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdStageImportContext, SceneActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_SceneActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_SceneActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_ObjectName_MetaData[] = {
		{ "Comment", "/** Name to use when importing a single mesh */" },
		{ "ModuleRelativePath", "Public/USDStageImportContext.h" },
		{ "ToolTip", "Name to use when importing a single mesh" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_ObjectName = { "ObjectName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdStageImportContext, ObjectName), METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_ObjectName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_ObjectName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_PackagePath_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDStageImportContext.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_PackagePath = { "PackagePath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdStageImportContext, PackagePath), METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_PackagePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_PackagePath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_FilePath_MetaData[] = {
		{ "Comment", "/** Path of the main usd file to import */" },
		{ "ModuleRelativePath", "Public/USDStageImportContext.h" },
		{ "ToolTip", "Path of the main usd file to import" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_FilePath = { "FilePath", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdStageImportContext, FilePath), METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_FilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_FilePath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_ImportOptions_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDStageImportContext.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_ImportOptions = { "ImportOptions", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdStageImportContext, ImportOptions), Z_Construct_UClass_UUsdStageImportOptions_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_ImportOptions_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_ImportOptions_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_ImportedAsset_MetaData[] = {
		{ "Comment", "/** Keep track of the last imported object so that we have something valid to return to upstream code that calls the import factories */" },
		{ "ModuleRelativePath", "Public/USDStageImportContext.h" },
		{ "ToolTip", "Keep track of the last imported object so that we have something valid to return to upstream code that calls the import factories" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_ImportedAsset = { "ImportedAsset", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdStageImportContext, ImportedAsset), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_ImportedAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_ImportedAsset_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_AssetCache_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDStageImportContext.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_AssetCache = { "AssetCache", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FUsdStageImportContext, AssetCache), Z_Construct_UClass_UUsdAssetCache_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_AssetCache_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_AssetCache_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_SceneActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_ObjectName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_PackagePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_FilePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_ImportOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_ImportedAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::NewProp_AssetCache,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_USDStageImporter,
		nullptr,
		&NewStructOps,
		"UsdStageImportContext",
		sizeof(FUsdStageImportContext),
		alignof(FUsdStageImportContext),
		Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FUsdStageImportContext()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FUsdStageImportContext_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_USDStageImporter();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("UsdStageImportContext"), sizeof(FUsdStageImportContext), Get_Z_Construct_UScriptStruct_FUsdStageImportContext_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FUsdStageImportContext_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FUsdStageImportContext_Hash() { return 3607572165U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
