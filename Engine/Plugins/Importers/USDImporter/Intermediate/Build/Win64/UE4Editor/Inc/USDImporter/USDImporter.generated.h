// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDIMPORTER_USDImporter_generated_h
#error "USDImporter.generated.h already included, missing '#pragma once' in USDImporter.h"
#endif
#define USDIMPORTER_USDImporter_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_88_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FUSDSceneImportContext_Statics; \
	static class UScriptStruct* StaticStruct(); \
	typedef FUsdImportContext Super;


template<> USDIMPORTER_API UScriptStruct* StaticStruct<struct FUSDSceneImportContext>();

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_33_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FUsdImportContext_Statics; \
	static class UScriptStruct* StaticStruct();


template<> USDIMPORTER_API UScriptStruct* StaticStruct<struct FUsdImportContext>();

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUsdSceneImportContextContainer(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUsdSceneImportContextContainer, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUsdSceneImportContextContainer)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUsdSceneImportContextContainer(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUsdSceneImportContextContainer_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUsdSceneImportContextContainer, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUsdSceneImportContextContainer)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUsdSceneImportContextContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUsdSceneImportContextContainer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUsdSceneImportContextContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUsdSceneImportContextContainer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUsdSceneImportContextContainer(UDEPRECATED_UUsdSceneImportContextContainer&&); \
	NO_API UDEPRECATED_UUsdSceneImportContextContainer(const UDEPRECATED_UUsdSceneImportContextContainer&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUsdSceneImportContextContainer(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUsdSceneImportContextContainer(UDEPRECATED_UUsdSceneImportContextContainer&&); \
	NO_API UDEPRECATED_UUsdSceneImportContextContainer(const UDEPRECATED_UUsdSceneImportContextContainer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUsdSceneImportContextContainer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUsdSceneImportContextContainer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUsdSceneImportContextContainer)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_111_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_114_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDIMPORTER_API UClass* StaticClass<class UDEPRECATED_UUsdSceneImportContextContainer>();

#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDImporter(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDImporter_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDImporter, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDImporter)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_INCLASS \
private: \
	static void StaticRegisterNativesUDEPRECATED_UUSDImporter(); \
	friend struct Z_Construct_UClass_UDEPRECATED_UUSDImporter_Statics; \
public: \
	DECLARE_CLASS(UDEPRECATED_UUSDImporter, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Deprecated), CASTCLASS_None, TEXT("/Script/USDImporter"), NO_API) \
	DECLARE_SERIALIZER(UDEPRECATED_UUSDImporter)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUSDImporter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDImporter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUSDImporter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDImporter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUSDImporter(UDEPRECATED_UUSDImporter&&); \
	NO_API UDEPRECATED_UUSDImporter(const UDEPRECATED_UUSDImporter&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UDEPRECATED_UUSDImporter(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UDEPRECATED_UUSDImporter(UDEPRECATED_UUSDImporter&&); \
	NO_API UDEPRECATED_UUSDImporter(const UDEPRECATED_UUSDImporter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UDEPRECATED_UUSDImporter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UDEPRECATED_UUSDImporter); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UDEPRECATED_UUSDImporter)


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_121_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h_124_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class UUSDImporter."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDIMPORTER_API UClass* StaticClass<class UDEPRECATED_UUSDImporter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDImporter_Public_USDImporter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
