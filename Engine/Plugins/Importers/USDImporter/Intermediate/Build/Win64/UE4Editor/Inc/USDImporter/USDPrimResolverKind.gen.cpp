// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDImporter/Private/USDPrimResolverKind.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDPrimResolverKind() {}
// Cross Module References
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDPrimResolverKind_NoRegister();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDPrimResolverKind();
	USDIMPORTER_API UClass* Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver();
	UPackage* Z_Construct_UPackage__Script_USDImporter();
// End Cross Module References
	void UDEPRECATED_UUSDPrimResolverKind::StaticRegisterNativesUDEPRECATED_UUSDPrimResolverKind()
	{
	}
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDPrimResolverKind_NoRegister()
	{
		return UDEPRECATED_UUSDPrimResolverKind::StaticClass();
	}
	struct Z_Construct_UClass_UDEPRECATED_UUSDPrimResolverKind_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UDEPRECATED_UUSDPrimResolverKind_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDEPRECATED_UUSDPrimResolver,
		(UObject* (*)())Z_Construct_UPackage__Script_USDImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UDEPRECATED_UUSDPrimResolverKind_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Evaluates USD prims based on USD kind metadata */" },
		{ "IncludePath", "USDPrimResolverKind.h" },
		{ "ModuleRelativePath", "Private/USDPrimResolverKind.h" },
		{ "ToolTip", "Evaluates USD prims based on USD kind metadata" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UDEPRECATED_UUSDPrimResolverKind_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UDEPRECATED_UUSDPrimResolverKind>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UDEPRECATED_UUSDPrimResolverKind_Statics::ClassParams = {
		&UDEPRECATED_UUSDPrimResolverKind::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x020802A8u,
		METADATA_PARAMS(Z_Construct_UClass_UDEPRECATED_UUSDPrimResolverKind_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UDEPRECATED_UUSDPrimResolverKind_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UDEPRECATED_UUSDPrimResolverKind()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UDEPRECATED_UUSDPrimResolverKind_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UDEPRECATED_UUSDPrimResolverKind, 4223571115);
	template<> USDIMPORTER_API UClass* StaticClass<UDEPRECATED_UUSDPrimResolverKind>()
	{
		return UDEPRECATED_UUSDPrimResolverKind::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UDEPRECATED_UUSDPrimResolverKind(Z_Construct_UClass_UDEPRECATED_UUSDPrimResolverKind, &UDEPRECATED_UUSDPrimResolverKind::StaticClass, TEXT("/Script/USDImporter"), TEXT("UDEPRECATED_UUSDPrimResolverKind"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UDEPRECATED_UUSDPrimResolverKind);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
