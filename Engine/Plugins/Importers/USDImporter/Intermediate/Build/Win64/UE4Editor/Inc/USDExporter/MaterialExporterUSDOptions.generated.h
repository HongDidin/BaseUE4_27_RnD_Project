// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef USDEXPORTER_MaterialExporterUSDOptions_generated_h
#error "MaterialExporterUSDOptions.generated.h already included, missing '#pragma once' in MaterialExporterUSDOptions.h"
#endif
#define USDEXPORTER_MaterialExporterUSDOptions_generated_h

#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_SPARSE_DATA
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_RPC_WRAPPERS
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMaterialExporterUSDOptions(); \
	friend struct Z_Construct_UClass_UMaterialExporterUSDOptions_Statics; \
public: \
	DECLARE_CLASS(UMaterialExporterUSDOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(UMaterialExporterUSDOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUMaterialExporterUSDOptions(); \
	friend struct Z_Construct_UClass_UMaterialExporterUSDOptions_Statics; \
public: \
	DECLARE_CLASS(UMaterialExporterUSDOptions, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/USDExporter"), NO_API) \
	DECLARE_SERIALIZER(UMaterialExporterUSDOptions) \
	static const TCHAR* StaticConfigName() {return TEXT("Editor");} \



#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMaterialExporterUSDOptions(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMaterialExporterUSDOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMaterialExporterUSDOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMaterialExporterUSDOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMaterialExporterUSDOptions(UMaterialExporterUSDOptions&&); \
	NO_API UMaterialExporterUSDOptions(const UMaterialExporterUSDOptions&); \
public:


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMaterialExporterUSDOptions(UMaterialExporterUSDOptions&&); \
	NO_API UMaterialExporterUSDOptions(const UMaterialExporterUSDOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMaterialExporterUSDOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMaterialExporterUSDOptions); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMaterialExporterUSDOptions)


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_15_PROLOG
#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_RPC_WRAPPERS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_INCLASS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_SPARSE_DATA \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> USDEXPORTER_API UClass* StaticClass<class UMaterialExporterUSDOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_Importers_USDImporter_Source_USDExporter_Public_MaterialExporterUSDOptions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
