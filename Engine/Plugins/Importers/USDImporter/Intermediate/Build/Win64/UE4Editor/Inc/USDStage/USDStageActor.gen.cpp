// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDStage/Public/USDStageActor.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDStageActor() {}
// Cross Module References
	USDSTAGE_API UClass* Z_Construct_UClass_AUsdStageActor_NoRegister();
	USDSTAGE_API UClass* Z_Construct_UClass_AUsdStageActor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_USDStage();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USceneComponent_NoRegister();
	UNREALUSDWRAPPER_API UEnum* Z_Construct_UEnum_UnrealUSDWrapper_EUsdInitialLoadSet();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FFilePath();
	LEVELSEQUENCE_API UClass* Z_Construct_UClass_ULevelSequence_NoRegister();
	USDSTAGE_API UClass* Z_Construct_UClass_UUsdPrimTwin_NoRegister();
	USDCLASSES_API UClass* Z_Construct_UClass_UUsdAssetCache_NoRegister();
	USDSTAGE_API UClass* Z_Construct_UClass_UUsdTransactor_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(AUsdStageActor::execGetSourcePrimPath)
	{
		P_GET_OBJECT(UObject,Z_Param_Object);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetSourcePrimPath(Z_Param_Object);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AUsdStageActor::execGetGeneratedAssets)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_PrimPath);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UObject*>*)Z_Param__Result=P_THIS->GetGeneratedAssets(Z_Param_PrimPath);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AUsdStageActor::execGetGeneratedComponent)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_PrimPath);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(USceneComponent**)Z_Param__Result=P_THIS->GetGeneratedComponent(Z_Param_PrimPath);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AUsdStageActor::execSetTime)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetTime(Z_Param_InTime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AUsdStageActor::execGetTime)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetTime();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AUsdStageActor::execSetRenderContext)
	{
		P_GET_PROPERTY_REF(FNameProperty,Z_Param_Out_NewRenderContext);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetRenderContext(Z_Param_Out_NewRenderContext);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AUsdStageActor::execSetPurposesToLoad)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_NewPurposesToLoad);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPurposesToLoad(Z_Param_NewPurposesToLoad);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AUsdStageActor::execSetInitialLoadSet)
	{
		P_GET_ENUM(EUsdInitialLoadSet,Z_Param_NewLoadSet);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetInitialLoadSet(EUsdInitialLoadSet(Z_Param_NewLoadSet));
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(AUsdStageActor::execSetRootLayer)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_RootFilePath);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetRootLayer(Z_Param_RootFilePath);
		P_NATIVE_END;
	}
	void AUsdStageActor::StaticRegisterNativesAUsdStageActor()
	{
		UClass* Class = AUsdStageActor::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetGeneratedAssets", &AUsdStageActor::execGetGeneratedAssets },
			{ "GetGeneratedComponent", &AUsdStageActor::execGetGeneratedComponent },
			{ "GetSourcePrimPath", &AUsdStageActor::execGetSourcePrimPath },
			{ "GetTime", &AUsdStageActor::execGetTime },
			{ "SetInitialLoadSet", &AUsdStageActor::execSetInitialLoadSet },
			{ "SetPurposesToLoad", &AUsdStageActor::execSetPurposesToLoad },
			{ "SetRenderContext", &AUsdStageActor::execSetRenderContext },
			{ "SetRootLayer", &AUsdStageActor::execSetRootLayer },
			{ "SetTime", &AUsdStageActor::execSetTime },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics
	{
		struct UsdStageActor_eventGetGeneratedAssets_Parms
		{
			FString PrimPath;
			TArray<UObject*> ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PrimPath;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::NewProp_PrimPath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::NewProp_PrimPath = { "PrimPath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdStageActor_eventGetGeneratedAssets_Parms, PrimPath), METADATA_PARAMS(Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::NewProp_PrimPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::NewProp_PrimPath_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdStageActor_eventGetGeneratedAssets_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::NewProp_PrimPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "USD" },
		{ "Comment", "/**\n\x09 * Gets the transient assets that were generated for a prim with a given prim path. Likely one asset (e.g. UStaticMesh), but can be multiple (USkeletalMesh, USkeleton, etc.)\n\x09 * @param PrimPath - Full path to the source prim, e.g. \"/root_prim/my_mesh\"\n\x09 * @return The corresponding generated assets. May be empty if path is invalid or if that prim led to no generated assets.\n\x09 */" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
		{ "ToolTip", "Gets the transient assets that were generated for a prim with a given prim path. Likely one asset (e.g. UStaticMesh), but can be multiple (USkeletalMesh, USkeleton, etc.)\n@param PrimPath - Full path to the source prim, e.g. \"/root_prim/my_mesh\"\n@return The corresponding generated assets. May be empty if path is invalid or if that prim led to no generated assets." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUsdStageActor, nullptr, "GetGeneratedAssets", nullptr, nullptr, sizeof(UsdStageActor_eventGetGeneratedAssets_Parms), Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020403, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics
	{
		struct UsdStageActor_eventGetGeneratedComponent_Parms
		{
			FString PrimPath;
			USceneComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PrimPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::NewProp_PrimPath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::NewProp_PrimPath = { "PrimPath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdStageActor_eventGetGeneratedComponent_Parms, PrimPath), METADATA_PARAMS(Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::NewProp_PrimPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::NewProp_PrimPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdStageActor_eventGetGeneratedComponent_Parms, ReturnValue), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::NewProp_PrimPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "USD" },
		{ "Comment", "/**\n\x09 * Gets the transient component that was generated for a prim with a given prim path.\n\x09 * Warning: The lifetime of the component is managed by the AUsdStageActor, and it may be force-destroyed at any time (e.g. when closing the stage)\n\x09 * @param PrimPath - Full path to the source prim, e.g. \"/root_prim/my_prim\"\n\x09 * @return The corresponding spawned component. It may correspond to a parent prim, if the prim at PrimPath was collapsed. Nullptr if path is invalid.\n\x09 */" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
		{ "ToolTip", "Gets the transient component that was generated for a prim with a given prim path.\nWarning: The lifetime of the component is managed by the AUsdStageActor, and it may be force-destroyed at any time (e.g. when closing the stage)\n@param PrimPath - Full path to the source prim, e.g. \"/root_prim/my_prim\"\n@return The corresponding spawned component. It may correspond to a parent prim, if the prim at PrimPath was collapsed. Nullptr if path is invalid." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUsdStageActor, nullptr, "GetGeneratedComponent", nullptr, nullptr, sizeof(UsdStageActor_eventGetGeneratedComponent_Parms), Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020403, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUsdStageActor_GetSourcePrimPath_Statics
	{
		struct UsdStageActor_eventGetSourcePrimPath_Parms
		{
			UObject* Object;
			FString ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Object;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_AUsdStageActor_GetSourcePrimPath_Statics::NewProp_Object = { "Object", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdStageActor_eventGetSourcePrimPath_Parms, Object), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AUsdStageActor_GetSourcePrimPath_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdStageActor_eventGetSourcePrimPath_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AUsdStageActor_GetSourcePrimPath_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUsdStageActor_GetSourcePrimPath_Statics::NewProp_Object,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUsdStageActor_GetSourcePrimPath_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUsdStageActor_GetSourcePrimPath_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "USD" },
		{ "Comment", "/**\n\x09 * Gets the path to the prim that was parsed to generate the given `Object`.\n\x09 * @param Object - UObject to query with. Can be one of the transient components generated when a stage was opened, or something like a UStaticMesh.\n\x09 * @return The path to the source prim, e.g. \"/root_prim/some_prim\". May be empty in case we couldn't find the source prim.\n\x09 */" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
		{ "ToolTip", "Gets the path to the prim that was parsed to generate the given `Object`.\n@param Object - UObject to query with. Can be one of the transient components generated when a stage was opened, or something like a UStaticMesh.\n@return The path to the source prim, e.g. \"/root_prim/some_prim\". May be empty in case we couldn't find the source prim." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUsdStageActor_GetSourcePrimPath_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUsdStageActor, nullptr, "GetSourcePrimPath", nullptr, nullptr, sizeof(UsdStageActor_eventGetSourcePrimPath_Parms), Z_Construct_UFunction_AUsdStageActor_GetSourcePrimPath_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_GetSourcePrimPath_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020403, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUsdStageActor_GetSourcePrimPath_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_GetSourcePrimPath_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUsdStageActor_GetSourcePrimPath()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUsdStageActor_GetSourcePrimPath_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUsdStageActor_GetTime_Statics
	{
		struct UsdStageActor_eventGetTime_Parms
		{
			float ReturnValue;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AUsdStageActor_GetTime_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdStageActor_eventGetTime_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AUsdStageActor_GetTime_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUsdStageActor_GetTime_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUsdStageActor_GetTime_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "USD" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUsdStageActor_GetTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUsdStageActor, nullptr, "GetTime", nullptr, nullptr, sizeof(UsdStageActor_eventGetTime_Parms), Z_Construct_UFunction_AUsdStageActor_GetTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_GetTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020403, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUsdStageActor_GetTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_GetTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUsdStageActor_GetTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUsdStageActor_GetTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUsdStageActor_SetInitialLoadSet_Statics
	{
		struct UsdStageActor_eventSetInitialLoadSet_Parms
		{
			EUsdInitialLoadSet NewLoadSet;
		};
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_NewLoadSet_Underlying;
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_NewLoadSet;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UFunction_AUsdStageActor_SetInitialLoadSet_Statics::NewProp_NewLoadSet_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UFunction_AUsdStageActor_SetInitialLoadSet_Statics::NewProp_NewLoadSet = { "NewLoadSet", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdStageActor_eventSetInitialLoadSet_Parms, NewLoadSet), Z_Construct_UEnum_UnrealUSDWrapper_EUsdInitialLoadSet, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AUsdStageActor_SetInitialLoadSet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUsdStageActor_SetInitialLoadSet_Statics::NewProp_NewLoadSet_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUsdStageActor_SetInitialLoadSet_Statics::NewProp_NewLoadSet,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUsdStageActor_SetInitialLoadSet_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "USD" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUsdStageActor_SetInitialLoadSet_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUsdStageActor, nullptr, "SetInitialLoadSet", nullptr, nullptr, sizeof(UsdStageActor_eventSetInitialLoadSet_Parms), Z_Construct_UFunction_AUsdStageActor_SetInitialLoadSet_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_SetInitialLoadSet_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020403, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUsdStageActor_SetInitialLoadSet_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_SetInitialLoadSet_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUsdStageActor_SetInitialLoadSet()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUsdStageActor_SetInitialLoadSet_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUsdStageActor_SetPurposesToLoad_Statics
	{
		struct UsdStageActor_eventSetPurposesToLoad_Parms
		{
			int32 NewPurposesToLoad;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NewPurposesToLoad;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_AUsdStageActor_SetPurposesToLoad_Statics::NewProp_NewPurposesToLoad = { "NewPurposesToLoad", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdStageActor_eventSetPurposesToLoad_Parms, NewPurposesToLoad), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AUsdStageActor_SetPurposesToLoad_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUsdStageActor_SetPurposesToLoad_Statics::NewProp_NewPurposesToLoad,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUsdStageActor_SetPurposesToLoad_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "USD" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUsdStageActor_SetPurposesToLoad_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUsdStageActor, nullptr, "SetPurposesToLoad", nullptr, nullptr, sizeof(UsdStageActor_eventSetPurposesToLoad_Parms), Z_Construct_UFunction_AUsdStageActor_SetPurposesToLoad_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_SetPurposesToLoad_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020403, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUsdStageActor_SetPurposesToLoad_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_SetPurposesToLoad_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUsdStageActor_SetPurposesToLoad()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUsdStageActor_SetPurposesToLoad_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUsdStageActor_SetRenderContext_Statics
	{
		struct UsdStageActor_eventSetRenderContext_Parms
		{
			FName NewRenderContext;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NewRenderContext_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NewRenderContext;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUsdStageActor_SetRenderContext_Statics::NewProp_NewRenderContext_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_AUsdStageActor_SetRenderContext_Statics::NewProp_NewRenderContext = { "NewRenderContext", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdStageActor_eventSetRenderContext_Parms, NewRenderContext), METADATA_PARAMS(Z_Construct_UFunction_AUsdStageActor_SetRenderContext_Statics::NewProp_NewRenderContext_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_SetRenderContext_Statics::NewProp_NewRenderContext_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AUsdStageActor_SetRenderContext_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUsdStageActor_SetRenderContext_Statics::NewProp_NewRenderContext,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUsdStageActor_SetRenderContext_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "USD" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUsdStageActor_SetRenderContext_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUsdStageActor, nullptr, "SetRenderContext", nullptr, nullptr, sizeof(UsdStageActor_eventSetRenderContext_Parms), Z_Construct_UFunction_AUsdStageActor_SetRenderContext_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_SetRenderContext_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420403, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUsdStageActor_SetRenderContext_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_SetRenderContext_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUsdStageActor_SetRenderContext()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUsdStageActor_SetRenderContext_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUsdStageActor_SetRootLayer_Statics
	{
		struct UsdStageActor_eventSetRootLayer_Parms
		{
			FString RootFilePath;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootFilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_RootFilePath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUsdStageActor_SetRootLayer_Statics::NewProp_RootFilePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_AUsdStageActor_SetRootLayer_Statics::NewProp_RootFilePath = { "RootFilePath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdStageActor_eventSetRootLayer_Parms, RootFilePath), METADATA_PARAMS(Z_Construct_UFunction_AUsdStageActor_SetRootLayer_Statics::NewProp_RootFilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_SetRootLayer_Statics::NewProp_RootFilePath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AUsdStageActor_SetRootLayer_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUsdStageActor_SetRootLayer_Statics::NewProp_RootFilePath,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUsdStageActor_SetRootLayer_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "USD" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUsdStageActor_SetRootLayer_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUsdStageActor, nullptr, "SetRootLayer", nullptr, nullptr, sizeof(UsdStageActor_eventSetRootLayer_Parms), Z_Construct_UFunction_AUsdStageActor_SetRootLayer_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_SetRootLayer_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020403, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUsdStageActor_SetRootLayer_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_SetRootLayer_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUsdStageActor_SetRootLayer()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUsdStageActor_SetRootLayer_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_AUsdStageActor_SetTime_Statics
	{
		struct UsdStageActor_eventSetTime_Parms
		{
			float InTime;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_AUsdStageActor_SetTime_Statics::NewProp_InTime = { "InTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdStageActor_eventSetTime_Parms, InTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_AUsdStageActor_SetTime_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_AUsdStageActor_SetTime_Statics::NewProp_InTime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_AUsdStageActor_SetTime_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "true" },
		{ "Category", "USD" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_AUsdStageActor_SetTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_AUsdStageActor, nullptr, "SetTime", nullptr, nullptr, sizeof(UsdStageActor_eventSetTime_Parms), Z_Construct_UFunction_AUsdStageActor_SetTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_SetTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020403, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_AUsdStageActor_SetTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_AUsdStageActor_SetTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_AUsdStageActor_SetTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_AUsdStageActor_SetTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_AUsdStageActor_NoRegister()
	{
		return AUsdStageActor::StaticClass();
	}
	struct Z_Construct_UClass_AUsdStageActor_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootLayer_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RootLayer;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_InitialLoadSet_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitialLoadSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_InitialLoadSet;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PurposesToLoad_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_PurposesToLoad;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderContext_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_RenderContext;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SceneComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SceneComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Time_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Time;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StartTimeCode_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StartTimeCode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EndTimeCode_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_EndTimeCode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TimeCodesPerSecond_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_TimeCodesPerSecond;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LevelSequence_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LevelSequence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RootUsdTwin_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RootUsdTwin;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PrimsToAnimate_ElementProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PrimsToAnimate_MetaData[];
#endif
		static const UE4CodeGen_Private::FSetPropertyParams NewProp_PrimsToAnimate;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ObjectsToWatch_ValueProp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObjectsToWatch_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectsToWatch_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_ObjectsToWatch;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetCache_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AssetCache;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Transactor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Transactor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AUsdStageActor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_USDStage,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_AUsdStageActor_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_AUsdStageActor_GetGeneratedAssets, "GetGeneratedAssets" }, // 3980503837
		{ &Z_Construct_UFunction_AUsdStageActor_GetGeneratedComponent, "GetGeneratedComponent" }, // 2429467635
		{ &Z_Construct_UFunction_AUsdStageActor_GetSourcePrimPath, "GetSourcePrimPath" }, // 2953700814
		{ &Z_Construct_UFunction_AUsdStageActor_GetTime, "GetTime" }, // 1934841740
		{ &Z_Construct_UFunction_AUsdStageActor_SetInitialLoadSet, "SetInitialLoadSet" }, // 2728569063
		{ &Z_Construct_UFunction_AUsdStageActor_SetPurposesToLoad, "SetPurposesToLoad" }, // 738633639
		{ &Z_Construct_UFunction_AUsdStageActor_SetRenderContext, "SetRenderContext" }, // 3886780583
		{ &Z_Construct_UFunction_AUsdStageActor_SetRootLayer, "SetRootLayer" }, // 1465593981
		{ &Z_Construct_UFunction_AUsdStageActor_SetTime, "SetTime" }, // 3522729322
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUsdStageActor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "USDStageActor.h" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUsdStageActor_Statics::NewProp_RootLayer_MetaData[] = {
		{ "Category", "USD" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
		{ "RelativeToGameDir", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_RootLayer = { "RootLayer", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUsdStageActor, RootLayer), Z_Construct_UScriptStruct_FFilePath, METADATA_PARAMS(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_RootLayer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_RootLayer_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_InitialLoadSet_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUsdStageActor_Statics::NewProp_InitialLoadSet_MetaData[] = {
		{ "Category", "USD" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_InitialLoadSet = { "InitialLoadSet", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUsdStageActor, InitialLoadSet), Z_Construct_UEnum_UnrealUSDWrapper_EUsdInitialLoadSet, METADATA_PARAMS(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_InitialLoadSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_InitialLoadSet_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUsdStageActor_Statics::NewProp_PurposesToLoad_MetaData[] = {
		{ "Bitmask", "" },
		{ "BitmaskEnum", "EUsdPurpose" },
		{ "Category", "USD" },
		{ "Comment", "/* Only load prims with these specific purposes from the USD file */" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
		{ "ToolTip", "Only load prims with these specific purposes from the USD file" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_PurposesToLoad = { "PurposesToLoad", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUsdStageActor, PurposesToLoad), METADATA_PARAMS(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_PurposesToLoad_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_PurposesToLoad_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUsdStageActor_Statics::NewProp_RenderContext_MetaData[] = {
		{ "Category", "USD" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_RenderContext = { "RenderContext", nullptr, (EPropertyFlags)0x0010000000000015, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUsdStageActor, RenderContext), METADATA_PARAMS(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_RenderContext_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_RenderContext_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUsdStageActor_Statics::NewProp_SceneComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "UsdStageActor" },
		{ "EditInline", "true" },
		{ "ExposeFunctionCategories", "Mesh,Rendering,Physics,Components|StaticMesh" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_SceneComponent = { "SceneComponent", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUsdStageActor, SceneComponent), Z_Construct_UClass_USceneComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_SceneComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_SceneComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUsdStageActor_Statics::NewProp_Time_MetaData[] = {
		{ "Category", "USD" },
		{ "Comment", "/* TimeCode to evaluate the USD stage at */" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
		{ "ToolTip", "TimeCode to evaluate the USD stage at" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_Time = { "Time", nullptr, (EPropertyFlags)0x0040000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUsdStageActor, Time), METADATA_PARAMS(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_Time_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_Time_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUsdStageActor_Statics::NewProp_StartTimeCode_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_StartTimeCode = { "StartTimeCode", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUsdStageActor, StartTimeCode_DEPRECATED), METADATA_PARAMS(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_StartTimeCode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_StartTimeCode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUsdStageActor_Statics::NewProp_EndTimeCode_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_EndTimeCode = { "EndTimeCode", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUsdStageActor, EndTimeCode_DEPRECATED), METADATA_PARAMS(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_EndTimeCode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_EndTimeCode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUsdStageActor_Statics::NewProp_TimeCodesPerSecond_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_TimeCodesPerSecond = { "TimeCodesPerSecond", nullptr, (EPropertyFlags)0x0040000020000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUsdStageActor, TimeCodesPerSecond_DEPRECATED), METADATA_PARAMS(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_TimeCodesPerSecond_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_TimeCodesPerSecond_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUsdStageActor_Statics::NewProp_LevelSequence_MetaData[] = {
		{ "Category", "USD" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_LevelSequence = { "LevelSequence", nullptr, (EPropertyFlags)0x0040000000022001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUsdStageActor, LevelSequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_LevelSequence_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_LevelSequence_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUsdStageActor_Statics::NewProp_RootUsdTwin_MetaData[] = {
		{ "Comment", "// Note: This cannot be instanced: Read the comment in the constructor\n" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
		{ "ToolTip", "Note: This cannot be instanced: Read the comment in the constructor" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_RootUsdTwin = { "RootUsdTwin", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUsdStageActor, RootUsdTwin), Z_Construct_UClass_UUsdPrimTwin_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_RootUsdTwin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_RootUsdTwin_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_PrimsToAnimate_ElementProp = { "PrimsToAnimate", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUsdStageActor_Statics::NewProp_PrimsToAnimate_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FSetPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_PrimsToAnimate = { "PrimsToAnimate", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Set, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUsdStageActor, PrimsToAnimate), METADATA_PARAMS(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_PrimsToAnimate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_PrimsToAnimate_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_ObjectsToWatch_ValueProp = { "ObjectsToWatch", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_ObjectsToWatch_Key_KeyProp = { "ObjectsToWatch_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUsdStageActor_Statics::NewProp_ObjectsToWatch_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_ObjectsToWatch = { "ObjectsToWatch", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUsdStageActor, ObjectsToWatch), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_ObjectsToWatch_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_ObjectsToWatch_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUsdStageActor_Statics::NewProp_AssetCache_MetaData[] = {
		{ "Category", "USD" },
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_AssetCache = { "AssetCache", nullptr, (EPropertyFlags)0x0040040000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUsdStageActor, AssetCache), Z_Construct_UClass_UUsdAssetCache_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_AssetCache_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_AssetCache_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AUsdStageActor_Statics::NewProp_Transactor_MetaData[] = {
		{ "ModuleRelativePath", "Public/USDStageActor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AUsdStageActor_Statics::NewProp_Transactor = { "Transactor", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AUsdStageActor, Transactor), Z_Construct_UClass_UUsdTransactor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_Transactor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::NewProp_Transactor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AUsdStageActor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_RootLayer,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_InitialLoadSet_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_InitialLoadSet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_PurposesToLoad,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_RenderContext,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_SceneComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_Time,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_StartTimeCode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_EndTimeCode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_TimeCodesPerSecond,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_LevelSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_RootUsdTwin,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_PrimsToAnimate_ElementProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_PrimsToAnimate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_ObjectsToWatch_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_ObjectsToWatch_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_ObjectsToWatch,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_AssetCache,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AUsdStageActor_Statics::NewProp_Transactor,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AUsdStageActor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AUsdStageActor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AUsdStageActor_Statics::ClassParams = {
		&AUsdStageActor::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_AUsdStageActor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::PropPointers),
		0,
		0x008800A4u,
		METADATA_PARAMS(Z_Construct_UClass_AUsdStageActor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AUsdStageActor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AUsdStageActor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AUsdStageActor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AUsdStageActor, 1507941427);
	template<> USDSTAGE_API UClass* StaticClass<AUsdStageActor>()
	{
		return AUsdStageActor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AUsdStageActor(Z_Construct_UClass_AUsdStageActor, &AUsdStageActor::StaticClass, TEXT("/Script/USDStage"), TEXT("AUsdStageActor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AUsdStageActor);
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(AUsdStageActor)
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
