// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "USDExporter/Public/USDExporterBlueprintLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUSDExporterBlueprintLibrary() {}
// Cross Module References
	USDEXPORTER_API UClass* Z_Construct_UClass_UUsdExporterBlueprintLibrary_NoRegister();
	USDEXPORTER_API UClass* Z_Construct_UClass_UUsdExporterBlueprintLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_USDExporter();
	ENGINE_API UClass* Z_Construct_UClass_ULevel_NoRegister();
	FOLIAGE_API UClass* Z_Construct_UClass_AInstancedFoliageActor_NoRegister();
	FOLIAGE_API UClass* Z_Construct_UClass_UFoliageType_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FTransform();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UUsdExporterBlueprintLibrary::execGetInstanceTransforms)
	{
		P_GET_OBJECT(AInstancedFoliageActor,Z_Param_Actor);
		P_GET_OBJECT(UFoliageType,Z_Param_FoliageType);
		P_GET_OBJECT(ULevel,Z_Param_InstancesLevel);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FTransform>*)Z_Param__Result=UUsdExporterBlueprintLibrary::GetInstanceTransforms(Z_Param_Actor,Z_Param_FoliageType,Z_Param_InstancesLevel);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUsdExporterBlueprintLibrary::execGetSource)
	{
		P_GET_OBJECT(UFoliageType,Z_Param_FoliageType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UObject**)Z_Param__Result=UUsdExporterBlueprintLibrary::GetSource(Z_Param_FoliageType);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUsdExporterBlueprintLibrary::execGetUsedFoliageTypes)
	{
		P_GET_OBJECT(AInstancedFoliageActor,Z_Param_Actor);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UFoliageType*>*)Z_Param__Result=UUsdExporterBlueprintLibrary::GetUsedFoliageTypes(Z_Param_Actor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UUsdExporterBlueprintLibrary::execGetInstancedFoliageActorForLevel)
	{
		P_GET_UBOOL(Z_Param_bCreateIfNone);
		P_GET_OBJECT(ULevel,Z_Param_Level);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AInstancedFoliageActor**)Z_Param__Result=UUsdExporterBlueprintLibrary::GetInstancedFoliageActorForLevel(Z_Param_bCreateIfNone,Z_Param_Level);
		P_NATIVE_END;
	}
	void UUsdExporterBlueprintLibrary::StaticRegisterNativesUUsdExporterBlueprintLibrary()
	{
		UClass* Class = UUsdExporterBlueprintLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetInstancedFoliageActorForLevel", &UUsdExporterBlueprintLibrary::execGetInstancedFoliageActorForLevel },
			{ "GetInstanceTransforms", &UUsdExporterBlueprintLibrary::execGetInstanceTransforms },
			{ "GetSource", &UUsdExporterBlueprintLibrary::execGetSource },
			{ "GetUsedFoliageTypes", &UUsdExporterBlueprintLibrary::execGetUsedFoliageTypes },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics
	{
		struct UsdExporterBlueprintLibrary_eventGetInstancedFoliageActorForLevel_Parms
		{
			bool bCreateIfNone;
			ULevel* Level;
			AInstancedFoliageActor* ReturnValue;
		};
		static void NewProp_bCreateIfNone_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCreateIfNone;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Level;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics::NewProp_bCreateIfNone_SetBit(void* Obj)
	{
		((UsdExporterBlueprintLibrary_eventGetInstancedFoliageActorForLevel_Parms*)Obj)->bCreateIfNone = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics::NewProp_bCreateIfNone = { "bCreateIfNone", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UsdExporterBlueprintLibrary_eventGetInstancedFoliageActorForLevel_Parms), &Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics::NewProp_bCreateIfNone_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics::NewProp_Level = { "Level", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdExporterBlueprintLibrary_eventGetInstancedFoliageActorForLevel_Parms, Level), Z_Construct_UClass_ULevel_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdExporterBlueprintLibrary_eventGetInstancedFoliageActorForLevel_Parms, ReturnValue), Z_Construct_UClass_AInstancedFoliageActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics::NewProp_bCreateIfNone,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics::NewProp_Level,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics::Function_MetaDataParams[] = {
		{ "Category", "USD Foliage Exporter" },
		{ "Comment", "/**\n\x09 * Wraps AInstancedFoliageActor::GetInstancedFoliageActorForLevel, and allows retrieving the current AInstancedFoliageActor\n\x09 * for a level. Will default to the current editor level if Level is left nullptr.\n\x09 * This function is useful because it's difficult to retrieve this actor otherwise, as it will be filtered from\n\x09 * the results of functions like EditorLevelLibrary.get_all_level_actors()\n\x09 */" },
		{ "CPP_Default_bCreateIfNone", "false" },
		{ "CPP_Default_Level", "None" },
		{ "ModuleRelativePath", "Public/USDExporterBlueprintLibrary.h" },
		{ "ToolTip", "Wraps AInstancedFoliageActor::GetInstancedFoliageActorForLevel, and allows retrieving the current AInstancedFoliageActor\nfor a level. Will default to the current editor level if Level is left nullptr.\nThis function is useful because it's difficult to retrieve this actor otherwise, as it will be filtered from\nthe results of functions like EditorLevelLibrary.get_all_level_actors()" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUsdExporterBlueprintLibrary, nullptr, "GetInstancedFoliageActorForLevel", nullptr, nullptr, sizeof(UsdExporterBlueprintLibrary_eventGetInstancedFoliageActorForLevel_Parms), Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics
	{
		struct UsdExporterBlueprintLibrary_eventGetInstanceTransforms_Parms
		{
			AInstancedFoliageActor* Actor;
			UFoliageType* FoliageType;
			ULevel* InstancesLevel;
			TArray<FTransform> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FoliageType;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InstancesLevel;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdExporterBlueprintLibrary_eventGetInstanceTransforms_Parms, Actor), Z_Construct_UClass_AInstancedFoliageActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::NewProp_FoliageType = { "FoliageType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdExporterBlueprintLibrary_eventGetInstanceTransforms_Parms, FoliageType), Z_Construct_UClass_UFoliageType_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::NewProp_InstancesLevel = { "InstancesLevel", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdExporterBlueprintLibrary_eventGetInstanceTransforms_Parms, InstancesLevel), Z_Construct_UClass_ULevel_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTransform, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdExporterBlueprintLibrary_eventGetInstanceTransforms_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::NewProp_Actor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::NewProp_FoliageType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::NewProp_InstancesLevel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::Function_MetaDataParams[] = {
		{ "Category", "USD Foliage Exporter" },
		{ "Comment", "/**\n\x09 * Returns the transforms of all instances of a particular UFoliageType on a given level (which defaults to the Actor's level if left nullptr).\n\x09 * Use GetUsedFoliageTypes() to retrieve all foliage types managed by a particular actor.\n\x09 */" },
		{ "CPP_Default_InstancesLevel", "None" },
		{ "ModuleRelativePath", "Public/USDExporterBlueprintLibrary.h" },
		{ "ScriptMethod", "" },
		{ "ToolTip", "Returns the transforms of all instances of a particular UFoliageType on a given level (which defaults to the Actor's level if left nullptr).\nUse GetUsedFoliageTypes() to retrieve all foliage types managed by a particular actor." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUsdExporterBlueprintLibrary, nullptr, "GetInstanceTransforms", nullptr, nullptr, sizeof(UsdExporterBlueprintLibrary_eventGetInstanceTransforms_Parms), Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetSource_Statics
	{
		struct UsdExporterBlueprintLibrary_eventGetSource_Parms
		{
			UFoliageType* FoliageType;
			UObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FoliageType;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetSource_Statics::NewProp_FoliageType = { "FoliageType", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdExporterBlueprintLibrary_eventGetSource_Parms, FoliageType), Z_Construct_UClass_UFoliageType_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetSource_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdExporterBlueprintLibrary_eventGetSource_Parms, ReturnValue), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetSource_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetSource_Statics::NewProp_FoliageType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetSource_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetSource_Statics::Function_MetaDataParams[] = {
		{ "Category", "USD Foliage Exporter" },
		{ "Comment", "/**\n\x09 * Returns the source asset for a UFoliageType.\n\x09 * It can be a UStaticMesh in case we're dealing with a UFoliageType_InstancedStaticMesh, but it can be other types of objects.\n\x09 */" },
		{ "ModuleRelativePath", "Public/USDExporterBlueprintLibrary.h" },
		{ "ScriptMethod", "" },
		{ "ToolTip", "Returns the source asset for a UFoliageType.\nIt can be a UStaticMesh in case we're dealing with a UFoliageType_InstancedStaticMesh, but it can be other types of objects." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetSource_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUsdExporterBlueprintLibrary, nullptr, "GetSource", nullptr, nullptr, sizeof(UsdExporterBlueprintLibrary_eventGetSource_Parms), Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetSource_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetSource_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetSource_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetSource_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetSource()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetSource_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes_Statics
	{
		struct UsdExporterBlueprintLibrary_eventGetUsedFoliageTypes_Parms
		{
			AInstancedFoliageActor* Actor;
			TArray<UFoliageType*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actor;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes_Statics::NewProp_Actor = { "Actor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdExporterBlueprintLibrary_eventGetUsedFoliageTypes_Parms, Actor), Z_Construct_UClass_AInstancedFoliageActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UFoliageType_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UsdExporterBlueprintLibrary_eventGetUsedFoliageTypes_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes_Statics::NewProp_Actor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes_Statics::Function_MetaDataParams[] = {
		{ "Category", "USD Foliage Exporter" },
		{ "Comment", "/**\n\x09 * Returns all the different types of UFoliageType assets that a particular AInstancedFoliageActor uses.\n\x09 * This function exists because we want to retrieve all instances of all foliage types on an actor, but we\n\x09 * can't return nested containers from UFUNCTIONs, so users of this API should call this, and then GetInstanceTransforms.\n\x09 */" },
		{ "ModuleRelativePath", "Public/USDExporterBlueprintLibrary.h" },
		{ "ScriptMethod", "" },
		{ "ToolTip", "Returns all the different types of UFoliageType assets that a particular AInstancedFoliageActor uses.\nThis function exists because we want to retrieve all instances of all foliage types on an actor, but we\ncan't return nested containers from UFUNCTIONs, so users of this API should call this, and then GetInstanceTransforms." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UUsdExporterBlueprintLibrary, nullptr, "GetUsedFoliageTypes", nullptr, nullptr, sizeof(UsdExporterBlueprintLibrary_eventGetUsedFoliageTypes_Parms), Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UUsdExporterBlueprintLibrary_NoRegister()
	{
		return UUsdExporterBlueprintLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UUsdExporterBlueprintLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUsdExporterBlueprintLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_USDExporter,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UUsdExporterBlueprintLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstancedFoliageActorForLevel, "GetInstancedFoliageActorForLevel" }, // 694654920
		{ &Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetInstanceTransforms, "GetInstanceTransforms" }, // 3554324582
		{ &Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetSource, "GetSource" }, // 3681234315
		{ &Z_Construct_UFunction_UUsdExporterBlueprintLibrary_GetUsedFoliageTypes, "GetUsedFoliageTypes" }, // 2049338880
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUsdExporterBlueprintLibrary_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Library of functions that can be used via Python scripting to help export Unreal scenes and assets to USD\n */" },
		{ "IncludePath", "USDExporterBlueprintLibrary.h" },
		{ "ModuleRelativePath", "Public/USDExporterBlueprintLibrary.h" },
		{ "ScriptName", "USDExporterLibrary" },
		{ "ToolTip", "Library of functions that can be used via Python scripting to help export Unreal scenes and assets to USD" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUsdExporterBlueprintLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUsdExporterBlueprintLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUsdExporterBlueprintLibrary_Statics::ClassParams = {
		&UUsdExporterBlueprintLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UUsdExporterBlueprintLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUsdExporterBlueprintLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUsdExporterBlueprintLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUsdExporterBlueprintLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUsdExporterBlueprintLibrary, 3513439818);
	template<> USDEXPORTER_API UClass* StaticClass<UUsdExporterBlueprintLibrary>()
	{
		return UUsdExporterBlueprintLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUsdExporterBlueprintLibrary(Z_Construct_UClass_UUsdExporterBlueprintLibrary, &UUsdExporterBlueprintLibrary::StaticClass, TEXT("/Script/USDExporter"), TEXT("UUsdExporterBlueprintLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUsdExporterBlueprintLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
