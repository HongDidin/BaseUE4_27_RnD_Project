// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ReplayTracksEditor/Private/ReplayTracksEditorModule.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeReplayTracksEditorModule() {}
// Cross Module References
	REPLAYTRACKSEDITOR_API UClass* Z_Construct_UClass_UReplayTracksCameraModifier_NoRegister();
	REPLAYTRACKSEDITOR_API UClass* Z_Construct_UClass_UReplayTracksCameraModifier();
	ENGINE_API UClass* Z_Construct_UClass_UCameraModifier();
	UPackage* Z_Construct_UPackage__Script_ReplayTracksEditor();
// End Cross Module References
	void UReplayTracksCameraModifier::StaticRegisterNativesUReplayTracksCameraModifier()
	{
	}
	UClass* Z_Construct_UClass_UReplayTracksCameraModifier_NoRegister()
	{
		return UReplayTracksCameraModifier::StaticClass();
	}
	struct Z_Construct_UClass_UReplayTracksCameraModifier_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UReplayTracksCameraModifier_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UCameraModifier,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplayTracksEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UReplayTracksCameraModifier_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Camera modifier that lets us set the correct FOV and post-process settings on the spectator pawn\n * when we want to lock into a camera.\n */" },
		{ "IncludePath", "ReplayTracksEditorModule.h" },
		{ "ModuleRelativePath", "Private/ReplayTracksEditorModule.h" },
		{ "ToolTip", "Camera modifier that lets us set the correct FOV and post-process settings on the spectator pawn\nwhen we want to lock into a camera." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UReplayTracksCameraModifier_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UReplayTracksCameraModifier>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UReplayTracksCameraModifier_Statics::ClassParams = {
		&UReplayTracksCameraModifier::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UReplayTracksCameraModifier_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UReplayTracksCameraModifier_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UReplayTracksCameraModifier()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UReplayTracksCameraModifier_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UReplayTracksCameraModifier, 3182678918);
	template<> REPLAYTRACKSEDITOR_API UClass* StaticClass<UReplayTracksCameraModifier>()
	{
		return UReplayTracksCameraModifier::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UReplayTracksCameraModifier(Z_Construct_UClass_UReplayTracksCameraModifier, &UReplayTracksCameraModifier::StaticClass, TEXT("/Script/ReplayTracksEditor"), TEXT("UReplayTracksCameraModifier"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UReplayTracksCameraModifier);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
