// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REPLAYTRACKS_MovieSceneReplaySection_generated_h
#error "MovieSceneReplaySection.generated.h already included, missing '#pragma once' in MovieSceneReplaySection.h"
#endif
#define REPLAYTRACKS_MovieSceneReplaySection_generated_h

#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_SPARSE_DATA
#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneReplaySection(); \
	friend struct Z_Construct_UClass_UMovieSceneReplaySection_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneReplaySection, UMovieSceneSection, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ReplayTracks"), REPLAYTRACKS_API) \
	DECLARE_SERIALIZER(UMovieSceneReplaySection) \
	virtual UObject* _getUObject() const override { return const_cast<UMovieSceneReplaySection*>(this); }


#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneReplaySection(); \
	friend struct Z_Construct_UClass_UMovieSceneReplaySection_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneReplaySection, UMovieSceneSection, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ReplayTracks"), REPLAYTRACKS_API) \
	DECLARE_SERIALIZER(UMovieSceneReplaySection) \
	virtual UObject* _getUObject() const override { return const_cast<UMovieSceneReplaySection*>(this); }


#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	REPLAYTRACKS_API UMovieSceneReplaySection(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneReplaySection) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(REPLAYTRACKS_API, UMovieSceneReplaySection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneReplaySection); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	REPLAYTRACKS_API UMovieSceneReplaySection(UMovieSceneReplaySection&&); \
	REPLAYTRACKS_API UMovieSceneReplaySection(const UMovieSceneReplaySection&); \
public:


#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	REPLAYTRACKS_API UMovieSceneReplaySection(UMovieSceneReplaySection&&); \
	REPLAYTRACKS_API UMovieSceneReplaySection(const UMovieSceneReplaySection&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(REPLAYTRACKS_API, UMovieSceneReplaySection); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneReplaySection); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneReplaySection)


#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_12_PROLOG
#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_SPARSE_DATA \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_INCLASS \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_SPARSE_DATA \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h_19_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLAYTRACKS_API UClass* StaticClass<class UMovieSceneReplaySection>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Sections_MovieSceneReplaySection_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
