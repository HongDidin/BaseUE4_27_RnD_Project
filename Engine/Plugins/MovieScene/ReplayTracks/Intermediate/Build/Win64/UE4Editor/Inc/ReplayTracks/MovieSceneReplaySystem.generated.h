// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REPLAYTRACKS_MovieSceneReplaySystem_generated_h
#error "MovieSceneReplaySystem.generated.h already included, missing '#pragma once' in MovieSceneReplaySystem.h"
#endif
#define REPLAYTRACKS_MovieSceneReplaySystem_generated_h

#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_SPARSE_DATA
#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneReplaySystem(); \
	friend struct Z_Construct_UClass_UMovieSceneReplaySystem_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneReplaySystem, UMovieSceneEntitySystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ReplayTracks"), REPLAYTRACKS_API) \
	DECLARE_SERIALIZER(UMovieSceneReplaySystem)


#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneReplaySystem(); \
	friend struct Z_Construct_UClass_UMovieSceneReplaySystem_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneReplaySystem, UMovieSceneEntitySystem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ReplayTracks"), REPLAYTRACKS_API) \
	DECLARE_SERIALIZER(UMovieSceneReplaySystem)


#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	REPLAYTRACKS_API UMovieSceneReplaySystem(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneReplaySystem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(REPLAYTRACKS_API, UMovieSceneReplaySystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneReplaySystem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	REPLAYTRACKS_API UMovieSceneReplaySystem(UMovieSceneReplaySystem&&); \
	REPLAYTRACKS_API UMovieSceneReplaySystem(const UMovieSceneReplaySystem&); \
public:


#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	REPLAYTRACKS_API UMovieSceneReplaySystem(UMovieSceneReplaySystem&&); \
	REPLAYTRACKS_API UMovieSceneReplaySystem(const UMovieSceneReplaySystem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(REPLAYTRACKS_API, UMovieSceneReplaySystem); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneReplaySystem); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneReplaySystem)


#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_45_PROLOG
#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_SPARSE_DATA \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_INCLASS \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_SPARSE_DATA \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h_49_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLAYTRACKS_API UClass* StaticClass<class UMovieSceneReplaySystem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Private_Systems_MovieSceneReplaySystem_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
