// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef REPLAYTRACKS_MovieSceneReplayTrack_generated_h
#error "MovieSceneReplayTrack.generated.h already included, missing '#pragma once' in MovieSceneReplayTrack.h"
#endif
#define REPLAYTRACKS_MovieSceneReplayTrack_generated_h

#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_SPARSE_DATA
#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneReplayTrack(); \
	friend struct Z_Construct_UClass_UMovieSceneReplayTrack_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneReplayTrack, UMovieSceneTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ReplayTracks"), REPLAYTRACKS_API) \
	DECLARE_SERIALIZER(UMovieSceneReplayTrack)


#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneReplayTrack(); \
	friend struct Z_Construct_UClass_UMovieSceneReplayTrack_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneReplayTrack, UMovieSceneTrack, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/ReplayTracks"), REPLAYTRACKS_API) \
	DECLARE_SERIALIZER(UMovieSceneReplayTrack)


#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	REPLAYTRACKS_API UMovieSceneReplayTrack(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneReplayTrack) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(REPLAYTRACKS_API, UMovieSceneReplayTrack); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneReplayTrack); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	REPLAYTRACKS_API UMovieSceneReplayTrack(UMovieSceneReplayTrack&&); \
	REPLAYTRACKS_API UMovieSceneReplayTrack(const UMovieSceneReplayTrack&); \
public:


#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	REPLAYTRACKS_API UMovieSceneReplayTrack(UMovieSceneReplayTrack&&); \
	REPLAYTRACKS_API UMovieSceneReplayTrack(const UMovieSceneReplayTrack&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(REPLAYTRACKS_API, UMovieSceneReplayTrack); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneReplayTrack); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneReplayTrack)


#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Sections() { return STRUCT_OFFSET(UMovieSceneReplayTrack, Sections); }


#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_13_PROLOG
#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_SPARSE_DATA \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_INCLASS \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_SPARSE_DATA \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h_18_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> REPLAYTRACKS_API UClass* StaticClass<class UMovieSceneReplayTrack>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_ReplayTracks_Source_ReplayTracks_Public_Tracks_MovieSceneReplayTrack_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
