// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "ReplayTracks/Private/Systems/MovieSceneReplaySystem.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneReplaySystem() {}
// Cross Module References
	REPLAYTRACKS_API UClass* Z_Construct_UClass_UMovieSceneReplaySystem_NoRegister();
	REPLAYTRACKS_API UClass* Z_Construct_UClass_UMovieSceneReplaySystem();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneEntitySystem();
	UPackage* Z_Construct_UPackage__Script_ReplayTracks();
// End Cross Module References
	void UMovieSceneReplaySystem::StaticRegisterNativesUMovieSceneReplaySystem()
	{
	}
	UClass* Z_Construct_UClass_UMovieSceneReplaySystem_NoRegister()
	{
		return UMovieSceneReplaySystem::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneReplaySystem_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneReplaySystem_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneEntitySystem,
		(UObject* (*)())Z_Construct_UPackage__Script_ReplayTracks,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneReplaySystem_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// namespace UE\n" },
		{ "IncludePath", "Systems/MovieSceneReplaySystem.h" },
		{ "ModuleRelativePath", "Private/Systems/MovieSceneReplaySystem.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "namespace UE" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneReplaySystem_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneReplaySystem>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneReplaySystem_Statics::ClassParams = {
		&UMovieSceneReplaySystem::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneReplaySystem_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneReplaySystem_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneReplaySystem()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneReplaySystem_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneReplaySystem, 835683056);
	template<> REPLAYTRACKS_API UClass* StaticClass<UMovieSceneReplaySystem>()
	{
		return UMovieSceneReplaySystem::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneReplaySystem(Z_Construct_UClass_UMovieSceneReplaySystem, &UMovieSceneReplaySystem::StaticClass, TEXT("/Script/ReplayTracks"), TEXT("UMovieSceneReplaySystem"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneReplaySystem);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
