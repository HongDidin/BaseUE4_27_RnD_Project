// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CustomizableSequencerTracks/Public/SequencerTrackBP.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSequencerTrackBP() {}
// Cross Module References
	CUSTOMIZABLESEQUENCERTRACKS_API UEnum* Z_Construct_UEnum_CustomizableSequencerTracks_ECustomSequencerTrackType();
	UPackage* Z_Construct_UPackage__Script_CustomizableSequencerTracks();
	CUSTOMIZABLESEQUENCERTRACKS_API UClass* Z_Construct_UClass_USequencerTrackBP_NoRegister();
	CUSTOMIZABLESEQUENCERTRACKS_API UClass* Z_Construct_UClass_USequencerTrackBP();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneNameableTrack();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	CUSTOMIZABLESEQUENCERTRACKS_API UClass* Z_Construct_UClass_USequencerSectionBP_NoRegister();
	CUSTOMIZABLESEQUENCERTRACKS_API UClass* Z_Construct_UClass_USequencerTrackInstanceBP_NoRegister();
	SLATECORE_API UScriptStruct* Z_Construct_UScriptStruct_FSlateBrush();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneSection_NoRegister();
// End Cross Module References
	static UEnum* ECustomSequencerTrackType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_CustomizableSequencerTracks_ECustomSequencerTrackType, Z_Construct_UPackage__Script_CustomizableSequencerTracks(), TEXT("ECustomSequencerTrackType"));
		}
		return Singleton;
	}
	template<> CUSTOMIZABLESEQUENCERTRACKS_API UEnum* StaticEnum<ECustomSequencerTrackType>()
	{
		return ECustomSequencerTrackType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ECustomSequencerTrackType(ECustomSequencerTrackType_StaticEnum, TEXT("/Script/CustomizableSequencerTracks"), TEXT("ECustomSequencerTrackType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_CustomizableSequencerTracks_ECustomSequencerTrackType_Hash() { return 178430933U; }
	UEnum* Z_Construct_UEnum_CustomizableSequencerTracks_ECustomSequencerTrackType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_CustomizableSequencerTracks();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ECustomSequencerTrackType"), 0, Get_Z_Construct_UEnum_CustomizableSequencerTracks_ECustomSequencerTrackType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ECustomSequencerTrackType::MasterTrack", (int64)ECustomSequencerTrackType::MasterTrack },
				{ "ECustomSequencerTrackType::ObjectTrack", (int64)ECustomSequencerTrackType::ObjectTrack },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "MasterTrack.Name", "ECustomSequencerTrackType::MasterTrack" },
				{ "ModuleRelativePath", "Public/SequencerTrackBP.h" },
				{ "ObjectTrack.Name", "ECustomSequencerTrackType::ObjectTrack" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_CustomizableSequencerTracks,
				nullptr,
				"ECustomSequencerTrackType",
				"ECustomSequencerTrackType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void USequencerTrackBP::StaticRegisterNativesUSequencerTrackBP()
	{
	}
	UClass* Z_Construct_UClass_USequencerTrackBP_NoRegister()
	{
		return USequencerTrackBP::StaticClass();
	}
	struct Z_Construct_UClass_USequencerTrackBP_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSupportsMultipleRows_MetaData[];
#endif
		static void NewProp_bSupportsMultipleRows_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSupportsMultipleRows;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSupportsBlending_MetaData[];
#endif
		static void NewProp_bSupportsBlending_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSupportsBlending;
		static const UE4CodeGen_Private::FUnsizedIntPropertyParams NewProp_TrackType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TrackType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SupportedObjectType_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SupportedObjectType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultSectionType_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_DefaultSectionType;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_SupportedSections_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SupportedSections_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SupportedSections;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackInstanceType_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_TrackInstanceType;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Icon_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Icon;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sections_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Sections_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sections_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Sections;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USequencerTrackBP_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneNameableTrack,
		(UObject* (*)())Z_Construct_UPackage__Script_CustomizableSequencerTracks,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USequencerTrackBP_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayName", "SequencerTrack" },
		{ "IncludePath", "SequencerTrackBP.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/SequencerTrackBP.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_bSupportsMultipleRows_MetaData[] = {
		{ "Category", "Sequencer" },
		{ "ModuleRelativePath", "Public/SequencerTrackBP.h" },
	};
#endif
	void Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_bSupportsMultipleRows_SetBit(void* Obj)
	{
		((USequencerTrackBP*)Obj)->bSupportsMultipleRows = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_bSupportsMultipleRows = { "bSupportsMultipleRows", nullptr, (EPropertyFlags)0x0010010000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USequencerTrackBP), &Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_bSupportsMultipleRows_SetBit, METADATA_PARAMS(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_bSupportsMultipleRows_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_bSupportsMultipleRows_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_bSupportsBlending_MetaData[] = {
		{ "Category", "Sequencer" },
		{ "ModuleRelativePath", "Public/SequencerTrackBP.h" },
	};
#endif
	void Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_bSupportsBlending_SetBit(void* Obj)
	{
		((USequencerTrackBP*)Obj)->bSupportsBlending = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_bSupportsBlending = { "bSupportsBlending", nullptr, (EPropertyFlags)0x0010010000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(USequencerTrackBP), &Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_bSupportsBlending_SetBit, METADATA_PARAMS(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_bSupportsBlending_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_bSupportsBlending_MetaData)) };
	const UE4CodeGen_Private::FUnsizedIntPropertyParams Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_TrackType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_TrackType_MetaData[] = {
		{ "Category", "Sequencer" },
		{ "ModuleRelativePath", "Public/SequencerTrackBP.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_TrackType = { "TrackType", nullptr, (EPropertyFlags)0x0010010000010001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USequencerTrackBP, TrackType), Z_Construct_UEnum_CustomizableSequencerTracks_ECustomSequencerTrackType, METADATA_PARAMS(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_TrackType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_TrackType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_SupportedObjectType_MetaData[] = {
		{ "Category", "Sequencer" },
		{ "EditCondition", "TrackType==ECustomSequencerTrackType::ObjectTrack" },
		{ "ModuleRelativePath", "Public/SequencerTrackBP.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_SupportedObjectType = { "SupportedObjectType", nullptr, (EPropertyFlags)0x0010010000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USequencerTrackBP, SupportedObjectType), Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_SupportedObjectType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_SupportedObjectType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_DefaultSectionType_MetaData[] = {
		{ "Category", "Sequencer" },
		{ "ModuleRelativePath", "Public/SequencerTrackBP.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_DefaultSectionType = { "DefaultSectionType", nullptr, (EPropertyFlags)0x0014010000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USequencerTrackBP, DefaultSectionType), Z_Construct_UClass_USequencerSectionBP_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_DefaultSectionType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_DefaultSectionType_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_SupportedSections_Inner = { "SupportedSections", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_USequencerSectionBP_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_SupportedSections_MetaData[] = {
		{ "Category", "Sequencer" },
		{ "ModuleRelativePath", "Public/SequencerTrackBP.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_SupportedSections = { "SupportedSections", nullptr, (EPropertyFlags)0x0014010000010001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USequencerTrackBP, SupportedSections), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_SupportedSections_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_SupportedSections_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_TrackInstanceType_MetaData[] = {
		{ "Category", "Sequencer" },
		{ "ModuleRelativePath", "Public/SequencerTrackBP.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_TrackInstanceType = { "TrackInstanceType", nullptr, (EPropertyFlags)0x0014010000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USequencerTrackBP, TrackInstanceType), Z_Construct_UClass_USequencerTrackInstanceBP_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_TrackInstanceType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_TrackInstanceType_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_Icon_MetaData[] = {
		{ "Category", "Sequencer" },
		{ "ModuleRelativePath", "Public/SequencerTrackBP.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_Icon = { "Icon", nullptr, (EPropertyFlags)0x0010010000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USequencerTrackBP, Icon), Z_Construct_UScriptStruct_FSlateBrush, METADATA_PARAMS(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_Icon_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_Icon_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_Sections_Inner_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/SequencerTrackBP.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_Sections_Inner = { "Sections", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMovieSceneSection_NoRegister, METADATA_PARAMS(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_Sections_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_Sections_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_Sections_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/SequencerTrackBP.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_Sections = { "Sections", nullptr, (EPropertyFlags)0x0040008000000008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(USequencerTrackBP, Sections), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_Sections_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_Sections_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_USequencerTrackBP_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_bSupportsMultipleRows,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_bSupportsBlending,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_TrackType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_TrackType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_SupportedObjectType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_DefaultSectionType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_SupportedSections_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_SupportedSections,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_TrackInstanceType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_Icon,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_Sections_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_USequencerTrackBP_Statics::NewProp_Sections,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USequencerTrackBP_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USequencerTrackBP>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USequencerTrackBP_Statics::ClassParams = {
		&USequencerTrackBP::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_USequencerTrackBP_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_USequencerTrackBP_Statics::PropPointers),
		0,
		0x00B000A1u,
		METADATA_PARAMS(Z_Construct_UClass_USequencerTrackBP_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USequencerTrackBP_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USequencerTrackBP()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USequencerTrackBP_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USequencerTrackBP, 1550962779);
	template<> CUSTOMIZABLESEQUENCERTRACKS_API UClass* StaticClass<USequencerTrackBP>()
	{
		return USequencerTrackBP::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USequencerTrackBP(Z_Construct_UClass_USequencerTrackBP, &USequencerTrackBP::StaticClass, TEXT("/Script/CustomizableSequencerTracks"), TEXT("USequencerTrackBP"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USequencerTrackBP);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
