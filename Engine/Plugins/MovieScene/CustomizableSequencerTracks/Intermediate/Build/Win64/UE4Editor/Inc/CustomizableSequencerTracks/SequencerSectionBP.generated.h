// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CUSTOMIZABLESEQUENCERTRACKS_SequencerSectionBP_generated_h
#error "SequencerSectionBP.generated.h already included, missing '#pragma once' in SequencerSectionBP.h"
#endif
#define CUSTOMIZABLESEQUENCERTRACKS_SequencerSectionBP_generated_h

#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_SPARSE_DATA
#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSequencerSectionBP(); \
	friend struct Z_Construct_UClass_USequencerSectionBP_Statics; \
public: \
	DECLARE_CLASS(USequencerSectionBP, UMovieSceneSection, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CustomizableSequencerTracks"), NO_API) \
	DECLARE_SERIALIZER(USequencerSectionBP) \
	virtual UObject* _getUObject() const override { return const_cast<USequencerSectionBP*>(this); }


#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUSequencerSectionBP(); \
	friend struct Z_Construct_UClass_USequencerSectionBP_Statics; \
public: \
	DECLARE_CLASS(USequencerSectionBP, UMovieSceneSection, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CustomizableSequencerTracks"), NO_API) \
	DECLARE_SERIALIZER(USequencerSectionBP) \
	virtual UObject* _getUObject() const override { return const_cast<USequencerSectionBP*>(this); }


#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USequencerSectionBP(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USequencerSectionBP) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USequencerSectionBP); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USequencerSectionBP); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USequencerSectionBP(USequencerSectionBP&&); \
	NO_API USequencerSectionBP(const USequencerSectionBP&); \
public:


#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USequencerSectionBP(USequencerSectionBP&&); \
	NO_API USequencerSectionBP(const USequencerSectionBP&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USequencerSectionBP); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USequencerSectionBP); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USequencerSectionBP)


#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_12_PROLOG
#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_SPARSE_DATA \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_INCLASS \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_SPARSE_DATA \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h_19_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CUSTOMIZABLESEQUENCERTRACKS_API UClass* StaticClass<class USequencerSectionBP>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerSectionBP_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
