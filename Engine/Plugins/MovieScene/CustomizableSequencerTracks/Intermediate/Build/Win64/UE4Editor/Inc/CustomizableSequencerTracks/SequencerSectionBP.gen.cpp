// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CustomizableSequencerTracks/Public/SequencerSectionBP.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSequencerSectionBP() {}
// Cross Module References
	CUSTOMIZABLESEQUENCERTRACKS_API UClass* Z_Construct_UClass_USequencerSectionBP_NoRegister();
	CUSTOMIZABLESEQUENCERTRACKS_API UClass* Z_Construct_UClass_USequencerSectionBP();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneSection();
	UPackage* Z_Construct_UPackage__Script_CustomizableSequencerTracks();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneEntityProvider_NoRegister();
// End Cross Module References
	void USequencerSectionBP::StaticRegisterNativesUSequencerSectionBP()
	{
	}
	UClass* Z_Construct_UClass_USequencerSectionBP_NoRegister()
	{
		return USequencerSectionBP::StaticClass();
	}
	struct Z_Construct_UClass_USequencerSectionBP_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USequencerSectionBP_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneSection,
		(UObject* (*)())Z_Construct_UPackage__Script_CustomizableSequencerTracks,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USequencerSectionBP_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayName", "SequencerSection" },
		{ "IncludePath", "SequencerSectionBP.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/SequencerSectionBP.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_USequencerSectionBP_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UMovieSceneEntityProvider_NoRegister, (int32)VTABLE_OFFSET(USequencerSectionBP, IMovieSceneEntityProvider), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_USequencerSectionBP_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USequencerSectionBP>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USequencerSectionBP_Statics::ClassParams = {
		&USequencerSectionBP::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x003000A1u,
		METADATA_PARAMS(Z_Construct_UClass_USequencerSectionBP_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USequencerSectionBP_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USequencerSectionBP()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USequencerSectionBP_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USequencerSectionBP, 1041530965);
	template<> CUSTOMIZABLESEQUENCERTRACKS_API UClass* StaticClass<USequencerSectionBP>()
	{
		return USequencerSectionBP::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USequencerSectionBP(Z_Construct_UClass_USequencerSectionBP, &USequencerSectionBP::StaticClass, TEXT("/Script/CustomizableSequencerTracks"), TEXT("USequencerSectionBP"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USequencerSectionBP);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
