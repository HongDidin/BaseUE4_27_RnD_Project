// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CUSTOMIZABLESEQUENCERTRACKS_SequencerTrackBP_generated_h
#error "SequencerTrackBP.generated.h already included, missing '#pragma once' in SequencerTrackBP.h"
#endif
#define CUSTOMIZABLESEQUENCERTRACKS_SequencerTrackBP_generated_h

#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_SPARSE_DATA
#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSequencerTrackBP(); \
	friend struct Z_Construct_UClass_USequencerTrackBP_Statics; \
public: \
	DECLARE_CLASS(USequencerTrackBP, UMovieSceneNameableTrack, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CustomizableSequencerTracks"), NO_API) \
	DECLARE_SERIALIZER(USequencerTrackBP)


#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUSequencerTrackBP(); \
	friend struct Z_Construct_UClass_USequencerTrackBP_Statics; \
public: \
	DECLARE_CLASS(USequencerTrackBP, UMovieSceneNameableTrack, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/CustomizableSequencerTracks"), NO_API) \
	DECLARE_SERIALIZER(USequencerTrackBP)


#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USequencerTrackBP(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USequencerTrackBP) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USequencerTrackBP); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USequencerTrackBP); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USequencerTrackBP(USequencerTrackBP&&); \
	NO_API USequencerTrackBP(const USequencerTrackBP&); \
public:


#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USequencerTrackBP(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USequencerTrackBP(USequencerTrackBP&&); \
	NO_API USequencerTrackBP(const USequencerTrackBP&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USequencerTrackBP); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USequencerTrackBP); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USequencerTrackBP)


#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Sections() { return STRUCT_OFFSET(USequencerTrackBP, Sections); }


#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_22_PROLOG
#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_SPARSE_DATA \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_INCLASS \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_SPARSE_DATA \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h_28_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CUSTOMIZABLESEQUENCERTRACKS_API UClass* StaticClass<class USequencerTrackBP>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_CustomizableSequencerTracks_Source_CustomizableSequencerTracks_Public_SequencerTrackBP_h


#define FOREACH_ENUM_ECUSTOMSEQUENCERTRACKTYPE(op) \
	op(ECustomSequencerTrackType::MasterTrack) \
	op(ECustomSequencerTrackType::ObjectTrack) 

enum class ECustomSequencerTrackType;
template<> CUSTOMIZABLESEQUENCERTRACKS_API UEnum* StaticEnum<ECustomSequencerTrackType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
