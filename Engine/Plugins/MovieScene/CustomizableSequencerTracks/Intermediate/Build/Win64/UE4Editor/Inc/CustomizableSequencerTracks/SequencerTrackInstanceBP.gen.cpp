// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CustomizableSequencerTracks/Public/SequencerTrackInstanceBP.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSequencerTrackInstanceBP() {}
// Cross Module References
	CUSTOMIZABLESEQUENCERTRACKS_API UScriptStruct* Z_Construct_UScriptStruct_FSequencerTrackInstanceInput();
	UPackage* Z_Construct_UPackage__Script_CustomizableSequencerTracks();
	CUSTOMIZABLESEQUENCERTRACKS_API UClass* Z_Construct_UClass_USequencerSectionBP_NoRegister();
	CUSTOMIZABLESEQUENCERTRACKS_API UClass* Z_Construct_UClass_USequencerTrackInstanceBP_NoRegister();
	CUSTOMIZABLESEQUENCERTRACKS_API UClass* Z_Construct_UClass_USequencerTrackInstanceBP();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneTrackInstance();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
class UScriptStruct* FSequencerTrackInstanceInput::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern CUSTOMIZABLESEQUENCERTRACKS_API uint32 Get_Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSequencerTrackInstanceInput, Z_Construct_UPackage__Script_CustomizableSequencerTracks(), TEXT("SequencerTrackInstanceInput"), sizeof(FSequencerTrackInstanceInput), Get_Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Hash());
	}
	return Singleton;
}
template<> CUSTOMIZABLESEQUENCERTRACKS_API UScriptStruct* StaticStruct<FSequencerTrackInstanceInput>()
{
	return FSequencerTrackInstanceInput::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSequencerTrackInstanceInput(FSequencerTrackInstanceInput::StaticStruct, TEXT("/Script/CustomizableSequencerTracks"), TEXT("SequencerTrackInstanceInput"), false, nullptr, nullptr);
static struct FScriptStruct_CustomizableSequencerTracks_StaticRegisterNativesFSequencerTrackInstanceInput
{
	FScriptStruct_CustomizableSequencerTracks_StaticRegisterNativesFSequencerTrackInstanceInput()
	{
		UScriptStruct::DeferCppStructOps<FSequencerTrackInstanceInput>(FName(TEXT("SequencerTrackInstanceInput")));
	}
} ScriptStruct_CustomizableSequencerTracks_StaticRegisterNativesFSequencerTrackInstanceInput;
	struct Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Section_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Section;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/SequencerTrackInstanceBP.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSequencerTrackInstanceInput>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Statics::NewProp_Section_MetaData[] = {
		{ "Category", "Sequencer" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/SequencerTrackInstanceBP.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Statics::NewProp_Section = { "Section", nullptr, (EPropertyFlags)0x001000000008001c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSequencerTrackInstanceInput, Section), Z_Construct_UClass_USequencerSectionBP_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Statics::NewProp_Section_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Statics::NewProp_Section_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Statics::NewProp_Section,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_CustomizableSequencerTracks,
		nullptr,
		&NewStructOps,
		"SequencerTrackInstanceInput",
		sizeof(FSequencerTrackInstanceInput),
		alignof(FSequencerTrackInstanceInput),
		Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000005),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSequencerTrackInstanceInput()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_CustomizableSequencerTracks();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SequencerTrackInstanceInput"), sizeof(FSequencerTrackInstanceInput), Get_Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSequencerTrackInstanceInput_Hash() { return 3489222096U; }
	DEFINE_FUNCTION(USequencerTrackInstanceBP::execGetInput)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_Index);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FSequencerTrackInstanceInput*)Z_Param__Result=P_THIS->GetInput(Z_Param_Index);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USequencerTrackInstanceBP::execGetNumInputs)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=P_THIS->GetNumInputs();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USequencerTrackInstanceBP::execGetInputs)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FSequencerTrackInstanceInput>*)Z_Param__Result=P_THIS->GetInputs();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USequencerTrackInstanceBP::execGetAnimatedObject)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UObject**)Z_Param__Result=P_THIS->GetAnimatedObject();
		P_NATIVE_END;
	}
	static FName NAME_USequencerTrackInstanceBP_K2_OnBeginUpdateInputs = FName(TEXT("K2_OnBeginUpdateInputs"));
	void USequencerTrackInstanceBP::K2_OnBeginUpdateInputs()
	{
		ProcessEvent(FindFunctionChecked(NAME_USequencerTrackInstanceBP_K2_OnBeginUpdateInputs),NULL);
	}
	static FName NAME_USequencerTrackInstanceBP_K2_OnDestroyed = FName(TEXT("K2_OnDestroyed"));
	void USequencerTrackInstanceBP::K2_OnDestroyed()
	{
		ProcessEvent(FindFunctionChecked(NAME_USequencerTrackInstanceBP_K2_OnDestroyed),NULL);
	}
	static FName NAME_USequencerTrackInstanceBP_K2_OnEndUpdateInputs = FName(TEXT("K2_OnEndUpdateInputs"));
	void USequencerTrackInstanceBP::K2_OnEndUpdateInputs()
	{
		ProcessEvent(FindFunctionChecked(NAME_USequencerTrackInstanceBP_K2_OnEndUpdateInputs),NULL);
	}
	static FName NAME_USequencerTrackInstanceBP_K2_OnInitialize = FName(TEXT("K2_OnInitialize"));
	void USequencerTrackInstanceBP::K2_OnInitialize()
	{
		ProcessEvent(FindFunctionChecked(NAME_USequencerTrackInstanceBP_K2_OnInitialize),NULL);
	}
	static FName NAME_USequencerTrackInstanceBP_K2_OnInputAdded = FName(TEXT("K2_OnInputAdded"));
	void USequencerTrackInstanceBP::K2_OnInputAdded(FSequencerTrackInstanceInput Input)
	{
		SequencerTrackInstanceBP_eventK2_OnInputAdded_Parms Parms;
		Parms.Input=Input;
		ProcessEvent(FindFunctionChecked(NAME_USequencerTrackInstanceBP_K2_OnInputAdded),&Parms);
	}
	static FName NAME_USequencerTrackInstanceBP_K2_OnInputRemoved = FName(TEXT("K2_OnInputRemoved"));
	void USequencerTrackInstanceBP::K2_OnInputRemoved(FSequencerTrackInstanceInput Input)
	{
		SequencerTrackInstanceBP_eventK2_OnInputRemoved_Parms Parms;
		Parms.Input=Input;
		ProcessEvent(FindFunctionChecked(NAME_USequencerTrackInstanceBP_K2_OnInputRemoved),&Parms);
	}
	static FName NAME_USequencerTrackInstanceBP_K2_OnUpdate = FName(TEXT("K2_OnUpdate"));
	void USequencerTrackInstanceBP::K2_OnUpdate()
	{
		ProcessEvent(FindFunctionChecked(NAME_USequencerTrackInstanceBP_K2_OnUpdate),NULL);
	}
	void USequencerTrackInstanceBP::StaticRegisterNativesUSequencerTrackInstanceBP()
	{
		UClass* Class = USequencerTrackInstanceBP::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetAnimatedObject", &USequencerTrackInstanceBP::execGetAnimatedObject },
			{ "GetInput", &USequencerTrackInstanceBP::execGetInput },
			{ "GetInputs", &USequencerTrackInstanceBP::execGetInputs },
			{ "GetNumInputs", &USequencerTrackInstanceBP::execGetNumInputs },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_USequencerTrackInstanceBP_GetAnimatedObject_Statics
	{
		struct SequencerTrackInstanceBP_eventGetAnimatedObject_Parms
		{
			UObject* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerTrackInstanceBP_GetAnimatedObject_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerTrackInstanceBP_eventGetAnimatedObject_Parms, ReturnValue), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerTrackInstanceBP_GetAnimatedObject_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerTrackInstanceBP_GetAnimatedObject_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerTrackInstanceBP_GetAnimatedObject_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sequencer" },
		{ "ModuleRelativePath", "Public/SequencerTrackInstanceBP.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerTrackInstanceBP_GetAnimatedObject_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerTrackInstanceBP, nullptr, "GetAnimatedObject", nullptr, nullptr, sizeof(SequencerTrackInstanceBP_eventGetAnimatedObject_Parms), Z_Construct_UFunction_USequencerTrackInstanceBP_GetAnimatedObject_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_GetAnimatedObject_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerTrackInstanceBP_GetAnimatedObject_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_GetAnimatedObject_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerTrackInstanceBP_GetAnimatedObject()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerTrackInstanceBP_GetAnimatedObject_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerTrackInstanceBP_GetInput_Statics
	{
		struct SequencerTrackInstanceBP_eventGetInput_Parms
		{
			int32 Index;
			FSequencerTrackInstanceInput ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_Index;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_USequencerTrackInstanceBP_GetInput_Statics::NewProp_Index = { "Index", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerTrackInstanceBP_eventGetInput_Parms, Index), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerTrackInstanceBP_GetInput_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010008000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerTrackInstanceBP_eventGetInput_Parms, ReturnValue), Z_Construct_UScriptStruct_FSequencerTrackInstanceInput, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerTrackInstanceBP_GetInput_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerTrackInstanceBP_GetInput_Statics::NewProp_Index,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerTrackInstanceBP_GetInput_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerTrackInstanceBP_GetInput_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sequencer" },
		{ "ModuleRelativePath", "Public/SequencerTrackInstanceBP.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerTrackInstanceBP_GetInput_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerTrackInstanceBP, nullptr, "GetInput", nullptr, nullptr, sizeof(SequencerTrackInstanceBP_eventGetInput_Parms), Z_Construct_UFunction_USequencerTrackInstanceBP_GetInput_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_GetInput_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x44020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerTrackInstanceBP_GetInput_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_GetInput_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerTrackInstanceBP_GetInput()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerTrackInstanceBP_GetInput_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerTrackInstanceBP_GetInputs_Statics
	{
		struct SequencerTrackInstanceBP_eventGetInputs_Parms
		{
			TArray<FSequencerTrackInstanceInput> ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerTrackInstanceBP_GetInputs_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000008000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSequencerTrackInstanceInput, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_USequencerTrackInstanceBP_GetInputs_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010008000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerTrackInstanceBP_eventGetInputs_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerTrackInstanceBP_GetInputs_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerTrackInstanceBP_GetInputs_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerTrackInstanceBP_GetInputs_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerTrackInstanceBP_GetInputs_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sequencer" },
		{ "ModuleRelativePath", "Public/SequencerTrackInstanceBP.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerTrackInstanceBP_GetInputs_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerTrackInstanceBP, nullptr, "GetInputs", nullptr, nullptr, sizeof(SequencerTrackInstanceBP_eventGetInputs_Parms), Z_Construct_UFunction_USequencerTrackInstanceBP_GetInputs_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_GetInputs_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerTrackInstanceBP_GetInputs_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_GetInputs_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerTrackInstanceBP_GetInputs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerTrackInstanceBP_GetInputs_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerTrackInstanceBP_GetNumInputs_Statics
	{
		struct SequencerTrackInstanceBP_eventGetNumInputs_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_USequencerTrackInstanceBP_GetNumInputs_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerTrackInstanceBP_eventGetNumInputs_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerTrackInstanceBP_GetNumInputs_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerTrackInstanceBP_GetNumInputs_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerTrackInstanceBP_GetNumInputs_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sequencer" },
		{ "ModuleRelativePath", "Public/SequencerTrackInstanceBP.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerTrackInstanceBP_GetNumInputs_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerTrackInstanceBP, nullptr, "GetNumInputs", nullptr, nullptr, sizeof(SequencerTrackInstanceBP_eventGetNumInputs_Parms), Z_Construct_UFunction_USequencerTrackInstanceBP_GetNumInputs_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_GetNumInputs_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerTrackInstanceBP_GetNumInputs_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_GetNumInputs_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerTrackInstanceBP_GetNumInputs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerTrackInstanceBP_GetNumInputs_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnBeginUpdateInputs_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnBeginUpdateInputs_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "TRUE" },
		{ "Category", "Sequencer" },
		{ "DisplayName", "OnBeginUpdateInputs" },
		{ "ModuleRelativePath", "Public/SequencerTrackInstanceBP.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnBeginUpdateInputs_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerTrackInstanceBP, nullptr, "K2_OnBeginUpdateInputs", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnBeginUpdateInputs_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnBeginUpdateInputs_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnBeginUpdateInputs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnBeginUpdateInputs_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnDestroyed_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnDestroyed_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "TRUE" },
		{ "Category", "Sequencer" },
		{ "DisplayName", "OnDestroyed" },
		{ "ModuleRelativePath", "Public/SequencerTrackInstanceBP.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnDestroyed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerTrackInstanceBP, nullptr, "K2_OnDestroyed", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnDestroyed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnDestroyed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnDestroyed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnDestroyed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnEndUpdateInputs_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnEndUpdateInputs_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "TRUE" },
		{ "Category", "Sequencer" },
		{ "DisplayName", "OnEndUpdateInputs" },
		{ "ModuleRelativePath", "Public/SequencerTrackInstanceBP.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnEndUpdateInputs_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerTrackInstanceBP, nullptr, "K2_OnEndUpdateInputs", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnEndUpdateInputs_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnEndUpdateInputs_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnEndUpdateInputs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnEndUpdateInputs_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInitialize_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInitialize_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "TRUE" },
		{ "Category", "Sequencer" },
		{ "Comment", "/*~ Implementable interface */" },
		{ "DisplayName", "OnInitialize" },
		{ "ModuleRelativePath", "Public/SequencerTrackInstanceBP.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInitialize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerTrackInstanceBP, nullptr, "K2_OnInitialize", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInitialize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInitialize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInitialize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInitialize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputAdded_Statics
	{
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Input;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputAdded_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010008000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerTrackInstanceBP_eventK2_OnInputAdded_Parms, Input), Z_Construct_UScriptStruct_FSequencerTrackInstanceInput, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputAdded_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputAdded_Statics::NewProp_Input,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputAdded_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "TRUE" },
		{ "Category", "Sequencer" },
		{ "DisplayName", "OnInputAdded" },
		{ "ModuleRelativePath", "Public/SequencerTrackInstanceBP.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputAdded_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerTrackInstanceBP, nullptr, "K2_OnInputAdded", nullptr, nullptr, sizeof(SequencerTrackInstanceBP_eventK2_OnInputAdded_Parms), Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputAdded_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputAdded_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputAdded_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputAdded_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputAdded()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputAdded_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputRemoved_Statics
	{
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Input;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputRemoved_Statics::NewProp_Input = { "Input", nullptr, (EPropertyFlags)0x0010008000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerTrackInstanceBP_eventK2_OnInputRemoved_Parms, Input), Z_Construct_UScriptStruct_FSequencerTrackInstanceInput, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputRemoved_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputRemoved_Statics::NewProp_Input,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputRemoved_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "TRUE" },
		{ "Category", "Sequencer" },
		{ "DisplayName", "OnInputRemoved" },
		{ "ModuleRelativePath", "Public/SequencerTrackInstanceBP.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputRemoved_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerTrackInstanceBP, nullptr, "K2_OnInputRemoved", nullptr, nullptr, sizeof(SequencerTrackInstanceBP_eventK2_OnInputRemoved_Parms), Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputRemoved_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputRemoved_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputRemoved_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputRemoved_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputRemoved()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputRemoved_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnUpdate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnUpdate_Statics::Function_MetaDataParams[] = {
		{ "CallInEditor", "TRUE" },
		{ "Category", "Sequencer" },
		{ "DisplayName", "OnUpdate" },
		{ "ModuleRelativePath", "Public/SequencerTrackInstanceBP.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnUpdate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerTrackInstanceBP, nullptr, "K2_OnUpdate", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020800, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnUpdate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnUpdate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnUpdate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnUpdate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_USequencerTrackInstanceBP_NoRegister()
	{
		return USequencerTrackInstanceBP::StaticClass();
	}
	struct Z_Construct_UClass_USequencerTrackInstanceBP_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USequencerTrackInstanceBP_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMovieSceneTrackInstance,
		(UObject* (*)())Z_Construct_UPackage__Script_CustomizableSequencerTracks,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_USequencerTrackInstanceBP_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_USequencerTrackInstanceBP_GetAnimatedObject, "GetAnimatedObject" }, // 2752674391
		{ &Z_Construct_UFunction_USequencerTrackInstanceBP_GetInput, "GetInput" }, // 3924980110
		{ &Z_Construct_UFunction_USequencerTrackInstanceBP_GetInputs, "GetInputs" }, // 1686985348
		{ &Z_Construct_UFunction_USequencerTrackInstanceBP_GetNumInputs, "GetNumInputs" }, // 4240517908
		{ &Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnBeginUpdateInputs, "K2_OnBeginUpdateInputs" }, // 2041056033
		{ &Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnDestroyed, "K2_OnDestroyed" }, // 3128927714
		{ &Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnEndUpdateInputs, "K2_OnEndUpdateInputs" }, // 310075072
		{ &Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInitialize, "K2_OnInitialize" }, // 3466261899
		{ &Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputAdded, "K2_OnInputAdded" }, // 1789553925
		{ &Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnInputRemoved, "K2_OnInputRemoved" }, // 3278245764
		{ &Z_Construct_UFunction_USequencerTrackInstanceBP_K2_OnUpdate, "K2_OnUpdate" }, // 3063047695
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USequencerTrackInstanceBP_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayName", "SequencerTrackInstance" },
		{ "IncludePath", "SequencerTrackInstanceBP.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/SequencerTrackInstanceBP.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USequencerTrackInstanceBP_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USequencerTrackInstanceBP>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USequencerTrackInstanceBP_Statics::ClassParams = {
		&USequencerTrackInstanceBP::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x009000A9u,
		METADATA_PARAMS(Z_Construct_UClass_USequencerTrackInstanceBP_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USequencerTrackInstanceBP_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USequencerTrackInstanceBP()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USequencerTrackInstanceBP_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USequencerTrackInstanceBP, 4084849507);
	template<> CUSTOMIZABLESEQUENCERTRACKS_API UClass* StaticClass<USequencerTrackInstanceBP>()
	{
		return USequencerTrackInstanceBP::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USequencerTrackInstanceBP(Z_Construct_UClass_USequencerTrackInstanceBP, &USequencerTrackInstanceBP::StaticClass, TEXT("/Script/CustomizableSequencerTracks"), TEXT("USequencerTrackInstanceBP"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USequencerTrackInstanceBP);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
