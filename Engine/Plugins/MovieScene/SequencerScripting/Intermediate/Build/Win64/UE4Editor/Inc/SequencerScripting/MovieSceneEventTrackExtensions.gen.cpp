// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SequencerScripting/Public/ExtensionLibraries/MovieSceneEventTrackExtensions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneEventTrackExtensions() {}
// Cross Module References
	SEQUENCERSCRIPTING_API UClass* Z_Construct_UClass_UMovieSceneEventTrackExtensions_NoRegister();
	SEQUENCERSCRIPTING_API UClass* Z_Construct_UClass_UMovieSceneEventTrackExtensions();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_SequencerScripting();
	MOVIESCENETRACKS_API UClass* Z_Construct_UClass_UMovieSceneEventTrack_NoRegister();
	MOVIESCENETRACKS_API UClass* Z_Construct_UClass_UMovieSceneEventRepeaterSection_NoRegister();
	MOVIESCENETRACKS_API UClass* Z_Construct_UClass_UMovieSceneEventTriggerSection_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UMovieSceneEventTrackExtensions::execAddEventTriggerSection)
	{
		P_GET_OBJECT(UMovieSceneEventTrack,Z_Param_InTrack);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMovieSceneEventTriggerSection**)Z_Param__Result=UMovieSceneEventTrackExtensions::AddEventTriggerSection(Z_Param_InTrack);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMovieSceneEventTrackExtensions::execAddEventRepeaterSection)
	{
		P_GET_OBJECT(UMovieSceneEventTrack,Z_Param_InTrack);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMovieSceneEventRepeaterSection**)Z_Param__Result=UMovieSceneEventTrackExtensions::AddEventRepeaterSection(Z_Param_InTrack);
		P_NATIVE_END;
	}
	void UMovieSceneEventTrackExtensions::StaticRegisterNativesUMovieSceneEventTrackExtensions()
	{
		UClass* Class = UMovieSceneEventTrackExtensions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AddEventRepeaterSection", &UMovieSceneEventTrackExtensions::execAddEventRepeaterSection },
			{ "AddEventTriggerSection", &UMovieSceneEventTrackExtensions::execAddEventTriggerSection },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics
	{
		struct MovieSceneEventTrackExtensions_eventAddEventRepeaterSection_Parms
		{
			UMovieSceneEventTrack* InTrack;
			UMovieSceneEventRepeaterSection* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InTrack_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InTrack;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::NewProp_InTrack_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::NewProp_InTrack = { "InTrack", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneEventTrackExtensions_eventAddEventRepeaterSection_Parms, InTrack), Z_Construct_UClass_UMovieSceneEventTrack_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::NewProp_InTrack_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::NewProp_InTrack_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneEventTrackExtensions_eventAddEventRepeaterSection_Parms, ReturnValue), Z_Construct_UClass_UMovieSceneEventRepeaterSection_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::NewProp_InTrack,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sequencer|Track" },
		{ "Comment", "/**\n\x09 * Create a new event repeater section for the given track\n\x09 *\n\x09 * @param Track        The event track to add the new event repeater section to\n\x09 * @return The newly created event repeater section\n\x09 */" },
		{ "ModuleRelativePath", "Public/ExtensionLibraries/MovieSceneEventTrackExtensions.h" },
		{ "ScriptMethod", "" },
		{ "ToolTip", "Create a new event repeater section for the given track\n\n@param Track        The event track to add the new event repeater section to\n@return The newly created event repeater section" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMovieSceneEventTrackExtensions, nullptr, "AddEventRepeaterSection", nullptr, nullptr, sizeof(MovieSceneEventTrackExtensions_eventAddEventRepeaterSection_Parms), Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics
	{
		struct MovieSceneEventTrackExtensions_eventAddEventTriggerSection_Parms
		{
			UMovieSceneEventTrack* InTrack;
			UMovieSceneEventTriggerSection* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InTrack_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InTrack;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::NewProp_InTrack_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::NewProp_InTrack = { "InTrack", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneEventTrackExtensions_eventAddEventTriggerSection_Parms, InTrack), Z_Construct_UClass_UMovieSceneEventTrack_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::NewProp_InTrack_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::NewProp_InTrack_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneEventTrackExtensions_eventAddEventTriggerSection_Parms, ReturnValue), Z_Construct_UClass_UMovieSceneEventTriggerSection_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::NewProp_InTrack,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sequencer|Track" },
		{ "Comment", "/**\n\x09 * Create a new event trigger section for the given track\n\x09 *\n\x09 * @param Track        The event track to add the new event trigger section to\n\x09 * @return The newly created event trigger section\n\x09 */" },
		{ "ModuleRelativePath", "Public/ExtensionLibraries/MovieSceneEventTrackExtensions.h" },
		{ "ScriptMethod", "" },
		{ "ToolTip", "Create a new event trigger section for the given track\n\n@param Track        The event track to add the new event trigger section to\n@return The newly created event trigger section" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMovieSceneEventTrackExtensions, nullptr, "AddEventTriggerSection", nullptr, nullptr, sizeof(MovieSceneEventTrackExtensions_eventAddEventTriggerSection_Parms), Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMovieSceneEventTrackExtensions_NoRegister()
	{
		return UMovieSceneEventTrackExtensions::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneEventTrackExtensions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneEventTrackExtensions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_SequencerScripting,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMovieSceneEventTrackExtensions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventRepeaterSection, "AddEventRepeaterSection" }, // 123742382
		{ &Z_Construct_UFunction_UMovieSceneEventTrackExtensions_AddEventTriggerSection, "AddEventTriggerSection" }, // 4055328371
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneEventTrackExtensions_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Function library containing methods that should be hoisted onto UMovieSceneEventTrack for scripting\n */" },
		{ "IncludePath", "ExtensionLibraries/MovieSceneEventTrackExtensions.h" },
		{ "ModuleRelativePath", "Public/ExtensionLibraries/MovieSceneEventTrackExtensions.h" },
		{ "ToolTip", "Function library containing methods that should be hoisted onto UMovieSceneEventTrack for scripting" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneEventTrackExtensions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneEventTrackExtensions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneEventTrackExtensions_Statics::ClassParams = {
		&UMovieSceneEventTrackExtensions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneEventTrackExtensions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneEventTrackExtensions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneEventTrackExtensions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneEventTrackExtensions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneEventTrackExtensions, 3860431981);
	template<> SEQUENCERSCRIPTING_API UClass* StaticClass<UMovieSceneEventTrackExtensions>()
	{
		return UMovieSceneEventTrackExtensions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneEventTrackExtensions(Z_Construct_UClass_UMovieSceneEventTrackExtensions, &UMovieSceneEventTrackExtensions::StaticClass, TEXT("/Script/SequencerScripting"), TEXT("UMovieSceneEventTrackExtensions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneEventTrackExtensions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
