// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SequencerScripting/Public/ExtensionLibraries/MovieSceneVectorTrackExtensions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieSceneVectorTrackExtensions() {}
// Cross Module References
	SEQUENCERSCRIPTING_API UClass* Z_Construct_UClass_UMovieSceneVectorTrackExtensions_NoRegister();
	SEQUENCERSCRIPTING_API UClass* Z_Construct_UClass_UMovieSceneVectorTrackExtensions();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_SequencerScripting();
	MOVIESCENETRACKS_API UClass* Z_Construct_UClass_UMovieSceneVectorTrack_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UMovieSceneVectorTrackExtensions::execGetNumChannelsUsed)
	{
		P_GET_OBJECT(UMovieSceneVectorTrack,Z_Param_Track);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=UMovieSceneVectorTrackExtensions::GetNumChannelsUsed(Z_Param_Track);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMovieSceneVectorTrackExtensions::execSetNumChannelsUsed)
	{
		P_GET_OBJECT(UMovieSceneVectorTrack,Z_Param_Track);
		P_GET_PROPERTY(FIntProperty,Z_Param_InNumChannelsUsed);
		P_FINISH;
		P_NATIVE_BEGIN;
		UMovieSceneVectorTrackExtensions::SetNumChannelsUsed(Z_Param_Track,Z_Param_InNumChannelsUsed);
		P_NATIVE_END;
	}
	void UMovieSceneVectorTrackExtensions::StaticRegisterNativesUMovieSceneVectorTrackExtensions()
	{
		UClass* Class = UMovieSceneVectorTrackExtensions::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetNumChannelsUsed", &UMovieSceneVectorTrackExtensions::execGetNumChannelsUsed },
			{ "SetNumChannelsUsed", &UMovieSceneVectorTrackExtensions::execSetNumChannelsUsed },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed_Statics
	{
		struct MovieSceneVectorTrackExtensions_eventGetNumChannelsUsed_Parms
		{
			UMovieSceneVectorTrack* Track;
			int32 ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Track_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Track;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed_Statics::NewProp_Track_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed_Statics::NewProp_Track = { "Track", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneVectorTrackExtensions_eventGetNumChannelsUsed_Parms, Track), Z_Construct_UClass_UMovieSceneVectorTrack_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed_Statics::NewProp_Track_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed_Statics::NewProp_Track_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneVectorTrackExtensions_eventGetNumChannelsUsed_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed_Statics::NewProp_Track,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sequencer|Track" },
		{ "Comment", "/**\n\x09 * Get the number of channels used for this track\n\x09 *\n\x09 * @param Track        The track to query for the number of channels used\n\x09 * @return The number of channels used for this track\n\x09 */" },
		{ "ModuleRelativePath", "Public/ExtensionLibraries/MovieSceneVectorTrackExtensions.h" },
		{ "ScriptMethod", "" },
		{ "ToolTip", "Get the number of channels used for this track\n\n@param Track        The track to query for the number of channels used\n@return The number of channels used for this track" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMovieSceneVectorTrackExtensions, nullptr, "GetNumChannelsUsed", nullptr, nullptr, sizeof(MovieSceneVectorTrackExtensions_eventGetNumChannelsUsed_Parms), Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed_Statics
	{
		struct MovieSceneVectorTrackExtensions_eventSetNumChannelsUsed_Parms
		{
			UMovieSceneVectorTrack* Track;
			int32 InNumChannelsUsed;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Track_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Track;
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InNumChannelsUsed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed_Statics::NewProp_Track_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed_Statics::NewProp_Track = { "Track", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneVectorTrackExtensions_eventSetNumChannelsUsed_Parms, Track), Z_Construct_UClass_UMovieSceneVectorTrack_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed_Statics::NewProp_Track_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed_Statics::NewProp_Track_MetaData)) };
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed_Statics::NewProp_InNumChannelsUsed = { "InNumChannelsUsed", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MovieSceneVectorTrackExtensions_eventSetNumChannelsUsed_Parms, InNumChannelsUsed), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed_Statics::NewProp_Track,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed_Statics::NewProp_InNumChannelsUsed,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Sequencer|Track" },
		{ "Comment", "/**\n\x09 * Set the number of channels used for this track\n\x09 *\n\x09 * @param Track        The track to set\n\x09 * @param InNumChannelsUsed The number of channels to use for this track\n\x09 */" },
		{ "ModuleRelativePath", "Public/ExtensionLibraries/MovieSceneVectorTrackExtensions.h" },
		{ "ScriptMethod", "" },
		{ "ToolTip", "Set the number of channels used for this track\n\n@param Track        The track to set\n@param InNumChannelsUsed The number of channels to use for this track" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMovieSceneVectorTrackExtensions, nullptr, "SetNumChannelsUsed", nullptr, nullptr, sizeof(MovieSceneVectorTrackExtensions_eventSetNumChannelsUsed_Parms), Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMovieSceneVectorTrackExtensions_NoRegister()
	{
		return UMovieSceneVectorTrackExtensions::StaticClass();
	}
	struct Z_Construct_UClass_UMovieSceneVectorTrackExtensions_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMovieSceneVectorTrackExtensions_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_SequencerScripting,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMovieSceneVectorTrackExtensions_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_GetNumChannelsUsed, "GetNumChannelsUsed" }, // 1162434535
		{ &Z_Construct_UFunction_UMovieSceneVectorTrackExtensions_SetNumChannelsUsed, "SetNumChannelsUsed" }, // 2444705173
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMovieSceneVectorTrackExtensions_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Function library containing methods that should be hoisted onto UMovieSceneVectorTrack for scripting\n */" },
		{ "IncludePath", "ExtensionLibraries/MovieSceneVectorTrackExtensions.h" },
		{ "ModuleRelativePath", "Public/ExtensionLibraries/MovieSceneVectorTrackExtensions.h" },
		{ "ToolTip", "Function library containing methods that should be hoisted onto UMovieSceneVectorTrack for scripting" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMovieSceneVectorTrackExtensions_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMovieSceneVectorTrackExtensions>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMovieSceneVectorTrackExtensions_Statics::ClassParams = {
		&UMovieSceneVectorTrackExtensions::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMovieSceneVectorTrackExtensions_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMovieSceneVectorTrackExtensions_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMovieSceneVectorTrackExtensions()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMovieSceneVectorTrackExtensions_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMovieSceneVectorTrackExtensions, 3287566080);
	template<> SEQUENCERSCRIPTING_API UClass* StaticClass<UMovieSceneVectorTrackExtensions>()
	{
		return UMovieSceneVectorTrackExtensions::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMovieSceneVectorTrackExtensions(Z_Construct_UClass_UMovieSceneVectorTrackExtensions, &UMovieSceneVectorTrackExtensions::StaticClass, TEXT("/Script/SequencerScripting"), TEXT("UMovieSceneVectorTrackExtensions"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMovieSceneVectorTrackExtensions);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
