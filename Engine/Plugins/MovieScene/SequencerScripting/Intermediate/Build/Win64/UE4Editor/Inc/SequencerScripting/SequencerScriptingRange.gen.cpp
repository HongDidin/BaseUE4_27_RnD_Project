// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SequencerScripting/Public/SequencerScriptingRange.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSequencerScriptingRange() {}
// Cross Module References
	SEQUENCERSCRIPTING_API UScriptStruct* Z_Construct_UScriptStruct_FSequencerScriptingRange();
	UPackage* Z_Construct_UPackage__Script_SequencerScripting();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FFrameRate();
// End Cross Module References
class UScriptStruct* FSequencerScriptingRange::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern SEQUENCERSCRIPTING_API uint32 Get_Z_Construct_UScriptStruct_FSequencerScriptingRange_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSequencerScriptingRange, Z_Construct_UPackage__Script_SequencerScripting(), TEXT("SequencerScriptingRange"), sizeof(FSequencerScriptingRange), Get_Z_Construct_UScriptStruct_FSequencerScriptingRange_Hash());
	}
	return Singleton;
}
template<> SEQUENCERSCRIPTING_API UScriptStruct* StaticStruct<FSequencerScriptingRange>()
{
	return FSequencerScriptingRange::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSequencerScriptingRange(FSequencerScriptingRange::StaticStruct, TEXT("/Script/SequencerScripting"), TEXT("SequencerScriptingRange"), false, nullptr, nullptr);
static struct FScriptStruct_SequencerScripting_StaticRegisterNativesFSequencerScriptingRange
{
	FScriptStruct_SequencerScripting_StaticRegisterNativesFSequencerScriptingRange()
	{
		UScriptStruct::DeferCppStructOps<FSequencerScriptingRange>(FName(TEXT("SequencerScriptingRange")));
	}
} ScriptStruct_SequencerScripting_StaticRegisterNativesFSequencerScriptingRange;
	struct Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHasStart_MetaData[];
#endif
		static void NewProp_bHasStart_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHasStart;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bHasEnd_MetaData[];
#endif
		static void NewProp_bHasEnd_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bHasEnd;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InclusiveStart_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InclusiveStart;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExclusiveEnd_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ExclusiveEnd;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InternalRate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InternalRate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/SequencerScriptingRange.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSequencerScriptingRange>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_bHasStart_MetaData[] = {
		{ "Category", "Range" },
		{ "ModuleRelativePath", "Public/SequencerScriptingRange.h" },
		{ "ScriptName", "HasStartValue" },
	};
#endif
	void Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_bHasStart_SetBit(void* Obj)
	{
		((FSequencerScriptingRange*)Obj)->bHasStart = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_bHasStart = { "bHasStart", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FSequencerScriptingRange), &Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_bHasStart_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_bHasStart_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_bHasStart_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_bHasEnd_MetaData[] = {
		{ "Category", "Range" },
		{ "ModuleRelativePath", "Public/SequencerScriptingRange.h" },
		{ "ScriptName", "HasEndValue" },
	};
#endif
	void Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_bHasEnd_SetBit(void* Obj)
	{
		((FSequencerScriptingRange*)Obj)->bHasEnd = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_bHasEnd = { "bHasEnd", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(FSequencerScriptingRange), &Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_bHasEnd_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_bHasEnd_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_bHasEnd_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_InclusiveStart_MetaData[] = {
		{ "Category", "Range" },
		{ "ModuleRelativePath", "Public/SequencerScriptingRange.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_InclusiveStart = { "InclusiveStart", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSequencerScriptingRange, InclusiveStart), METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_InclusiveStart_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_InclusiveStart_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_ExclusiveEnd_MetaData[] = {
		{ "Category", "Range" },
		{ "ModuleRelativePath", "Public/SequencerScriptingRange.h" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_ExclusiveEnd = { "ExclusiveEnd", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSequencerScriptingRange, ExclusiveEnd), METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_ExclusiveEnd_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_ExclusiveEnd_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_InternalRate_MetaData[] = {
		{ "ModuleRelativePath", "Public/SequencerScriptingRange.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_InternalRate = { "InternalRate", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSequencerScriptingRange, InternalRate), Z_Construct_UScriptStruct_FFrameRate, METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_InternalRate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_InternalRate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_bHasStart,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_bHasEnd,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_InclusiveStart,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_ExclusiveEnd,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::NewProp_InternalRate,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_SequencerScripting,
		nullptr,
		&NewStructOps,
		"SequencerScriptingRange",
		sizeof(FSequencerScriptingRange),
		alignof(FSequencerScriptingRange),
		Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSequencerScriptingRange()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSequencerScriptingRange_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_SequencerScripting();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SequencerScriptingRange"), sizeof(FSequencerScriptingRange), Get_Z_Construct_UScriptStruct_FSequencerScriptingRange_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSequencerScriptingRange_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSequencerScriptingRange_Hash() { return 1251944868U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
