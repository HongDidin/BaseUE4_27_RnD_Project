// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SequencerScripting/Public/SequenceTimeUnit.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSequenceTimeUnit() {}
// Cross Module References
	SEQUENCERSCRIPTING_API UEnum* Z_Construct_UEnum_SequencerScripting_ESequenceTimeUnit();
	UPackage* Z_Construct_UPackage__Script_SequencerScripting();
// End Cross Module References
	static UEnum* ESequenceTimeUnit_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_SequencerScripting_ESequenceTimeUnit, Z_Construct_UPackage__Script_SequencerScripting(), TEXT("ESequenceTimeUnit"));
		}
		return Singleton;
	}
	template<> SEQUENCERSCRIPTING_API UEnum* StaticEnum<ESequenceTimeUnit>()
	{
		return ESequenceTimeUnit_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESequenceTimeUnit(ESequenceTimeUnit_StaticEnum, TEXT("/Script/SequencerScripting"), TEXT("ESequenceTimeUnit"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_SequencerScripting_ESequenceTimeUnit_Hash() { return 2206103352U; }
	UEnum* Z_Construct_UEnum_SequencerScripting_ESequenceTimeUnit()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_SequencerScripting();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESequenceTimeUnit"), 0, Get_Z_Construct_UEnum_SequencerScripting_ESequenceTimeUnit_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESequenceTimeUnit::DisplayRate", (int64)ESequenceTimeUnit::DisplayRate },
				{ "ESequenceTimeUnit::TickResolution", (int64)ESequenceTimeUnit::TickResolution },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/**\n* Specifies which frame of reference you want to set/get time values in. This allows users to work\n* in reference space without having to manually convert back and forth all of the time.\n*/" },
				{ "DisplayRate.Comment", "/** Display Rate matches the values shown in the UI such as 30fps giving you 30 frames per second. Supports sub-frame values (precision limited to Tick Resolution) */" },
				{ "DisplayRate.Name", "ESequenceTimeUnit::DisplayRate" },
				{ "DisplayRate.ToolTip", "Display Rate matches the values shown in the UI such as 30fps giving you 30 frames per second. Supports sub-frame values (precision limited to Tick Resolution)" },
				{ "ModuleRelativePath", "Public/SequenceTimeUnit.h" },
				{ "TickResolution.Comment", "/** Tick Resolution is the internal resolution that data is actually stored in, such as 24000 giving you 24,000 frames per second. This is the smallest interval that data can be stored on. */" },
				{ "TickResolution.Name", "ESequenceTimeUnit::TickResolution" },
				{ "TickResolution.ToolTip", "Tick Resolution is the internal resolution that data is actually stored in, such as 24000 giving you 24,000 frames per second. This is the smallest interval that data can be stored on." },
				{ "ToolTip", "Specifies which frame of reference you want to set/get time values in. This allows users to work\nin reference space without having to manually convert back and forth all of the time." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_SequencerScripting,
				nullptr,
				"ESequenceTimeUnit",
				"ESequenceTimeUnit",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
