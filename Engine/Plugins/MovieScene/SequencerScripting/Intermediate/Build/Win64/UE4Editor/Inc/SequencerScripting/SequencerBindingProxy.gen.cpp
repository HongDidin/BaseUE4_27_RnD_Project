// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SequencerScripting/Public/SequencerBindingProxy.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSequencerBindingProxy() {}
// Cross Module References
	SEQUENCERSCRIPTING_API UScriptStruct* Z_Construct_UScriptStruct_FSequencerBindingProxy();
	UPackage* Z_Construct_UPackage__Script_SequencerScripting();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneSequence_NoRegister();
// End Cross Module References
class UScriptStruct* FSequencerBindingProxy::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern SEQUENCERSCRIPTING_API uint32 Get_Z_Construct_UScriptStruct_FSequencerBindingProxy_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSequencerBindingProxy, Z_Construct_UPackage__Script_SequencerScripting(), TEXT("SequencerBindingProxy"), sizeof(FSequencerBindingProxy), Get_Z_Construct_UScriptStruct_FSequencerBindingProxy_Hash());
	}
	return Singleton;
}
template<> SEQUENCERSCRIPTING_API UScriptStruct* StaticStruct<FSequencerBindingProxy>()
{
	return FSequencerBindingProxy::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSequencerBindingProxy(FSequencerBindingProxy::StaticStruct, TEXT("/Script/SequencerScripting"), TEXT("SequencerBindingProxy"), false, nullptr, nullptr);
static struct FScriptStruct_SequencerScripting_StaticRegisterNativesFSequencerBindingProxy
{
	FScriptStruct_SequencerScripting_StaticRegisterNativesFSequencerBindingProxy()
	{
		UScriptStruct::DeferCppStructOps<FSequencerBindingProxy>(FName(TEXT("SequencerBindingProxy")));
	}
} ScriptStruct_SequencerScripting_StaticRegisterNativesFSequencerBindingProxy;
	struct Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BindingID_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BindingID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sequence_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Sequence;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/SequencerBindingProxy.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSequencerBindingProxy>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::NewProp_BindingID_MetaData[] = {
		{ "ModuleRelativePath", "Public/SequencerBindingProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::NewProp_BindingID = { "BindingID", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSequencerBindingProxy, BindingID), Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::NewProp_BindingID_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::NewProp_BindingID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::NewProp_Sequence_MetaData[] = {
		{ "Category", "Binding" },
		{ "ModuleRelativePath", "Public/SequencerBindingProxy.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::NewProp_Sequence = { "Sequence", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSequencerBindingProxy, Sequence), Z_Construct_UClass_UMovieSceneSequence_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::NewProp_Sequence_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::NewProp_Sequence_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::NewProp_BindingID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::NewProp_Sequence,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_SequencerScripting,
		nullptr,
		&NewStructOps,
		"SequencerBindingProxy",
		sizeof(FSequencerBindingProxy),
		alignof(FSequencerBindingProxy),
		Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSequencerBindingProxy()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSequencerBindingProxy_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_SequencerScripting();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SequencerBindingProxy"), sizeof(FSequencerBindingProxy), Get_Z_Construct_UScriptStruct_FSequencerBindingProxy_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSequencerBindingProxy_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSequencerBindingProxy_Hash() { return 880164344U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
