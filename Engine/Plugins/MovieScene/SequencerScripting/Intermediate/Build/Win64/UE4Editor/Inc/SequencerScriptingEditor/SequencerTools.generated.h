// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UWorld;
class ULevelSequence;
class UMovieSceneUserImportFBXControlRigSettings;
class UMovieSceneSequence;
class UObject;
struct FSequencerQuickBindingResult;
class UMovieSceneEventSectionBase;
struct FMovieSceneEvent;
class UTemplateSequence;
struct FSequencerBindingProxy;
class UMovieSceneUserImportFBXSettings;
class UAnimSequence;
class UAnimSeqExportOption;
class UFbxExportOption;
struct FSequencerScriptingRange;
struct FSequencerBoundObjects;
class UMovieSceneCapture;
#ifdef SEQUENCERSCRIPTINGEDITOR_SequencerTools_generated_h
#error "SequencerTools.generated.h already included, missing '#pragma once' in SequencerTools.h"
#endif
#define SEQUENCERSCRIPTINGEDITOR_SequencerTools_generated_h

#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_52_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics; \
	SEQUENCERSCRIPTINGEDITOR_API static class UScriptStruct* StaticStruct();


template<> SEQUENCERSCRIPTINGEDITOR_API UScriptStruct* StaticStruct<struct FSequencerQuickBindingResult>();

#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_31_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics; \
	static class UScriptStruct* StaticStruct();


template<> SEQUENCERSCRIPTINGEDITOR_API UScriptStruct* StaticStruct<struct FSequencerBoundObjects>();

#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_26_DELEGATE \
struct _Script_SequencerScriptingEditor_eventOnRenderMovieStopped_Parms \
{ \
	bool bSuccess; \
}; \
static inline void FOnRenderMovieStopped_DelegateWrapper(const FScriptDelegate& OnRenderMovieStopped, bool bSuccess) \
{ \
	_Script_SequencerScriptingEditor_eventOnRenderMovieStopped_Parms Parms; \
	Parms.bSuccess=bSuccess ? true : false; \
	OnRenderMovieStopped.ProcessDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_SPARSE_DATA
#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execImportFBXToControlRig); \
	DECLARE_FUNCTION(execCreateQuickBinding); \
	DECLARE_FUNCTION(execIsEventEndpointValid); \
	DECLARE_FUNCTION(execCreateEvent); \
	DECLARE_FUNCTION(execImportTemplateSequenceFBX); \
	DECLARE_FUNCTION(execImportLevelSequenceFBX); \
	DECLARE_FUNCTION(execExportAnimSequence); \
	DECLARE_FUNCTION(execExportTemplateSequenceFBX); \
	DECLARE_FUNCTION(execExportLevelSequenceFBX); \
	DECLARE_FUNCTION(execGetObjectBindings); \
	DECLARE_FUNCTION(execGetBoundObjects); \
	DECLARE_FUNCTION(execCancelMovieRender); \
	DECLARE_FUNCTION(execIsRenderingMovie); \
	DECLARE_FUNCTION(execRenderMovie);


#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execImportFBXToControlRig); \
	DECLARE_FUNCTION(execCreateQuickBinding); \
	DECLARE_FUNCTION(execIsEventEndpointValid); \
	DECLARE_FUNCTION(execCreateEvent); \
	DECLARE_FUNCTION(execImportTemplateSequenceFBX); \
	DECLARE_FUNCTION(execImportLevelSequenceFBX); \
	DECLARE_FUNCTION(execExportAnimSequence); \
	DECLARE_FUNCTION(execExportTemplateSequenceFBX); \
	DECLARE_FUNCTION(execExportLevelSequenceFBX); \
	DECLARE_FUNCTION(execGetObjectBindings); \
	DECLARE_FUNCTION(execGetBoundObjects); \
	DECLARE_FUNCTION(execCancelMovieRender); \
	DECLARE_FUNCTION(execIsRenderingMovie); \
	DECLARE_FUNCTION(execRenderMovie);


#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSequencerToolsFunctionLibrary(); \
	friend struct Z_Construct_UClass_USequencerToolsFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(USequencerToolsFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/SequencerScriptingEditor"), NO_API) \
	DECLARE_SERIALIZER(USequencerToolsFunctionLibrary)


#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_INCLASS \
private: \
	static void StaticRegisterNativesUSequencerToolsFunctionLibrary(); \
	friend struct Z_Construct_UClass_USequencerToolsFunctionLibrary_Statics; \
public: \
	DECLARE_CLASS(USequencerToolsFunctionLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/SequencerScriptingEditor"), NO_API) \
	DECLARE_SERIALIZER(USequencerToolsFunctionLibrary)


#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USequencerToolsFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USequencerToolsFunctionLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USequencerToolsFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USequencerToolsFunctionLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USequencerToolsFunctionLibrary(USequencerToolsFunctionLibrary&&); \
	NO_API USequencerToolsFunctionLibrary(const USequencerToolsFunctionLibrary&); \
public:


#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USequencerToolsFunctionLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USequencerToolsFunctionLibrary(USequencerToolsFunctionLibrary&&); \
	NO_API USequencerToolsFunctionLibrary(const USequencerToolsFunctionLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USequencerToolsFunctionLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USequencerToolsFunctionLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USequencerToolsFunctionLibrary)


#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_70_PROLOG
#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_SPARSE_DATA \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_INCLASS \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_SPARSE_DATA \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h_73_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SEQUENCERSCRIPTINGEDITOR_API UClass* StaticClass<class USequencerToolsFunctionLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScriptingEditor_Public_SequencerTools_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
