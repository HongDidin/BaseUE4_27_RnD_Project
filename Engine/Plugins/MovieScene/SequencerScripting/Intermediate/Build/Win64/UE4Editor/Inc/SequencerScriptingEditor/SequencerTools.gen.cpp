// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SequencerScriptingEditor/Public/SequencerTools.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSequencerTools() {}
// Cross Module References
	SEQUENCERSCRIPTINGEDITOR_API UFunction* Z_Construct_UDelegateFunction_SequencerScriptingEditor_OnRenderMovieStopped__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_SequencerScriptingEditor();
	SEQUENCERSCRIPTINGEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FSequencerQuickBindingResult();
	BLUEPRINTGRAPH_API UClass* Z_Construct_UClass_UK2Node_CustomEvent_NoRegister();
	SEQUENCERSCRIPTINGEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FSequencerBoundObjects();
	SEQUENCERSCRIPTING_API UScriptStruct* Z_Construct_UScriptStruct_FSequencerBindingProxy();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	SEQUENCERSCRIPTINGEDITOR_API UClass* Z_Construct_UClass_USequencerToolsFunctionLibrary_NoRegister();
	SEQUENCERSCRIPTINGEDITOR_API UClass* Z_Construct_UClass_USequencerToolsFunctionLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneSequence_NoRegister();
	MOVIESCENETRACKS_API UClass* Z_Construct_UClass_UMovieSceneEventSectionBase_NoRegister();
	MOVIESCENETRACKS_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneEvent();
	ENGINE_API UClass* Z_Construct_UClass_UWorld_NoRegister();
	LEVELSEQUENCE_API UClass* Z_Construct_UClass_ULevelSequence_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UAnimSequence_NoRegister();
	UNREALED_API UClass* Z_Construct_UClass_UAnimSeqExportOption_NoRegister();
	UNREALED_API UClass* Z_Construct_UClass_UFbxExportOption_NoRegister();
	TEMPLATESEQUENCE_API UClass* Z_Construct_UClass_UTemplateSequence_NoRegister();
	SEQUENCERSCRIPTING_API UScriptStruct* Z_Construct_UScriptStruct_FSequencerScriptingRange();
	MOVIESCENETOOLS_API UClass* Z_Construct_UClass_UMovieSceneUserImportFBXControlRigSettings_NoRegister();
	MOVIESCENETOOLS_API UClass* Z_Construct_UClass_UMovieSceneUserImportFBXSettings_NoRegister();
	MOVIESCENECAPTURE_API UClass* Z_Construct_UClass_UMovieSceneCapture_NoRegister();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_SequencerScriptingEditor_OnRenderMovieStopped__DelegateSignature_Statics
	{
		struct _Script_SequencerScriptingEditor_eventOnRenderMovieStopped_Parms
		{
			bool bSuccess;
		};
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UDelegateFunction_SequencerScriptingEditor_OnRenderMovieStopped__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_SequencerScriptingEditor_eventOnRenderMovieStopped_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_SequencerScriptingEditor_OnRenderMovieStopped__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_SequencerScriptingEditor_eventOnRenderMovieStopped_Parms), &Z_Construct_UDelegateFunction_SequencerScriptingEditor_OnRenderMovieStopped__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_SequencerScriptingEditor_OnRenderMovieStopped__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_SequencerScriptingEditor_OnRenderMovieStopped__DelegateSignature_Statics::NewProp_bSuccess,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_SequencerScriptingEditor_OnRenderMovieStopped__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_SequencerScriptingEditor_OnRenderMovieStopped__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_SequencerScriptingEditor, nullptr, "OnRenderMovieStopped__DelegateSignature", nullptr, nullptr, sizeof(_Script_SequencerScriptingEditor_eventOnRenderMovieStopped_Parms), Z_Construct_UDelegateFunction_SequencerScriptingEditor_OnRenderMovieStopped__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_SequencerScriptingEditor_OnRenderMovieStopped__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00120000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_SequencerScriptingEditor_OnRenderMovieStopped__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_SequencerScriptingEditor_OnRenderMovieStopped__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_SequencerScriptingEditor_OnRenderMovieStopped__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_SequencerScriptingEditor_OnRenderMovieStopped__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
class UScriptStruct* FSequencerQuickBindingResult::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern SEQUENCERSCRIPTINGEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSequencerQuickBindingResult, Z_Construct_UPackage__Script_SequencerScriptingEditor(), TEXT("SequencerQuickBindingResult"), sizeof(FSequencerQuickBindingResult), Get_Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Hash());
	}
	return Singleton;
}
template<> SEQUENCERSCRIPTINGEDITOR_API UScriptStruct* StaticStruct<FSequencerQuickBindingResult>()
{
	return FSequencerQuickBindingResult::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSequencerQuickBindingResult(FSequencerQuickBindingResult::StaticStruct, TEXT("/Script/SequencerScriptingEditor"), TEXT("SequencerQuickBindingResult"), false, nullptr, nullptr);
static struct FScriptStruct_SequencerScriptingEditor_StaticRegisterNativesFSequencerQuickBindingResult
{
	FScriptStruct_SequencerScriptingEditor_StaticRegisterNativesFSequencerQuickBindingResult()
	{
		UScriptStruct::DeferCppStructOps<FSequencerQuickBindingResult>(FName(TEXT("SequencerQuickBindingResult")));
	}
} ScriptStruct_SequencerScriptingEditor_StaticRegisterNativesFSequencerQuickBindingResult;
	struct Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EventEndpoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_EventEndpoint;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PayloadNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PayloadNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PayloadNames;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Wrapper around result of quick binding for event track in sequencer. */" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
		{ "ToolTip", "Wrapper around result of quick binding for event track in sequencer." },
	};
#endif
	void* Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSequencerQuickBindingResult>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::NewProp_EventEndpoint_MetaData[] = {
		{ "Comment", "/** Actual endpoint wrapped by this structure.  */" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
		{ "ToolTip", "Actual endpoint wrapped by this structure." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::NewProp_EventEndpoint = { "EventEndpoint", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSequencerQuickBindingResult, EventEndpoint), Z_Construct_UClass_UK2Node_CustomEvent_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::NewProp_EventEndpoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::NewProp_EventEndpoint_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::NewProp_PayloadNames_Inner = { "PayloadNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::NewProp_PayloadNames_MetaData[] = {
		{ "Category", "Data" },
		{ "Comment", "/** Names of the payload variables of the event. */" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
		{ "ToolTip", "Names of the payload variables of the event." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::NewProp_PayloadNames = { "PayloadNames", nullptr, (EPropertyFlags)0x0010000000000014, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSequencerQuickBindingResult, PayloadNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::NewProp_PayloadNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::NewProp_PayloadNames_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::NewProp_EventEndpoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::NewProp_PayloadNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::NewProp_PayloadNames,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_SequencerScriptingEditor,
		nullptr,
		&NewStructOps,
		"SequencerQuickBindingResult",
		sizeof(FSequencerQuickBindingResult),
		alignof(FSequencerQuickBindingResult),
		Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSequencerQuickBindingResult()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_SequencerScriptingEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SequencerQuickBindingResult"), sizeof(FSequencerQuickBindingResult), Get_Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSequencerQuickBindingResult_Hash() { return 1392130491U; }
class UScriptStruct* FSequencerBoundObjects::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern SEQUENCERSCRIPTINGEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FSequencerBoundObjects_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSequencerBoundObjects, Z_Construct_UPackage__Script_SequencerScriptingEditor(), TEXT("SequencerBoundObjects"), sizeof(FSequencerBoundObjects), Get_Z_Construct_UScriptStruct_FSequencerBoundObjects_Hash());
	}
	return Singleton;
}
template<> SEQUENCERSCRIPTINGEDITOR_API UScriptStruct* StaticStruct<FSequencerBoundObjects>()
{
	return FSequencerBoundObjects::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSequencerBoundObjects(FSequencerBoundObjects::StaticStruct, TEXT("/Script/SequencerScriptingEditor"), TEXT("SequencerBoundObjects"), false, nullptr, nullptr);
static struct FScriptStruct_SequencerScriptingEditor_StaticRegisterNativesFSequencerBoundObjects
{
	FScriptStruct_SequencerScriptingEditor_StaticRegisterNativesFSequencerBoundObjects()
	{
		UScriptStruct::DeferCppStructOps<FSequencerBoundObjects>(FName(TEXT("SequencerBoundObjects")));
	}
} ScriptStruct_SequencerScriptingEditor_StaticRegisterNativesFSequencerBoundObjects;
	struct Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BindingProxy_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BindingProxy;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BoundObjects_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoundObjects_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_BoundObjects;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSequencerBoundObjects>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::NewProp_BindingProxy_MetaData[] = {
		{ "Category", "Binding" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::NewProp_BindingProxy = { "BindingProxy", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSequencerBoundObjects, BindingProxy), Z_Construct_UScriptStruct_FSequencerBindingProxy, METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::NewProp_BindingProxy_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::NewProp_BindingProxy_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::NewProp_BoundObjects_Inner = { "BoundObjects", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::NewProp_BoundObjects_MetaData[] = {
		{ "Category", "Binding" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::NewProp_BoundObjects = { "BoundObjects", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSequencerBoundObjects, BoundObjects), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::NewProp_BoundObjects_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::NewProp_BoundObjects_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::NewProp_BindingProxy,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::NewProp_BoundObjects_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::NewProp_BoundObjects,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_SequencerScriptingEditor,
		nullptr,
		&NewStructOps,
		"SequencerBoundObjects",
		sizeof(FSequencerBoundObjects),
		alignof(FSequencerBoundObjects),
		Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000201),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSequencerBoundObjects()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSequencerBoundObjects_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_SequencerScriptingEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SequencerBoundObjects"), sizeof(FSequencerBoundObjects), Get_Z_Construct_UScriptStruct_FSequencerBoundObjects_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSequencerBoundObjects_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSequencerBoundObjects_Hash() { return 3056999466U; }
	DEFINE_FUNCTION(USequencerToolsFunctionLibrary::execImportFBXToControlRig)
	{
		P_GET_OBJECT(UWorld,Z_Param_World);
		P_GET_OBJECT(ULevelSequence,Z_Param_InSequence);
		P_GET_PROPERTY(FStrProperty,Z_Param_ActorWithControlRigTrack);
		P_GET_TARRAY_REF(FString,Z_Param_Out_SelectedControlRigNames);
		P_GET_OBJECT(UMovieSceneUserImportFBXControlRigSettings,Z_Param_ImportFBXControlRigSettings);
		P_GET_PROPERTY(FStrProperty,Z_Param_ImportFilename);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=USequencerToolsFunctionLibrary::ImportFBXToControlRig(Z_Param_World,Z_Param_InSequence,Z_Param_ActorWithControlRigTrack,Z_Param_Out_SelectedControlRigNames,Z_Param_ImportFBXControlRigSettings,Z_Param_ImportFilename);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USequencerToolsFunctionLibrary::execCreateQuickBinding)
	{
		P_GET_OBJECT(UMovieSceneSequence,Z_Param_InSequence);
		P_GET_OBJECT(UObject,Z_Param_InObject);
		P_GET_PROPERTY(FStrProperty,Z_Param_InFunctionName);
		P_GET_UBOOL(Z_Param_bCallInEditor);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FSequencerQuickBindingResult*)Z_Param__Result=USequencerToolsFunctionLibrary::CreateQuickBinding(Z_Param_InSequence,Z_Param_InObject,Z_Param_InFunctionName,Z_Param_bCallInEditor);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USequencerToolsFunctionLibrary::execIsEventEndpointValid)
	{
		P_GET_STRUCT_REF(FSequencerQuickBindingResult,Z_Param_Out_InEndpoint);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=USequencerToolsFunctionLibrary::IsEventEndpointValid(Z_Param_Out_InEndpoint);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USequencerToolsFunctionLibrary::execCreateEvent)
	{
		P_GET_OBJECT(UMovieSceneSequence,Z_Param_InSequence);
		P_GET_OBJECT(UMovieSceneEventSectionBase,Z_Param_InSection);
		P_GET_STRUCT_REF(FSequencerQuickBindingResult,Z_Param_Out_InEndpoint);
		P_GET_TARRAY_REF(FString,Z_Param_Out_InPayload);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FMovieSceneEvent*)Z_Param__Result=USequencerToolsFunctionLibrary::CreateEvent(Z_Param_InSequence,Z_Param_InSection,Z_Param_Out_InEndpoint,Z_Param_Out_InPayload);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USequencerToolsFunctionLibrary::execImportTemplateSequenceFBX)
	{
		P_GET_OBJECT(UWorld,Z_Param_InWorld);
		P_GET_OBJECT(UTemplateSequence,Z_Param_InSequence);
		P_GET_TARRAY_REF(FSequencerBindingProxy,Z_Param_Out_InBindings);
		P_GET_OBJECT(UMovieSceneUserImportFBXSettings,Z_Param_InImportFBXSettings);
		P_GET_PROPERTY(FStrProperty,Z_Param_InImportFilename);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=USequencerToolsFunctionLibrary::ImportTemplateSequenceFBX(Z_Param_InWorld,Z_Param_InSequence,Z_Param_Out_InBindings,Z_Param_InImportFBXSettings,Z_Param_InImportFilename);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USequencerToolsFunctionLibrary::execImportLevelSequenceFBX)
	{
		P_GET_OBJECT(UWorld,Z_Param_InWorld);
		P_GET_OBJECT(ULevelSequence,Z_Param_InSequence);
		P_GET_TARRAY_REF(FSequencerBindingProxy,Z_Param_Out_InBindings);
		P_GET_OBJECT(UMovieSceneUserImportFBXSettings,Z_Param_InImportFBXSettings);
		P_GET_PROPERTY(FStrProperty,Z_Param_InImportFilename);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=USequencerToolsFunctionLibrary::ImportLevelSequenceFBX(Z_Param_InWorld,Z_Param_InSequence,Z_Param_Out_InBindings,Z_Param_InImportFBXSettings,Z_Param_InImportFilename);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USequencerToolsFunctionLibrary::execExportAnimSequence)
	{
		P_GET_OBJECT(UWorld,Z_Param_World);
		P_GET_OBJECT(ULevelSequence,Z_Param_Sequence);
		P_GET_OBJECT(UAnimSequence,Z_Param_AnimSequence);
		P_GET_OBJECT(UAnimSeqExportOption,Z_Param_ExportOption);
		P_GET_STRUCT_REF(FSequencerBindingProxy,Z_Param_Out_Binding);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=USequencerToolsFunctionLibrary::ExportAnimSequence(Z_Param_World,Z_Param_Sequence,Z_Param_AnimSequence,Z_Param_ExportOption,Z_Param_Out_Binding);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USequencerToolsFunctionLibrary::execExportTemplateSequenceFBX)
	{
		P_GET_OBJECT(UWorld,Z_Param_InWorld);
		P_GET_OBJECT(UTemplateSequence,Z_Param_InSequence);
		P_GET_TARRAY_REF(FSequencerBindingProxy,Z_Param_Out_InBindings);
		P_GET_OBJECT(UFbxExportOption,Z_Param_OverrideOptions);
		P_GET_PROPERTY(FStrProperty,Z_Param_InFBXFileName);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=USequencerToolsFunctionLibrary::ExportTemplateSequenceFBX(Z_Param_InWorld,Z_Param_InSequence,Z_Param_Out_InBindings,Z_Param_OverrideOptions,Z_Param_InFBXFileName);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USequencerToolsFunctionLibrary::execExportLevelSequenceFBX)
	{
		P_GET_OBJECT(UWorld,Z_Param_InWorld);
		P_GET_OBJECT(ULevelSequence,Z_Param_InSequence);
		P_GET_TARRAY_REF(FSequencerBindingProxy,Z_Param_Out_InBindings);
		P_GET_OBJECT(UFbxExportOption,Z_Param_OverrideOptions);
		P_GET_PROPERTY(FStrProperty,Z_Param_InFBXFileName);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=USequencerToolsFunctionLibrary::ExportLevelSequenceFBX(Z_Param_InWorld,Z_Param_InSequence,Z_Param_Out_InBindings,Z_Param_OverrideOptions,Z_Param_InFBXFileName);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USequencerToolsFunctionLibrary::execGetObjectBindings)
	{
		P_GET_OBJECT(UWorld,Z_Param_InWorld);
		P_GET_OBJECT(ULevelSequence,Z_Param_InSequence);
		P_GET_TARRAY_REF(UObject*,Z_Param_Out_InObject);
		P_GET_STRUCT_REF(FSequencerScriptingRange,Z_Param_Out_InRange);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FSequencerBoundObjects>*)Z_Param__Result=USequencerToolsFunctionLibrary::GetObjectBindings(Z_Param_InWorld,Z_Param_InSequence,Z_Param_Out_InObject,Z_Param_Out_InRange);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USequencerToolsFunctionLibrary::execGetBoundObjects)
	{
		P_GET_OBJECT(UWorld,Z_Param_InWorld);
		P_GET_OBJECT(ULevelSequence,Z_Param_InSequence);
		P_GET_TARRAY_REF(FSequencerBindingProxy,Z_Param_Out_InBindings);
		P_GET_STRUCT_REF(FSequencerScriptingRange,Z_Param_Out_InRange);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FSequencerBoundObjects>*)Z_Param__Result=USequencerToolsFunctionLibrary::GetBoundObjects(Z_Param_InWorld,Z_Param_InSequence,Z_Param_Out_InBindings,Z_Param_Out_InRange);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USequencerToolsFunctionLibrary::execCancelMovieRender)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		USequencerToolsFunctionLibrary::CancelMovieRender();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USequencerToolsFunctionLibrary::execIsRenderingMovie)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=USequencerToolsFunctionLibrary::IsRenderingMovie();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(USequencerToolsFunctionLibrary::execRenderMovie)
	{
		P_GET_OBJECT(UMovieSceneCapture,Z_Param_InCaptureSettings);
		P_GET_PROPERTY(FDelegateProperty,Z_Param_OnFinishedCallback);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=USequencerToolsFunctionLibrary::RenderMovie(Z_Param_InCaptureSettings,FOnRenderMovieStopped(Z_Param_OnFinishedCallback));
		P_NATIVE_END;
	}
	void USequencerToolsFunctionLibrary::StaticRegisterNativesUSequencerToolsFunctionLibrary()
	{
		UClass* Class = USequencerToolsFunctionLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CancelMovieRender", &USequencerToolsFunctionLibrary::execCancelMovieRender },
			{ "CreateEvent", &USequencerToolsFunctionLibrary::execCreateEvent },
			{ "CreateQuickBinding", &USequencerToolsFunctionLibrary::execCreateQuickBinding },
			{ "ExportAnimSequence", &USequencerToolsFunctionLibrary::execExportAnimSequence },
			{ "ExportLevelSequenceFBX", &USequencerToolsFunctionLibrary::execExportLevelSequenceFBX },
			{ "ExportTemplateSequenceFBX", &USequencerToolsFunctionLibrary::execExportTemplateSequenceFBX },
			{ "GetBoundObjects", &USequencerToolsFunctionLibrary::execGetBoundObjects },
			{ "GetObjectBindings", &USequencerToolsFunctionLibrary::execGetObjectBindings },
			{ "ImportFBXToControlRig", &USequencerToolsFunctionLibrary::execImportFBXToControlRig },
			{ "ImportLevelSequenceFBX", &USequencerToolsFunctionLibrary::execImportLevelSequenceFBX },
			{ "ImportTemplateSequenceFBX", &USequencerToolsFunctionLibrary::execImportTemplateSequenceFBX },
			{ "IsEventEndpointValid", &USequencerToolsFunctionLibrary::execIsEventEndpointValid },
			{ "IsRenderingMovie", &USequencerToolsFunctionLibrary::execIsRenderingMovie },
			{ "RenderMovie", &USequencerToolsFunctionLibrary::execRenderMovie },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_USequencerToolsFunctionLibrary_CancelMovieRender_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_CancelMovieRender_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Sequencer Tools | Movie Rendering" },
		{ "Comment", "/**\n\x09* Attempts to cancel an in-progress Render to Movie. Does nothing if there is no render in progress.\n\x09*/" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
		{ "ToolTip", "Attempts to cancel an in-progress Render to Movie. Does nothing if there is no render in progress." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_CancelMovieRender_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerToolsFunctionLibrary, nullptr, "CancelMovieRender", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_CancelMovieRender_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_CancelMovieRender_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerToolsFunctionLibrary_CancelMovieRender()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerToolsFunctionLibrary_CancelMovieRender_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics
	{
		struct SequencerToolsFunctionLibrary_eventCreateEvent_Parms
		{
			UMovieSceneSequence* InSequence;
			UMovieSceneEventSectionBase* InSection;
			FSequencerQuickBindingResult InEndpoint;
			TArray<FString> InPayload;
			FMovieSceneEvent ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InSequence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InSection_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InSection;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InEndpoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InEndpoint;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InPayload_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InPayload_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InPayload;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InSequence = { "InSequence", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventCreateEvent_Parms, InSequence), Z_Construct_UClass_UMovieSceneSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InSection_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InSection = { "InSection", nullptr, (EPropertyFlags)0x0010000000080080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventCreateEvent_Parms, InSection), Z_Construct_UClass_UMovieSceneEventSectionBase_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InSection_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InSection_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InEndpoint_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InEndpoint = { "InEndpoint", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventCreateEvent_Parms, InEndpoint), Z_Construct_UScriptStruct_FSequencerQuickBindingResult, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InEndpoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InEndpoint_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InPayload_Inner = { "InPayload", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InPayload_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InPayload = { "InPayload", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventCreateEvent_Parms, InPayload), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InPayload_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InPayload_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventCreateEvent_Parms, ReturnValue), Z_Construct_UScriptStruct_FMovieSceneEvent, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InSection,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InEndpoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InPayload_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_InPayload,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Sequencer Tools | Animation" },
		{ "Comment", "/**\n\x09 * Create an event from a previously created blueprint endpoint and a payload. The resulting event should be added only\n\x09 * to a channel of the section that was given as a parameter.\n\x09 * @param InSequence Main level sequence that holds the event track and to which the resulting event should be added.\n\x09 * @param InSection Section of the event track of the main sequence.\n\x09 * @param InEndpoint Previously created endpoint.\n\x09 * @param InPayload Values passed as payload to event, count must match the numbers of payload variable names in @InEndpoint.\n\x09 * @return The created movie event.\n\x09 * @see CreateQuickBinding\n\x09 */" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
		{ "ToolTip", "Create an event from a previously created blueprint endpoint and a payload. The resulting event should be added only\nto a channel of the section that was given as a parameter.\n@param InSequence Main level sequence that holds the event track and to which the resulting event should be added.\n@param InSection Section of the event track of the main sequence.\n@param InEndpoint Previously created endpoint.\n@param InPayload Values passed as payload to event, count must match the numbers of payload variable names in @InEndpoint.\n@return The created movie event.\n@see CreateQuickBinding" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerToolsFunctionLibrary, nullptr, "CreateEvent", nullptr, nullptr, sizeof(SequencerToolsFunctionLibrary_eventCreateEvent_Parms), Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics
	{
		struct SequencerToolsFunctionLibrary_eventCreateQuickBinding_Parms
		{
			UMovieSceneSequence* InSequence;
			UObject* InObject;
			FString InFunctionName;
			bool bCallInEditor;
			FSequencerQuickBindingResult ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InSequence;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InFunctionName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InFunctionName;
		static void NewProp_bCallInEditor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCallInEditor;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::NewProp_InSequence = { "InSequence", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventCreateQuickBinding_Parms, InSequence), Z_Construct_UClass_UMovieSceneSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::NewProp_InObject = { "InObject", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventCreateQuickBinding_Parms, InObject), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::NewProp_InFunctionName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::NewProp_InFunctionName = { "InFunctionName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventCreateQuickBinding_Parms, InFunctionName), METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::NewProp_InFunctionName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::NewProp_InFunctionName_MetaData)) };
	void Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::NewProp_bCallInEditor_SetBit(void* Obj)
	{
		((SequencerToolsFunctionLibrary_eventCreateQuickBinding_Parms*)Obj)->bCallInEditor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::NewProp_bCallInEditor = { "bCallInEditor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SequencerToolsFunctionLibrary_eventCreateQuickBinding_Parms), &Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::NewProp_bCallInEditor_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventCreateQuickBinding_Parms, ReturnValue), Z_Construct_UScriptStruct_FSequencerQuickBindingResult, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::NewProp_InSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::NewProp_InObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::NewProp_InFunctionName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::NewProp_bCallInEditor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Sequencer Tools | Animation" },
		{ "Comment", "/**\n\x09 * Create a quick binding to an actor's member method to be used in an event sequence.\n\x09 * @param InActor Actor that will be bound\n\x09 * @param InFunctionName Name of the method, as it is displayed in the Blueprint Editor. eg. \"Set Actor Scale 3D\"\n\x09 * @param bCallInEditor Should the event be callable in editor.\n\x09 * @return The created binding.\n\x09 */" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
		{ "ToolTip", "Create a quick binding to an actor's member method to be used in an event sequence.\n@param InActor Actor that will be bound\n@param InFunctionName Name of the method, as it is displayed in the Blueprint Editor. eg. \"Set Actor Scale 3D\"\n@param bCallInEditor Should the event be callable in editor.\n@return The created binding." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerToolsFunctionLibrary, nullptr, "CreateQuickBinding", nullptr, nullptr, sizeof(SequencerToolsFunctionLibrary_eventCreateQuickBinding_Parms), Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics
	{
		struct SequencerToolsFunctionLibrary_eventExportAnimSequence_Parms
		{
			UWorld* World;
			ULevelSequence* Sequence;
			UAnimSequence* AnimSequence;
			UAnimSeqExportOption* ExportOption;
			FSequencerBindingProxy Binding;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_World;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Sequence;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AnimSequence;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ExportOption;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Binding_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Binding;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_World = { "World", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventExportAnimSequence_Parms, World), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_Sequence = { "Sequence", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventExportAnimSequence_Parms, Sequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_AnimSequence = { "AnimSequence", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventExportAnimSequence_Parms, AnimSequence), Z_Construct_UClass_UAnimSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_ExportOption = { "ExportOption", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventExportAnimSequence_Parms, ExportOption), Z_Construct_UClass_UAnimSeqExportOption_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_Binding_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_Binding = { "Binding", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventExportAnimSequence_Parms, Binding), Z_Construct_UScriptStruct_FSequencerBindingProxy, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_Binding_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_Binding_MetaData)) };
	void Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SequencerToolsFunctionLibrary_eventExportAnimSequence_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SequencerToolsFunctionLibrary_eventExportAnimSequence_Parms), &Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_World,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_Sequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_AnimSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_ExportOption,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_Binding,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Sequencer Tools | Animation" },
		{ "Comment", "/*\n\x09 * Export Passed in Binding as an Anim Seqquence.\n\x09 *\n\x09 * @InWorld World to export\n\x09 * @InSequence Sequence to export\n\x09 * @AnimSequence The AnimSequence to save into.\n\x09 * @ExportOption The export options for the sequence.\n\x09 * @InBinding Binding to export that has a skelmesh component on it\n\x09 * @InAnimSequenceFilename File to create\n\x09 */" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
		{ "ToolTip", "* Export Passed in Binding as an Anim Seqquence.\n*\n* @InWorld World to export\n* @InSequence Sequence to export\n* @AnimSequence The AnimSequence to save into.\n* @ExportOption The export options for the sequence.\n* @InBinding Binding to export that has a skelmesh component on it\n* @InAnimSequenceFilename File to create" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerToolsFunctionLibrary, nullptr, "ExportAnimSequence", nullptr, nullptr, sizeof(SequencerToolsFunctionLibrary_eventExportAnimSequence_Parms), Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics
	{
		struct SequencerToolsFunctionLibrary_eventExportLevelSequenceFBX_Parms
		{
			UWorld* InWorld;
			ULevelSequence* InSequence;
			TArray<FSequencerBindingProxy> InBindings;
			UFbxExportOption* OverrideOptions;
			FString InFBXFileName;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InWorld;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InSequence;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InBindings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InBindings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InBindings;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverrideOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InFBXFileName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InFBXFileName;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_InWorld = { "InWorld", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventExportLevelSequenceFBX_Parms, InWorld), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_InSequence = { "InSequence", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventExportLevelSequenceFBX_Parms, InSequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_InBindings_Inner = { "InBindings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSequencerBindingProxy, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_InBindings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_InBindings = { "InBindings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventExportLevelSequenceFBX_Parms, InBindings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_InBindings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_InBindings_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_OverrideOptions = { "OverrideOptions", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventExportLevelSequenceFBX_Parms, OverrideOptions), Z_Construct_UClass_UFbxExportOption_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_InFBXFileName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_InFBXFileName = { "InFBXFileName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventExportLevelSequenceFBX_Parms, InFBXFileName), METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_InFBXFileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_InFBXFileName_MetaData)) };
	void Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SequencerToolsFunctionLibrary_eventExportLevelSequenceFBX_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SequencerToolsFunctionLibrary_eventExportLevelSequenceFBX_Parms), &Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_InWorld,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_InSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_InBindings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_InBindings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_OverrideOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_InFBXFileName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Sequencer Tools | FBX" },
		{ "Comment", "/*\n\x09 * Export Passed in Bindings to FBX\n\x09 *\n\x09 * @InWorld World to export\n\x09 * @InSequence Sequence to export\n\x09 * @InBindings Bindings to export\n\x09 * @InFBXFileName File to create\n\x09 */" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
		{ "ToolTip", "* Export Passed in Bindings to FBX\n*\n* @InWorld World to export\n* @InSequence Sequence to export\n* @InBindings Bindings to export\n* @InFBXFileName File to create" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerToolsFunctionLibrary, nullptr, "ExportLevelSequenceFBX", nullptr, nullptr, sizeof(SequencerToolsFunctionLibrary_eventExportLevelSequenceFBX_Parms), Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics
	{
		struct SequencerToolsFunctionLibrary_eventExportTemplateSequenceFBX_Parms
		{
			UWorld* InWorld;
			UTemplateSequence* InSequence;
			TArray<FSequencerBindingProxy> InBindings;
			UFbxExportOption* OverrideOptions;
			FString InFBXFileName;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InWorld;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InSequence;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InBindings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InBindings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InBindings;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OverrideOptions;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InFBXFileName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InFBXFileName;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_InWorld = { "InWorld", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventExportTemplateSequenceFBX_Parms, InWorld), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_InSequence = { "InSequence", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventExportTemplateSequenceFBX_Parms, InSequence), Z_Construct_UClass_UTemplateSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_InBindings_Inner = { "InBindings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSequencerBindingProxy, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_InBindings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_InBindings = { "InBindings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventExportTemplateSequenceFBX_Parms, InBindings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_InBindings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_InBindings_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_OverrideOptions = { "OverrideOptions", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventExportTemplateSequenceFBX_Parms, OverrideOptions), Z_Construct_UClass_UFbxExportOption_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_InFBXFileName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_InFBXFileName = { "InFBXFileName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventExportTemplateSequenceFBX_Parms, InFBXFileName), METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_InFBXFileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_InFBXFileName_MetaData)) };
	void Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SequencerToolsFunctionLibrary_eventExportTemplateSequenceFBX_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SequencerToolsFunctionLibrary_eventExportTemplateSequenceFBX_Parms), &Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_InWorld,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_InSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_InBindings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_InBindings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_OverrideOptions,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_InFBXFileName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Sequencer Tools | FBX" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerToolsFunctionLibrary, nullptr, "ExportTemplateSequenceFBX", nullptr, nullptr, sizeof(SequencerToolsFunctionLibrary_eventExportTemplateSequenceFBX_Parms), Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics
	{
		struct SequencerToolsFunctionLibrary_eventGetBoundObjects_Parms
		{
			UWorld* InWorld;
			ULevelSequence* InSequence;
			TArray<FSequencerBindingProxy> InBindings;
			FSequencerScriptingRange InRange;
			TArray<FSequencerBoundObjects> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InWorld;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InSequence;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InBindings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InBindings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InBindings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InRange;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_InWorld = { "InWorld", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventGetBoundObjects_Parms, InWorld), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_InSequence = { "InSequence", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventGetBoundObjects_Parms, InSequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_InBindings_Inner = { "InBindings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSequencerBindingProxy, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_InBindings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_InBindings = { "InBindings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventGetBoundObjects_Parms, InBindings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_InBindings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_InBindings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_InRange_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_InRange = { "InRange", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventGetBoundObjects_Parms, InRange), Z_Construct_UScriptStruct_FSequencerScriptingRange, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_InRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_InRange_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSequencerBoundObjects, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventGetBoundObjects_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_InWorld,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_InSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_InBindings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_InBindings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_InRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Sequencer Tools" },
		{ "Comment", "/*\n\x09 * Retrieve all objects currently bound to the specified binding identifiers. The sequence will be evaluated in lower bound of the specified range, \n\x09 * which allows for retrieving spawnables in that period of time.\n\x09 */" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
		{ "ToolTip", "* Retrieve all objects currently bound to the specified binding identifiers. The sequence will be evaluated in lower bound of the specified range,\n* which allows for retrieving spawnables in that period of time." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerToolsFunctionLibrary, nullptr, "GetBoundObjects", nullptr, nullptr, sizeof(SequencerToolsFunctionLibrary_eventGetBoundObjects_Parms), Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics
	{
		struct SequencerToolsFunctionLibrary_eventGetObjectBindings_Parms
		{
			UWorld* InWorld;
			ULevelSequence* InSequence;
			TArray<UObject*> InObject;
			FSequencerScriptingRange InRange;
			TArray<FSequencerBoundObjects> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InWorld;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InSequence;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InObject_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InObject_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InObject;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InRange_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InRange;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_InWorld = { "InWorld", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventGetObjectBindings_Parms, InWorld), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_InSequence = { "InSequence", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventGetObjectBindings_Parms, InSequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_InObject_Inner = { "InObject", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_InObject_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_InObject = { "InObject", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventGetObjectBindings_Parms, InObject), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_InObject_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_InObject_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_InRange_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_InRange = { "InRange", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventGetObjectBindings_Parms, InRange), Z_Construct_UScriptStruct_FSequencerScriptingRange, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_InRange_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_InRange_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSequencerBoundObjects, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventGetObjectBindings_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_InWorld,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_InSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_InObject_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_InObject,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_InRange,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Sequencer Tools" },
		{ "Comment", "/*\n\x09 * Get the object bindings for the requested object. The sequence will be evaluated in lower bound of the specified range, \n\x09 * which allows for retrieving spawnables in that period of time.\n\x09 */" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
		{ "ToolTip", "* Get the object bindings for the requested object. The sequence will be evaluated in lower bound of the specified range,\n* which allows for retrieving spawnables in that period of time." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerToolsFunctionLibrary, nullptr, "GetObjectBindings", nullptr, nullptr, sizeof(SequencerToolsFunctionLibrary_eventGetObjectBindings_Parms), Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics
	{
		struct SequencerToolsFunctionLibrary_eventImportFBXToControlRig_Parms
		{
			UWorld* World;
			ULevelSequence* InSequence;
			FString ActorWithControlRigTrack;
			TArray<FString> SelectedControlRigNames;
			UMovieSceneUserImportFBXControlRigSettings* ImportFBXControlRigSettings;
			FString ImportFilename;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_World;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InSequence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ActorWithControlRigTrack_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ActorWithControlRigTrack;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SelectedControlRigNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectedControlRigNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SelectedControlRigNames;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ImportFBXControlRigSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportFilename_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ImportFilename;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_World = { "World", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventImportFBXToControlRig_Parms, World), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_InSequence = { "InSequence", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventImportFBXToControlRig_Parms, InSequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_ActorWithControlRigTrack_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_ActorWithControlRigTrack = { "ActorWithControlRigTrack", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventImportFBXToControlRig_Parms, ActorWithControlRigTrack), METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_ActorWithControlRigTrack_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_ActorWithControlRigTrack_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_SelectedControlRigNames_Inner = { "SelectedControlRigNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_SelectedControlRigNames_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_SelectedControlRigNames = { "SelectedControlRigNames", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventImportFBXToControlRig_Parms, SelectedControlRigNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_SelectedControlRigNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_SelectedControlRigNames_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_ImportFBXControlRigSettings = { "ImportFBXControlRigSettings", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventImportFBXToControlRig_Parms, ImportFBXControlRigSettings), Z_Construct_UClass_UMovieSceneUserImportFBXControlRigSettings_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_ImportFilename_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_ImportFilename = { "ImportFilename", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventImportFBXToControlRig_Parms, ImportFilename), METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_ImportFilename_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_ImportFilename_MetaData)) };
	void Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SequencerToolsFunctionLibrary_eventImportFBXToControlRig_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SequencerToolsFunctionLibrary_eventImportFBXToControlRig_Parms), &Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_World,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_InSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_ActorWithControlRigTrack,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_SelectedControlRigNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_SelectedControlRigNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_ImportFBXControlRigSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_ImportFilename,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Sequencer Tools | FBX" },
		{ "Comment", "/*\n\x09 * Import FBX onto a control rig with the specified track name\n\x09 *\n\x09 * @InWorld World to import to\n\x09 * @InSequence InSequence to import\n\x09 * @ActorWithControlRigTrack ActorWithControlRigTrack The name of the actor with the control rig track we are importing onto\n\x09 * @SelectedControlRigNames  List of selected control rig names. Will use them if  ImportFBXControlRigSettings->bImportOntoSelectedControls is true\n\x09 * @ImportFBXControlRigSettings Settings to control import.\n\x09 * @InImportFileName Path to fbx file to create\n\x09 */" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
		{ "ToolTip", "* Import FBX onto a control rig with the specified track name\n*\n* @InWorld World to import to\n* @InSequence InSequence to import\n* @ActorWithControlRigTrack ActorWithControlRigTrack The name of the actor with the control rig track we are importing onto\n* @SelectedControlRigNames  List of selected control rig names. Will use them if  ImportFBXControlRigSettings->bImportOntoSelectedControls is true\n* @ImportFBXControlRigSettings Settings to control import.\n* @InImportFileName Path to fbx file to create" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerToolsFunctionLibrary, nullptr, "ImportFBXToControlRig", nullptr, nullptr, sizeof(SequencerToolsFunctionLibrary_eventImportFBXToControlRig_Parms), Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics
	{
		struct SequencerToolsFunctionLibrary_eventImportLevelSequenceFBX_Parms
		{
			UWorld* InWorld;
			ULevelSequence* InSequence;
			TArray<FSequencerBindingProxy> InBindings;
			UMovieSceneUserImportFBXSettings* InImportFBXSettings;
			FString InImportFilename;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InWorld;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InSequence;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InBindings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InBindings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InBindings;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InImportFBXSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InImportFilename_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InImportFilename;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InWorld = { "InWorld", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventImportLevelSequenceFBX_Parms, InWorld), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InSequence = { "InSequence", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventImportLevelSequenceFBX_Parms, InSequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InBindings_Inner = { "InBindings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSequencerBindingProxy, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InBindings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InBindings = { "InBindings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventImportLevelSequenceFBX_Parms, InBindings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InBindings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InBindings_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InImportFBXSettings = { "InImportFBXSettings", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventImportLevelSequenceFBX_Parms, InImportFBXSettings), Z_Construct_UClass_UMovieSceneUserImportFBXSettings_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InImportFilename_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InImportFilename = { "InImportFilename", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventImportLevelSequenceFBX_Parms, InImportFilename), METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InImportFilename_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InImportFilename_MetaData)) };
	void Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SequencerToolsFunctionLibrary_eventImportLevelSequenceFBX_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SequencerToolsFunctionLibrary_eventImportLevelSequenceFBX_Parms), &Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InWorld,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InBindings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InBindings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InImportFBXSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_InImportFilename,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Sequencer Tools | FBX" },
		{ "Comment", "/*\n\x09 * Import FBX onto Passed in Bindings\n\x09 *\n\x09 * @InWorld World to import to\n\x09 * @InSequence InSequence to import\n\x09 * @InBindings InBindings to import\n\x09 * @InImportFBXSettings Settings to control import.\n\x09 * @InImportFileName Path to fbx file to import from\n\x09 * @InPlayer Player to bind to\n\x09 */" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
		{ "ToolTip", "* Import FBX onto Passed in Bindings\n*\n* @InWorld World to import to\n* @InSequence InSequence to import\n* @InBindings InBindings to import\n* @InImportFBXSettings Settings to control import.\n* @InImportFileName Path to fbx file to import from\n* @InPlayer Player to bind to" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerToolsFunctionLibrary, nullptr, "ImportLevelSequenceFBX", nullptr, nullptr, sizeof(SequencerToolsFunctionLibrary_eventImportLevelSequenceFBX_Parms), Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics
	{
		struct SequencerToolsFunctionLibrary_eventImportTemplateSequenceFBX_Parms
		{
			UWorld* InWorld;
			UTemplateSequence* InSequence;
			TArray<FSequencerBindingProxy> InBindings;
			UMovieSceneUserImportFBXSettings* InImportFBXSettings;
			FString InImportFilename;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InWorld;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InSequence;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InBindings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InBindings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InBindings;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InImportFBXSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InImportFilename_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InImportFilename;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InWorld = { "InWorld", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventImportTemplateSequenceFBX_Parms, InWorld), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InSequence = { "InSequence", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventImportTemplateSequenceFBX_Parms, InSequence), Z_Construct_UClass_UTemplateSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InBindings_Inner = { "InBindings", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSequencerBindingProxy, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InBindings_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InBindings = { "InBindings", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventImportTemplateSequenceFBX_Parms, InBindings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InBindings_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InBindings_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InImportFBXSettings = { "InImportFBXSettings", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventImportTemplateSequenceFBX_Parms, InImportFBXSettings), Z_Construct_UClass_UMovieSceneUserImportFBXSettings_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InImportFilename_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InImportFilename = { "InImportFilename", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventImportTemplateSequenceFBX_Parms, InImportFilename), METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InImportFilename_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InImportFilename_MetaData)) };
	void Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SequencerToolsFunctionLibrary_eventImportTemplateSequenceFBX_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SequencerToolsFunctionLibrary_eventImportTemplateSequenceFBX_Parms), &Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InWorld,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InBindings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InBindings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InImportFBXSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_InImportFilename,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Sequencer Tools | FBX" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerToolsFunctionLibrary, nullptr, "ImportTemplateSequenceFBX", nullptr, nullptr, sizeof(SequencerToolsFunctionLibrary_eventImportTemplateSequenceFBX_Parms), Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics
	{
		struct SequencerToolsFunctionLibrary_eventIsEventEndpointValid_Parms
		{
			FSequencerQuickBindingResult InEndpoint;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InEndpoint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InEndpoint;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::NewProp_InEndpoint_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::NewProp_InEndpoint = { "InEndpoint", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventIsEventEndpointValid_Parms, InEndpoint), Z_Construct_UScriptStruct_FSequencerQuickBindingResult, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::NewProp_InEndpoint_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::NewProp_InEndpoint_MetaData)) };
	void Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SequencerToolsFunctionLibrary_eventIsEventEndpointValid_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SequencerToolsFunctionLibrary_eventIsEventEndpointValid_Parms), &Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::NewProp_InEndpoint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Sequencer Tools | Animation" },
		{ "Comment", "/**\n\x09 * Check if an endpoint is valid and can be used to create movie scene event.\n\x09 * @param InEndpoint Endpoint to check.\n\x09 */" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
		{ "ToolTip", "Check if an endpoint is valid and can be used to create movie scene event.\n@param InEndpoint Endpoint to check." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerToolsFunctionLibrary, nullptr, "IsEventEndpointValid", nullptr, nullptr, sizeof(SequencerToolsFunctionLibrary_eventIsEventEndpointValid_Parms), Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsRenderingMovie_Statics
	{
		struct SequencerToolsFunctionLibrary_eventIsRenderingMovie_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsRenderingMovie_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SequencerToolsFunctionLibrary_eventIsRenderingMovie_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsRenderingMovie_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SequencerToolsFunctionLibrary_eventIsRenderingMovie_Parms), &Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsRenderingMovie_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsRenderingMovie_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsRenderingMovie_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsRenderingMovie_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Sequencer Tools | Movie Rendering" },
		{ "Comment", "/** \n\x09* Returns if Render to Movie is currently in progress.\n\x09*/" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
		{ "ToolTip", "Returns if Render to Movie is currently in progress." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsRenderingMovie_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerToolsFunctionLibrary, nullptr, "IsRenderingMovie", nullptr, nullptr, sizeof(SequencerToolsFunctionLibrary_eventIsRenderingMovie_Parms), Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsRenderingMovie_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsRenderingMovie_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsRenderingMovie_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsRenderingMovie_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsRenderingMovie()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsRenderingMovie_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics
	{
		struct SequencerToolsFunctionLibrary_eventRenderMovie_Parms
		{
			UMovieSceneCapture* InCaptureSettings;
			FScriptDelegate OnFinishedCallback;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InCaptureSettings;
		static const UE4CodeGen_Private::FDelegatePropertyParams NewProp_OnFinishedCallback;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics::NewProp_InCaptureSettings = { "InCaptureSettings", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventRenderMovie_Parms, InCaptureSettings), Z_Construct_UClass_UMovieSceneCapture_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FDelegatePropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics::NewProp_OnFinishedCallback = { "OnFinishedCallback", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Delegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(SequencerToolsFunctionLibrary_eventRenderMovie_Parms, OnFinishedCallback), Z_Construct_UDelegateFunction_SequencerScriptingEditor_OnRenderMovieStopped__DelegateSignature, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((SequencerToolsFunctionLibrary_eventRenderMovie_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(SequencerToolsFunctionLibrary_eventRenderMovie_Parms), &Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics::NewProp_InCaptureSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics::NewProp_OnFinishedCallback,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics::Function_MetaDataParams[] = {
		{ "Category", "Editor Scripting | Sequencer Tools | Movie Rendering" },
		{ "Comment", "/**\n\x09* Attempts to render a sequence to movie based on the specified settings. This will automatically detect\n\x09* if we're rendering via a PIE instance or a new process based on the passed in settings. Will return false\n\x09* if the state is not valid (ie: null or missing required parameters, capture in progress, etc.), true otherwise.\n\x09*/" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
		{ "ToolTip", "Attempts to render a sequence to movie based on the specified settings. This will automatically detect\nif we're rendering via a PIE instance or a new process based on the passed in settings. Will return false\nif the state is not valid (ie: null or missing required parameters, capture in progress, etc.), true otherwise." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_USequencerToolsFunctionLibrary, nullptr, "RenderMovie", nullptr, nullptr, sizeof(SequencerToolsFunctionLibrary_eventRenderMovie_Parms), Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_USequencerToolsFunctionLibrary_NoRegister()
	{
		return USequencerToolsFunctionLibrary::StaticClass();
	}
	struct Z_Construct_UClass_USequencerToolsFunctionLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_USequencerToolsFunctionLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_SequencerScriptingEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_USequencerToolsFunctionLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_USequencerToolsFunctionLibrary_CancelMovieRender, "CancelMovieRender" }, // 2368375795
		{ &Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateEvent, "CreateEvent" }, // 630135755
		{ &Z_Construct_UFunction_USequencerToolsFunctionLibrary_CreateQuickBinding, "CreateQuickBinding" }, // 1423390463
		{ &Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportAnimSequence, "ExportAnimSequence" }, // 861242705
		{ &Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportLevelSequenceFBX, "ExportLevelSequenceFBX" }, // 2489832924
		{ &Z_Construct_UFunction_USequencerToolsFunctionLibrary_ExportTemplateSequenceFBX, "ExportTemplateSequenceFBX" }, // 382650287
		{ &Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetBoundObjects, "GetBoundObjects" }, // 3566362968
		{ &Z_Construct_UFunction_USequencerToolsFunctionLibrary_GetObjectBindings, "GetObjectBindings" }, // 1631370136
		{ &Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportFBXToControlRig, "ImportFBXToControlRig" }, // 1849449605
		{ &Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportLevelSequenceFBX, "ImportLevelSequenceFBX" }, // 1941153827
		{ &Z_Construct_UFunction_USequencerToolsFunctionLibrary_ImportTemplateSequenceFBX, "ImportTemplateSequenceFBX" }, // 358821536
		{ &Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsEventEndpointValid, "IsEventEndpointValid" }, // 3361377679
		{ &Z_Construct_UFunction_USequencerToolsFunctionLibrary_IsRenderingMovie, "IsRenderingMovie" }, // 1082729535
		{ &Z_Construct_UFunction_USequencerToolsFunctionLibrary_RenderMovie, "RenderMovie" }, // 3051039861
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_USequencerToolsFunctionLibrary_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** \n * This is a set of helper functions to access various parts of the Sequencer API via Python. Because Sequencer itself is not suitable for exposing, most functionality\n * gets wrapped by UObjects that have an easier API to work with. This UObject provides access to these wrapper UObjects where needed. \n */" },
		{ "IncludePath", "SequencerTools.h" },
		{ "ModuleRelativePath", "Public/SequencerTools.h" },
		{ "ScriptName", "SequencerTools" },
		{ "ToolTip", "This is a set of helper functions to access various parts of the Sequencer API via Python. Because Sequencer itself is not suitable for exposing, most functionality\ngets wrapped by UObjects that have an easier API to work with. This UObject provides access to these wrapper UObjects where needed." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_USequencerToolsFunctionLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<USequencerToolsFunctionLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_USequencerToolsFunctionLibrary_Statics::ClassParams = {
		&USequencerToolsFunctionLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_USequencerToolsFunctionLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_USequencerToolsFunctionLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_USequencerToolsFunctionLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_USequencerToolsFunctionLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(USequencerToolsFunctionLibrary, 919111508);
	template<> SEQUENCERSCRIPTINGEDITOR_API UClass* StaticClass<USequencerToolsFunctionLibrary>()
	{
		return USequencerToolsFunctionLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_USequencerToolsFunctionLibrary(Z_Construct_UClass_USequencerToolsFunctionLibrary, &USequencerToolsFunctionLibrary::StaticClass, TEXT("/Script/SequencerScriptingEditor"), TEXT("USequencerToolsFunctionLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(USequencerToolsFunctionLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
