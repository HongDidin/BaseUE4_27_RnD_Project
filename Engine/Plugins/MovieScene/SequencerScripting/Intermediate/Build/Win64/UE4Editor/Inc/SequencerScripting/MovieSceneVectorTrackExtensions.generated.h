// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMovieSceneVectorTrack;
#ifdef SEQUENCERSCRIPTING_MovieSceneVectorTrackExtensions_generated_h
#error "MovieSceneVectorTrackExtensions.generated.h already included, missing '#pragma once' in MovieSceneVectorTrackExtensions.h"
#endif
#define SEQUENCERSCRIPTING_MovieSceneVectorTrackExtensions_generated_h

#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_SPARSE_DATA
#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetNumChannelsUsed); \
	DECLARE_FUNCTION(execSetNumChannelsUsed);


#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetNumChannelsUsed); \
	DECLARE_FUNCTION(execSetNumChannelsUsed);


#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieSceneVectorTrackExtensions(); \
	friend struct Z_Construct_UClass_UMovieSceneVectorTrackExtensions_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneVectorTrackExtensions, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SequencerScripting"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneVectorTrackExtensions)


#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUMovieSceneVectorTrackExtensions(); \
	friend struct Z_Construct_UClass_UMovieSceneVectorTrackExtensions_Statics; \
public: \
	DECLARE_CLASS(UMovieSceneVectorTrackExtensions, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/SequencerScripting"), NO_API) \
	DECLARE_SERIALIZER(UMovieSceneVectorTrackExtensions)


#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneVectorTrackExtensions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneVectorTrackExtensions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneVectorTrackExtensions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneVectorTrackExtensions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneVectorTrackExtensions(UMovieSceneVectorTrackExtensions&&); \
	NO_API UMovieSceneVectorTrackExtensions(const UMovieSceneVectorTrackExtensions&); \
public:


#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieSceneVectorTrackExtensions(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieSceneVectorTrackExtensions(UMovieSceneVectorTrackExtensions&&); \
	NO_API UMovieSceneVectorTrackExtensions(const UMovieSceneVectorTrackExtensions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieSceneVectorTrackExtensions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieSceneVectorTrackExtensions); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieSceneVectorTrackExtensions)


#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_17_PROLOG
#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_SPARSE_DATA \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_INCLASS \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_SPARSE_DATA \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SEQUENCERSCRIPTING_API UClass* StaticClass<class UMovieSceneVectorTrackExtensions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_SequencerScripting_Source_SequencerScripting_Public_ExtensionLibraries_MovieSceneVectorTrackExtensions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
