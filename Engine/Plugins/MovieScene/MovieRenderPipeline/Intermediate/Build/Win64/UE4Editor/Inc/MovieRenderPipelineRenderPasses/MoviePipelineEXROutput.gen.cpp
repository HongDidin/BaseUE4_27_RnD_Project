// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MovieRenderPipelineRenderPasses/Private/MoviePipelineEXROutput.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePipelineEXROutput() {}
// Cross Module References
	MOVIERENDERPIPELINERENDERPASSES_API UEnum* Z_Construct_UEnum_MovieRenderPipelineRenderPasses_EEXRCompressionFormat();
	UPackage* Z_Construct_UPackage__Script_MovieRenderPipelineRenderPasses();
	MOVIERENDERPIPELINERENDERPASSES_API UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_NoRegister();
	MOVIERENDERPIPELINERENDERPASSES_API UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR();
	MOVIERENDERPIPELINERENDERPASSES_API UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase();
// End Cross Module References
	static UEnum* EEXRCompressionFormat_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MovieRenderPipelineRenderPasses_EEXRCompressionFormat, Z_Construct_UPackage__Script_MovieRenderPipelineRenderPasses(), TEXT("EEXRCompressionFormat"));
		}
		return Singleton;
	}
	template<> MOVIERENDERPIPELINERENDERPASSES_API UEnum* StaticEnum<EEXRCompressionFormat>()
	{
		return EEXRCompressionFormat_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EEXRCompressionFormat(EEXRCompressionFormat_StaticEnum, TEXT("/Script/MovieRenderPipelineRenderPasses"), TEXT("EEXRCompressionFormat"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MovieRenderPipelineRenderPasses_EEXRCompressionFormat_Hash() { return 2796820464U; }
	UEnum* Z_Construct_UEnum_MovieRenderPipelineRenderPasses_EEXRCompressionFormat()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MovieRenderPipelineRenderPasses();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EEXRCompressionFormat"), 0, Get_Z_Construct_UEnum_MovieRenderPipelineRenderPasses_EEXRCompressionFormat_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EEXRCompressionFormat::None", (int64)EEXRCompressionFormat::None },
				{ "EEXRCompressionFormat::PIZ", (int64)EEXRCompressionFormat::PIZ },
				{ "EEXRCompressionFormat::ZIP", (int64)EEXRCompressionFormat::ZIP },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Private/MoviePipelineEXROutput.h" },
				{ "None.Comment", "/** No compression is applied. */" },
				{ "None.Name", "EEXRCompressionFormat::None" },
				{ "None.ToolTip", "No compression is applied." },
				{ "PIZ.Comment", "/** Good compression quality for grainy images. Lossless.*/" },
				{ "PIZ.Name", "EEXRCompressionFormat::PIZ" },
				{ "PIZ.ToolTip", "Good compression quality for grainy images. Lossless." },
				{ "ZIP.Comment", "/** Good compression quality for images with low amounts of noise. Lossless. */" },
				{ "ZIP.Name", "EEXRCompressionFormat::ZIP" },
				{ "ZIP.ToolTip", "Good compression quality for images with low amounts of noise. Lossless." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MovieRenderPipelineRenderPasses,
				nullptr,
				"EEXRCompressionFormat",
				"EEXRCompressionFormat",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMoviePipelineImageSequenceOutput_EXR::StaticRegisterNativesUMoviePipelineImageSequenceOutput_EXR()
	{
	}
	UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_NoRegister()
	{
		return UMoviePipelineImageSequenceOutput_EXR::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Compression_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Compression_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Compression;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bMultilayer_MetaData[];
#endif
		static void NewProp_bMultilayer_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bMultilayer;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineRenderPasses,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MoviePipelineEXROutput.h" },
		{ "ModuleRelativePath", "Private/MoviePipelineEXROutput.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::NewProp_Compression_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::NewProp_Compression_MetaData[] = {
		{ "Category", "EXR" },
		{ "Comment", "/**\n\x09* Which compression method should the resulting EXR file be compressed with\n\x09*/" },
		{ "ModuleRelativePath", "Private/MoviePipelineEXROutput.h" },
		{ "ToolTip", "Which compression method should the resulting EXR file be compressed with" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::NewProp_Compression = { "Compression", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineImageSequenceOutput_EXR, Compression), Z_Construct_UEnum_MovieRenderPipelineRenderPasses_EEXRCompressionFormat, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::NewProp_Compression_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::NewProp_Compression_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::NewProp_bMultilayer_MetaData[] = {
		{ "Category", "EXR" },
		{ "Comment", "/**\n\x09* Should we write all render passes to the same exr file? Not all software supports multi-layer exr files.\n\x09*/" },
		{ "ModuleRelativePath", "Private/MoviePipelineEXROutput.h" },
		{ "ToolTip", "Should we write all render passes to the same exr file? Not all software supports multi-layer exr files." },
	};
#endif
	void Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::NewProp_bMultilayer_SetBit(void* Obj)
	{
		((UMoviePipelineImageSequenceOutput_EXR*)Obj)->bMultilayer = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::NewProp_bMultilayer = { "bMultilayer", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMoviePipelineImageSequenceOutput_EXR), &Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::NewProp_bMultilayer_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::NewProp_bMultilayer_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::NewProp_bMultilayer_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::NewProp_Compression_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::NewProp_Compression,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::NewProp_bMultilayer,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineImageSequenceOutput_EXR>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::ClassParams = {
		&UMoviePipelineImageSequenceOutput_EXR::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineImageSequenceOutput_EXR, 2721430607);
	template<> MOVIERENDERPIPELINERENDERPASSES_API UClass* StaticClass<UMoviePipelineImageSequenceOutput_EXR>()
	{
		return UMoviePipelineImageSequenceOutput_EXR::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineImageSequenceOutput_EXR(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR, &UMoviePipelineImageSequenceOutput_EXR::StaticClass, TEXT("/Script/MovieRenderPipelineRenderPasses"), TEXT("UMoviePipelineImageSequenceOutput_EXR"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineImageSequenceOutput_EXR);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
