// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MOVIERENDERPIPELINERENDERPASSES_MoviePipelineWaveOutput_generated_h
#error "MoviePipelineWaveOutput.generated.h already included, missing '#pragma once' in MoviePipelineWaveOutput.h"
#endif
#define MOVIERENDERPIPELINERENDERPASSES_MoviePipelineWaveOutput_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineWaveOutput(); \
	friend struct Z_Construct_UClass_UMoviePipelineWaveOutput_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineWaveOutput, UMoviePipelineOutputBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineRenderPasses"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineWaveOutput)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineWaveOutput(); \
	friend struct Z_Construct_UClass_UMoviePipelineWaveOutput_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineWaveOutput, UMoviePipelineOutputBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineRenderPasses"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineWaveOutput)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineWaveOutput(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineWaveOutput) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineWaveOutput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineWaveOutput); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineWaveOutput(UMoviePipelineWaveOutput&&); \
	NO_API UMoviePipelineWaveOutput(const UMoviePipelineWaveOutput&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineWaveOutput(UMoviePipelineWaveOutput&&); \
	NO_API UMoviePipelineWaveOutput(const UMoviePipelineWaveOutput&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineWaveOutput); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineWaveOutput); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMoviePipelineWaveOutput)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_8_PROLOG
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h_11_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINERENDERPASSES_API UClass* StaticClass<class UMoviePipelineWaveOutput>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Public_MoviePipelineWaveOutput_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
