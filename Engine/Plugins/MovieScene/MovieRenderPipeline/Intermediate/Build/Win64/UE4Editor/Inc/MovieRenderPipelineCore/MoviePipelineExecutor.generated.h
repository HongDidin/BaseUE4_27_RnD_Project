// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMoviePipelineExecutorBase;
class UMoviePipeline;
 
class UObject;
class UMoviePipelineQueue;
#ifdef MOVIERENDERPIPELINECORE_MoviePipelineExecutor_generated_h
#error "MoviePipelineExecutor.generated.h already included, missing '#pragma once' in MoviePipelineExecutor.h"
#endif
#define MOVIERENDERPIPELINECORE_MoviePipelineExecutor_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_29_DELEGATE \
struct _Script_MovieRenderPipelineCore_eventMoviePipelineHttpResponseRecieved_Parms \
{ \
	int32 RequestIndex; \
	int32 ResponseCode; \
	FString Message; \
}; \
static inline void FMoviePipelineHttpResponseRecieved_DelegateWrapper(const FMulticastScriptDelegate& MoviePipelineHttpResponseRecieved, int32 RequestIndex, int32 ResponseCode, const FString& Message) \
{ \
	_Script_MovieRenderPipelineCore_eventMoviePipelineHttpResponseRecieved_Parms Parms; \
	Parms.RequestIndex=RequestIndex; \
	Parms.ResponseCode=ResponseCode; \
	Parms.Message=Message; \
	MoviePipelineHttpResponseRecieved.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_26_DELEGATE \
struct _Script_MovieRenderPipelineCore_eventMoviePipelineSocketMessageRecieved_Parms \
{ \
	FString Message; \
}; \
static inline void FMoviePipelineSocketMessageRecieved_DelegateWrapper(const FMulticastScriptDelegate& MoviePipelineSocketMessageRecieved, const FString& Message) \
{ \
	_Script_MovieRenderPipelineCore_eventMoviePipelineSocketMessageRecieved_Parms Parms; \
	Parms.Message=Message; \
	MoviePipelineSocketMessageRecieved.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_23_DELEGATE \
struct _Script_MovieRenderPipelineCore_eventOnMoviePipelineExecutorErrored_Parms \
{ \
	UMoviePipelineExecutorBase* PipelineExecutor; \
	UMoviePipeline* PipelineWithError; \
	bool bIsFatal; \
	FText ErrorText; \
}; \
static inline void FOnMoviePipelineExecutorErrored_DelegateWrapper(const FMulticastScriptDelegate& OnMoviePipelineExecutorErrored, UMoviePipelineExecutorBase* PipelineExecutor, UMoviePipeline* PipelineWithError, bool bIsFatal, const FText& ErrorText) \
{ \
	_Script_MovieRenderPipelineCore_eventOnMoviePipelineExecutorErrored_Parms Parms; \
	Parms.PipelineExecutor=PipelineExecutor; \
	Parms.PipelineWithError=PipelineWithError; \
	Parms.bIsFatal=bIsFatal ? true : false; \
	Parms.ErrorText=ErrorText; \
	OnMoviePipelineExecutorErrored.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_20_DELEGATE \
struct _Script_MovieRenderPipelineCore_eventOnMoviePipelineExecutorFinished_Parms \
{ \
	UMoviePipelineExecutorBase* PipelineExecutor; \
	bool bSuccess; \
}; \
static inline void FOnMoviePipelineExecutorFinished_DelegateWrapper(const FMulticastScriptDelegate& OnMoviePipelineExecutorFinished, UMoviePipelineExecutorBase* PipelineExecutor, bool bSuccess) \
{ \
	_Script_MovieRenderPipelineCore_eventOnMoviePipelineExecutorFinished_Parms Parms; \
	Parms.PipelineExecutor=PipelineExecutor; \
	Parms.bSuccess=bSuccess ? true : false; \
	OnMoviePipelineExecutorFinished.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_RPC_WRAPPERS \
	virtual void CancelAllJobs_Implementation(); \
	virtual void CancelCurrentJob_Implementation(); \
	virtual float GetStatusProgress_Implementation() const; \
	virtual void SetStatusProgress_Implementation(const float InProgress); \
	virtual FString GetStatusMessage_Implementation() const; \
	virtual void SetStatusMessage_Implementation(const FString& InStatus); \
	virtual void OnBeginFrame_Implementation(); \
	virtual bool IsRendering_Implementation() const; \
	virtual void Execute_Implementation(UMoviePipelineQueue* InPipelineQueue); \
 \
	DECLARE_FUNCTION(execSendHTTPRequest); \
	DECLARE_FUNCTION(execIsSocketConnected); \
	DECLARE_FUNCTION(execSendSocketMessage); \
	DECLARE_FUNCTION(execDisconnectSocket); \
	DECLARE_FUNCTION(execConnectSocket); \
	DECLARE_FUNCTION(execOnExecutorErroredImpl); \
	DECLARE_FUNCTION(execOnExecutorFinishedImpl); \
	DECLARE_FUNCTION(execSetMoviePipelineClass); \
	DECLARE_FUNCTION(execCancelAllJobs); \
	DECLARE_FUNCTION(execCancelCurrentJob); \
	DECLARE_FUNCTION(execGetStatusProgress); \
	DECLARE_FUNCTION(execSetStatusProgress); \
	DECLARE_FUNCTION(execGetStatusMessage); \
	DECLARE_FUNCTION(execSetStatusMessage); \
	DECLARE_FUNCTION(execOnBeginFrame); \
	DECLARE_FUNCTION(execIsRendering); \
	DECLARE_FUNCTION(execExecute);


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSendHTTPRequest); \
	DECLARE_FUNCTION(execIsSocketConnected); \
	DECLARE_FUNCTION(execSendSocketMessage); \
	DECLARE_FUNCTION(execDisconnectSocket); \
	DECLARE_FUNCTION(execConnectSocket); \
	DECLARE_FUNCTION(execOnExecutorErroredImpl); \
	DECLARE_FUNCTION(execOnExecutorFinishedImpl); \
	DECLARE_FUNCTION(execSetMoviePipelineClass); \
	DECLARE_FUNCTION(execCancelAllJobs); \
	DECLARE_FUNCTION(execCancelCurrentJob); \
	DECLARE_FUNCTION(execGetStatusProgress); \
	DECLARE_FUNCTION(execSetStatusProgress); \
	DECLARE_FUNCTION(execGetStatusMessage); \
	DECLARE_FUNCTION(execSetStatusMessage); \
	DECLARE_FUNCTION(execOnBeginFrame); \
	DECLARE_FUNCTION(execIsRendering); \
	DECLARE_FUNCTION(execExecute);


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_EVENT_PARMS \
	struct MoviePipelineExecutorBase_eventExecute_Parms \
	{ \
		UMoviePipelineQueue* InPipelineQueue; \
	}; \
	struct MoviePipelineExecutorBase_eventGetStatusMessage_Parms \
	{ \
		FString ReturnValue; \
	}; \
	struct MoviePipelineExecutorBase_eventGetStatusProgress_Parms \
	{ \
		float ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		MoviePipelineExecutorBase_eventGetStatusProgress_Parms() \
			: ReturnValue(0) \
		{ \
		} \
	}; \
	struct MoviePipelineExecutorBase_eventIsRendering_Parms \
	{ \
		bool ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		MoviePipelineExecutorBase_eventIsRendering_Parms() \
			: ReturnValue(false) \
		{ \
		} \
	}; \
	struct MoviePipelineExecutorBase_eventSetStatusMessage_Parms \
	{ \
		FString InStatus; \
	}; \
	struct MoviePipelineExecutorBase_eventSetStatusProgress_Parms \
	{ \
		float InProgress; \
	};


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_CALLBACK_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineExecutorBase(); \
	friend struct Z_Construct_UClass_UMoviePipelineExecutorBase_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineExecutorBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineExecutorBase)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineExecutorBase(); \
	friend struct Z_Construct_UClass_UMoviePipelineExecutorBase_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineExecutorBase, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineExecutorBase)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineExecutorBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineExecutorBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineExecutorBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineExecutorBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineExecutorBase(UMoviePipelineExecutorBase&&); \
	NO_API UMoviePipelineExecutorBase(const UMoviePipelineExecutorBase&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineExecutorBase(UMoviePipelineExecutorBase&&); \
	NO_API UMoviePipelineExecutorBase(const UMoviePipelineExecutorBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineExecutorBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineExecutorBase); \
	DEFINE_ABSTRACT_DEFAULT_CONSTRUCTOR_CALL(UMoviePipelineExecutorBase)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OnExecutorFinishedDelegate() { return STRUCT_OFFSET(UMoviePipelineExecutorBase, OnExecutorFinishedDelegate); } \
	FORCEINLINE static uint32 __PPO__OnExecutorErroredDelegate() { return STRUCT_OFFSET(UMoviePipelineExecutorBase, OnExecutorErroredDelegate); } \
	FORCEINLINE static uint32 __PPO__SocketMessageRecievedDelegate() { return STRUCT_OFFSET(UMoviePipelineExecutorBase, SocketMessageRecievedDelegate); } \
	FORCEINLINE static uint32 __PPO__HTTPResponseRecievedDelegate() { return STRUCT_OFFSET(UMoviePipelineExecutorBase, HTTPResponseRecievedDelegate); } \
	FORCEINLINE static uint32 __PPO__TargetPipelineClass() { return STRUCT_OFFSET(UMoviePipelineExecutorBase, TargetPipelineClass); }


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_43_PROLOG \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_EVENT_PARMS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_CALLBACK_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_CALLBACK_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h_46_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<class UMoviePipelineExecutorBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineExecutor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
