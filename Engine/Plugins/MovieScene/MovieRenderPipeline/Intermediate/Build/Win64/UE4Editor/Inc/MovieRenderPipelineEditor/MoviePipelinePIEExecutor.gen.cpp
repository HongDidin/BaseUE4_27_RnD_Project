// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MovieRenderPipelineEditor/Public/MoviePipelinePIEExecutor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePipelinePIEExecutor() {}
// Cross Module References
	MOVIERENDERPIPELINEEDITOR_API UFunction* Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MovieRenderPipelineEditor();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister();
	MOVIERENDERPIPELINEEDITOR_API UClass* Z_Construct_UClass_UMoviePipelinePIEExecutor_NoRegister();
	MOVIERENDERPIPELINEEDITOR_API UClass* Z_Construct_UClass_UMoviePipelinePIEExecutor();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineLinearExecutorBase();
	MOVIERENDERPIPELINECORE_API UFunction* Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature_Statics
	{
		struct _Script_MovieRenderPipelineEditor_eventOnMoviePipelineIndividualJobFinished_Parms
		{
			UMoviePipelineExecutorJob* FinishedJob;
			bool bSuccess;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FinishedJob;
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature_Statics::NewProp_FinishedJob = { "FinishedJob", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MovieRenderPipelineEditor_eventOnMoviePipelineIndividualJobFinished_Parms, FinishedJob), Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((_Script_MovieRenderPipelineEditor_eventOnMoviePipelineIndividualJobFinished_Parms*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MovieRenderPipelineEditor_eventOnMoviePipelineIndividualJobFinished_Parms), &Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature_Statics::NewProp_FinishedJob,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature_Statics::NewProp_bSuccess,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MoviePipelinePIEExecutor.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MovieRenderPipelineEditor, nullptr, "OnMoviePipelineIndividualJobFinished__DelegateSignature", nullptr, nullptr, sizeof(_Script_MovieRenderPipelineEditor_eventOnMoviePipelineIndividualJobFinished_Parms), Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	void UMoviePipelinePIEExecutor::StaticRegisterNativesUMoviePipelinePIEExecutor()
	{
	}
	UClass* Z_Construct_UClass_UMoviePipelinePIEExecutor_NoRegister()
	{
		return UMoviePipelinePIEExecutor::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnIndividualJobFinishedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnIndividualJobFinishedDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnIndividualJobWorkFinishedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnIndividualJobWorkFinishedDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnIndividualShotWorkFinishedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnIndividualShotWorkFinishedDelegate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMoviePipelineLinearExecutorBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n* This is the implementation responsible for executing the rendering of\n* multiple movie pipelines in the currently running Editor process. This\n* involves launching a Play in Editor session for each Movie Pipeline to\n* process.\n*/" },
		{ "IncludePath", "MoviePipelinePIEExecutor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MoviePipelinePIEExecutor.h" },
		{ "ToolTip", "This is the implementation responsible for executing the rendering of\nmultiple movie pipelines in the currently running Editor process. This\ninvolves launching a Play in Editor session for each Movie Pipeline to\nprocess." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::NewProp_OnIndividualJobFinishedDelegate_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "ModuleRelativePath", "Public/MoviePipelinePIEExecutor.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::NewProp_OnIndividualJobFinishedDelegate = { "OnIndividualJobFinishedDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelinePIEExecutor, OnIndividualJobFinishedDelegate), Z_Construct_UDelegateFunction_MovieRenderPipelineEditor_OnMoviePipelineIndividualJobFinished__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::NewProp_OnIndividualJobFinishedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::NewProp_OnIndividualJobFinishedDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::NewProp_OnIndividualJobWorkFinishedDelegate_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Called after each job is finished in the queue. Params struct contains an output of all files written. */" },
		{ "ModuleRelativePath", "Public/MoviePipelinePIEExecutor.h" },
		{ "ToolTip", "Called after each job is finished in the queue. Params struct contains an output of all files written." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::NewProp_OnIndividualJobWorkFinishedDelegate = { "OnIndividualJobWorkFinishedDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelinePIEExecutor, OnIndividualJobWorkFinishedDelegate), Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::NewProp_OnIndividualJobWorkFinishedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::NewProp_OnIndividualJobWorkFinishedDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::NewProp_OnIndividualShotWorkFinishedDelegate_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** \n\x09* Called after each shot is finished for a particular render. Params struct contains an output of files written for this shot. \n\x09* Only called if the UMoviePipeline is set up correctly, requires a flag in the output setting to be set. \n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelinePIEExecutor.h" },
		{ "ToolTip", "Called after each shot is finished for a particular render. Params struct contains an output of files written for this shot.\nOnly called if the UMoviePipeline is set up correctly, requires a flag in the output setting to be set." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::NewProp_OnIndividualShotWorkFinishedDelegate = { "OnIndividualShotWorkFinishedDelegate", nullptr, (EPropertyFlags)0x0040000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelinePIEExecutor, OnIndividualShotWorkFinishedDelegate), Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::NewProp_OnIndividualShotWorkFinishedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::NewProp_OnIndividualShotWorkFinishedDelegate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::NewProp_OnIndividualJobFinishedDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::NewProp_OnIndividualJobWorkFinishedDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::NewProp_OnIndividualShotWorkFinishedDelegate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelinePIEExecutor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::ClassParams = {
		&UMoviePipelinePIEExecutor::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelinePIEExecutor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelinePIEExecutor, 2583508661);
	template<> MOVIERENDERPIPELINEEDITOR_API UClass* StaticClass<UMoviePipelinePIEExecutor>()
	{
		return UMoviePipelinePIEExecutor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelinePIEExecutor(Z_Construct_UClass_UMoviePipelinePIEExecutor, &UMoviePipelinePIEExecutor::StaticClass, TEXT("/Script/MovieRenderPipelineEditor"), TEXT("UMoviePipelinePIEExecutor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelinePIEExecutor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
