// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MovieRenderPipelineCore/Public/MoviePipelineCameraSetting.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePipelineCameraSetting() {}
// Cross Module References
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineCameraSetting_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineCameraSetting();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineSetting();
	UPackage* Z_Construct_UPackage__Script_MovieRenderPipelineCore();
	MOVIERENDERPIPELINECORE_API UEnum* Z_Construct_UEnum_MovieRenderPipelineCore_EMoviePipelineShutterTiming();
// End Cross Module References
	void UMoviePipelineCameraSetting::StaticRegisterNativesUMoviePipelineCameraSetting()
	{
	}
	UClass* Z_Construct_UClass_UMoviePipelineCameraSetting_NoRegister()
	{
		return UMoviePipelineCameraSetting::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineCameraSetting_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ShutterTiming_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShutterTiming_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ShutterTiming;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OverscanPercentage_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_OverscanPercentage;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMoviePipelineSetting,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "MoviePipelineCameraSetting.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MoviePipelineCameraSetting.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::NewProp_ShutterTiming_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::NewProp_ShutterTiming_MetaData[] = {
		{ "Category", "Camera Settings" },
		{ "Comment", "/**\n\x09* Shutter Timing allows you to bias the timing of your shutter angle to either be before, during, or after\n\x09* a frame. When set to FrameClose, it means that the motion gathered up to produce frame N comes from \n\x09* before and right up to frame N. When set to FrameCenter, the motion represents half the time before the\n\x09* frame and half the time after the frame. When set to FrameOpen, the motion represents the time from \n\x09* Frame N onwards.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineCameraSetting.h" },
		{ "ToolTip", "Shutter Timing allows you to bias the timing of your shutter angle to either be before, during, or after\na frame. When set to FrameClose, it means that the motion gathered up to produce frame N comes from\nbefore and right up to frame N. When set to FrameCenter, the motion represents half the time before the\nframe and half the time after the frame. When set to FrameOpen, the motion represents the time from\nFrame N onwards." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::NewProp_ShutterTiming = { "ShutterTiming", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineCameraSetting, ShutterTiming), Z_Construct_UEnum_MovieRenderPipelineCore_EMoviePipelineShutterTiming, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::NewProp_ShutterTiming_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::NewProp_ShutterTiming_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::NewProp_OverscanPercentage_MetaData[] = {
		{ "Category", "Camera Settings" },
		{ "ClampMax", "1" },
		{ "ClampMin", "0" },
		{ "Comment", "/**\n\x09* Overscan percent allows to render additional pixels beyond the set resolution and can be used in conjunction \n\x09* with EXR file output to add post-processing effects such as lens distortion.\n\x09* Please note that using this feature might affect the results due to auto-exposure and other camera settings.\n\x09* On EXR this will produce a 1080p image with extra pixel data hidden around the outside edges for use \n\x09* in post production. For all other formats this will increase the final resolution and no pixels will be hidden \n\x09* (ie: 1080p /w 0.1 overscan will make a 2112x1188 jpg, but a 1080p exr /w 96/54 pixels hidden on each side)\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineCameraSetting.h" },
		{ "ToolTip", "Overscan percent allows to render additional pixels beyond the set resolution and can be used in conjunction\nwith EXR file output to add post-processing effects such as lens distortion.\nPlease note that using this feature might affect the results due to auto-exposure and other camera settings.\nOn EXR this will produce a 1080p image with extra pixel data hidden around the outside edges for use\nin post production. For all other formats this will increase the final resolution and no pixels will be hidden\n(ie: 1080p /w 0.1 overscan will make a 2112x1188 jpg, but a 1080p exr /w 96/54 pixels hidden on each side)" },
		{ "UIMax", "1" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::NewProp_OverscanPercentage = { "OverscanPercentage", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineCameraSetting, OverscanPercentage), METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::NewProp_OverscanPercentage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::NewProp_OverscanPercentage_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::NewProp_ShutterTiming_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::NewProp_ShutterTiming,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::NewProp_OverscanPercentage,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineCameraSetting>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::ClassParams = {
		&UMoviePipelineCameraSetting::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineCameraSetting()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineCameraSetting_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineCameraSetting, 2595721821);
	template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<UMoviePipelineCameraSetting>()
	{
		return UMoviePipelineCameraSetting::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineCameraSetting(Z_Construct_UClass_UMoviePipelineCameraSetting, &UMoviePipelineCameraSetting::StaticClass, TEXT("/Script/MovieRenderPipelineCore"), TEXT("UMoviePipelineCameraSetting"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineCameraSetting);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
