// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MovieRenderPipelineCore/Public/MoviePipelineDebugSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePipelineDebugSettings() {}
// Cross Module References
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineDebugSettings_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineDebugSettings();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineSetting();
	UPackage* Z_Construct_UPackage__Script_MovieRenderPipelineCore();
// End Cross Module References
	void UMoviePipelineDebugSettings::StaticRegisterNativesUMoviePipelineDebugSettings()
	{
	}
	UClass* Z_Construct_UClass_UMoviePipelineDebugSettings_NoRegister()
	{
		return UMoviePipelineDebugSettings::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineDebugSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWriteAllSamples_MetaData[];
#endif
		static void NewProp_bWriteAllSamples_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWriteAllSamples;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bCaptureFramesWithRenderDoc_MetaData[];
#endif
		static void NewProp_bCaptureFramesWithRenderDoc_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bCaptureFramesWithRenderDoc;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CaptureFrame_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CaptureFrame;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMoviePipelineSetting,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "MoviePipelineDebugSettings.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MoviePipelineDebugSettings.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_bWriteAllSamples_MetaData[] = {
		{ "Category", "Render Settings" },
		{ "Comment", "/**\n\x09* If true, we will write all samples that get generated to disk individually. This can be useful for debugging or if you need to accumulate\n\x09* render passes differently than provided.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineDebugSettings.h" },
		{ "ToolTip", "If true, we will write all samples that get generated to disk individually. This can be useful for debugging or if you need to accumulate\nrender passes differently than provided." },
	};
#endif
	void Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_bWriteAllSamples_SetBit(void* Obj)
	{
		((UMoviePipelineDebugSettings*)Obj)->bWriteAllSamples = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_bWriteAllSamples = { "bWriteAllSamples", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMoviePipelineDebugSettings), &Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_bWriteAllSamples_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_bWriteAllSamples_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_bWriteAllSamples_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_bCaptureFramesWithRenderDoc_MetaData[] = {
		{ "Category", "RenderDoc" },
		{ "Comment", "/** If true, automatically trigger RenderDoc to capture rendering information for frames from CaptureStartFrame to CaptureEndFrame, inclusive */" },
		{ "ModuleRelativePath", "Public/MoviePipelineDebugSettings.h" },
		{ "ToolTip", "If true, automatically trigger RenderDoc to capture rendering information for frames from CaptureStartFrame to CaptureEndFrame, inclusive" },
	};
#endif
	void Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_bCaptureFramesWithRenderDoc_SetBit(void* Obj)
	{
		((UMoviePipelineDebugSettings*)Obj)->bCaptureFramesWithRenderDoc = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_bCaptureFramesWithRenderDoc = { "bCaptureFramesWithRenderDoc", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMoviePipelineDebugSettings), &Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_bCaptureFramesWithRenderDoc_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_bCaptureFramesWithRenderDoc_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_bCaptureFramesWithRenderDoc_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_CaptureFrame_MetaData[] = {
		{ "Category", "RenderDoc" },
		{ "Comment", "/** Used when capturing rendering information with RenderDoc. In Display Rate frames.*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineDebugSettings.h" },
		{ "ToolTip", "Used when capturing rendering information with RenderDoc. In Display Rate frames." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_CaptureFrame = { "CaptureFrame", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineDebugSettings, CaptureFrame), METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_CaptureFrame_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_CaptureFrame_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_bWriteAllSamples,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_bCaptureFramesWithRenderDoc,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::NewProp_CaptureFrame,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineDebugSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::ClassParams = {
		&UMoviePipelineDebugSettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineDebugSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineDebugSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineDebugSettings, 814417246);
	template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<UMoviePipelineDebugSettings>()
	{
		return UMoviePipelineDebugSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineDebugSettings(Z_Construct_UClass_UMoviePipelineDebugSettings, &UMoviePipelineDebugSettings::StaticClass, TEXT("/Script/MovieRenderPipelineCore"), TEXT("UMoviePipelineDebugSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineDebugSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
