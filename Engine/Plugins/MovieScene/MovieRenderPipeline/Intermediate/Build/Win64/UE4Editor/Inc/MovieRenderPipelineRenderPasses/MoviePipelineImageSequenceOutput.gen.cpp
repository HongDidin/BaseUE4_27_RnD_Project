// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MovieRenderPipelineRenderPasses/Public/MoviePipelineImageSequenceOutput.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePipelineImageSequenceOutput() {}
// Cross Module References
	MOVIERENDERPIPELINERENDERPASSES_API UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase_NoRegister();
	MOVIERENDERPIPELINERENDERPASSES_API UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineOutputBase();
	UPackage* Z_Construct_UPackage__Script_MovieRenderPipelineRenderPasses();
	MOVIERENDERPIPELINERENDERPASSES_API UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutput_BMP_NoRegister();
	MOVIERENDERPIPELINERENDERPASSES_API UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutput_BMP();
	MOVIERENDERPIPELINERENDERPASSES_API UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutput_PNG_NoRegister();
	MOVIERENDERPIPELINERENDERPASSES_API UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutput_PNG();
	MOVIERENDERPIPELINERENDERPASSES_API UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutput_JPG_NoRegister();
	MOVIERENDERPIPELINERENDERPASSES_API UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutput_JPG();
// End Cross Module References
	void UMoviePipelineImageSequenceOutputBase::StaticRegisterNativesUMoviePipelineImageSequenceOutputBase()
	{
	}
	UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase_NoRegister()
	{
		return UMoviePipelineImageSequenceOutputBase::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMoviePipelineOutputBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineRenderPasses,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "MoviePipelineImageSequenceOutput.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MoviePipelineImageSequenceOutput.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineImageSequenceOutputBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase_Statics::ClassParams = {
		&UMoviePipelineImageSequenceOutputBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineImageSequenceOutputBase, 2313158719);
	template<> MOVIERENDERPIPELINERENDERPASSES_API UClass* StaticClass<UMoviePipelineImageSequenceOutputBase>()
	{
		return UMoviePipelineImageSequenceOutputBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineImageSequenceOutputBase(Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase, &UMoviePipelineImageSequenceOutputBase::StaticClass, TEXT("/Script/MovieRenderPipelineRenderPasses"), TEXT("UMoviePipelineImageSequenceOutputBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineImageSequenceOutputBase);
	void UMoviePipelineImageSequenceOutput_BMP::StaticRegisterNativesUMoviePipelineImageSequenceOutput_BMP()
	{
	}
	UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutput_BMP_NoRegister()
	{
		return UMoviePipelineImageSequenceOutput_BMP::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineImageSequenceOutput_BMP_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineImageSequenceOutput_BMP_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineRenderPasses,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineImageSequenceOutput_BMP_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MoviePipelineImageSequenceOutput.h" },
		{ "ModuleRelativePath", "Public/MoviePipelineImageSequenceOutput.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineImageSequenceOutput_BMP_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineImageSequenceOutput_BMP>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineImageSequenceOutput_BMP_Statics::ClassParams = {
		&UMoviePipelineImageSequenceOutput_BMP::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_BMP_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_BMP_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutput_BMP()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineImageSequenceOutput_BMP_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineImageSequenceOutput_BMP, 515772812);
	template<> MOVIERENDERPIPELINERENDERPASSES_API UClass* StaticClass<UMoviePipelineImageSequenceOutput_BMP>()
	{
		return UMoviePipelineImageSequenceOutput_BMP::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineImageSequenceOutput_BMP(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_BMP, &UMoviePipelineImageSequenceOutput_BMP::StaticClass, TEXT("/Script/MovieRenderPipelineRenderPasses"), TEXT("UMoviePipelineImageSequenceOutput_BMP"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineImageSequenceOutput_BMP);
	void UMoviePipelineImageSequenceOutput_PNG::StaticRegisterNativesUMoviePipelineImageSequenceOutput_PNG()
	{
	}
	UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutput_PNG_NoRegister()
	{
		return UMoviePipelineImageSequenceOutput_PNG::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineImageSequenceOutput_PNG_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineImageSequenceOutput_PNG_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineRenderPasses,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineImageSequenceOutput_PNG_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MoviePipelineImageSequenceOutput.h" },
		{ "ModuleRelativePath", "Public/MoviePipelineImageSequenceOutput.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineImageSequenceOutput_PNG_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineImageSequenceOutput_PNG>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineImageSequenceOutput_PNG_Statics::ClassParams = {
		&UMoviePipelineImageSequenceOutput_PNG::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_PNG_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_PNG_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutput_PNG()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineImageSequenceOutput_PNG_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineImageSequenceOutput_PNG, 1650074963);
	template<> MOVIERENDERPIPELINERENDERPASSES_API UClass* StaticClass<UMoviePipelineImageSequenceOutput_PNG>()
	{
		return UMoviePipelineImageSequenceOutput_PNG::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineImageSequenceOutput_PNG(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_PNG, &UMoviePipelineImageSequenceOutput_PNG::StaticClass, TEXT("/Script/MovieRenderPipelineRenderPasses"), TEXT("UMoviePipelineImageSequenceOutput_PNG"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineImageSequenceOutput_PNG);
	void UMoviePipelineImageSequenceOutput_JPG::StaticRegisterNativesUMoviePipelineImageSequenceOutput_JPG()
	{
	}
	UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutput_JPG_NoRegister()
	{
		return UMoviePipelineImageSequenceOutput_JPG::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineImageSequenceOutput_JPG_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineImageSequenceOutput_JPG_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMoviePipelineImageSequenceOutputBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineRenderPasses,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineImageSequenceOutput_JPG_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MoviePipelineImageSequenceOutput.h" },
		{ "ModuleRelativePath", "Public/MoviePipelineImageSequenceOutput.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineImageSequenceOutput_JPG_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineImageSequenceOutput_JPG>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineImageSequenceOutput_JPG_Statics::ClassParams = {
		&UMoviePipelineImageSequenceOutput_JPG::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_JPG_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_JPG_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineImageSequenceOutput_JPG()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineImageSequenceOutput_JPG_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineImageSequenceOutput_JPG, 3963106359);
	template<> MOVIERENDERPIPELINERENDERPASSES_API UClass* StaticClass<UMoviePipelineImageSequenceOutput_JPG>()
	{
		return UMoviePipelineImageSequenceOutput_JPG::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineImageSequenceOutput_JPG(Z_Construct_UClass_UMoviePipelineImageSequenceOutput_JPG, &UMoviePipelineImageSequenceOutput_JPG::StaticClass, TEXT("/Script/MovieRenderPipelineRenderPasses"), TEXT("UMoviePipelineImageSequenceOutput_JPG"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineImageSequenceOutput_JPG);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
