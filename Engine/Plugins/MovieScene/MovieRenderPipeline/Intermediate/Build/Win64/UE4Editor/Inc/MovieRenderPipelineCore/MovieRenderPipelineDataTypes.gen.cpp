// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MovieRenderPipelineCore/Public/MovieRenderPipelineDataTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMovieRenderPipelineDataTypes() {}
// Cross Module References
	MOVIERENDERPIPELINECORE_API UEnum* Z_Construct_UEnum_MovieRenderPipelineCore_EMoviePipelineShutterTiming();
	UPackage* Z_Construct_UPackage__Script_MovieRenderPipelineCore();
	MOVIERENDERPIPELINECORE_API UEnum* Z_Construct_UEnum_MovieRenderPipelineCore_EMovieRenderShotState();
	MOVIERENDERPIPELINECORE_API UEnum* Z_Construct_UEnum_MovieRenderPipelineCore_EMovieRenderPipelineState();
	MOVIERENDERPIPELINECORE_API UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelineOutputData();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipeline_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister();
	MOVIERENDERPIPELINECORE_API UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelineShotOutputData();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineExecutorShot_NoRegister();
	MOVIERENDERPIPELINECORE_API UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData();
	MOVIERENDERPIPELINECORE_API UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier();
	MOVIERENDERPIPELINECORE_API UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FDateTime();
	MOVIERENDERPIPELINECORE_API UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelineFormatArgs();
	MOVIERENDERPIPELINECORE_API UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelineCameraCutInfo();
	MOVIERENDERPIPELINECORE_API UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics();
// End Cross Module References
	static UEnum* EMoviePipelineShutterTiming_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MovieRenderPipelineCore_EMoviePipelineShutterTiming, Z_Construct_UPackage__Script_MovieRenderPipelineCore(), TEXT("EMoviePipelineShutterTiming"));
		}
		return Singleton;
	}
	template<> MOVIERENDERPIPELINECORE_API UEnum* StaticEnum<EMoviePipelineShutterTiming>()
	{
		return EMoviePipelineShutterTiming_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMoviePipelineShutterTiming(EMoviePipelineShutterTiming_StaticEnum, TEXT("/Script/MovieRenderPipelineCore"), TEXT("EMoviePipelineShutterTiming"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MovieRenderPipelineCore_EMoviePipelineShutterTiming_Hash() { return 3798194895U; }
	UEnum* Z_Construct_UEnum_MovieRenderPipelineCore_EMoviePipelineShutterTiming()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MovieRenderPipelineCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMoviePipelineShutterTiming"), 0, Get_Z_Construct_UEnum_MovieRenderPipelineCore_EMoviePipelineShutterTiming_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMoviePipelineShutterTiming::FrameOpen", (int64)EMoviePipelineShutterTiming::FrameOpen },
				{ "EMoviePipelineShutterTiming::FrameCenter", (int64)EMoviePipelineShutterTiming::FrameCenter },
				{ "EMoviePipelineShutterTiming::FrameClose", (int64)EMoviePipelineShutterTiming::FrameClose },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "FrameCenter.Name", "EMoviePipelineShutterTiming::FrameCenter" },
				{ "FrameClose.Name", "EMoviePipelineShutterTiming::FrameClose" },
				{ "FrameOpen.Name", "EMoviePipelineShutterTiming::FrameOpen" },
				{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
				nullptr,
				"EMoviePipelineShutterTiming",
				"EMoviePipelineShutterTiming",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMovieRenderShotState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MovieRenderPipelineCore_EMovieRenderShotState, Z_Construct_UPackage__Script_MovieRenderPipelineCore(), TEXT("EMovieRenderShotState"));
		}
		return Singleton;
	}
	template<> MOVIERENDERPIPELINECORE_API UEnum* StaticEnum<EMovieRenderShotState>()
	{
		return EMovieRenderShotState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMovieRenderShotState(EMovieRenderShotState_StaticEnum, TEXT("/Script/MovieRenderPipelineCore"), TEXT("EMovieRenderShotState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MovieRenderPipelineCore_EMovieRenderShotState_Hash() { return 2494045288U; }
	UEnum* Z_Construct_UEnum_MovieRenderPipelineCore_EMovieRenderShotState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MovieRenderPipelineCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMovieRenderShotState"), 0, Get_Z_Construct_UEnum_MovieRenderPipelineCore_EMovieRenderShotState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMovieRenderShotState::Uninitialized", (int64)EMovieRenderShotState::Uninitialized },
				{ "EMovieRenderShotState::WarmingUp", (int64)EMovieRenderShotState::WarmingUp },
				{ "EMovieRenderShotState::MotionBlur", (int64)EMovieRenderShotState::MotionBlur },
				{ "EMovieRenderShotState::Rendering", (int64)EMovieRenderShotState::Rendering },
				{ "EMovieRenderShotState::Finished", (int64)EMovieRenderShotState::Finished },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/**\n* What is the current state of a shot? States are processed in order from first to last but not\n* all states are required, ie: WarmUp and MotionBlur can be disabled and the shot will never\n* pass through this state.\n*/" },
				{ "Finished.Comment", "/*\n\x09* The shot has produced all frames it will produce. No more evaluation should be\n\x09* done for this shot once it reaches this state.\n\x09*/" },
				{ "Finished.Name", "EMovieRenderShotState::Finished" },
				{ "Finished.ToolTip", "* The shot has produced all frames it will produce. No more evaluation should be\n* done for this shot once it reaches this state." },
				{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
				{ "MotionBlur.Comment", "/*\n\x09* The shot is doing additional pre-roll for motion blur. No frames are being produced,\n\x09* but the rendering pipeline is being run to seed histories.\n\x09*/" },
				{ "MotionBlur.Name", "EMovieRenderShotState::MotionBlur" },
				{ "MotionBlur.ToolTip", "* The shot is doing additional pre-roll for motion blur. No frames are being produced,\n* but the rendering pipeline is being run to seed histories." },
				{ "Rendering.Comment", "/*\n\x09* The shot is working on producing frames and may be currently doing a sub-frame or\n\x09* a whole frame.\n\x09*/" },
				{ "Rendering.Name", "EMovieRenderShotState::Rendering" },
				{ "Rendering.ToolTip", "* The shot is working on producing frames and may be currently doing a sub-frame or\n* a whole frame." },
				{ "ToolTip", "What is the current state of a shot? States are processed in order from first to last but not\nall states are required, ie: WarmUp and MotionBlur can be disabled and the shot will never\npass through this state." },
				{ "Uninitialized.Comment", "/** The shot has not been initialized yet.*/" },
				{ "Uninitialized.Name", "EMovieRenderShotState::Uninitialized" },
				{ "Uninitialized.ToolTip", "The shot has not been initialized yet." },
				{ "WarmingUp.Comment", "/** The shot is warming up. Engine ticks are passing but no frames are being produced. */" },
				{ "WarmingUp.Name", "EMovieRenderShotState::WarmingUp" },
				{ "WarmingUp.ToolTip", "The shot is warming up. Engine ticks are passing but no frames are being produced." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
				nullptr,
				"EMovieRenderShotState",
				"EMovieRenderShotState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMovieRenderPipelineState_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MovieRenderPipelineCore_EMovieRenderPipelineState, Z_Construct_UPackage__Script_MovieRenderPipelineCore(), TEXT("EMovieRenderPipelineState"));
		}
		return Singleton;
	}
	template<> MOVIERENDERPIPELINECORE_API UEnum* StaticEnum<EMovieRenderPipelineState>()
	{
		return EMovieRenderPipelineState_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMovieRenderPipelineState(EMovieRenderPipelineState_StaticEnum, TEXT("/Script/MovieRenderPipelineCore"), TEXT("EMovieRenderPipelineState"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MovieRenderPipelineCore_EMovieRenderPipelineState_Hash() { return 2005131576U; }
	UEnum* Z_Construct_UEnum_MovieRenderPipelineCore_EMovieRenderPipelineState()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MovieRenderPipelineCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMovieRenderPipelineState"), 0, Get_Z_Construct_UEnum_MovieRenderPipelineCore_EMovieRenderPipelineState_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMovieRenderPipelineState::Uninitialized", (int64)EMovieRenderPipelineState::Uninitialized },
				{ "EMovieRenderPipelineState::ProducingFrames", (int64)EMovieRenderPipelineState::ProducingFrames },
				{ "EMovieRenderPipelineState::Finalize", (int64)EMovieRenderPipelineState::Finalize },
				{ "EMovieRenderPipelineState::Export", (int64)EMovieRenderPipelineState::Export },
				{ "EMovieRenderPipelineState::Finished", (int64)EMovieRenderPipelineState::Finished },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Comment", "/**\n* What is the current overall state of the Pipeline? States are processed in order from first to\n* last and all states will be hit (though there is no guarantee the state will not be transitioned\n* away from on the same frame it entered it). Used to help track overall progress and validate\n* code flow.\n*/" },
				{ "Export.Comment", "/** All outputs have finished writing to disk or otherwise processing. Additional exports that may have needed information about the produced file can now be run. */" },
				{ "Export.Name", "EMovieRenderPipelineState::Export" },
				{ "Export.ToolTip", "All outputs have finished writing to disk or otherwise processing. Additional exports that may have needed information about the produced file can now be run." },
				{ "Finalize.Comment", "/** All desired frames have been produced. Audio is already finalized. Outputs have a chance to finish long processing tasks. */" },
				{ "Finalize.Name", "EMovieRenderPipelineState::Finalize" },
				{ "Finalize.ToolTip", "All desired frames have been produced. Audio is already finalized. Outputs have a chance to finish long processing tasks." },
				{ "Finished.Comment", "/** The pipeline has been shut down. It is an error to shut it down again. */" },
				{ "Finished.Name", "EMovieRenderPipelineState::Finished" },
				{ "Finished.ToolTip", "The pipeline has been shut down. It is an error to shut it down again." },
				{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
				{ "ProducingFrames.Comment", "/** The pipeline has been initialized and is now controlling time and working on producing frames. */" },
				{ "ProducingFrames.Name", "EMovieRenderPipelineState::ProducingFrames" },
				{ "ProducingFrames.ToolTip", "The pipeline has been initialized and is now controlling time and working on producing frames." },
				{ "ToolTip", "What is the current overall state of the Pipeline? States are processed in order from first to\nlast and all states will be hit (though there is no guarantee the state will not be transitioned\naway from on the same frame it entered it). Used to help track overall progress and validate\ncode flow." },
				{ "Uninitialized.Comment", "/** The pipeline has not been initialized yet. Only valid operation is to call Initialize. */" },
				{ "Uninitialized.Name", "EMovieRenderPipelineState::Uninitialized" },
				{ "Uninitialized.ToolTip", "The pipeline has not been initialized yet. Only valid operation is to call Initialize." },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
				nullptr,
				"EMovieRenderPipelineState",
				"EMovieRenderPipelineState",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FMoviePipelineOutputData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MOVIERENDERPIPELINECORE_API uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineOutputData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMoviePipelineOutputData, Z_Construct_UPackage__Script_MovieRenderPipelineCore(), TEXT("MoviePipelineOutputData"), sizeof(FMoviePipelineOutputData), Get_Z_Construct_UScriptStruct_FMoviePipelineOutputData_Hash());
	}
	return Singleton;
}
template<> MOVIERENDERPIPELINECORE_API UScriptStruct* StaticStruct<FMoviePipelineOutputData>()
{
	return FMoviePipelineOutputData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMoviePipelineOutputData(FMoviePipelineOutputData::StaticStruct, TEXT("/Script/MovieRenderPipelineCore"), TEXT("MoviePipelineOutputData"), false, nullptr, nullptr);
static struct FScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineOutputData
{
	FScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineOutputData()
	{
		UScriptStruct::DeferCppStructOps<FMoviePipelineOutputData>(FName(TEXT("MoviePipelineOutputData")));
	}
} ScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineOutputData;
	struct Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pipeline_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Pipeline;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Job_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Job;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSuccess_MetaData[];
#endif
		static void NewProp_bSuccess_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSuccess;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ShotData_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShotData_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ShotData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n* Contains information about the to-disk output generated by a movie pipeline. This structure is used both for per-shot work finished\n* callbacks and for the final render finished callback. When used as a per-shot callback ShotData will only have one entry (for the\n* shot that was just finished), and for the final render callback it will have data for all shots that managed to render. Can be empty\n* if the job failed to produce any files.\n*/" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "Contains information about the to-disk output generated by a movie pipeline. This structure is used both for per-shot work finished\ncallbacks and for the final render finished callback. When used as a per-shot callback ShotData will only have one entry (for the\nshot that was just finished), and for the final render callback it will have data for all shots that managed to render. Can be empty\nif the job failed to produce any files." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMoviePipelineOutputData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_Pipeline_MetaData[] = {
		{ "Category", "Movie Pipeline" },
		{ "Comment", "/** \n\x09* The UMoviePipeline instance that generated this data. This is only provided as an id (in the event you were the one who created\n\x09* the UMoviePipeline instance. DO NOT CALL FUNCTIONS ON THIS (unless you know what you're doing)\n\x09*\n\x09* Provided here for backwards compatibility.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "The UMoviePipeline instance that generated this data. This is only provided as an id (in the event you were the one who created\nthe UMoviePipeline instance. DO NOT CALL FUNCTIONS ON THIS (unless you know what you're doing)\n\nProvided here for backwards compatibility." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_Pipeline = { "Pipeline", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineOutputData, Pipeline), Z_Construct_UClass_UMoviePipeline_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_Pipeline_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_Pipeline_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_Job_MetaData[] = {
		{ "Category", "Movie Pipeline" },
		{ "Comment", "/** Job the data is for. Job may still be in progress (if a shot callback) so be careful about modifying properties on it */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "Job the data is for. Job may still be in progress (if a shot callback) so be careful about modifying properties on it" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_Job = { "Job", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineOutputData, Job), Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_Job_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_Job_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_bSuccess_MetaData[] = {
		{ "Category", "Movie Pipeline" },
		{ "Comment", "/** Did the job succeed, or was it canceled early due to an error (such as failure to write file to disk)? */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "Did the job succeed, or was it canceled early due to an error (such as failure to write file to disk)?" },
	};
#endif
	void Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_bSuccess_SetBit(void* Obj)
	{
		((FMoviePipelineOutputData*)Obj)->bSuccess = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_bSuccess = { "bSuccess", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMoviePipelineOutputData), &Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_bSuccess_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_bSuccess_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_bSuccess_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_ShotData_Inner = { "ShotData", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMoviePipelineShotOutputData, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_ShotData_MetaData[] = {
		{ "Category", "Movie Pipeline" },
		{ "Comment", "/** \n\x09* The file data for each shot that was rendered. If no files were written this will be empty. If this is from the per-shot work\n\x09* finished callback it will only have one entry (for the just finished shot). Will not include shots that did not get rendered\n\x09* due to the pipeline encountering an error.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "The file data for each shot that was rendered. If no files were written this will be empty. If this is from the per-shot work\nfinished callback it will only have one entry (for the just finished shot). Will not include shots that did not get rendered\ndue to the pipeline encountering an error." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_ShotData = { "ShotData", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineOutputData, ShotData), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_ShotData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_ShotData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_Pipeline,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_Job,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_bSuccess,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_ShotData_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::NewProp_ShotData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
		nullptr,
		&NewStructOps,
		"MoviePipelineOutputData",
		sizeof(FMoviePipelineOutputData),
		alignof(FMoviePipelineOutputData),
		Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelineOutputData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineOutputData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MovieRenderPipelineCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MoviePipelineOutputData"), sizeof(FMoviePipelineOutputData), Get_Z_Construct_UScriptStruct_FMoviePipelineOutputData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineOutputData_Hash() { return 520954498U; }
class UScriptStruct* FMoviePipelineShotOutputData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MOVIERENDERPIPELINECORE_API uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMoviePipelineShotOutputData, Z_Construct_UPackage__Script_MovieRenderPipelineCore(), TEXT("MoviePipelineShotOutputData"), sizeof(FMoviePipelineShotOutputData), Get_Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Hash());
	}
	return Singleton;
}
template<> MOVIERENDERPIPELINECORE_API UScriptStruct* StaticStruct<FMoviePipelineShotOutputData>()
{
	return FMoviePipelineShotOutputData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMoviePipelineShotOutputData(FMoviePipelineShotOutputData::StaticStruct, TEXT("/Script/MovieRenderPipelineCore"), TEXT("MoviePipelineShotOutputData"), false, nullptr, nullptr);
static struct FScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineShotOutputData
{
	FScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineShotOutputData()
	{
		UScriptStruct::DeferCppStructOps<FMoviePipelineShotOutputData>(FName(TEXT("MoviePipelineShotOutputData")));
	}
} ScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineShotOutputData;
	struct Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Shot_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_Shot;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RenderPassData_ValueProp;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RenderPassData_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderPassData_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_RenderPassData;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMoviePipelineShotOutputData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::NewProp_Shot_MetaData[] = {
		{ "Category", "Movie Pipeline" },
		{ "Comment", "/** Which shot was this output data for? */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "Which shot was this output data for?" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::NewProp_Shot = { "Shot", nullptr, (EPropertyFlags)0x0014000000020015, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineShotOutputData, Shot), Z_Construct_UClass_UMoviePipelineExecutorShot_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::NewProp_Shot_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::NewProp_Shot_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::NewProp_RenderPassData_ValueProp = { "RenderPassData", nullptr, (EPropertyFlags)0x0000000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::NewProp_RenderPassData_Key_KeyProp = { "RenderPassData_Key", nullptr, (EPropertyFlags)0x0000000000020001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::NewProp_RenderPassData_MetaData[] = {
		{ "Category", "Movie Pipeline" },
		{ "Comment", "/** \n\x09* A mapping between render passes (such as 'FinalImage') and an array containing the files written for that shot.\n\x09* Will be multiple files if using image sequences.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "A mapping between render passes (such as 'FinalImage') and an array containing the files written for that shot.\nWill be multiple files if using image sequences." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::NewProp_RenderPassData = { "RenderPassData", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineShotOutputData, RenderPassData), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::NewProp_RenderPassData_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::NewProp_RenderPassData_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::NewProp_Shot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::NewProp_RenderPassData_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::NewProp_RenderPassData_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::NewProp_RenderPassData,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
		nullptr,
		&NewStructOps,
		"MoviePipelineShotOutputData",
		sizeof(FMoviePipelineShotOutputData),
		alignof(FMoviePipelineShotOutputData),
		Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelineShotOutputData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MovieRenderPipelineCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MoviePipelineShotOutputData"), sizeof(FMoviePipelineShotOutputData), Get_Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Hash() { return 617122903U; }
class UScriptStruct* FMoviePipelineRenderPassOutputData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MOVIERENDERPIPELINECORE_API uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData, Z_Construct_UPackage__Script_MovieRenderPipelineCore(), TEXT("MoviePipelineRenderPassOutputData"), sizeof(FMoviePipelineRenderPassOutputData), Get_Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Hash());
	}
	return Singleton;
}
template<> MOVIERENDERPIPELINECORE_API UScriptStruct* StaticStruct<FMoviePipelineRenderPassOutputData>()
{
	return FMoviePipelineRenderPassOutputData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMoviePipelineRenderPassOutputData(FMoviePipelineRenderPassOutputData::StaticStruct, TEXT("/Script/MovieRenderPipelineCore"), TEXT("MoviePipelineRenderPassOutputData"), false, nullptr, nullptr);
static struct FScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineRenderPassOutputData
{
	FScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineRenderPassOutputData()
	{
		UScriptStruct::DeferCppStructOps<FMoviePipelineRenderPassOutputData>(FName(TEXT("MoviePipelineRenderPassOutputData")));
	}
} ScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineRenderPassOutputData;
	struct Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FilePaths_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilePaths_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FilePaths;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMoviePipelineRenderPassOutputData>();
	}
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics::NewProp_FilePaths_Inner = { "FilePaths", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics::NewProp_FilePaths_MetaData[] = {
		{ "Category", "Movie Pipeline" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics::NewProp_FilePaths = { "FilePaths", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineRenderPassOutputData, FilePaths), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics::NewProp_FilePaths_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics::NewProp_FilePaths_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics::NewProp_FilePaths_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics::NewProp_FilePaths,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
		nullptr,
		&NewStructOps,
		"MoviePipelineRenderPassOutputData",
		sizeof(FMoviePipelineRenderPassOutputData),
		alignof(FMoviePipelineRenderPassOutputData),
		Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MovieRenderPipelineCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MoviePipelineRenderPassOutputData"), sizeof(FMoviePipelineRenderPassOutputData), Get_Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Hash() { return 4181376451U; }
class UScriptStruct* FMoviePipelineFilenameResolveParams::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MOVIERENDERPIPELINECORE_API uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams, Z_Construct_UPackage__Script_MovieRenderPipelineCore(), TEXT("MoviePipelineFilenameResolveParams"), sizeof(FMoviePipelineFilenameResolveParams), Get_Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Hash());
	}
	return Singleton;
}
template<> MOVIERENDERPIPELINECORE_API UScriptStruct* StaticStruct<FMoviePipelineFilenameResolveParams>()
{
	return FMoviePipelineFilenameResolveParams::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMoviePipelineFilenameResolveParams(FMoviePipelineFilenameResolveParams::StaticStruct, TEXT("/Script/MovieRenderPipelineCore"), TEXT("MoviePipelineFilenameResolveParams"), false, nullptr, nullptr);
static struct FScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineFilenameResolveParams
{
	FScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineFilenameResolveParams()
	{
		UScriptStruct::DeferCppStructOps<FMoviePipelineFilenameResolveParams>(FName(TEXT("MoviePipelineFilenameResolveParams")));
	}
} ScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineFilenameResolveParams;
	struct Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameNumber_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FrameNumber;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameNumberShot_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FrameNumberShot;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameNumberRel_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FrameNumberRel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FrameNumberShotRel_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_FrameNumberShotRel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraNameOverride_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_CameraNameOverride;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShotNameOverride_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ShotNameOverride;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ZeroPadFrameNumberCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ZeroPadFrameNumberCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bForceRelativeFrameNumbers_MetaData[];
#endif
		static void NewProp_bForceRelativeFrameNumbers_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bForceRelativeFrameNumbers;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileNameFormatOverrides_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileNameFormatOverrides_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileNameFormatOverrides_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_FileNameFormatOverrides;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileMetadata_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileMetadata_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileMetadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_FileMetadata;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitializationTime_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InitializationTime;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitializationVersion_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_InitializationVersion;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Job_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Job;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShotOverride_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ShotOverride;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdditionalFrameNumberOffset_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_AdditionalFrameNumberOffset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMoviePipelineFilenameResolveParams>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumber_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Frame Number for the Master (matching what you see in the Sequencer timeline. ie: If the Sequence PlaybackRange starts on 50, this value would be 50 on the first frame.*/" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "Frame Number for the Master (matching what you see in the Sequencer timeline. ie: If the Sequence PlaybackRange starts on 50, this value would be 50 on the first frame." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumber = { "FrameNumber", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFilenameResolveParams, FrameNumber), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumber_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumber_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumberShot_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Frame Number for the Shot (matching what you would see in Sequencer at the sub-sequence level. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "Frame Number for the Shot (matching what you would see in Sequencer at the sub-sequence level." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumberShot = { "FrameNumberShot", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFilenameResolveParams, FrameNumberShot), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumberShot_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumberShot_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumberRel_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Frame Number for the Master (relative to 0, not what you would see in the Sequencer timeline. ie: If sequence PlaybackRange starts on 50, this value would be 0 on the first frame. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "Frame Number for the Master (relative to 0, not what you would see in the Sequencer timeline. ie: If sequence PlaybackRange starts on 50, this value would be 0 on the first frame." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumberRel = { "FrameNumberRel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFilenameResolveParams, FrameNumberRel), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumberRel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumberRel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumberShotRel_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Frame Number for the Shot (relative to 0, not what you would see in the Sequencer timeline. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "Frame Number for the Shot (relative to 0, not what you would see in the Sequencer timeline." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumberShotRel = { "FrameNumberShotRel", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFilenameResolveParams, FrameNumberShotRel), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumberShotRel_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumberShotRel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_CameraNameOverride_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Name used by the {camera_name} format tag. If specified, this will override the camera name (which is normally pulled from the ShotOverride object). */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "Name used by the {camera_name} format tag. If specified, this will override the camera name (which is normally pulled from the ShotOverride object)." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_CameraNameOverride = { "CameraNameOverride", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFilenameResolveParams, CameraNameOverride), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_CameraNameOverride_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_CameraNameOverride_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_ShotNameOverride_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Name used by the {shot_name} format tag. If specified, this will override the shot name (which is normally pulled from the ShotOverride object) */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "Name used by the {shot_name} format tag. If specified, this will override the shot name (which is normally pulled from the ShotOverride object)" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_ShotNameOverride = { "ShotNameOverride", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFilenameResolveParams, ShotNameOverride), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_ShotNameOverride_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_ShotNameOverride_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_ZeroPadFrameNumberCount_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** When converitng frame numbers to strings, how many digits should we pad them up to? ie: 5 => 0005 with a count of 4. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "When converitng frame numbers to strings, how many digits should we pad them up to? ie: 5 => 0005 with a count of 4." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_ZeroPadFrameNumberCount = { "ZeroPadFrameNumberCount", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFilenameResolveParams, ZeroPadFrameNumberCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_ZeroPadFrameNumberCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_ZeroPadFrameNumberCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_bForceRelativeFrameNumbers_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** If true, force format strings (like {frame_number}) to resolve using the relative version. Used when slow-mo is detected as frame numbers would overlap. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "If true, force format strings (like {frame_number}) to resolve using the relative version. Used when slow-mo is detected as frame numbers would overlap." },
	};
#endif
	void Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_bForceRelativeFrameNumbers_SetBit(void* Obj)
	{
		((FMoviePipelineFilenameResolveParams*)Obj)->bForceRelativeFrameNumbers = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_bForceRelativeFrameNumbers = { "bForceRelativeFrameNumbers", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(FMoviePipelineFilenameResolveParams), &Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_bForceRelativeFrameNumbers_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_bForceRelativeFrameNumbers_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_bForceRelativeFrameNumbers_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileNameFormatOverrides_ValueProp = { "FileNameFormatOverrides", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileNameFormatOverrides_Key_KeyProp = { "FileNameFormatOverrides_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileNameFormatOverrides_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** \n\x09* A map between \"{format}\" tokens and their values. These are applied after the auto-generated ones from the system,\n\x09* which allows the caller to override things like {.ext} depending or {render_pass} which have dummy names by default.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "A map between \"{format}\" tokens and their values. These are applied after the auto-generated ones from the system,\nwhich allows the caller to override things like {.ext} depending or {render_pass} which have dummy names by default." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileNameFormatOverrides = { "FileNameFormatOverrides", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFilenameResolveParams, FileNameFormatOverrides), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileNameFormatOverrides_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileNameFormatOverrides_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileMetadata_ValueProp = { "FileMetadata", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileMetadata_Key_KeyProp = { "FileMetadata_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileMetadata_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** A key/value pair that maps metadata names to their values. Output is only supported in exr formats at the moment. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "A key/value pair that maps metadata names to their values. Output is only supported in exr formats at the moment." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileMetadata = { "FileMetadata", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFilenameResolveParams, FileMetadata), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileMetadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileMetadata_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_InitializationTime_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** The initialization time for this job. Used to resolve time-based format arguments. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "The initialization time for this job. Used to resolve time-based format arguments." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_InitializationTime = { "InitializationTime", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFilenameResolveParams, InitializationTime), Z_Construct_UScriptStruct_FDateTime, METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_InitializationTime_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_InitializationTime_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_InitializationVersion_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** The version for this job. Used to resolve version format arguments. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "The version for this job. Used to resolve version format arguments." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_InitializationVersion = { "InitializationVersion", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFilenameResolveParams, InitializationVersion), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_InitializationVersion_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_InitializationVersion_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_Job_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Required. This is the job all of the settings should be pulled from.*/" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "Required. This is the job all of the settings should be pulled from." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_Job = { "Job", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFilenameResolveParams, Job), Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_Job_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_Job_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_ShotOverride_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Optional. If specified, settings will be pulled from this shot (if overriden by the shot). If null, always use the master configuration in the job. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "Optional. If specified, settings will be pulled from this shot (if overriden by the shot). If null, always use the master configuration in the job." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_ShotOverride = { "ShotOverride", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFilenameResolveParams, ShotOverride), Z_Construct_UClass_UMoviePipelineExecutorShot_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_ShotOverride_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_ShotOverride_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_AdditionalFrameNumberOffset_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Additional offset added onto the offset provided by the Output Settings in the Job. Required for some internal things (FCPXML). */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "Additional offset added onto the offset provided by the Output Settings in the Job. Required for some internal things (FCPXML)." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_AdditionalFrameNumberOffset = { "AdditionalFrameNumberOffset", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFilenameResolveParams, AdditionalFrameNumberOffset), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_AdditionalFrameNumberOffset_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_AdditionalFrameNumberOffset_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumber,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumberShot,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumberRel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FrameNumberShotRel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_CameraNameOverride,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_ShotNameOverride,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_ZeroPadFrameNumberCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_bForceRelativeFrameNumbers,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileNameFormatOverrides_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileNameFormatOverrides_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileNameFormatOverrides,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileMetadata_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileMetadata_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_FileMetadata,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_InitializationTime,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_InitializationVersion,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_Job,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_ShotOverride,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::NewProp_AdditionalFrameNumberOffset,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
		nullptr,
		&NewStructOps,
		"MoviePipelineFilenameResolveParams",
		sizeof(FMoviePipelineFilenameResolveParams),
		alignof(FMoviePipelineFilenameResolveParams),
		Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MovieRenderPipelineCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MoviePipelineFilenameResolveParams"), sizeof(FMoviePipelineFilenameResolveParams), Get_Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Hash() { return 268036140U; }
class UScriptStruct* FMoviePipelineFormatArgs::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MOVIERENDERPIPELINECORE_API uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMoviePipelineFormatArgs, Z_Construct_UPackage__Script_MovieRenderPipelineCore(), TEXT("MoviePipelineFormatArgs"), sizeof(FMoviePipelineFormatArgs), Get_Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Hash());
	}
	return Singleton;
}
template<> MOVIERENDERPIPELINECORE_API UScriptStruct* StaticStruct<FMoviePipelineFormatArgs>()
{
	return FMoviePipelineFormatArgs::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMoviePipelineFormatArgs(FMoviePipelineFormatArgs::StaticStruct, TEXT("/Script/MovieRenderPipelineCore"), TEXT("MoviePipelineFormatArgs"), false, nullptr, nullptr);
static struct FScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineFormatArgs
{
	FScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineFormatArgs()
	{
		UScriptStruct::DeferCppStructOps<FMoviePipelineFormatArgs>(FName(TEXT("MoviePipelineFormatArgs")));
	}
} ScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineFormatArgs;
	struct Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FilenameArguments_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FilenameArguments_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FilenameArguments_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_FilenameArguments;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileMetadata_ValueProp;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileMetadata_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileMetadata_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_FileMetadata;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InJob_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InJob;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMoviePipelineFormatArgs>();
	}
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FilenameArguments_ValueProp = { "FilenameArguments", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FilenameArguments_Key_KeyProp = { "FilenameArguments_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FilenameArguments_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** A set of Key/Value pairs for output filename format strings (without {}) and their values. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "A set of Key/Value pairs for output filename format strings (without {}) and their values." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FilenameArguments = { "FilenameArguments", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFormatArgs, FilenameArguments), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FilenameArguments_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FilenameArguments_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FileMetadata_ValueProp = { "FileMetadata", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FileMetadata_Key_KeyProp = { "FileMetadata_Key", nullptr, (EPropertyFlags)0x0000000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FileMetadata_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** A set of Key/Value pairs for file metadata for file formats that support metadata. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "A set of Key/Value pairs for file metadata for file formats that support metadata." },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FileMetadata = { "FileMetadata", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFormatArgs, FileMetadata), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FileMetadata_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FileMetadata_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_InJob_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Which job is this for? Some settings are specific to the level sequence being rendered. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "Which job is this for? Some settings are specific to the level sequence being rendered." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_InJob = { "InJob", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineFormatArgs, InJob), Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_InJob_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_InJob_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FilenameArguments_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FilenameArguments_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FilenameArguments,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FileMetadata_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FileMetadata_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_FileMetadata,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::NewProp_InJob,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
		nullptr,
		&NewStructOps,
		"MoviePipelineFormatArgs",
		sizeof(FMoviePipelineFormatArgs),
		alignof(FMoviePipelineFormatArgs),
		Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelineFormatArgs()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MovieRenderPipelineCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MoviePipelineFormatArgs"), sizeof(FMoviePipelineFormatArgs), Get_Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Hash() { return 1541473679U; }
class UScriptStruct* FMoviePipelineCameraCutInfo::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MOVIERENDERPIPELINECORE_API uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineCameraCutInfo_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMoviePipelineCameraCutInfo, Z_Construct_UPackage__Script_MovieRenderPipelineCore(), TEXT("MoviePipelineCameraCutInfo"), sizeof(FMoviePipelineCameraCutInfo), Get_Z_Construct_UScriptStruct_FMoviePipelineCameraCutInfo_Hash());
	}
	return Singleton;
}
template<> MOVIERENDERPIPELINECORE_API UScriptStruct* StaticStruct<FMoviePipelineCameraCutInfo>()
{
	return FMoviePipelineCameraCutInfo::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMoviePipelineCameraCutInfo(FMoviePipelineCameraCutInfo::StaticStruct, TEXT("/Script/MovieRenderPipelineCore"), TEXT("MoviePipelineCameraCutInfo"), false, nullptr, nullptr);
static struct FScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineCameraCutInfo
{
	FScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineCameraCutInfo()
	{
		UScriptStruct::DeferCppStructOps<FMoviePipelineCameraCutInfo>(FName(TEXT("MoviePipelineCameraCutInfo")));
	}
} ScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineCameraCutInfo;
	struct Z_Construct_UScriptStruct_FMoviePipelineCameraCutInfo_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineCameraCutInfo_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "// ToDo: Rename this to segment.\n" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "ToDo: Rename this to segment." },
	};
#endif
	void* Z_Construct_UScriptStruct_FMoviePipelineCameraCutInfo_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMoviePipelineCameraCutInfo>();
	}
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMoviePipelineCameraCutInfo_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
		nullptr,
		&NewStructOps,
		"MoviePipelineCameraCutInfo",
		sizeof(FMoviePipelineCameraCutInfo),
		alignof(FMoviePipelineCameraCutInfo),
		nullptr,
		0,
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineCameraCutInfo_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineCameraCutInfo_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelineCameraCutInfo()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineCameraCutInfo_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MovieRenderPipelineCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MoviePipelineCameraCutInfo"), sizeof(FMoviePipelineCameraCutInfo), Get_Z_Construct_UScriptStruct_FMoviePipelineCameraCutInfo_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMoviePipelineCameraCutInfo_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineCameraCutInfo_Hash() { return 3069711770U; }
class UScriptStruct* FMoviePipelineSegmentWorkMetrics::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MOVIERENDERPIPELINECORE_API uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics, Z_Construct_UPackage__Script_MovieRenderPipelineCore(), TEXT("MoviePipelineSegmentWorkMetrics"), sizeof(FMoviePipelineSegmentWorkMetrics), Get_Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Hash());
	}
	return Singleton;
}
template<> MOVIERENDERPIPELINECORE_API UScriptStruct* StaticStruct<FMoviePipelineSegmentWorkMetrics>()
{
	return FMoviePipelineSegmentWorkMetrics::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMoviePipelineSegmentWorkMetrics(FMoviePipelineSegmentWorkMetrics::StaticStruct, TEXT("/Script/MovieRenderPipelineCore"), TEXT("MoviePipelineSegmentWorkMetrics"), false, nullptr, nullptr);
static struct FScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineSegmentWorkMetrics
{
	FScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineSegmentWorkMetrics()
	{
		UScriptStruct::DeferCppStructOps<FMoviePipelineSegmentWorkMetrics>(FName(TEXT("MoviePipelineSegmentWorkMetrics")));
	}
} ScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelineSegmentWorkMetrics;
	struct Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SegmentName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SegmentName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputFrameIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OutputFrameIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TotalOutputFrameCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TotalOutputFrameCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutputSubSampleIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_OutputSubSampleIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TotalSubSampleCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TotalSubSampleCount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EngineWarmUpFrameIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_EngineWarmUpFrameIndex;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TotalEngineWarmUpFrameCount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_TotalEngineWarmUpFrameCount;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMoviePipelineSegmentWorkMetrics>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_SegmentName_MetaData[] = {
		{ "Category", "Pipeline Segment" },
		{ "Comment", "/** The name of the segment (if any) */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "The name of the segment (if any)" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_SegmentName = { "SegmentName", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineSegmentWorkMetrics, SegmentName), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_SegmentName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_SegmentName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_OutputFrameIndex_MetaData[] = {
		{ "Category", "Pipeline Segment" },
		{ "Comment", "/** Index of the output frame we are working on right now. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "Index of the output frame we are working on right now." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_OutputFrameIndex = { "OutputFrameIndex", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineSegmentWorkMetrics, OutputFrameIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_OutputFrameIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_OutputFrameIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_TotalOutputFrameCount_MetaData[] = {
		{ "Category", "Pipeline Segment" },
		{ "Comment", "/** The number of output frames we expect to make for this segment. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "The number of output frames we expect to make for this segment." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_TotalOutputFrameCount = { "TotalOutputFrameCount", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineSegmentWorkMetrics, TotalOutputFrameCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_TotalOutputFrameCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_TotalOutputFrameCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_OutputSubSampleIndex_MetaData[] = {
		{ "Category", "Pipeline Segment" },
		{ "Comment", "/** Which temporal/spatial sub sample are we working on right now. This counts temporal, spatial, and tile samples to accurately reflect how much work is needed for this output frame. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "Which temporal/spatial sub sample are we working on right now. This counts temporal, spatial, and tile samples to accurately reflect how much work is needed for this output frame." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_OutputSubSampleIndex = { "OutputSubSampleIndex", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineSegmentWorkMetrics, OutputSubSampleIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_OutputSubSampleIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_OutputSubSampleIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_TotalSubSampleCount_MetaData[] = {
		{ "Category", "Pipeline Segment" },
		{ "Comment", "/** The total number of samples we will have to build to render this output frame. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "The total number of samples we will have to build to render this output frame." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_TotalSubSampleCount = { "TotalSubSampleCount", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineSegmentWorkMetrics, TotalSubSampleCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_TotalSubSampleCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_TotalSubSampleCount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_EngineWarmUpFrameIndex_MetaData[] = {
		{ "Category", "Pipeline Segment" },
		{ "Comment", "/** The index of the engine warm up frame that we are working on. No rendering samples are produced for these. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "The index of the engine warm up frame that we are working on. No rendering samples are produced for these." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_EngineWarmUpFrameIndex = { "EngineWarmUpFrameIndex", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineSegmentWorkMetrics, EngineWarmUpFrameIndex), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_EngineWarmUpFrameIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_EngineWarmUpFrameIndex_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_TotalEngineWarmUpFrameCount_MetaData[] = {
		{ "Category", "Pipeline Segment" },
		{ "Comment", "/** The total number of engine warm up frames for this segment. */" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
		{ "ToolTip", "The total number of engine warm up frames for this segment." },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_TotalEngineWarmUpFrameCount = { "TotalEngineWarmUpFrameCount", nullptr, (EPropertyFlags)0x0010000000020015, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelineSegmentWorkMetrics, TotalEngineWarmUpFrameCount), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_TotalEngineWarmUpFrameCount_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_TotalEngineWarmUpFrameCount_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_SegmentName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_OutputFrameIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_TotalOutputFrameCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_OutputSubSampleIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_TotalSubSampleCount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_EngineWarmUpFrameIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::NewProp_TotalEngineWarmUpFrameCount,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
		nullptr,
		&NewStructOps,
		"MoviePipelineSegmentWorkMetrics",
		sizeof(FMoviePipelineSegmentWorkMetrics),
		alignof(FMoviePipelineSegmentWorkMetrics),
		Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MovieRenderPipelineCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MoviePipelineSegmentWorkMetrics"), sizeof(FMoviePipelineSegmentWorkMetrics), Get_Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Hash() { return 3538751313U; }
class UScriptStruct* FMoviePipelinePassIdentifier::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MOVIERENDERPIPELINECORE_API uint32 Get_Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier, Z_Construct_UPackage__Script_MovieRenderPipelineCore(), TEXT("MoviePipelinePassIdentifier"), sizeof(FMoviePipelinePassIdentifier), Get_Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Hash());
	}
	return Singleton;
}
template<> MOVIERENDERPIPELINECORE_API UScriptStruct* StaticStruct<FMoviePipelinePassIdentifier>()
{
	return FMoviePipelinePassIdentifier::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMoviePipelinePassIdentifier(FMoviePipelinePassIdentifier::StaticStruct, TEXT("/Script/MovieRenderPipelineCore"), TEXT("MoviePipelinePassIdentifier"), false, nullptr, nullptr);
static struct FScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelinePassIdentifier
{
	FScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelinePassIdentifier()
	{
		UScriptStruct::DeferCppStructOps<FMoviePipelinePassIdentifier>(FName(TEXT("MoviePipelinePassIdentifier")));
	}
} ScriptStruct_MovieRenderPipelineCore_StaticRegisterNativesFMoviePipelinePassIdentifier;
	struct Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Name_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Name;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FMoviePipelinePassIdentifier>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Statics::NewProp_Name_MetaData[] = {
		{ "Category", "Movie Pipeline" },
		{ "ModuleRelativePath", "Public/MovieRenderPipelineDataTypes.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Statics::NewProp_Name = { "Name", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FMoviePipelinePassIdentifier, Name), METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Statics::NewProp_Name_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Statics::NewProp_Name_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Statics::NewProp_Name,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
		nullptr,
		&NewStructOps,
		"MoviePipelinePassIdentifier",
		sizeof(FMoviePipelinePassIdentifier),
		alignof(FMoviePipelinePassIdentifier),
		Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MovieRenderPipelineCore();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MoviePipelinePassIdentifier"), sizeof(FMoviePipelinePassIdentifier), Get_Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Hash() { return 1546021755U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
