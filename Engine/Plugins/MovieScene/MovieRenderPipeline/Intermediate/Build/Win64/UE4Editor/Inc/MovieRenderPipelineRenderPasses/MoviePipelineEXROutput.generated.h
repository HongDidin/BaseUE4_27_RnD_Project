// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MOVIERENDERPIPELINERENDERPASSES_MoviePipelineEXROutput_generated_h
#error "MoviePipelineEXROutput.generated.h already included, missing '#pragma once' in MoviePipelineEXROutput.h"
#endif
#define MOVIERENDERPIPELINERENDERPASSES_MoviePipelineEXROutput_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineImageSequenceOutput_EXR(); \
	friend struct Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineImageSequenceOutput_EXR, UMoviePipelineImageSequenceOutputBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineRenderPasses"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineImageSequenceOutput_EXR)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineImageSequenceOutput_EXR(); \
	friend struct Z_Construct_UClass_UMoviePipelineImageSequenceOutput_EXR_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineImageSequenceOutput_EXR, UMoviePipelineImageSequenceOutputBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineRenderPasses"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineImageSequenceOutput_EXR)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineImageSequenceOutput_EXR(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineImageSequenceOutput_EXR) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineImageSequenceOutput_EXR); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineImageSequenceOutput_EXR); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineImageSequenceOutput_EXR(UMoviePipelineImageSequenceOutput_EXR&&); \
	NO_API UMoviePipelineImageSequenceOutput_EXR(const UMoviePipelineImageSequenceOutput_EXR&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineImageSequenceOutput_EXR(UMoviePipelineImageSequenceOutput_EXR&&); \
	NO_API UMoviePipelineImageSequenceOutput_EXR(const UMoviePipelineImageSequenceOutput_EXR&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineImageSequenceOutput_EXR); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineImageSequenceOutput_EXR); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMoviePipelineImageSequenceOutput_EXR)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_108_PROLOG
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h_111_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINERENDERPASSES_API UClass* StaticClass<class UMoviePipelineImageSequenceOutput_EXR>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineRenderPasses_Private_MoviePipelineEXROutput_h


#define FOREACH_ENUM_EEXRCOMPRESSIONFORMAT(op) \
	op(EEXRCompressionFormat::None) \
	op(EEXRCompressionFormat::PIZ) \
	op(EEXRCompressionFormat::ZIP) 

enum class EEXRCompressionFormat : uint8;
template<> MOVIERENDERPIPELINERENDERPASSES_API UEnum* StaticEnum<EEXRCompressionFormat>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
