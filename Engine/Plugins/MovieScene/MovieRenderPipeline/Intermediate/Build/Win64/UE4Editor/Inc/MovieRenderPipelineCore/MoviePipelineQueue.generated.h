// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMoviePipelineShotConfig;
struct FSoftObjectPath;
class UMoviePipelineMasterConfig;
class UMoviePipelineQueue;
class UMoviePipelineExecutorJob;
#ifdef MOVIERENDERPIPELINECORE_MoviePipelineQueue_generated_h
#error "MoviePipelineQueue.generated.h already included, missing '#pragma once' in MoviePipelineQueue.h"
#endif
#define MOVIERENDERPIPELINECORE_MoviePipelineQueue_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_RPC_WRAPPERS \
	virtual float GetStatusProgress_Implementation() const; \
	virtual void SetStatusProgress_Implementation(const float InProgress); \
	virtual FString GetStatusMessage_Implementation() const; \
	virtual void SetStatusMessage_Implementation(const FString& InStatus); \
 \
	DECLARE_FUNCTION(execShouldRender); \
	DECLARE_FUNCTION(execGetShotOverridePresetOrigin); \
	DECLARE_FUNCTION(execGetShotOverrideConfiguration); \
	DECLARE_FUNCTION(execSetShotOverridePresetOrigin); \
	DECLARE_FUNCTION(execSetShotOverrideConfiguration); \
	DECLARE_FUNCTION(execAllocateNewShotOverrideConfig); \
	DECLARE_FUNCTION(execGetStatusProgress); \
	DECLARE_FUNCTION(execSetStatusProgress); \
	DECLARE_FUNCTION(execGetStatusMessage); \
	DECLARE_FUNCTION(execSetStatusMessage);


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execShouldRender); \
	DECLARE_FUNCTION(execGetShotOverridePresetOrigin); \
	DECLARE_FUNCTION(execGetShotOverrideConfiguration); \
	DECLARE_FUNCTION(execSetShotOverridePresetOrigin); \
	DECLARE_FUNCTION(execSetShotOverrideConfiguration); \
	DECLARE_FUNCTION(execAllocateNewShotOverrideConfig); \
	DECLARE_FUNCTION(execGetStatusProgress); \
	DECLARE_FUNCTION(execSetStatusProgress); \
	DECLARE_FUNCTION(execGetStatusMessage); \
	DECLARE_FUNCTION(execSetStatusMessage);


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_EVENT_PARMS \
	struct MoviePipelineExecutorShot_eventGetStatusMessage_Parms \
	{ \
		FString ReturnValue; \
	}; \
	struct MoviePipelineExecutorShot_eventGetStatusProgress_Parms \
	{ \
		float ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		MoviePipelineExecutorShot_eventGetStatusProgress_Parms() \
			: ReturnValue(0) \
		{ \
		} \
	}; \
	struct MoviePipelineExecutorShot_eventSetStatusMessage_Parms \
	{ \
		FString InStatus; \
	}; \
	struct MoviePipelineExecutorShot_eventSetStatusProgress_Parms \
	{ \
		float InProgress; \
	};


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_CALLBACK_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineExecutorShot(); \
	friend struct Z_Construct_UClass_UMoviePipelineExecutorShot_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineExecutorShot, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineExecutorShot)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineExecutorShot(); \
	friend struct Z_Construct_UClass_UMoviePipelineExecutorShot_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineExecutorShot, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineExecutorShot)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineExecutorShot(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineExecutorShot) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineExecutorShot); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineExecutorShot); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineExecutorShot(UMoviePipelineExecutorShot&&); \
	NO_API UMoviePipelineExecutorShot(const UMoviePipelineExecutorShot&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineExecutorShot(UMoviePipelineExecutorShot&&); \
	NO_API UMoviePipelineExecutorShot(const UMoviePipelineExecutorShot&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineExecutorShot); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineExecutorShot); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMoviePipelineExecutorShot)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Progress() { return STRUCT_OFFSET(UMoviePipelineExecutorShot, Progress); } \
	FORCEINLINE static uint32 __PPO__StatusMessage() { return STRUCT_OFFSET(UMoviePipelineExecutorShot, StatusMessage); } \
	FORCEINLINE static uint32 __PPO__ShotOverrideConfig() { return STRUCT_OFFSET(UMoviePipelineExecutorShot, ShotOverrideConfig); } \
	FORCEINLINE static uint32 __PPO__ShotOverridePresetOrigin() { return STRUCT_OFFSET(UMoviePipelineExecutorShot, ShotOverridePresetOrigin); }


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_23_PROLOG \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_EVENT_PARMS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_CALLBACK_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_CALLBACK_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_26_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<class UMoviePipelineExecutorShot>();

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_RPC_WRAPPERS \
	virtual void OnDuplicated_Implementation(); \
	virtual bool IsConsumed_Implementation() const; \
	virtual void SetConsumed_Implementation(bool bInConsumed); \
	virtual float GetStatusProgress_Implementation() const; \
	virtual void SetStatusProgress_Implementation(const float InProgress); \
	virtual FString GetStatusMessage_Implementation() const; \
	virtual void SetStatusMessage_Implementation(const FString& InStatus); \
 \
	DECLARE_FUNCTION(execSetSequence); \
	DECLARE_FUNCTION(execSetConfiguration); \
	DECLARE_FUNCTION(execGetConfiguration); \
	DECLARE_FUNCTION(execGetPresetOrigin); \
	DECLARE_FUNCTION(execSetPresetOrigin); \
	DECLARE_FUNCTION(execOnDuplicated); \
	DECLARE_FUNCTION(execIsConsumed); \
	DECLARE_FUNCTION(execSetConsumed); \
	DECLARE_FUNCTION(execGetStatusProgress); \
	DECLARE_FUNCTION(execSetStatusProgress); \
	DECLARE_FUNCTION(execGetStatusMessage); \
	DECLARE_FUNCTION(execSetStatusMessage);


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetSequence); \
	DECLARE_FUNCTION(execSetConfiguration); \
	DECLARE_FUNCTION(execGetConfiguration); \
	DECLARE_FUNCTION(execGetPresetOrigin); \
	DECLARE_FUNCTION(execSetPresetOrigin); \
	DECLARE_FUNCTION(execOnDuplicated); \
	DECLARE_FUNCTION(execIsConsumed); \
	DECLARE_FUNCTION(execSetConsumed); \
	DECLARE_FUNCTION(execGetStatusProgress); \
	DECLARE_FUNCTION(execSetStatusProgress); \
	DECLARE_FUNCTION(execGetStatusMessage); \
	DECLARE_FUNCTION(execSetStatusMessage);


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_EVENT_PARMS \
	struct MoviePipelineExecutorJob_eventGetStatusMessage_Parms \
	{ \
		FString ReturnValue; \
	}; \
	struct MoviePipelineExecutorJob_eventGetStatusProgress_Parms \
	{ \
		float ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		MoviePipelineExecutorJob_eventGetStatusProgress_Parms() \
			: ReturnValue(0) \
		{ \
		} \
	}; \
	struct MoviePipelineExecutorJob_eventIsConsumed_Parms \
	{ \
		bool ReturnValue; \
 \
		/** Constructor, initializes return property only **/ \
		MoviePipelineExecutorJob_eventIsConsumed_Parms() \
			: ReturnValue(false) \
		{ \
		} \
	}; \
	struct MoviePipelineExecutorJob_eventSetConsumed_Parms \
	{ \
		bool bInConsumed; \
	}; \
	struct MoviePipelineExecutorJob_eventSetStatusMessage_Parms \
	{ \
		FString InStatus; \
	}; \
	struct MoviePipelineExecutorJob_eventSetStatusProgress_Parms \
	{ \
		float InProgress; \
	};


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_CALLBACK_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineExecutorJob(); \
	friend struct Z_Construct_UClass_UMoviePipelineExecutorJob_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineExecutorJob, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineExecutorJob)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineExecutorJob(); \
	friend struct Z_Construct_UClass_UMoviePipelineExecutorJob_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineExecutorJob, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineExecutorJob)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineExecutorJob(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineExecutorJob) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineExecutorJob); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineExecutorJob); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineExecutorJob(UMoviePipelineExecutorJob&&); \
	NO_API UMoviePipelineExecutorJob(const UMoviePipelineExecutorJob&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineExecutorJob(UMoviePipelineExecutorJob&&); \
	NO_API UMoviePipelineExecutorJob(const UMoviePipelineExecutorJob&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineExecutorJob); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineExecutorJob); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMoviePipelineExecutorJob)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StatusMessage() { return STRUCT_OFFSET(UMoviePipelineExecutorJob, StatusMessage); } \
	FORCEINLINE static uint32 __PPO__StatusProgress() { return STRUCT_OFFSET(UMoviePipelineExecutorJob, StatusProgress); } \
	FORCEINLINE static uint32 __PPO__bIsConsumed() { return STRUCT_OFFSET(UMoviePipelineExecutorJob, bIsConsumed); } \
	FORCEINLINE static uint32 __PPO__Configuration() { return STRUCT_OFFSET(UMoviePipelineExecutorJob, Configuration); } \
	FORCEINLINE static uint32 __PPO__PresetOrigin() { return STRUCT_OFFSET(UMoviePipelineExecutorJob, PresetOrigin); }


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_162_PROLOG \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_EVENT_PARMS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_CALLBACK_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_CALLBACK_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_165_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<class UMoviePipelineExecutorJob>();

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execCopyFrom); \
	DECLARE_FUNCTION(execGetJobs); \
	DECLARE_FUNCTION(execDuplicateJob); \
	DECLARE_FUNCTION(execDeleteJob); \
	DECLARE_FUNCTION(execAllocateNewJob);


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execCopyFrom); \
	DECLARE_FUNCTION(execGetJobs); \
	DECLARE_FUNCTION(execDuplicateJob); \
	DECLARE_FUNCTION(execDeleteJob); \
	DECLARE_FUNCTION(execAllocateNewJob);


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineQueue(); \
	friend struct Z_Construct_UClass_UMoviePipelineQueue_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineQueue, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineQueue)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineQueue(); \
	friend struct Z_Construct_UClass_UMoviePipelineQueue_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineQueue, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineQueue)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineQueue(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineQueue) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineQueue); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineQueue); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineQueue(UMoviePipelineQueue&&); \
	NO_API UMoviePipelineQueue(const UMoviePipelineQueue&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineQueue(UMoviePipelineQueue&&); \
	NO_API UMoviePipelineQueue(const UMoviePipelineQueue&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineQueue); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineQueue); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMoviePipelineQueue)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Jobs() { return STRUCT_OFFSET(UMoviePipelineQueue, Jobs); }


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_367_PROLOG
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h_370_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<class UMoviePipelineQueue>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineQueue_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
