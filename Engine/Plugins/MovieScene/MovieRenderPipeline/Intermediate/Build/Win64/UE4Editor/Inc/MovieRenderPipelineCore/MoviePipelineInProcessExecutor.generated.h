// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MOVIERENDERPIPELINECORE_MoviePipelineInProcessExecutor_generated_h
#error "MoviePipelineInProcessExecutor.generated.h already included, missing '#pragma once' in MoviePipelineInProcessExecutor.h"
#endif
#define MOVIERENDERPIPELINECORE_MoviePipelineInProcessExecutor_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineInProcessExecutor(); \
	friend struct Z_Construct_UClass_UMoviePipelineInProcessExecutor_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineInProcessExecutor, UMoviePipelineLinearExecutorBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineInProcessExecutor)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineInProcessExecutor(); \
	friend struct Z_Construct_UClass_UMoviePipelineInProcessExecutor_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineInProcessExecutor, UMoviePipelineLinearExecutorBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineInProcessExecutor)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineInProcessExecutor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineInProcessExecutor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineInProcessExecutor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineInProcessExecutor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineInProcessExecutor(UMoviePipelineInProcessExecutor&&); \
	NO_API UMoviePipelineInProcessExecutor(const UMoviePipelineInProcessExecutor&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineInProcessExecutor(UMoviePipelineInProcessExecutor&&); \
	NO_API UMoviePipelineInProcessExecutor(const UMoviePipelineInProcessExecutor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineInProcessExecutor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineInProcessExecutor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMoviePipelineInProcessExecutor)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_16_PROLOG
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<class UMoviePipelineInProcessExecutor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineInProcessExecutor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
