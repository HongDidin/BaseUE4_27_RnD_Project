// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MovieRenderPipelineEditor/Public/MoviePipelineEditorBlueprintLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePipelineEditorBlueprintLibrary() {}
// Cross Module References
	MOVIERENDERPIPELINEEDITOR_API UClass* Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary_NoRegister();
	MOVIERENDERPIPELINEEDITOR_API UClass* Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_MovieRenderPipelineEditor();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineQueue_NoRegister();
	LEVELSEQUENCE_API UClass* Z_Construct_UClass_ULevelSequence_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineMasterConfig_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UMoviePipelineEditorBlueprintLibrary::execEnsureJobHasDefaultSettings)
	{
		P_GET_OBJECT(UMoviePipelineExecutorJob,Z_Param_InJob);
		P_FINISH;
		P_NATIVE_BEGIN;
		UMoviePipelineEditorBlueprintLibrary::EnsureJobHasDefaultSettings(Z_Param_InJob);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineEditorBlueprintLibrary::execCreateJobFromSequence)
	{
		P_GET_OBJECT(UMoviePipelineQueue,Z_Param_InPipelineQueue);
		P_GET_OBJECT(ULevelSequence,Z_Param_InSequence);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMoviePipelineExecutorJob**)Z_Param__Result=UMoviePipelineEditorBlueprintLibrary::CreateJobFromSequence(Z_Param_InPipelineQueue,Z_Param_InSequence);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineEditorBlueprintLibrary::execConvertManifestFileToString)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InManifestFilePath);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=UMoviePipelineEditorBlueprintLibrary::ConvertManifestFileToString(Z_Param_InManifestFilePath);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineEditorBlueprintLibrary::execSaveQueueToManifestFile)
	{
		P_GET_OBJECT(UMoviePipelineQueue,Z_Param_InPipelineQueue);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_OutManifestFilePath);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMoviePipelineQueue**)Z_Param__Result=UMoviePipelineEditorBlueprintLibrary::SaveQueueToManifestFile(Z_Param_InPipelineQueue,Z_Param_Out_OutManifestFilePath);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineEditorBlueprintLibrary::execWarnUserOfUnsavedMap)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		UMoviePipelineEditorBlueprintLibrary::WarnUserOfUnsavedMap();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineEditorBlueprintLibrary::execIsMapValidForRemoteRender)
	{
		P_GET_TARRAY_REF(UMoviePipelineExecutorJob*,Z_Param_Out_InJobs);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMoviePipelineEditorBlueprintLibrary::IsMapValidForRemoteRender(Z_Param_Out_InJobs);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineEditorBlueprintLibrary::execExportConfigToAsset)
	{
		P_GET_OBJECT(UMoviePipelineMasterConfig,Z_Param_InConfig);
		P_GET_PROPERTY(FStrProperty,Z_Param_InPackagePath);
		P_GET_PROPERTY(FStrProperty,Z_Param_InFileName);
		P_GET_UBOOL(Z_Param_bInSaveAsset);
		P_GET_OBJECT_REF(UMoviePipelineMasterConfig,Z_Param_Out_OutAsset);
		P_GET_PROPERTY_REF(FTextProperty,Z_Param_Out_OutErrorReason);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=UMoviePipelineEditorBlueprintLibrary::ExportConfigToAsset(Z_Param_InConfig,Z_Param_InPackagePath,Z_Param_InFileName,Z_Param_bInSaveAsset,Z_Param_Out_OutAsset,Z_Param_Out_OutErrorReason);
		P_NATIVE_END;
	}
	void UMoviePipelineEditorBlueprintLibrary::StaticRegisterNativesUMoviePipelineEditorBlueprintLibrary()
	{
		UClass* Class = UMoviePipelineEditorBlueprintLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "ConvertManifestFileToString", &UMoviePipelineEditorBlueprintLibrary::execConvertManifestFileToString },
			{ "CreateJobFromSequence", &UMoviePipelineEditorBlueprintLibrary::execCreateJobFromSequence },
			{ "EnsureJobHasDefaultSettings", &UMoviePipelineEditorBlueprintLibrary::execEnsureJobHasDefaultSettings },
			{ "ExportConfigToAsset", &UMoviePipelineEditorBlueprintLibrary::execExportConfigToAsset },
			{ "IsMapValidForRemoteRender", &UMoviePipelineEditorBlueprintLibrary::execIsMapValidForRemoteRender },
			{ "SaveQueueToManifestFile", &UMoviePipelineEditorBlueprintLibrary::execSaveQueueToManifestFile },
			{ "WarnUserOfUnsavedMap", &UMoviePipelineEditorBlueprintLibrary::execWarnUserOfUnsavedMap },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString_Statics
	{
		struct MoviePipelineEditorBlueprintLibrary_eventConvertManifestFileToString_Parms
		{
			FString InManifestFilePath;
			FString ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InManifestFilePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InManifestFilePath;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString_Statics::NewProp_InManifestFilePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString_Statics::NewProp_InManifestFilePath = { "InManifestFilePath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineEditorBlueprintLibrary_eventConvertManifestFileToString_Parms, InManifestFilePath), METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString_Statics::NewProp_InManifestFilePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString_Statics::NewProp_InManifestFilePath_MetaData)) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineEditorBlueprintLibrary_eventConvertManifestFileToString_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString_Statics::NewProp_InManifestFilePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Loads the specified manifest file and converts it into an FString to be embedded with HTTP REST requests. Use in combination with SaveQueueToManifestFile. */" },
		{ "ModuleRelativePath", "Public/MoviePipelineEditorBlueprintLibrary.h" },
		{ "ToolTip", "Loads the specified manifest file and converts it into an FString to be embedded with HTTP REST requests. Use in combination with SaveQueueToManifestFile." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary, nullptr, "ConvertManifestFileToString", nullptr, nullptr, sizeof(MoviePipelineEditorBlueprintLibrary_eventConvertManifestFileToString_Parms), Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics
	{
		struct MoviePipelineEditorBlueprintLibrary_eventCreateJobFromSequence_Parms
		{
			UMoviePipelineQueue* InPipelineQueue;
			const ULevelSequence* InSequence;
			UMoviePipelineExecutorJob* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InPipelineQueue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InSequence_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InSequence;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::NewProp_InPipelineQueue = { "InPipelineQueue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineEditorBlueprintLibrary_eventCreateJobFromSequence_Parms, InPipelineQueue), Z_Construct_UClass_UMoviePipelineQueue_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::NewProp_InSequence_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::NewProp_InSequence = { "InSequence", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineEditorBlueprintLibrary_eventCreateJobFromSequence_Parms, InSequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::NewProp_InSequence_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::NewProp_InSequence_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineEditorBlueprintLibrary_eventCreateJobFromSequence_Parms, ReturnValue), Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::NewProp_InPipelineQueue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::NewProp_InSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Create a job from a level sequence. Sets the map as the currently editor world, the author, the sequence and the job name as the sequence name on the new job. Returns the newly created job. */" },
		{ "ModuleRelativePath", "Public/MoviePipelineEditorBlueprintLibrary.h" },
		{ "ToolTip", "Create a job from a level sequence. Sets the map as the currently editor world, the author, the sequence and the job name as the sequence name on the new job. Returns the newly created job." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary, nullptr, "CreateJobFromSequence", nullptr, nullptr, sizeof(MoviePipelineEditorBlueprintLibrary_eventCreateJobFromSequence_Parms), Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_EnsureJobHasDefaultSettings_Statics
	{
		struct MoviePipelineEditorBlueprintLibrary_eventEnsureJobHasDefaultSettings_Parms
		{
			UMoviePipelineExecutorJob* InJob;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InJob;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_EnsureJobHasDefaultSettings_Statics::NewProp_InJob = { "InJob", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineEditorBlueprintLibrary_eventEnsureJobHasDefaultSettings_Parms, InJob), Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_EnsureJobHasDefaultSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_EnsureJobHasDefaultSettings_Statics::NewProp_InJob,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_EnsureJobHasDefaultSettings_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Ensure the job has the settings specified by the project settings added. If they're already added we don't modify the object so that we don't make it confused about whether or not you've modified the preset. */" },
		{ "ModuleRelativePath", "Public/MoviePipelineEditorBlueprintLibrary.h" },
		{ "ToolTip", "Ensure the job has the settings specified by the project settings added. If they're already added we don't modify the object so that we don't make it confused about whether or not you've modified the preset." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_EnsureJobHasDefaultSettings_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary, nullptr, "EnsureJobHasDefaultSettings", nullptr, nullptr, sizeof(MoviePipelineEditorBlueprintLibrary_eventEnsureJobHasDefaultSettings_Parms), Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_EnsureJobHasDefaultSettings_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_EnsureJobHasDefaultSettings_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_EnsureJobHasDefaultSettings_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_EnsureJobHasDefaultSettings_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_EnsureJobHasDefaultSettings()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_EnsureJobHasDefaultSettings_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics
	{
		struct MoviePipelineEditorBlueprintLibrary_eventExportConfigToAsset_Parms
		{
			const UMoviePipelineMasterConfig* InConfig;
			FString InPackagePath;
			FString InFileName;
			bool bInSaveAsset;
			UMoviePipelineMasterConfig* OutAsset;
			FText OutErrorReason;
			bool ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InConfig_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InConfig;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InPackagePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InPackagePath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InFileName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InFileName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInSaveAsset_MetaData[];
#endif
		static void NewProp_bInSaveAsset_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInSaveAsset;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_OutAsset;
		static const UE4CodeGen_Private::FTextPropertyParams NewProp_OutErrorReason;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_InConfig_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_InConfig = { "InConfig", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineEditorBlueprintLibrary_eventExportConfigToAsset_Parms, InConfig), Z_Construct_UClass_UMoviePipelineMasterConfig_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_InConfig_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_InConfig_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_InPackagePath_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_InPackagePath = { "InPackagePath", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineEditorBlueprintLibrary_eventExportConfigToAsset_Parms, InPackagePath), METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_InPackagePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_InPackagePath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_InFileName_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_InFileName = { "InFileName", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineEditorBlueprintLibrary_eventExportConfigToAsset_Parms, InFileName), METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_InFileName_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_InFileName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_bInSaveAsset_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_bInSaveAsset_SetBit(void* Obj)
	{
		((MoviePipelineEditorBlueprintLibrary_eventExportConfigToAsset_Parms*)Obj)->bInSaveAsset = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_bInSaveAsset = { "bInSaveAsset", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MoviePipelineEditorBlueprintLibrary_eventExportConfigToAsset_Parms), &Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_bInSaveAsset_SetBit, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_bInSaveAsset_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_bInSaveAsset_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_OutAsset = { "OutAsset", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineEditorBlueprintLibrary_eventExportConfigToAsset_Parms, OutAsset), Z_Construct_UClass_UMoviePipelineMasterConfig_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FTextPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_OutErrorReason = { "OutErrorReason", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Text, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineEditorBlueprintLibrary_eventExportConfigToAsset_Parms, OutErrorReason), METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MoviePipelineEditorBlueprintLibrary_eventExportConfigToAsset_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MoviePipelineEditorBlueprintLibrary_eventExportConfigToAsset_Parms), &Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_InConfig,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_InPackagePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_InFileName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_bInSaveAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_OutAsset,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_OutErrorReason,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "ModuleRelativePath", "Public/MoviePipelineEditorBlueprintLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary, nullptr, "ExportConfigToAsset", nullptr, nullptr, sizeof(MoviePipelineEditorBlueprintLibrary_eventExportConfigToAsset_Parms), Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics
	{
		struct MoviePipelineEditorBlueprintLibrary_eventIsMapValidForRemoteRender_Parms
		{
			TArray<UMoviePipelineExecutorJob*> InJobs;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InJobs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InJobs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_InJobs;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::NewProp_InJobs_Inner = { "InJobs", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::NewProp_InJobs_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::NewProp_InJobs = { "InJobs", nullptr, (EPropertyFlags)0x0010000008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineEditorBlueprintLibrary_eventIsMapValidForRemoteRender_Parms, InJobs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::NewProp_InJobs_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::NewProp_InJobs_MetaData)) };
	void Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MoviePipelineEditorBlueprintLibrary_eventIsMapValidForRemoteRender_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MoviePipelineEditorBlueprintLibrary_eventIsMapValidForRemoteRender_Parms), &Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::NewProp_InJobs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::NewProp_InJobs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Checks to see if any of the Jobs try to point to maps that wouldn't be valid on a remote render (ie: unsaved maps) */" },
		{ "ModuleRelativePath", "Public/MoviePipelineEditorBlueprintLibrary.h" },
		{ "ToolTip", "Checks to see if any of the Jobs try to point to maps that wouldn't be valid on a remote render (ie: unsaved maps)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary, nullptr, "IsMapValidForRemoteRender", nullptr, nullptr, sizeof(MoviePipelineEditorBlueprintLibrary_eventIsMapValidForRemoteRender_Parms), Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile_Statics
	{
		struct MoviePipelineEditorBlueprintLibrary_eventSaveQueueToManifestFile_Parms
		{
			UMoviePipelineQueue* InPipelineQueue;
			FString OutManifestFilePath;
			UMoviePipelineQueue* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InPipelineQueue;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OutManifestFilePath;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile_Statics::NewProp_InPipelineQueue = { "InPipelineQueue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineEditorBlueprintLibrary_eventSaveQueueToManifestFile_Parms, InPipelineQueue), Z_Construct_UClass_UMoviePipelineQueue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile_Statics::NewProp_OutManifestFilePath = { "OutManifestFilePath", nullptr, (EPropertyFlags)0x0010000000000180, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineEditorBlueprintLibrary_eventSaveQueueToManifestFile_Parms, OutManifestFilePath), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineEditorBlueprintLibrary_eventSaveQueueToManifestFile_Parms, ReturnValue), Z_Construct_UClass_UMoviePipelineQueue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile_Statics::NewProp_InPipelineQueue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile_Statics::NewProp_OutManifestFilePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Take the specified Queue, duplicate it and write it to disk in the ../Saved/MovieRenderPipeline/ folder. Returns the duplicated queue. */" },
		{ "ModuleRelativePath", "Public/MoviePipelineEditorBlueprintLibrary.h" },
		{ "ToolTip", "Take the specified Queue, duplicate it and write it to disk in the ../Saved/MovieRenderPipeline/ folder. Returns the duplicated queue." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary, nullptr, "SaveQueueToManifestFile", nullptr, nullptr, sizeof(MoviePipelineEditorBlueprintLibrary_eventSaveQueueToManifestFile_Parms), Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_WarnUserOfUnsavedMap_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_WarnUserOfUnsavedMap_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Pop a dialog box that specifies that they cannot render due to never saved map. Only shows OK button. */" },
		{ "ModuleRelativePath", "Public/MoviePipelineEditorBlueprintLibrary.h" },
		{ "ToolTip", "Pop a dialog box that specifies that they cannot render due to never saved map. Only shows OK button." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_WarnUserOfUnsavedMap_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary, nullptr, "WarnUserOfUnsavedMap", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_WarnUserOfUnsavedMap_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_WarnUserOfUnsavedMap_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_WarnUserOfUnsavedMap()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_WarnUserOfUnsavedMap_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary_NoRegister()
	{
		return UMoviePipelineEditorBlueprintLibrary::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ConvertManifestFileToString, "ConvertManifestFileToString" }, // 1582587591
		{ &Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_CreateJobFromSequence, "CreateJobFromSequence" }, // 1990977868
		{ &Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_EnsureJobHasDefaultSettings, "EnsureJobHasDefaultSettings" }, // 2275156306
		{ &Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_ExportConfigToAsset, "ExportConfigToAsset" }, // 2956520616
		{ &Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_IsMapValidForRemoteRender, "IsMapValidForRemoteRender" }, // 3642260862
		{ &Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_SaveQueueToManifestFile, "SaveQueueToManifestFile" }, // 1939702786
		{ &Z_Construct_UFunction_UMoviePipelineEditorBlueprintLibrary_WarnUserOfUnsavedMap, "WarnUserOfUnsavedMap" }, // 785851337
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MoviePipelineEditorBlueprintLibrary.h" },
		{ "ModuleRelativePath", "Public/MoviePipelineEditorBlueprintLibrary.h" },
		{ "ScriptName", "MoviePipelineEditorLibrary" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineEditorBlueprintLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary_Statics::ClassParams = {
		&UMoviePipelineEditorBlueprintLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineEditorBlueprintLibrary, 2387636241);
	template<> MOVIERENDERPIPELINEEDITOR_API UClass* StaticClass<UMoviePipelineEditorBlueprintLibrary>()
	{
		return UMoviePipelineEditorBlueprintLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineEditorBlueprintLibrary(Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary, &UMoviePipelineEditorBlueprintLibrary::StaticClass, TEXT("/Script/MovieRenderPipelineEditor"), TEXT("UMoviePipelineEditorBlueprintLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineEditorBlueprintLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
