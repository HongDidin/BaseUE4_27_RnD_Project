// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MovieRenderPipelineCore/Public/MoviePipelineQueue.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePipelineQueue() {}
// Cross Module References
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineExecutorShot_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineExecutorShot();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MovieRenderPipelineCore();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineShotConfig_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineExecutorJob();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineMasterConfig_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftObjectPath();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineQueue_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineQueue();
// End Cross Module References
	DEFINE_FUNCTION(UMoviePipelineExecutorShot::execShouldRender)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->ShouldRender();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorShot::execGetShotOverridePresetOrigin)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMoviePipelineShotConfig**)Z_Param__Result=P_THIS->GetShotOverridePresetOrigin();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorShot::execGetShotOverrideConfiguration)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMoviePipelineShotConfig**)Z_Param__Result=P_THIS->GetShotOverrideConfiguration();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorShot::execSetShotOverridePresetOrigin)
	{
		P_GET_OBJECT(UMoviePipelineShotConfig,Z_Param_InPreset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetShotOverridePresetOrigin(Z_Param_InPreset);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorShot::execSetShotOverrideConfiguration)
	{
		P_GET_OBJECT(UMoviePipelineShotConfig,Z_Param_InPreset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetShotOverrideConfiguration(Z_Param_InPreset);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorShot::execAllocateNewShotOverrideConfig)
	{
		P_GET_OBJECT(UClass,Z_Param_InConfigType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMoviePipelineShotConfig**)Z_Param__Result=P_THIS->AllocateNewShotOverrideConfig(Z_Param_InConfigType);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorShot::execGetStatusProgress)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetStatusProgress_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorShot::execSetStatusProgress)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InProgress);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetStatusProgress_Implementation(Z_Param_InProgress);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorShot::execGetStatusMessage)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetStatusMessage_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorShot::execSetStatusMessage)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InStatus);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetStatusMessage_Implementation(Z_Param_InStatus);
		P_NATIVE_END;
	}
	static FName NAME_UMoviePipelineExecutorShot_GetStatusMessage = FName(TEXT("GetStatusMessage"));
	FString UMoviePipelineExecutorShot::GetStatusMessage() const
	{
		MoviePipelineExecutorShot_eventGetStatusMessage_Parms Parms;
		const_cast<UMoviePipelineExecutorShot*>(this)->ProcessEvent(FindFunctionChecked(NAME_UMoviePipelineExecutorShot_GetStatusMessage),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UMoviePipelineExecutorShot_GetStatusProgress = FName(TEXT("GetStatusProgress"));
	float UMoviePipelineExecutorShot::GetStatusProgress() const
	{
		MoviePipelineExecutorShot_eventGetStatusProgress_Parms Parms;
		const_cast<UMoviePipelineExecutorShot*>(this)->ProcessEvent(FindFunctionChecked(NAME_UMoviePipelineExecutorShot_GetStatusProgress),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UMoviePipelineExecutorShot_SetStatusMessage = FName(TEXT("SetStatusMessage"));
	void UMoviePipelineExecutorShot::SetStatusMessage(const FString& InStatus)
	{
		MoviePipelineExecutorShot_eventSetStatusMessage_Parms Parms;
		Parms.InStatus=InStatus;
		ProcessEvent(FindFunctionChecked(NAME_UMoviePipelineExecutorShot_SetStatusMessage),&Parms);
	}
	static FName NAME_UMoviePipelineExecutorShot_SetStatusProgress = FName(TEXT("SetStatusProgress"));
	void UMoviePipelineExecutorShot::SetStatusProgress(const float InProgress)
	{
		MoviePipelineExecutorShot_eventSetStatusProgress_Parms Parms;
		Parms.InProgress=InProgress;
		ProcessEvent(FindFunctionChecked(NAME_UMoviePipelineExecutorShot_SetStatusProgress),&Parms);
	}
	void UMoviePipelineExecutorShot::StaticRegisterNativesUMoviePipelineExecutorShot()
	{
		UClass* Class = UMoviePipelineExecutorShot::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AllocateNewShotOverrideConfig", &UMoviePipelineExecutorShot::execAllocateNewShotOverrideConfig },
			{ "GetShotOverrideConfiguration", &UMoviePipelineExecutorShot::execGetShotOverrideConfiguration },
			{ "GetShotOverridePresetOrigin", &UMoviePipelineExecutorShot::execGetShotOverridePresetOrigin },
			{ "GetStatusMessage", &UMoviePipelineExecutorShot::execGetStatusMessage },
			{ "GetStatusProgress", &UMoviePipelineExecutorShot::execGetStatusProgress },
			{ "SetShotOverrideConfiguration", &UMoviePipelineExecutorShot::execSetShotOverrideConfiguration },
			{ "SetShotOverridePresetOrigin", &UMoviePipelineExecutorShot::execSetShotOverridePresetOrigin },
			{ "SetStatusMessage", &UMoviePipelineExecutorShot::execSetStatusMessage },
			{ "SetStatusProgress", &UMoviePipelineExecutorShot::execSetStatusProgress },
			{ "ShouldRender", &UMoviePipelineExecutorShot::execShouldRender },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorShot_AllocateNewShotOverrideConfig_Statics
	{
		struct MoviePipelineExecutorShot_eventAllocateNewShotOverrideConfig_Parms
		{
			TSubclassOf<UMoviePipelineShotConfig>  InConfigType;
			UMoviePipelineShotConfig* ReturnValue;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_InConfigType;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorShot_AllocateNewShotOverrideConfig_Statics::NewProp_InConfigType = { "InConfigType", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorShot_eventAllocateNewShotOverrideConfig_Parms, InConfigType), Z_Construct_UClass_UMoviePipelineShotConfig_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorShot_AllocateNewShotOverrideConfig_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorShot_eventAllocateNewShotOverrideConfig_Parms, ReturnValue), Z_Construct_UClass_UMoviePipelineShotConfig_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorShot_AllocateNewShotOverrideConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorShot_AllocateNewShotOverrideConfig_Statics::NewProp_InConfigType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorShot_AllocateNewShotOverrideConfig_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorShot_AllocateNewShotOverrideConfig_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "DeterminesOutputType", "InConfigType" },
		{ "InConfigType", "/Script/MovieRenderPipelineCore.MoviePipelineShotConfig" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorShot_AllocateNewShotOverrideConfig_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorShot, nullptr, "AllocateNewShotOverrideConfig", nullptr, nullptr, sizeof(MoviePipelineExecutorShot_eventAllocateNewShotOverrideConfig_Parms), Z_Construct_UFunction_UMoviePipelineExecutorShot_AllocateNewShotOverrideConfig_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_AllocateNewShotOverrideConfig_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorShot_AllocateNewShotOverrideConfig_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_AllocateNewShotOverrideConfig_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorShot_AllocateNewShotOverrideConfig()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorShot_AllocateNewShotOverrideConfig_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverrideConfiguration_Statics
	{
		struct MoviePipelineExecutorShot_eventGetShotOverrideConfiguration_Parms
		{
			UMoviePipelineShotConfig* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverrideConfiguration_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorShot_eventGetShotOverrideConfiguration_Parms, ReturnValue), Z_Construct_UClass_UMoviePipelineShotConfig_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverrideConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverrideConfiguration_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverrideConfiguration_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverrideConfiguration_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorShot, nullptr, "GetShotOverrideConfiguration", nullptr, nullptr, sizeof(MoviePipelineExecutorShot_eventGetShotOverrideConfiguration_Parms), Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverrideConfiguration_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverrideConfiguration_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverrideConfiguration_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverrideConfiguration_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverrideConfiguration()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverrideConfiguration_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverridePresetOrigin_Statics
	{
		struct MoviePipelineExecutorShot_eventGetShotOverridePresetOrigin_Parms
		{
			UMoviePipelineShotConfig* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverridePresetOrigin_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorShot_eventGetShotOverridePresetOrigin_Parms, ReturnValue), Z_Construct_UClass_UMoviePipelineShotConfig_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverridePresetOrigin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverridePresetOrigin_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverridePresetOrigin_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverridePresetOrigin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorShot, nullptr, "GetShotOverridePresetOrigin", nullptr, nullptr, sizeof(MoviePipelineExecutorShot_eventGetShotOverridePresetOrigin_Parms), Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverridePresetOrigin_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverridePresetOrigin_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverridePresetOrigin_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverridePresetOrigin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverridePresetOrigin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverridePresetOrigin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusMessage_Statics
	{
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusMessage_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorShot_eventGetStatusMessage_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusMessage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusMessage_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusMessage_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* Get the current status message for this shot. May be an empty string.\n\x09*\n\x09* For C++ implementations override `virtual FString GetStatusMessage_Implementation() override`\n\x09* For Python/BP implementations override\n\x09*\x09@unreal.ufunction(override=True)\n\x09*\x09""def get_status_message(self):\n\x09*\x09\x09return ?\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Get the current status message for this shot. May be an empty string.\n\nFor C++ implementations override `virtual FString GetStatusMessage_Implementation() override`\nFor Python/BP implementations override\n      @unreal.ufunction(override=True)\n      def get_status_message(self):\n              return ?" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusMessage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorShot, nullptr, "GetStatusMessage", nullptr, nullptr, sizeof(MoviePipelineExecutorShot_eventGetStatusMessage_Parms), Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusMessage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusMessage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x5C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusMessage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusMessage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusMessage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusMessage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusProgress_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusProgress_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorShot_eventGetStatusProgress_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusProgress_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusProgress_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusProgress_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* Get the current progress as last set by SetStatusProgress. 0 by default.\n\x09*\n\x09* For C++ implementations override `virtual float GetStatusProgress_Implementation() override`\n\x09* For Python/BP implementations override\n\x09*\x09@unreal.ufunction(override=True)\n\x09*\x09""def get_status_progress(self):\n\x09*\x09\x09return ?\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Get the current progress as last set by SetStatusProgress. 0 by default.\n\nFor C++ implementations override `virtual float GetStatusProgress_Implementation() override`\nFor Python/BP implementations override\n      @unreal.ufunction(override=True)\n      def get_status_progress(self):\n              return ?" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusProgress_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorShot, nullptr, "GetStatusProgress", nullptr, nullptr, sizeof(MoviePipelineExecutorShot_eventGetStatusProgress_Parms), Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusProgress_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusProgress_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x5C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusProgress_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusProgress_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusProgress()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusProgress_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverrideConfiguration_Statics
	{
		struct MoviePipelineExecutorShot_eventSetShotOverrideConfiguration_Parms
		{
			UMoviePipelineShotConfig* InPreset;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InPreset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverrideConfiguration_Statics::NewProp_InPreset = { "InPreset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorShot_eventSetShotOverrideConfiguration_Parms, InPreset), Z_Construct_UClass_UMoviePipelineShotConfig_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverrideConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverrideConfiguration_Statics::NewProp_InPreset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverrideConfiguration_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverrideConfiguration_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorShot, nullptr, "SetShotOverrideConfiguration", nullptr, nullptr, sizeof(MoviePipelineExecutorShot_eventSetShotOverrideConfiguration_Parms), Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverrideConfiguration_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverrideConfiguration_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverrideConfiguration_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverrideConfiguration_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverrideConfiguration()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverrideConfiguration_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverridePresetOrigin_Statics
	{
		struct MoviePipelineExecutorShot_eventSetShotOverridePresetOrigin_Parms
		{
			UMoviePipelineShotConfig* InPreset;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InPreset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverridePresetOrigin_Statics::NewProp_InPreset = { "InPreset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorShot_eventSetShotOverridePresetOrigin_Parms, InPreset), Z_Construct_UClass_UMoviePipelineShotConfig_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverridePresetOrigin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverridePresetOrigin_Statics::NewProp_InPreset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverridePresetOrigin_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverridePresetOrigin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorShot, nullptr, "SetShotOverridePresetOrigin", nullptr, nullptr, sizeof(MoviePipelineExecutorShot_eventSetShotOverridePresetOrigin_Parms), Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverridePresetOrigin_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverridePresetOrigin_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverridePresetOrigin_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverridePresetOrigin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverridePresetOrigin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverridePresetOrigin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusMessage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InStatus_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InStatus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusMessage_Statics::NewProp_InStatus_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusMessage_Statics::NewProp_InStatus = { "InStatus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorShot_eventSetStatusMessage_Parms, InStatus), METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusMessage_Statics::NewProp_InStatus_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusMessage_Statics::NewProp_InStatus_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusMessage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusMessage_Statics::NewProp_InStatus,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusMessage_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* Set the status of this shot to the given value. This will be shown on the UI if progress\n\x09* is set to a value less than zero. If progress is > 0 then the progress bar will be shown\n\x09* on the UI instead. Progress and Status Message are cosmetic.\n\x09*\n\x09* For C++ implementations override `virtual void SetStatusMessage_Implementation() override`\n\x09* For Python/BP implementations override\n\x09*\x09@unreal.ufunction(override=True)\n\x09*\x09""def set_status_message(self, inStatus):\n\x09*\n\x09* @param InStatus\x09The status message you wish the executor to have.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Set the status of this shot to the given value. This will be shown on the UI if progress\nis set to a value less than zero. If progress is > 0 then the progress bar will be shown\non the UI instead. Progress and Status Message are cosmetic.\n\nFor C++ implementations override `virtual void SetStatusMessage_Implementation() override`\nFor Python/BP implementations override\n      @unreal.ufunction(override=True)\n      def set_status_message(self, inStatus):\n\n@param InStatus       The status message you wish the executor to have." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusMessage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorShot, nullptr, "SetStatusMessage", nullptr, nullptr, sizeof(MoviePipelineExecutorShot_eventSetStatusMessage_Parms), Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusMessage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusMessage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusMessage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusMessage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusMessage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusMessage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusProgress_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InProgress_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InProgress;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusProgress_Statics::NewProp_InProgress_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusProgress_Statics::NewProp_InProgress = { "InProgress", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorShot_eventSetStatusProgress_Parms, InProgress), METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusProgress_Statics::NewProp_InProgress_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusProgress_Statics::NewProp_InProgress_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusProgress_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusProgress_Statics::NewProp_InProgress,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusProgress_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* Set the progress of this shot to the given value. If a positive value is provided\n\x09* the UI will show the progress bar, while a negative value will make the UI show the\n\x09* status message instead. Progress and Status Message are cosmetic and dependent on the\n\x09* executor to update. Similar to the UMoviePipelineExecutor::SetStatusProgress function,\n\x09* but at a per-job level basis instead.\n\x09*\n\x09* For C++ implementations override `virtual void SetStatusProgress_Implementation() override`\n\x09* For Python/BP implementations override\n\x09*\x09@unreal.ufunction(override=True)\n\x09*\x09""def set_status_progress(self, inStatus):\n\x09*\n\x09* @param InProgress\x09The progress (0-1 range) the executor should have.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Set the progress of this shot to the given value. If a positive value is provided\nthe UI will show the progress bar, while a negative value will make the UI show the\nstatus message instead. Progress and Status Message are cosmetic and dependent on the\nexecutor to update. Similar to the UMoviePipelineExecutor::SetStatusProgress function,\nbut at a per-job level basis instead.\n\nFor C++ implementations override `virtual void SetStatusProgress_Implementation() override`\nFor Python/BP implementations override\n      @unreal.ufunction(override=True)\n      def set_status_progress(self, inStatus):\n\n@param InProgress     The progress (0-1 range) the executor should have." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusProgress_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorShot, nullptr, "SetStatusProgress", nullptr, nullptr, sizeof(MoviePipelineExecutorShot_eventSetStatusProgress_Parms), Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusProgress_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusProgress_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusProgress_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusProgress_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusProgress()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusProgress_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorShot_ShouldRender_Statics
	{
		struct MoviePipelineExecutorShot_eventShouldRender_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMoviePipelineExecutorShot_ShouldRender_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MoviePipelineExecutorShot_eventShouldRender_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorShot_ShouldRender_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MoviePipelineExecutorShot_eventShouldRender_Parms), &Z_Construct_UFunction_UMoviePipelineExecutorShot_ShouldRender_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorShot_ShouldRender_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorShot_ShouldRender_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorShot_ShouldRender_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Returns whether this should should be rendered */" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Returns whether this should should be rendered" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorShot_ShouldRender_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorShot, nullptr, "ShouldRender", nullptr, nullptr, sizeof(MoviePipelineExecutorShot_eventShouldRender_Parms), Z_Construct_UFunction_UMoviePipelineExecutorShot_ShouldRender_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_ShouldRender_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorShot_ShouldRender_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorShot_ShouldRender_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorShot_ShouldRender()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorShot_ShouldRender_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMoviePipelineExecutorShot_NoRegister()
	{
		return UMoviePipelineExecutorShot::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineExecutorShot_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OuterName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_OuterName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InnerName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InnerName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Progress_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Progress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StatusMessage_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StatusMessage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShotOverrideConfig_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ShotOverrideConfig;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShotOverridePresetOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_ShotOverridePresetOrigin;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMoviePipelineExecutorShot_AllocateNewShotOverrideConfig, "AllocateNewShotOverrideConfig" }, // 3886730342
		{ &Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverrideConfiguration, "GetShotOverrideConfiguration" }, // 2861393216
		{ &Z_Construct_UFunction_UMoviePipelineExecutorShot_GetShotOverridePresetOrigin, "GetShotOverridePresetOrigin" }, // 1812192607
		{ &Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusMessage, "GetStatusMessage" }, // 1579407956
		{ &Z_Construct_UFunction_UMoviePipelineExecutorShot_GetStatusProgress, "GetStatusProgress" }, // 3566411272
		{ &Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverrideConfiguration, "SetShotOverrideConfiguration" }, // 3231548933
		{ &Z_Construct_UFunction_UMoviePipelineExecutorShot_SetShotOverridePresetOrigin, "SetShotOverridePresetOrigin" }, // 4092776504
		{ &Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusMessage, "SetStatusMessage" }, // 2051147037
		{ &Z_Construct_UFunction_UMoviePipelineExecutorShot_SetStatusProgress, "SetStatusProgress" }, // 52370804
		{ &Z_Construct_UFunction_UMoviePipelineExecutorShot_ShouldRender, "ShouldRender" }, // 1654813637
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n* This class represents a segment of work within the Executor Job. This should be owned\n* by the UMoviePipelineExecutorJob and can be created before the movie pipeline starts to\n* configure some aspects about the segment (such as disabling it). When the movie pipeline\n* starts, it will use the already existing ones, or generate new ones as needed.\n*/" },
		{ "IncludePath", "MoviePipelineQueue.h" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "This class represents a segment of work within the Executor Job. This should be owned\nby the UMoviePipelineExecutorJob and can be created before the movie pipeline starts to\nconfigure some aspects about the segment (such as disabling it). When the movie pipeline\nstarts, it will use the already existing ones, or generate new ones as needed." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Does the user want to render this shot? */" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Does the user want to render this shot?" },
	};
#endif
	void Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((UMoviePipelineExecutorShot*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMoviePipelineExecutorShot), &Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_bEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_OuterName_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** The name of the shot section that contains this shot. Can be empty. */" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "The name of the shot section that contains this shot. Can be empty." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_OuterName = { "OuterName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineExecutorShot, OuterName), METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_OuterName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_OuterName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_InnerName_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** The name of the camera cut section that this shot represents. Can be empty. */" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "The name of the camera cut section that this shot represents. Can be empty." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_InnerName = { "InnerName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineExecutorShot, InnerName), METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_InnerName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_InnerName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_Progress_MetaData[] = {
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_Progress = { "Progress", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineExecutorShot, Progress), METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_Progress_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_Progress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_StatusMessage_MetaData[] = {
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_StatusMessage = { "StatusMessage", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineExecutorShot, StatusMessage), METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_StatusMessage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_StatusMessage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_ShotOverrideConfig_MetaData[] = {
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_ShotOverrideConfig = { "ShotOverrideConfig", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineExecutorShot, ShotOverrideConfig), Z_Construct_UClass_UMoviePipelineShotConfig_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_ShotOverrideConfig_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_ShotOverrideConfig_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_ShotOverridePresetOrigin_MetaData[] = {
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_ShotOverridePresetOrigin = { "ShotOverridePresetOrigin", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineExecutorShot, ShotOverridePresetOrigin), Z_Construct_UClass_UMoviePipelineShotConfig_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_ShotOverridePresetOrigin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_ShotOverridePresetOrigin_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_bEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_OuterName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_InnerName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_Progress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_StatusMessage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_ShotOverrideConfig,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::NewProp_ShotOverridePresetOrigin,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineExecutorShot>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::ClassParams = {
		&UMoviePipelineExecutorShot::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineExecutorShot()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineExecutorShot_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineExecutorShot, 160576500);
	template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<UMoviePipelineExecutorShot>()
	{
		return UMoviePipelineExecutorShot::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineExecutorShot(Z_Construct_UClass_UMoviePipelineExecutorShot, &UMoviePipelineExecutorShot::StaticClass, TEXT("/Script/MovieRenderPipelineCore"), TEXT("UMoviePipelineExecutorShot"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineExecutorShot);
	DEFINE_FUNCTION(UMoviePipelineExecutorJob::execSetSequence)
	{
		P_GET_STRUCT(FSoftObjectPath,Z_Param_InSequence);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetSequence(Z_Param_InSequence);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorJob::execSetConfiguration)
	{
		P_GET_OBJECT(UMoviePipelineMasterConfig,Z_Param_InPreset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetConfiguration(Z_Param_InPreset);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorJob::execGetConfiguration)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMoviePipelineMasterConfig**)Z_Param__Result=P_THIS->GetConfiguration();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorJob::execGetPresetOrigin)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMoviePipelineMasterConfig**)Z_Param__Result=P_THIS->GetPresetOrigin();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorJob::execSetPresetOrigin)
	{
		P_GET_OBJECT(UMoviePipelineMasterConfig,Z_Param_InPreset);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetPresetOrigin(Z_Param_InPreset);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorJob::execOnDuplicated)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnDuplicated_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorJob::execIsConsumed)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsConsumed_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorJob::execSetConsumed)
	{
		P_GET_UBOOL(Z_Param_bInConsumed);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetConsumed_Implementation(Z_Param_bInConsumed);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorJob::execGetStatusProgress)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(float*)Z_Param__Result=P_THIS->GetStatusProgress_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorJob::execSetStatusProgress)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_InProgress);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetStatusProgress_Implementation(Z_Param_InProgress);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorJob::execGetStatusMessage)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(FString*)Z_Param__Result=P_THIS->GetStatusMessage_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineExecutorJob::execSetStatusMessage)
	{
		P_GET_PROPERTY(FStrProperty,Z_Param_InStatus);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->SetStatusMessage_Implementation(Z_Param_InStatus);
		P_NATIVE_END;
	}
	static FName NAME_UMoviePipelineExecutorJob_GetStatusMessage = FName(TEXT("GetStatusMessage"));
	FString UMoviePipelineExecutorJob::GetStatusMessage() const
	{
		MoviePipelineExecutorJob_eventGetStatusMessage_Parms Parms;
		const_cast<UMoviePipelineExecutorJob*>(this)->ProcessEvent(FindFunctionChecked(NAME_UMoviePipelineExecutorJob_GetStatusMessage),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UMoviePipelineExecutorJob_GetStatusProgress = FName(TEXT("GetStatusProgress"));
	float UMoviePipelineExecutorJob::GetStatusProgress() const
	{
		MoviePipelineExecutorJob_eventGetStatusProgress_Parms Parms;
		const_cast<UMoviePipelineExecutorJob*>(this)->ProcessEvent(FindFunctionChecked(NAME_UMoviePipelineExecutorJob_GetStatusProgress),&Parms);
		return Parms.ReturnValue;
	}
	static FName NAME_UMoviePipelineExecutorJob_IsConsumed = FName(TEXT("IsConsumed"));
	bool UMoviePipelineExecutorJob::IsConsumed() const
	{
		MoviePipelineExecutorJob_eventIsConsumed_Parms Parms;
		const_cast<UMoviePipelineExecutorJob*>(this)->ProcessEvent(FindFunctionChecked(NAME_UMoviePipelineExecutorJob_IsConsumed),&Parms);
		return !!Parms.ReturnValue;
	}
	static FName NAME_UMoviePipelineExecutorJob_OnDuplicated = FName(TEXT("OnDuplicated"));
	void UMoviePipelineExecutorJob::OnDuplicated()
	{
		ProcessEvent(FindFunctionChecked(NAME_UMoviePipelineExecutorJob_OnDuplicated),NULL);
	}
	static FName NAME_UMoviePipelineExecutorJob_SetConsumed = FName(TEXT("SetConsumed"));
	void UMoviePipelineExecutorJob::SetConsumed(bool bInConsumed)
	{
		MoviePipelineExecutorJob_eventSetConsumed_Parms Parms;
		Parms.bInConsumed=bInConsumed ? true : false;
		ProcessEvent(FindFunctionChecked(NAME_UMoviePipelineExecutorJob_SetConsumed),&Parms);
	}
	static FName NAME_UMoviePipelineExecutorJob_SetStatusMessage = FName(TEXT("SetStatusMessage"));
	void UMoviePipelineExecutorJob::SetStatusMessage(const FString& InStatus)
	{
		MoviePipelineExecutorJob_eventSetStatusMessage_Parms Parms;
		Parms.InStatus=InStatus;
		ProcessEvent(FindFunctionChecked(NAME_UMoviePipelineExecutorJob_SetStatusMessage),&Parms);
	}
	static FName NAME_UMoviePipelineExecutorJob_SetStatusProgress = FName(TEXT("SetStatusProgress"));
	void UMoviePipelineExecutorJob::SetStatusProgress(const float InProgress)
	{
		MoviePipelineExecutorJob_eventSetStatusProgress_Parms Parms;
		Parms.InProgress=InProgress;
		ProcessEvent(FindFunctionChecked(NAME_UMoviePipelineExecutorJob_SetStatusProgress),&Parms);
	}
	void UMoviePipelineExecutorJob::StaticRegisterNativesUMoviePipelineExecutorJob()
	{
		UClass* Class = UMoviePipelineExecutorJob::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetConfiguration", &UMoviePipelineExecutorJob::execGetConfiguration },
			{ "GetPresetOrigin", &UMoviePipelineExecutorJob::execGetPresetOrigin },
			{ "GetStatusMessage", &UMoviePipelineExecutorJob::execGetStatusMessage },
			{ "GetStatusProgress", &UMoviePipelineExecutorJob::execGetStatusProgress },
			{ "IsConsumed", &UMoviePipelineExecutorJob::execIsConsumed },
			{ "OnDuplicated", &UMoviePipelineExecutorJob::execOnDuplicated },
			{ "SetConfiguration", &UMoviePipelineExecutorJob::execSetConfiguration },
			{ "SetConsumed", &UMoviePipelineExecutorJob::execSetConsumed },
			{ "SetPresetOrigin", &UMoviePipelineExecutorJob::execSetPresetOrigin },
			{ "SetSequence", &UMoviePipelineExecutorJob::execSetSequence },
			{ "SetStatusMessage", &UMoviePipelineExecutorJob::execSetStatusMessage },
			{ "SetStatusProgress", &UMoviePipelineExecutorJob::execSetStatusProgress },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorJob_GetConfiguration_Statics
	{
		struct MoviePipelineExecutorJob_eventGetConfiguration_Parms
		{
			UMoviePipelineMasterConfig* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorJob_GetConfiguration_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorJob_eventGetConfiguration_Parms, ReturnValue), Z_Construct_UClass_UMoviePipelineMasterConfig_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorJob_GetConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorJob_GetConfiguration_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorJob_GetConfiguration_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorJob_GetConfiguration_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorJob, nullptr, "GetConfiguration", nullptr, nullptr, sizeof(MoviePipelineExecutorJob_eventGetConfiguration_Parms), Z_Construct_UFunction_UMoviePipelineExecutorJob_GetConfiguration_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_GetConfiguration_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorJob_GetConfiguration_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_GetConfiguration_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorJob_GetConfiguration()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorJob_GetConfiguration_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorJob_GetPresetOrigin_Statics
	{
		struct MoviePipelineExecutorJob_eventGetPresetOrigin_Parms
		{
			UMoviePipelineMasterConfig* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorJob_GetPresetOrigin_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorJob_eventGetPresetOrigin_Parms, ReturnValue), Z_Construct_UClass_UMoviePipelineMasterConfig_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorJob_GetPresetOrigin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorJob_GetPresetOrigin_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorJob_GetPresetOrigin_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorJob_GetPresetOrigin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorJob, nullptr, "GetPresetOrigin", nullptr, nullptr, sizeof(MoviePipelineExecutorJob_eventGetPresetOrigin_Parms), Z_Construct_UFunction_UMoviePipelineExecutorJob_GetPresetOrigin_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_GetPresetOrigin_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorJob_GetPresetOrigin_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_GetPresetOrigin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorJob_GetPresetOrigin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorJob_GetPresetOrigin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusMessage_Statics
	{
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusMessage_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorJob_eventGetStatusMessage_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusMessage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusMessage_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusMessage_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* Get the current status message for this job. May be an empty string.\n\x09*\n\x09* For C++ implementations override `virtual FString GetStatusMessage_Implementation() override`\n\x09* For Python/BP implementations override\n\x09*\x09@unreal.ufunction(override=True)\n\x09*\x09""def get_status_message(self):\n\x09*\x09\x09return ?\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Get the current status message for this job. May be an empty string.\n\nFor C++ implementations override `virtual FString GetStatusMessage_Implementation() override`\nFor Python/BP implementations override\n      @unreal.ufunction(override=True)\n      def get_status_message(self):\n              return ?" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusMessage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorJob, nullptr, "GetStatusMessage", nullptr, nullptr, sizeof(MoviePipelineExecutorJob_eventGetStatusMessage_Parms), Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusMessage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusMessage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x5C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusMessage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusMessage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusMessage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusMessage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusProgress_Statics
	{
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusProgress_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorJob_eventGetStatusProgress_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusProgress_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusProgress_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusProgress_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* Get the current progress as last set by SetStatusProgress. 0 by default.\n\x09*\n\x09* For C++ implementations override `virtual float GetStatusProgress_Implementation() override`\n\x09* For Python/BP implementations override\n\x09*\x09@unreal.ufunction(override=True)\n\x09*\x09""def get_status_progress(self):\n\x09*\x09\x09return ?\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Get the current progress as last set by SetStatusProgress. 0 by default.\n\nFor C++ implementations override `virtual float GetStatusProgress_Implementation() override`\nFor Python/BP implementations override\n      @unreal.ufunction(override=True)\n      def get_status_progress(self):\n              return ?" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusProgress_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorJob, nullptr, "GetStatusProgress", nullptr, nullptr, sizeof(MoviePipelineExecutorJob_eventGetStatusProgress_Parms), Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusProgress_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusProgress_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x5C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusProgress_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusProgress_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusProgress()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusProgress_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorJob_IsConsumed_Statics
	{
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMoviePipelineExecutorJob_IsConsumed_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MoviePipelineExecutorJob_eventIsConsumed_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorJob_IsConsumed_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MoviePipelineExecutorJob_eventIsConsumed_Parms), &Z_Construct_UFunction_UMoviePipelineExecutorJob_IsConsumed_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorJob_IsConsumed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorJob_IsConsumed_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorJob_IsConsumed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* Gets whether or not the job has been marked as being consumed. A consumed job is not editable\n\x09* in the UI and should not be submitted for rendering as it is either already finished or\n\x09* already in progress.\n\x09*\n\x09* For C++ implementations override `virtual bool IsConsumed_Implementation() override`\n\x09* For Python/BP implementations override\n\x09*\x09@unreal.ufunction(override=True)\n\x09*\x09""def is_consumed(self):\n\x09*\x09\x09return ?\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Gets whether or not the job has been marked as being consumed. A consumed job is not editable\nin the UI and should not be submitted for rendering as it is either already finished or\nalready in progress.\n\nFor C++ implementations override `virtual bool IsConsumed_Implementation() override`\nFor Python/BP implementations override\n      @unreal.ufunction(override=True)\n      def is_consumed(self):\n              return ?" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorJob_IsConsumed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorJob, nullptr, "IsConsumed", nullptr, nullptr, sizeof(MoviePipelineExecutorJob_eventIsConsumed_Parms), Z_Construct_UFunction_UMoviePipelineExecutorJob_IsConsumed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_IsConsumed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x5C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorJob_IsConsumed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_IsConsumed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorJob_IsConsumed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorJob_IsConsumed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorJob_OnDuplicated_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorJob_OnDuplicated_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* Should be called to clear status and user data after duplication so that jobs stay\n\x09* unique and don't pick up ids or other unwanted behavior from the pareant job.\n\x09*\n\x09* For C++ implementations override `virtual bool OnDuplicated_Implementation() override`\n\x09* For Python/BP implementations override\n\x09*\x09@unreal.ufunction(override=True)\n\x09*\x09""def on_duplicated(self):\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Should be called to clear status and user data after duplication so that jobs stay\nunique and don't pick up ids or other unwanted behavior from the pareant job.\n\nFor C++ implementations override `virtual bool OnDuplicated_Implementation() override`\nFor Python/BP implementations override\n      @unreal.ufunction(override=True)\n      def on_duplicated(self):" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorJob_OnDuplicated_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorJob, nullptr, "OnDuplicated", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorJob_OnDuplicated_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_OnDuplicated_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorJob_OnDuplicated()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorJob_OnDuplicated_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConfiguration_Statics
	{
		struct MoviePipelineExecutorJob_eventSetConfiguration_Parms
		{
			UMoviePipelineMasterConfig* InPreset;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InPreset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConfiguration_Statics::NewProp_InPreset = { "InPreset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorJob_eventSetConfiguration_Parms, InPreset), Z_Construct_UClass_UMoviePipelineMasterConfig_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConfiguration_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConfiguration_Statics::NewProp_InPreset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConfiguration_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConfiguration_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorJob, nullptr, "SetConfiguration", nullptr, nullptr, sizeof(MoviePipelineExecutorJob_eventSetConfiguration_Parms), Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConfiguration_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConfiguration_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConfiguration_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConfiguration_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConfiguration()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConfiguration_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInConsumed_MetaData[];
#endif
		static void NewProp_bInConsumed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInConsumed;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed_Statics::NewProp_bInConsumed_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	void Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed_Statics::NewProp_bInConsumed_SetBit(void* Obj)
	{
		((MoviePipelineExecutorJob_eventSetConsumed_Parms*)Obj)->bInConsumed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed_Statics::NewProp_bInConsumed = { "bInConsumed", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MoviePipelineExecutorJob_eventSetConsumed_Parms), &Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed_Statics::NewProp_bInConsumed_SetBit, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed_Statics::NewProp_bInConsumed_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed_Statics::NewProp_bInConsumed_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed_Statics::NewProp_bInConsumed,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* Set the job to be consumed. A consumed job is disabled in the UI and should not be\n\x09* submitted for rendering again. This allows jobs to be added to a queue, the queue\n\x09* submitted to a remote farm (consume the jobs) and then more jobs to be added and\n\x09* the second submission to the farm won't re-submit the already in-progress jobs.\n\x09*\n\x09* Jobs can be unconsumed when the render finishes to re-enable editing.\n\x09*\n\x09* For C++ implementations override `virtual void SetConsumed_Implementation() override`\n\x09* For Python/BP implementations override\n\x09*\x09@unreal.ufunction(override=True)\n\x09*\x09""def set_consumed(self, isConsumed):\n\x09*\n\x09* @param bInConsumed\x09True if the job should be consumed and disabled for editing in the UI.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Set the job to be consumed. A consumed job is disabled in the UI and should not be\nsubmitted for rendering again. This allows jobs to be added to a queue, the queue\nsubmitted to a remote farm (consume the jobs) and then more jobs to be added and\nthe second submission to the farm won't re-submit the already in-progress jobs.\n\nJobs can be unconsumed when the render finishes to re-enable editing.\n\nFor C++ implementations override `virtual void SetConsumed_Implementation() override`\nFor Python/BP implementations override\n      @unreal.ufunction(override=True)\n      def set_consumed(self, isConsumed):\n\n@param bInConsumed    True if the job should be consumed and disabled for editing in the UI." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorJob, nullptr, "SetConsumed", nullptr, nullptr, sizeof(MoviePipelineExecutorJob_eventSetConsumed_Parms), Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorJob_SetPresetOrigin_Statics
	{
		struct MoviePipelineExecutorJob_eventSetPresetOrigin_Parms
		{
			UMoviePipelineMasterConfig* InPreset;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InPreset;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorJob_SetPresetOrigin_Statics::NewProp_InPreset = { "InPreset", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorJob_eventSetPresetOrigin_Parms, InPreset), Z_Construct_UClass_UMoviePipelineMasterConfig_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorJob_SetPresetOrigin_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorJob_SetPresetOrigin_Statics::NewProp_InPreset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorJob_SetPresetOrigin_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorJob_SetPresetOrigin_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorJob, nullptr, "SetPresetOrigin", nullptr, nullptr, sizeof(MoviePipelineExecutorJob_eventSetPresetOrigin_Parms), Z_Construct_UFunction_UMoviePipelineExecutorJob_SetPresetOrigin_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetPresetOrigin_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetPresetOrigin_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetPresetOrigin_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorJob_SetPresetOrigin()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorJob_SetPresetOrigin_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorJob_SetSequence_Statics
	{
		struct MoviePipelineExecutorJob_eventSetSequence_Parms
		{
			FSoftObjectPath InSequence;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_InSequence;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorJob_SetSequence_Statics::NewProp_InSequence = { "InSequence", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorJob_eventSetSequence_Parms, InSequence), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorJob_SetSequence_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorJob_SetSequence_Statics::NewProp_InSequence,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorJob_SetSequence_Statics::Function_MetaDataParams[] = {
		{ "BlueprintSetter", "" },
		{ "Category", "Movie Render Pipeline" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorJob_SetSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorJob, nullptr, "SetSequence", nullptr, nullptr, sizeof(MoviePipelineExecutorJob_eventSetSequence_Parms), Z_Construct_UFunction_UMoviePipelineExecutorJob_SetSequence_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetSequence_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04820401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorJob_SetSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorJob_SetSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusMessage_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InStatus_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InStatus;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusMessage_Statics::NewProp_InStatus_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusMessage_Statics::NewProp_InStatus = { "InStatus", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorJob_eventSetStatusMessage_Parms, InStatus), METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusMessage_Statics::NewProp_InStatus_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusMessage_Statics::NewProp_InStatus_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusMessage_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusMessage_Statics::NewProp_InStatus,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusMessage_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* Set the status of this job to the given value. This will be shown on the UI if progress\n\x09* is set to a value less than zero. If progress is > 0 then the progress bar will be shown\n\x09* on the UI instead. Progress and Status Message are cosmetic and dependent on the\n\x09* executor to update. Similar to the UMoviePipelineExecutor::SetStatusMessage function,\n\x09* but at a per-job level basis instead. \n\x09*\n\x09* For C++ implementations override `virtual void SetStatusMessage_Implementation() override`\n\x09* For Python/BP implementations override\n\x09*\x09@unreal.ufunction(override=True)\n\x09*\x09""def set_status_message(self, inStatus):\n\x09*\n\x09* @param InStatus\x09The status message you wish the executor to have.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Set the status of this job to the given value. This will be shown on the UI if progress\nis set to a value less than zero. If progress is > 0 then the progress bar will be shown\non the UI instead. Progress and Status Message are cosmetic and dependent on the\nexecutor to update. Similar to the UMoviePipelineExecutor::SetStatusMessage function,\nbut at a per-job level basis instead.\n\nFor C++ implementations override `virtual void SetStatusMessage_Implementation() override`\nFor Python/BP implementations override\n      @unreal.ufunction(override=True)\n      def set_status_message(self, inStatus):\n\n@param InStatus       The status message you wish the executor to have." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusMessage_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorJob, nullptr, "SetStatusMessage", nullptr, nullptr, sizeof(MoviePipelineExecutorJob_eventSetStatusMessage_Parms), Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusMessage_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusMessage_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusMessage_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusMessage_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusMessage()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusMessage_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusProgress_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InProgress_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_InProgress;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusProgress_Statics::NewProp_InProgress_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusProgress_Statics::NewProp_InProgress = { "InProgress", nullptr, (EPropertyFlags)0x0010000000000082, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineExecutorJob_eventSetStatusProgress_Parms, InProgress), METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusProgress_Statics::NewProp_InProgress_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusProgress_Statics::NewProp_InProgress_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusProgress_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusProgress_Statics::NewProp_InProgress,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusProgress_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* Set the progress of this job to the given value. If a positive value is provided\n\x09* the UI will show the progress bar, while a negative value will make the UI show the \n\x09* status message instead. Progress and Status Message are cosmetic and dependent on the\n\x09* executor to update. Similar to the UMoviePipelineExecutor::SetStatusProgress function,\n\x09* but at a per-job level basis instead.\n\x09*\n\x09* For C++ implementations override `virtual void SetStatusProgress_Implementation() override`\n\x09* For Python/BP implementations override\n\x09*\x09@unreal.ufunction(override=True)\n\x09*\x09""def set_status_progress(self, inProgress):\n\x09*\n\x09* @param InProgress\x09The progress (0-1 range) the executor should have.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Set the progress of this job to the given value. If a positive value is provided\nthe UI will show the progress bar, while a negative value will make the UI show the\nstatus message instead. Progress and Status Message are cosmetic and dependent on the\nexecutor to update. Similar to the UMoviePipelineExecutor::SetStatusProgress function,\nbut at a per-job level basis instead.\n\nFor C++ implementations override `virtual void SetStatusProgress_Implementation() override`\nFor Python/BP implementations override\n      @unreal.ufunction(override=True)\n      def set_status_progress(self, inProgress):\n\n@param InProgress     The progress (0-1 range) the executor should have." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusProgress_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineExecutorJob, nullptr, "SetStatusProgress", nullptr, nullptr, sizeof(MoviePipelineExecutorJob_eventSetStatusProgress_Parms), Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusProgress_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusProgress_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x0C020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusProgress_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusProgress_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusProgress()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusProgress_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister()
	{
		return UMoviePipelineExecutorJob::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineExecutorJob_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_JobName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_JobName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sequence_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Sequence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Map_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Map;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Author_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Author;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShotInfo_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ShotInfo_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ShotInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ShotInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UserData_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_UserData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StatusMessage_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_StatusMessage;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_StatusProgress_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_StatusProgress;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsConsumed_MetaData[];
#endif
		static void NewProp_bIsConsumed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsConsumed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Configuration_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Configuration;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PresetOrigin_MetaData[];
#endif
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_PresetOrigin;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMoviePipelineExecutorJob_GetConfiguration, "GetConfiguration" }, // 1382933140
		{ &Z_Construct_UFunction_UMoviePipelineExecutorJob_GetPresetOrigin, "GetPresetOrigin" }, // 2226006382
		{ &Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusMessage, "GetStatusMessage" }, // 366978618
		{ &Z_Construct_UFunction_UMoviePipelineExecutorJob_GetStatusProgress, "GetStatusProgress" }, // 1782057957
		{ &Z_Construct_UFunction_UMoviePipelineExecutorJob_IsConsumed, "IsConsumed" }, // 3443701330
		{ &Z_Construct_UFunction_UMoviePipelineExecutorJob_OnDuplicated, "OnDuplicated" }, // 4093169484
		{ &Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConfiguration, "SetConfiguration" }, // 2108840216
		{ &Z_Construct_UFunction_UMoviePipelineExecutorJob_SetConsumed, "SetConsumed" }, // 3874156084
		{ &Z_Construct_UFunction_UMoviePipelineExecutorJob_SetPresetOrigin, "SetPresetOrigin" }, // 1257071995
		{ &Z_Construct_UFunction_UMoviePipelineExecutorJob_SetSequence, "SetSequence" }, // 4087541015
		{ &Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusMessage, "SetStatusMessage" }, // 1934066871
		{ &Z_Construct_UFunction_UMoviePipelineExecutorJob_SetStatusProgress, "SetStatusProgress" }, // 2582634272
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n* A particular job within the Queue\n*/" },
		{ "IncludePath", "MoviePipelineQueue.h" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "A particular job within the Queue" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_JobName_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** (Optional) Name of the job. Shown on the default burn-in. */" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "(Optional) Name of the job. Shown on the default burn-in." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_JobName = { "JobName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineExecutorJob, JobName), METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_JobName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_JobName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Sequence_MetaData[] = {
		{ "AllowedClasses", "LevelSequence" },
		{ "BlueprintSetter", "SetSequence" },
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Which sequence should this job render? */" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Which sequence should this job render?" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Sequence = { "Sequence", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineExecutorJob, Sequence), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Sequence_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Sequence_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Map_MetaData[] = {
		{ "AllowedClasses", "World" },
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** Which map should this job render on */" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Which map should this job render on" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Map = { "Map", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineExecutorJob, Map), Z_Construct_UScriptStruct_FSoftObjectPath, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Map_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Map_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Author_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** (Optional) Name of the person who submitted the job. Can be shown in burn in as a first point of contact about the content. */" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "(Optional) Name of the person who submitted the job. Can be shown in burn in as a first point of contact about the content." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Author = { "Author", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineExecutorJob, Author), METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Author_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Author_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_ShotInfo_Inner_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** (Optional) Shot specific information. If a shot is missing from this list it will assume to be enabled and will be rendered. */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "(Optional) Shot specific information. If a shot is missing from this list it will assume to be enabled and will be rendered." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_ShotInfo_Inner = { "ShotInfo", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMoviePipelineExecutorShot_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_ShotInfo_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_ShotInfo_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_ShotInfo_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** (Optional) Shot specific information. If a shot is missing from this list it will assume to be enabled and will be rendered. */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "(Optional) Shot specific information. If a shot is missing from this list it will assume to be enabled and will be rendered." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_ShotInfo = { "ShotInfo", nullptr, (EPropertyFlags)0x001000800000000c, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineExecutorJob, ShotInfo), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_ShotInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_ShotInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_UserData_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** \n\x09* Arbitrary data that can be associated with the job. Not used by default implementations, nor read.\n\x09* This can be used to attach third party metadata such as job ids from remote farms. \n\x09* Not shown in the user interface.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Arbitrary data that can be associated with the job. Not used by default implementations, nor read.\nThis can be used to attach third party metadata such as job ids from remote farms.\nNot shown in the user interface." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_UserData = { "UserData", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineExecutorJob, UserData), METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_UserData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_UserData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_StatusMessage_MetaData[] = {
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_StatusMessage = { "StatusMessage", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineExecutorJob, StatusMessage), METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_StatusMessage_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_StatusMessage_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_StatusProgress_MetaData[] = {
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_StatusProgress = { "StatusProgress", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineExecutorJob, StatusProgress), METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_StatusProgress_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_StatusProgress_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_bIsConsumed_MetaData[] = {
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	void Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_bIsConsumed_SetBit(void* Obj)
	{
		((UMoviePipelineExecutorJob*)Obj)->bIsConsumed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_bIsConsumed = { "bIsConsumed", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMoviePipelineExecutorJob), &Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_bIsConsumed_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_bIsConsumed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_bIsConsumed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Configuration_MetaData[] = {
		{ "Comment", "/** \n\x09*/" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Configuration = { "Configuration", nullptr, (EPropertyFlags)0x0042000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineExecutorJob, Configuration), Z_Construct_UClass_UMoviePipelineMasterConfig_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Configuration_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Configuration_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_PresetOrigin_MetaData[] = {
		{ "Comment", "/**\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_PresetOrigin = { "PresetOrigin", nullptr, (EPropertyFlags)0x0044000000000000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineExecutorJob, PresetOrigin), Z_Construct_UClass_UMoviePipelineMasterConfig_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_PresetOrigin_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_PresetOrigin_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_JobName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Sequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Map,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Author,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_ShotInfo_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_ShotInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_UserData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_StatusMessage,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_StatusProgress,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_bIsConsumed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_Configuration,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::NewProp_PresetOrigin,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineExecutorJob>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::ClassParams = {
		&UMoviePipelineExecutorJob::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineExecutorJob()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineExecutorJob_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineExecutorJob, 2624135114);
	template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<UMoviePipelineExecutorJob>()
	{
		return UMoviePipelineExecutorJob::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineExecutorJob(Z_Construct_UClass_UMoviePipelineExecutorJob, &UMoviePipelineExecutorJob::StaticClass, TEXT("/Script/MovieRenderPipelineCore"), TEXT("UMoviePipelineExecutorJob"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineExecutorJob);
	DEFINE_FUNCTION(UMoviePipelineQueue::execCopyFrom)
	{
		P_GET_OBJECT(UMoviePipelineQueue,Z_Param_InQueue);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CopyFrom(Z_Param_InQueue);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineQueue::execGetJobs)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UMoviePipelineExecutorJob*>*)Z_Param__Result=P_THIS->GetJobs();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineQueue::execDuplicateJob)
	{
		P_GET_OBJECT(UMoviePipelineExecutorJob,Z_Param_InJob);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMoviePipelineExecutorJob**)Z_Param__Result=P_THIS->DuplicateJob(Z_Param_InJob);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineQueue::execDeleteJob)
	{
		P_GET_OBJECT(UMoviePipelineExecutorJob,Z_Param_InJob);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->DeleteJob(Z_Param_InJob);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipelineQueue::execAllocateNewJob)
	{
		P_GET_OBJECT(UClass,Z_Param_InJobType);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMoviePipelineExecutorJob**)Z_Param__Result=P_THIS->AllocateNewJob(Z_Param_InJobType);
		P_NATIVE_END;
	}
	void UMoviePipelineQueue::StaticRegisterNativesUMoviePipelineQueue()
	{
		UClass* Class = UMoviePipelineQueue::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AllocateNewJob", &UMoviePipelineQueue::execAllocateNewJob },
			{ "CopyFrom", &UMoviePipelineQueue::execCopyFrom },
			{ "DeleteJob", &UMoviePipelineQueue::execDeleteJob },
			{ "DuplicateJob", &UMoviePipelineQueue::execDuplicateJob },
			{ "GetJobs", &UMoviePipelineQueue::execGetJobs },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMoviePipelineQueue_AllocateNewJob_Statics
	{
		struct MoviePipelineQueue_eventAllocateNewJob_Parms
		{
			TSubclassOf<UMoviePipelineExecutorJob>  InJobType;
			UMoviePipelineExecutorJob* ReturnValue;
		};
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_InJobType;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UFunction_UMoviePipelineQueue_AllocateNewJob_Statics::NewProp_InJobType = { "InJobType", nullptr, (EPropertyFlags)0x0014000000000080, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineQueue_eventAllocateNewJob_Parms, InJobType), Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineQueue_AllocateNewJob_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineQueue_eventAllocateNewJob_Parms, ReturnValue), Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineQueue_AllocateNewJob_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineQueue_AllocateNewJob_Statics::NewProp_InJobType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineQueue_AllocateNewJob_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineQueue_AllocateNewJob_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline|Queue" },
		{ "Comment", "/**\n\x09* Allocates a new Job in this Queue. The Queue owns the jobs for memory management purposes,\n\x09* and this will handle that for you. \n\x09*\n\x09* @param InJobType\x09Specify the specific Job type that should be created. Custom Executors can use custom Job types to allow the user to provide more information.\n\x09* @return\x09The created Executor job instance.\n\x09*/" },
		{ "DeterminesOutputType", "InClass" },
		{ "InJobType", "/Script/MovieRenderPipelineCore.MoviePipelineExecutorJob" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Allocates a new Job in this Queue. The Queue owns the jobs for memory management purposes,\nand this will handle that for you.\n\n@param InJobType      Specify the specific Job type that should be created. Custom Executors can use custom Job types to allow the user to provide more information.\n@return       The created Executor job instance." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineQueue_AllocateNewJob_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineQueue, nullptr, "AllocateNewJob", nullptr, nullptr, sizeof(MoviePipelineQueue_eventAllocateNewJob_Parms), Z_Construct_UFunction_UMoviePipelineQueue_AllocateNewJob_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineQueue_AllocateNewJob_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineQueue_AllocateNewJob_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineQueue_AllocateNewJob_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineQueue_AllocateNewJob()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineQueue_AllocateNewJob_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineQueue_CopyFrom_Statics
	{
		struct MoviePipelineQueue_eventCopyFrom_Parms
		{
			UMoviePipelineQueue* InQueue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InQueue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineQueue_CopyFrom_Statics::NewProp_InQueue = { "InQueue", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineQueue_eventCopyFrom_Parms, InQueue), Z_Construct_UClass_UMoviePipelineQueue_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineQueue_CopyFrom_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineQueue_CopyFrom_Statics::NewProp_InQueue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineQueue_CopyFrom_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** \n\x09* Replace the contents of this queue with a copy of the contents from another queue. \n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Replace the contents of this queue with a copy of the contents from another queue." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineQueue_CopyFrom_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineQueue, nullptr, "CopyFrom", nullptr, nullptr, sizeof(MoviePipelineQueue_eventCopyFrom_Parms), Z_Construct_UFunction_UMoviePipelineQueue_CopyFrom_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineQueue_CopyFrom_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineQueue_CopyFrom_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineQueue_CopyFrom_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineQueue_CopyFrom()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineQueue_CopyFrom_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineQueue_DeleteJob_Statics
	{
		struct MoviePipelineQueue_eventDeleteJob_Parms
		{
			UMoviePipelineExecutorJob* InJob;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InJob;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineQueue_DeleteJob_Statics::NewProp_InJob = { "InJob", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineQueue_eventDeleteJob_Parms, InJob), Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineQueue_DeleteJob_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineQueue_DeleteJob_Statics::NewProp_InJob,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineQueue_DeleteJob_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline|Queue" },
		{ "Comment", "/**\n\x09* Deletes the specified job from the Queue. \n\x09*\n\x09* @param InJob\x09The job to look for and delete. \n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Deletes the specified job from the Queue.\n\n@param InJob  The job to look for and delete." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineQueue_DeleteJob_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineQueue, nullptr, "DeleteJob", nullptr, nullptr, sizeof(MoviePipelineQueue_eventDeleteJob_Parms), Z_Construct_UFunction_UMoviePipelineQueue_DeleteJob_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineQueue_DeleteJob_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineQueue_DeleteJob_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineQueue_DeleteJob_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineQueue_DeleteJob()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineQueue_DeleteJob_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineQueue_DuplicateJob_Statics
	{
		struct MoviePipelineQueue_eventDuplicateJob_Parms
		{
			UMoviePipelineExecutorJob* InJob;
			UMoviePipelineExecutorJob* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InJob;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineQueue_DuplicateJob_Statics::NewProp_InJob = { "InJob", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineQueue_eventDuplicateJob_Parms, InJob), Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineQueue_DuplicateJob_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineQueue_eventDuplicateJob_Parms, ReturnValue), Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineQueue_DuplicateJob_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineQueue_DuplicateJob_Statics::NewProp_InJob,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineQueue_DuplicateJob_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineQueue_DuplicateJob_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline|Queue" },
		{ "Comment", "/**\n\x09* Duplicate the specific job and return the duplicate. Configurations are duplicated and not shared.\n\x09*\n\x09* @param InJob\x09The job to look for to duplicate.\n\x09* @return The duplicated instance or nullptr if a duplicate could not be made.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Duplicate the specific job and return the duplicate. Configurations are duplicated and not shared.\n\n@param InJob  The job to look for to duplicate.\n@return The duplicated instance or nullptr if a duplicate could not be made." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineQueue_DuplicateJob_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineQueue, nullptr, "DuplicateJob", nullptr, nullptr, sizeof(MoviePipelineQueue_eventDuplicateJob_Parms), Z_Construct_UFunction_UMoviePipelineQueue_DuplicateJob_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineQueue_DuplicateJob_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineQueue_DuplicateJob_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineQueue_DuplicateJob_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineQueue_DuplicateJob()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineQueue_DuplicateJob_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipelineQueue_GetJobs_Statics
	{
		struct MoviePipelineQueue_eventGetJobs_Parms
		{
			TArray<UMoviePipelineExecutorJob*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipelineQueue_GetJobs_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_UMoviePipelineQueue_GetJobs_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineQueue_eventGetJobs_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineQueue_GetJobs_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineQueue_GetJobs_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineQueue_GetJobs_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineQueue_GetJobs_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline|Queue" },
		{ "Comment", "/**\n\x09* Get all of the Jobs contained in this Queue.\n\x09* @return The jobs contained by this queue.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "Get all of the Jobs contained in this Queue.\n@return The jobs contained by this queue." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineQueue_GetJobs_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineQueue, nullptr, "GetJobs", nullptr, nullptr, sizeof(MoviePipelineQueue_eventGetJobs_Parms), Z_Construct_UFunction_UMoviePipelineQueue_GetJobs_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineQueue_GetJobs_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineQueue_GetJobs_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineQueue_GetJobs_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineQueue_GetJobs()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineQueue_GetJobs_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMoviePipelineQueue_NoRegister()
	{
		return UMoviePipelineQueue::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineQueue_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Jobs_Inner_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Jobs_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Jobs_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Jobs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineQueue_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMoviePipelineQueue_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMoviePipelineQueue_AllocateNewJob, "AllocateNewJob" }, // 572800164
		{ &Z_Construct_UFunction_UMoviePipelineQueue_CopyFrom, "CopyFrom" }, // 3217096219
		{ &Z_Construct_UFunction_UMoviePipelineQueue_DeleteJob, "DeleteJob" }, // 3385728462
		{ &Z_Construct_UFunction_UMoviePipelineQueue_DuplicateJob, "DuplicateJob" }, // 696001005
		{ &Z_Construct_UFunction_UMoviePipelineQueue_GetJobs, "GetJobs" }, // 531145312
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineQueue_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n* A queue is a list of jobs that have been executed, are executing and are waiting to be executed. These can be saved\n* to specific assets to allow \n*/" },
		{ "IncludePath", "MoviePipelineQueue.h" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
		{ "ToolTip", "A queue is a list of jobs that have been executed, are executing and are waiting to be executed. These can be saved\nto specific assets to allow" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineQueue_Statics::NewProp_Jobs_Inner_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMoviePipelineQueue_Statics::NewProp_Jobs_Inner = { "Jobs", nullptr, (EPropertyFlags)0x0002000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineQueue_Statics::NewProp_Jobs_Inner_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineQueue_Statics::NewProp_Jobs_Inner_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineQueue_Statics::NewProp_Jobs_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MoviePipelineQueue.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMoviePipelineQueue_Statics::NewProp_Jobs = { "Jobs", nullptr, (EPropertyFlags)0x0040008000000008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineQueue, Jobs), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineQueue_Statics::NewProp_Jobs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineQueue_Statics::NewProp_Jobs_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMoviePipelineQueue_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineQueue_Statics::NewProp_Jobs_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineQueue_Statics::NewProp_Jobs,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineQueue_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineQueue>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineQueue_Statics::ClassParams = {
		&UMoviePipelineQueue::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMoviePipelineQueue_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineQueue_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineQueue_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineQueue_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineQueue()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineQueue_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineQueue, 2065004099);
	template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<UMoviePipelineQueue>()
	{
		return UMoviePipelineQueue::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineQueue(Z_Construct_UClass_UMoviePipelineQueue, &UMoviePipelineQueue::StaticClass, TEXT("/Script/MovieRenderPipelineCore"), TEXT("UMoviePipelineQueue"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineQueue);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
