// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UWorld;
class UMoviePipelineQueue;
#ifdef MOVIERENDERPIPELINECORE_MoviePipelinePythonHostExecutor_generated_h
#error "MoviePipelinePythonHostExecutor.generated.h already included, missing '#pragma once' in MoviePipelinePythonHostExecutor.h"
#endif
#define MOVIERENDERPIPELINECORE_MoviePipelinePythonHostExecutor_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_RPC_WRAPPERS \
	virtual void OnMapLoad_Implementation(UWorld* InWorld); \
	virtual void ExecuteDelayed_Implementation(UMoviePipelineQueue* InPipelineQueue); \
 \
	DECLARE_FUNCTION(execGetLastLoadedWorld); \
	DECLARE_FUNCTION(execOnMapLoad); \
	DECLARE_FUNCTION(execExecuteDelayed);


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetLastLoadedWorld); \
	DECLARE_FUNCTION(execOnMapLoad); \
	DECLARE_FUNCTION(execExecuteDelayed);


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_EVENT_PARMS \
	struct MoviePipelinePythonHostExecutor_eventExecuteDelayed_Parms \
	{ \
		UMoviePipelineQueue* InPipelineQueue; \
	}; \
	struct MoviePipelinePythonHostExecutor_eventOnMapLoad_Parms \
	{ \
		UWorld* InWorld; \
	};


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_CALLBACK_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelinePythonHostExecutor(); \
	friend struct Z_Construct_UClass_UMoviePipelinePythonHostExecutor_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelinePythonHostExecutor, UMoviePipelineExecutorBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelinePythonHostExecutor)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelinePythonHostExecutor(); \
	friend struct Z_Construct_UClass_UMoviePipelinePythonHostExecutor_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelinePythonHostExecutor, UMoviePipelineExecutorBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelinePythonHostExecutor)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelinePythonHostExecutor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelinePythonHostExecutor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelinePythonHostExecutor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelinePythonHostExecutor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelinePythonHostExecutor(UMoviePipelinePythonHostExecutor&&); \
	NO_API UMoviePipelinePythonHostExecutor(const UMoviePipelinePythonHostExecutor&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelinePythonHostExecutor(UMoviePipelinePythonHostExecutor&&); \
	NO_API UMoviePipelinePythonHostExecutor(const UMoviePipelinePythonHostExecutor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelinePythonHostExecutor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelinePythonHostExecutor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMoviePipelinePythonHostExecutor)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__LastLoadedWorld() { return STRUCT_OFFSET(UMoviePipelinePythonHostExecutor, LastLoadedWorld); }


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_16_PROLOG \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_EVENT_PARMS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_CALLBACK_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_CALLBACK_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<class UMoviePipelinePythonHostExecutor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelinePythonHostExecutor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
