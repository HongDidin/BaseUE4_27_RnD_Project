// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MovieRenderPipelineEditor/Private/MoviePipelineAssetEditor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePipelineAssetEditor() {}
// Cross Module References
	MOVIERENDERPIPELINEEDITOR_API UClass* Z_Construct_UClass_UMoviePipelineAssetEditor_NoRegister();
	MOVIERENDERPIPELINEEDITOR_API UClass* Z_Construct_UClass_UMoviePipelineAssetEditor();
	UNREALED_API UClass* Z_Construct_UClass_UAssetEditor();
	UPackage* Z_Construct_UPackage__Script_MovieRenderPipelineEditor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	void UMoviePipelineAssetEditor::StaticRegisterNativesUMoviePipelineAssetEditor()
	{
	}
	UClass* Z_Construct_UClass_UMoviePipelineAssetEditor_NoRegister()
	{
		return UMoviePipelineAssetEditor::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineAssetEditor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObjectToEdit_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObjectToEdit;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineAssetEditor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAssetEditor,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineAssetEditor_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MoviePipelineAssetEditor.h" },
		{ "ModuleRelativePath", "Private/MoviePipelineAssetEditor.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineAssetEditor_Statics::NewProp_ObjectToEdit_MetaData[] = {
		{ "ModuleRelativePath", "Private/MoviePipelineAssetEditor.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMoviePipelineAssetEditor_Statics::NewProp_ObjectToEdit = { "ObjectToEdit", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineAssetEditor, ObjectToEdit), Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineAssetEditor_Statics::NewProp_ObjectToEdit_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineAssetEditor_Statics::NewProp_ObjectToEdit_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMoviePipelineAssetEditor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineAssetEditor_Statics::NewProp_ObjectToEdit,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineAssetEditor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineAssetEditor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineAssetEditor_Statics::ClassParams = {
		&UMoviePipelineAssetEditor::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMoviePipelineAssetEditor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineAssetEditor_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineAssetEditor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineAssetEditor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineAssetEditor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineAssetEditor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineAssetEditor, 1867332069);
	template<> MOVIERENDERPIPELINEEDITOR_API UClass* StaticClass<UMoviePipelineAssetEditor>()
	{
		return UMoviePipelineAssetEditor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineAssetEditor(Z_Construct_UClass_UMoviePipelineAssetEditor, &UMoviePipelineAssetEditor::StaticClass, TEXT("/Script/MovieRenderPipelineEditor"), TEXT("UMoviePipelineAssetEditor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineAssetEditor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
