// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMoviePipeline;
#ifdef MOVIERENDERPIPELINECORE_MovieRenderDebugWidget_generated_h
#error "MovieRenderDebugWidget.generated.h already included, missing '#pragma once' in MovieRenderDebugWidget.h"
#endif
#define MOVIERENDERPIPELINECORE_MovieRenderDebugWidget_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_EVENT_PARMS \
	struct MovieRenderDebugWidget_eventOnInitializedForPipeline_Parms \
	{ \
		UMoviePipeline* ForPipeline; \
	};


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_CALLBACK_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMovieRenderDebugWidget(); \
	friend struct Z_Construct_UClass_UMovieRenderDebugWidget_Statics; \
public: \
	DECLARE_CLASS(UMovieRenderDebugWidget, UUserWidget, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMovieRenderDebugWidget)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUMovieRenderDebugWidget(); \
	friend struct Z_Construct_UClass_UMovieRenderDebugWidget_Statics; \
public: \
	DECLARE_CLASS(UMovieRenderDebugWidget, UUserWidget, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMovieRenderDebugWidget)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieRenderDebugWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieRenderDebugWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieRenderDebugWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieRenderDebugWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieRenderDebugWidget(UMovieRenderDebugWidget&&); \
	NO_API UMovieRenderDebugWidget(const UMovieRenderDebugWidget&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMovieRenderDebugWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMovieRenderDebugWidget(UMovieRenderDebugWidget&&); \
	NO_API UMovieRenderDebugWidget(const UMovieRenderDebugWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMovieRenderDebugWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMovieRenderDebugWidget); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMovieRenderDebugWidget)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_14_PROLOG \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_EVENT_PARMS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_CALLBACK_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_CALLBACK_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<class UMovieRenderDebugWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Private_MovieRenderDebugWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
