// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FMoviePipelineOutputData;
class UMoviePipeline;
class UTexture;
class UMoviePipelineMasterConfig;
class UMoviePipelineExecutorJob;
#ifdef MOVIERENDERPIPELINECORE_MoviePipeline_generated_h
#error "MoviePipeline.generated.h already included, missing '#pragma once' in MoviePipeline.h"
#endif
#define MOVIERENDERPIPELINECORE_MoviePipeline_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_39_DELEGATE \
struct _Script_MovieRenderPipelineCore_eventMoviePipelineWorkFinished_Parms \
{ \
	FMoviePipelineOutputData Results; \
}; \
static inline void FMoviePipelineWorkFinished_DelegateWrapper(const FMulticastScriptDelegate& MoviePipelineWorkFinished, FMoviePipelineOutputData Results) \
{ \
	_Script_MovieRenderPipelineCore_eventMoviePipelineWorkFinished_Parms Parms; \
	Parms.Results=Results; \
	MoviePipelineWorkFinished.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_38_DELEGATE \
struct _Script_MovieRenderPipelineCore_eventMoviePipelineFinished_Parms \
{ \
	UMoviePipeline* MoviePipeline; \
	bool bFatalError; \
}; \
static inline void FMoviePipelineFinished_DelegateWrapper(const FMulticastScriptDelegate& MoviePipelineFinished, UMoviePipeline* MoviePipeline, bool bFatalError) \
{ \
	_Script_MovieRenderPipelineCore_eventMoviePipelineFinished_Parms Parms; \
	Parms.MoviePipeline=MoviePipeline; \
	Parms.bFatalError=bFatalError ? true : false; \
	MoviePipelineFinished.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnMoviePipelineFinishedImpl); \
	DECLARE_FUNCTION(execGetPreviewTexture); \
	DECLARE_FUNCTION(execGetPipelineMasterConfig); \
	DECLARE_FUNCTION(execIsShutdownRequested); \
	DECLARE_FUNCTION(execShutdown); \
	DECLARE_FUNCTION(execRequestShutdown); \
	DECLARE_FUNCTION(execInitialize);


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnMoviePipelineFinishedImpl); \
	DECLARE_FUNCTION(execGetPreviewTexture); \
	DECLARE_FUNCTION(execGetPipelineMasterConfig); \
	DECLARE_FUNCTION(execIsShutdownRequested); \
	DECLARE_FUNCTION(execShutdown); \
	DECLARE_FUNCTION(execRequestShutdown); \
	DECLARE_FUNCTION(execInitialize);


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipeline(); \
	friend struct Z_Construct_UClass_UMoviePipeline_Statics; \
public: \
	DECLARE_CLASS(UMoviePipeline, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipeline)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipeline(); \
	friend struct Z_Construct_UClass_UMoviePipeline_Statics; \
public: \
	DECLARE_CLASS(UMoviePipeline, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipeline)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipeline(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipeline) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipeline); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipeline); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipeline(UMoviePipeline&&); \
	NO_API UMoviePipeline(const UMoviePipeline&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipeline(UMoviePipeline&&); \
	NO_API UMoviePipeline(const UMoviePipeline&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipeline); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipeline); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMoviePipeline)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CustomTimeStep() { return STRUCT_OFFSET(UMoviePipeline, CustomTimeStep); } \
	FORCEINLINE static uint32 __PPO__CachedPrevCustomTimeStep() { return STRUCT_OFFSET(UMoviePipeline, CachedPrevCustomTimeStep); } \
	FORCEINLINE static uint32 __PPO__TargetSequence() { return STRUCT_OFFSET(UMoviePipeline, TargetSequence); } \
	FORCEINLINE static uint32 __PPO__LevelSequenceActor() { return STRUCT_OFFSET(UMoviePipeline, LevelSequenceActor); } \
	FORCEINLINE static uint32 __PPO__DebugWidget() { return STRUCT_OFFSET(UMoviePipeline, DebugWidget); } \
	FORCEINLINE static uint32 __PPO__PreviewTexture() { return STRUCT_OFFSET(UMoviePipeline, PreviewTexture); } \
	FORCEINLINE static uint32 __PPO__CurrentJob() { return STRUCT_OFFSET(UMoviePipeline, CurrentJob); }


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_41_PROLOG
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_44_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<class UMoviePipeline>();

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineCustomTimeStep(); \
	friend struct Z_Construct_UClass_UMoviePipelineCustomTimeStep_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineCustomTimeStep, UEngineCustomTimeStep, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineCustomTimeStep)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineCustomTimeStep(); \
	friend struct Z_Construct_UClass_UMoviePipelineCustomTimeStep_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineCustomTimeStep, UEngineCustomTimeStep, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineCustomTimeStep)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineCustomTimeStep(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineCustomTimeStep) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineCustomTimeStep); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineCustomTimeStep); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineCustomTimeStep(UMoviePipelineCustomTimeStep&&); \
	NO_API UMoviePipelineCustomTimeStep(const UMoviePipelineCustomTimeStep&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineCustomTimeStep(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineCustomTimeStep(UMoviePipelineCustomTimeStep&&); \
	NO_API UMoviePipelineCustomTimeStep(const UMoviePipelineCustomTimeStep&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineCustomTimeStep); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineCustomTimeStep); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineCustomTimeStep)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_465_PROLOG
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h_468_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<class UMoviePipelineCustomTimeStep>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipeline_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
