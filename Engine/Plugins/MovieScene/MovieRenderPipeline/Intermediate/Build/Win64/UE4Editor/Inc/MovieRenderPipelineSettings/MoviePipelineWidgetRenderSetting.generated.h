// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MOVIERENDERPIPELINESETTINGS_MoviePipelineWidgetRenderSetting_generated_h
#error "MoviePipelineWidgetRenderSetting.generated.h already included, missing '#pragma once' in MoviePipelineWidgetRenderSetting.h"
#endif
#define MOVIERENDERPIPELINESETTINGS_MoviePipelineWidgetRenderSetting_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineWidgetRenderer(); \
	friend struct Z_Construct_UClass_UMoviePipelineWidgetRenderer_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineWidgetRenderer, UMoviePipelineRenderPass, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineSettings"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineWidgetRenderer)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineWidgetRenderer(); \
	friend struct Z_Construct_UClass_UMoviePipelineWidgetRenderer_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineWidgetRenderer, UMoviePipelineRenderPass, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineSettings"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineWidgetRenderer)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineWidgetRenderer(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineWidgetRenderer) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineWidgetRenderer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineWidgetRenderer); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineWidgetRenderer(UMoviePipelineWidgetRenderer&&); \
	NO_API UMoviePipelineWidgetRenderer(const UMoviePipelineWidgetRenderer&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineWidgetRenderer(UMoviePipelineWidgetRenderer&&); \
	NO_API UMoviePipelineWidgetRenderer(const UMoviePipelineWidgetRenderer&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineWidgetRenderer); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineWidgetRenderer); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMoviePipelineWidgetRenderer)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RenderTarget() { return STRUCT_OFFSET(UMoviePipelineWidgetRenderer, RenderTarget); }


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_14_PROLOG
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINESETTINGS_API UClass* StaticClass<class UMoviePipelineWidgetRenderer>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineWidgetRenderSetting_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
