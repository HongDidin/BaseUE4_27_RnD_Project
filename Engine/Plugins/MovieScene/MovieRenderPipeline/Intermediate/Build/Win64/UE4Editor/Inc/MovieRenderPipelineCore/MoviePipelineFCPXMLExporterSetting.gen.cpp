// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MovieRenderPipelineCore/Public/MoviePipelineFCPXMLExporterSetting.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePipelineFCPXMLExporterSetting() {}
// Cross Module References
	MOVIERENDERPIPELINECORE_API UEnum* Z_Construct_UEnum_MovieRenderPipelineCore_FCPXMLExportDataSource();
	UPackage* Z_Construct_UPackage__Script_MovieRenderPipelineCore();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineFCPXMLExporter_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineFCPXMLExporter();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineOutputBase();
// End Cross Module References
	static UEnum* FCPXMLExportDataSource_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MovieRenderPipelineCore_FCPXMLExportDataSource, Z_Construct_UPackage__Script_MovieRenderPipelineCore(), TEXT("FCPXMLExportDataSource"));
		}
		return Singleton;
	}
	template<> MOVIERENDERPIPELINECORE_API UEnum* StaticEnum<FCPXMLExportDataSource>()
	{
		return FCPXMLExportDataSource_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_FCPXMLExportDataSource(FCPXMLExportDataSource_StaticEnum, TEXT("/Script/MovieRenderPipelineCore"), TEXT("FCPXMLExportDataSource"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MovieRenderPipelineCore_FCPXMLExportDataSource_Hash() { return 623516044U; }
	UEnum* Z_Construct_UEnum_MovieRenderPipelineCore_FCPXMLExportDataSource()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MovieRenderPipelineCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("FCPXMLExportDataSource"), 0, Get_Z_Construct_UEnum_MovieRenderPipelineCore_FCPXMLExportDataSource_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "FCPXMLExportDataSource::OutputMetadata", (int64)FCPXMLExportDataSource::OutputMetadata },
				{ "FCPXMLExportDataSource::SequenceData", (int64)FCPXMLExportDataSource::SequenceData },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "ModuleRelativePath", "Public/MoviePipelineFCPXMLExporterSetting.h" },
				{ "OutputMetadata.Name", "FCPXMLExportDataSource::OutputMetadata" },
				{ "SequenceData.Name", "FCPXMLExportDataSource::SequenceData" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
				nullptr,
				"FCPXMLExportDataSource",
				"FCPXMLExportDataSource",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMoviePipelineFCPXMLExporter::StaticRegisterNativesUMoviePipelineFCPXMLExporter()
	{
	}
	UClass* Z_Construct_UClass_UMoviePipelineFCPXMLExporter_NoRegister()
	{
		return UMoviePipelineFCPXMLExporter::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileNameFormatOverride_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileNameFormatOverride;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_DataSource_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DataSource_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_DataSource;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMoviePipelineOutputBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "MoviePipelineFCPXMLExporterSetting.h" },
		{ "ModuleRelativePath", "Public/MoviePipelineFCPXMLExporterSetting.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::NewProp_FileNameFormatOverride_MetaData[] = {
		{ "Category", "File Output" },
		{ "Comment", "/** File name format string override. If specified it will override the FileNameFormat from the Output setting. Can include folder prefixes, and format string tags ({sequence_name}, etc.) */" },
		{ "ModuleRelativePath", "Public/MoviePipelineFCPXMLExporterSetting.h" },
		{ "ToolTip", "File name format string override. If specified it will override the FileNameFormat from the Output setting. Can include folder prefixes, and format string tags ({sequence_name}, etc.)" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::NewProp_FileNameFormatOverride = { "FileNameFormatOverride", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineFCPXMLExporter, FileNameFormatOverride), METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::NewProp_FileNameFormatOverride_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::NewProp_FileNameFormatOverride_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::NewProp_DataSource_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::NewProp_DataSource_MetaData[] = {
		{ "Category", "File Output" },
		{ "Comment", "/** Whether to build the FCPXML from sequence data directly (for reimporting) or from actual frame output data (for post processing) */" },
		{ "ModuleRelativePath", "Public/MoviePipelineFCPXMLExporterSetting.h" },
		{ "ToolTip", "Whether to build the FCPXML from sequence data directly (for reimporting) or from actual frame output data (for post processing)" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::NewProp_DataSource = { "DataSource", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineFCPXMLExporter, DataSource), Z_Construct_UEnum_MovieRenderPipelineCore_FCPXMLExportDataSource, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::NewProp_DataSource_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::NewProp_DataSource_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::NewProp_FileNameFormatOverride,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::NewProp_DataSource_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::NewProp_DataSource,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineFCPXMLExporter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::ClassParams = {
		&UMoviePipelineFCPXMLExporter::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineFCPXMLExporter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineFCPXMLExporter, 812485960);
	template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<UMoviePipelineFCPXMLExporter>()
	{
		return UMoviePipelineFCPXMLExporter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineFCPXMLExporter(Z_Construct_UClass_UMoviePipelineFCPXMLExporter, &UMoviePipelineFCPXMLExporter::StaticClass, TEXT("/Script/MovieRenderPipelineCore"), TEXT("UMoviePipelineFCPXMLExporter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineFCPXMLExporter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
