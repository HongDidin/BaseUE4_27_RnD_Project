// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMoviePipelineExecutorJob;
#ifdef MOVIERENDERPIPELINEEDITOR_MoviePipelinePIEExecutor_generated_h
#error "MoviePipelinePIEExecutor.generated.h already included, missing '#pragma once' in MoviePipelinePIEExecutor.h"
#endif
#define MOVIERENDERPIPELINEEDITOR_MoviePipelinePIEExecutor_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_13_DELEGATE \
struct _Script_MovieRenderPipelineEditor_eventOnMoviePipelineIndividualJobFinished_Parms \
{ \
	UMoviePipelineExecutorJob* FinishedJob; \
	bool bSuccess; \
}; \
static inline void FOnMoviePipelineIndividualJobFinished_DelegateWrapper(const FMulticastScriptDelegate& OnMoviePipelineIndividualJobFinished, UMoviePipelineExecutorJob* FinishedJob, bool bSuccess) \
{ \
	_Script_MovieRenderPipelineEditor_eventOnMoviePipelineIndividualJobFinished_Parms Parms; \
	Parms.FinishedJob=FinishedJob; \
	Parms.bSuccess=bSuccess ? true : false; \
	OnMoviePipelineIndividualJobFinished.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelinePIEExecutor(); \
	friend struct Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelinePIEExecutor, UMoviePipelineLinearExecutorBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineEditor"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelinePIEExecutor)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelinePIEExecutor(); \
	friend struct Z_Construct_UClass_UMoviePipelinePIEExecutor_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelinePIEExecutor, UMoviePipelineLinearExecutorBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineEditor"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelinePIEExecutor)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelinePIEExecutor(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelinePIEExecutor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelinePIEExecutor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelinePIEExecutor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelinePIEExecutor(UMoviePipelinePIEExecutor&&); \
	NO_API UMoviePipelinePIEExecutor(const UMoviePipelinePIEExecutor&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelinePIEExecutor(UMoviePipelinePIEExecutor&&); \
	NO_API UMoviePipelinePIEExecutor(const UMoviePipelinePIEExecutor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelinePIEExecutor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelinePIEExecutor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMoviePipelinePIEExecutor)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__OnIndividualJobFinishedDelegate() { return STRUCT_OFFSET(UMoviePipelinePIEExecutor, OnIndividualJobFinishedDelegate); } \
	FORCEINLINE static uint32 __PPO__OnIndividualJobWorkFinishedDelegate() { return STRUCT_OFFSET(UMoviePipelinePIEExecutor, OnIndividualJobWorkFinishedDelegate); } \
	FORCEINLINE static uint32 __PPO__OnIndividualShotWorkFinishedDelegate() { return STRUCT_OFFSET(UMoviePipelinePIEExecutor, OnIndividualShotWorkFinishedDelegate); }


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_21_PROLOG
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINEEDITOR_API UClass* StaticClass<class UMoviePipelinePIEExecutor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelinePIEExecutor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
