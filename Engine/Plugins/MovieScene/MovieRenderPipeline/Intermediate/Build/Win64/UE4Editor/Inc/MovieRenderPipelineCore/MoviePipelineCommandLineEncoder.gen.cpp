// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MovieRenderPipelineCore/Public/MoviePipelineCommandLineEncoder.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePipelineCommandLineEncoder() {}
// Cross Module References
	MOVIERENDERPIPELINECORE_API UEnum* Z_Construct_UEnum_MovieRenderPipelineCore_EMoviePipelineEncodeQuality();
	UPackage* Z_Construct_UPackage__Script_MovieRenderPipelineCore();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineCommandLineEncoder_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineCommandLineEncoder();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineSetting();
// End Cross Module References
	static UEnum* EMoviePipelineEncodeQuality_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MovieRenderPipelineCore_EMoviePipelineEncodeQuality, Z_Construct_UPackage__Script_MovieRenderPipelineCore(), TEXT("EMoviePipelineEncodeQuality"));
		}
		return Singleton;
	}
	template<> MOVIERENDERPIPELINECORE_API UEnum* StaticEnum<EMoviePipelineEncodeQuality>()
	{
		return EMoviePipelineEncodeQuality_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMoviePipelineEncodeQuality(EMoviePipelineEncodeQuality_StaticEnum, TEXT("/Script/MovieRenderPipelineCore"), TEXT("EMoviePipelineEncodeQuality"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MovieRenderPipelineCore_EMoviePipelineEncodeQuality_Hash() { return 2798597072U; }
	UEnum* Z_Construct_UEnum_MovieRenderPipelineCore_EMoviePipelineEncodeQuality()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MovieRenderPipelineCore();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMoviePipelineEncodeQuality"), 0, Get_Z_Construct_UEnum_MovieRenderPipelineCore_EMoviePipelineEncodeQuality_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMoviePipelineEncodeQuality::Low", (int64)EMoviePipelineEncodeQuality::Low },
				{ "EMoviePipelineEncodeQuality::Medium", (int64)EMoviePipelineEncodeQuality::Medium },
				{ "EMoviePipelineEncodeQuality::High", (int64)EMoviePipelineEncodeQuality::High },
				{ "EMoviePipelineEncodeQuality::Epic", (int64)EMoviePipelineEncodeQuality::Epic },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "BlueprintType", "true" },
				{ "Epic.Name", "EMoviePipelineEncodeQuality::Epic" },
				{ "High.Name", "EMoviePipelineEncodeQuality::High" },
				{ "Low.Name", "EMoviePipelineEncodeQuality::Low" },
				{ "Medium.Name", "EMoviePipelineEncodeQuality::Medium" },
				{ "ModuleRelativePath", "Public/MoviePipelineCommandLineEncoder.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
				nullptr,
				"EMoviePipelineEncodeQuality",
				"EMoviePipelineEncodeQuality",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMoviePipelineCommandLineEncoder::StaticRegisterNativesUMoviePipelineCommandLineEncoder()
	{
	}
	UClass* Z_Construct_UClass_UMoviePipelineCommandLineEncoder_NoRegister()
	{
		return UMoviePipelineCommandLineEncoder::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FileNameFormatOverride_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_FileNameFormatOverride;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_Quality_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Quality_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_Quality;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AdditionalCommandLineArgs_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_AdditionalCommandLineArgs;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bDeleteSourceFiles_MetaData[];
#endif
		static void NewProp_bDeleteSourceFiles_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bDeleteSourceFiles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSkipEncodeOnRenderCanceled_MetaData[];
#endif
		static void NewProp_bSkipEncodeOnRenderCanceled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSkipEncodeOnRenderCanceled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMoviePipelineSetting,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "MoviePipelineCommandLineEncoder.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MoviePipelineCommandLineEncoder.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_FileNameFormatOverride_MetaData[] = {
		{ "Category", "Command Line Encoder" },
		{ "Comment", "/** \n\x09* File name format string override. If specified it will override the FileNameFormat from the Output setting.\n\x09* If {shot_name} or {camera_name} is used, encoding will begin after each shot finishes rendering.\n\x09* Can be different from the main one in the Output setting so you can render out frames to individual\n\x09* shot folders but encode to one file.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineCommandLineEncoder.h" },
		{ "ToolTip", "File name format string override. If specified it will override the FileNameFormat from the Output setting.\nIf {shot_name} or {camera_name} is used, encoding will begin after each shot finishes rendering.\nCan be different from the main one in the Output setting so you can render out frames to individual\nshot folders but encode to one file." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_FileNameFormatOverride = { "FileNameFormatOverride", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineCommandLineEncoder, FileNameFormatOverride), METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_FileNameFormatOverride_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_FileNameFormatOverride_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_Quality_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_Quality_MetaData[] = {
		{ "Category", "Command Line Encoder" },
		{ "Comment", "/** What encoding quality to use for this job? Exact command line arguments for each one are specified in Project Settings. */" },
		{ "ModuleRelativePath", "Public/MoviePipelineCommandLineEncoder.h" },
		{ "ToolTip", "What encoding quality to use for this job? Exact command line arguments for each one are specified in Project Settings." },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_Quality = { "Quality", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineCommandLineEncoder, Quality), Z_Construct_UEnum_MovieRenderPipelineCore_EMoviePipelineEncodeQuality, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_Quality_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_Quality_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_AdditionalCommandLineArgs_MetaData[] = {
		{ "Category", "Command Line Encoder" },
		{ "Comment", "/** Any additional arguments to pass to the CLI encode for this particular job. */" },
		{ "ModuleRelativePath", "Public/MoviePipelineCommandLineEncoder.h" },
		{ "ToolTip", "Any additional arguments to pass to the CLI encode for this particular job." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_AdditionalCommandLineArgs = { "AdditionalCommandLineArgs", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineCommandLineEncoder, AdditionalCommandLineArgs), METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_AdditionalCommandLineArgs_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_AdditionalCommandLineArgs_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_bDeleteSourceFiles_MetaData[] = {
		{ "Category", "Command Line Encoder" },
		{ "Comment", "/** Should we delete the source files from disk after encoding? */" },
		{ "ModuleRelativePath", "Public/MoviePipelineCommandLineEncoder.h" },
		{ "ToolTip", "Should we delete the source files from disk after encoding?" },
	};
#endif
	void Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_bDeleteSourceFiles_SetBit(void* Obj)
	{
		((UMoviePipelineCommandLineEncoder*)Obj)->bDeleteSourceFiles = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_bDeleteSourceFiles = { "bDeleteSourceFiles", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMoviePipelineCommandLineEncoder), &Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_bDeleteSourceFiles_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_bDeleteSourceFiles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_bDeleteSourceFiles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_bSkipEncodeOnRenderCanceled_MetaData[] = {
		{ "Category", "Command Line Encoder" },
		{ "Comment", "/** If a render was canceled (via hitting escape mid render) should we skip trying to encode the files we did produce? */" },
		{ "ModuleRelativePath", "Public/MoviePipelineCommandLineEncoder.h" },
		{ "ToolTip", "If a render was canceled (via hitting escape mid render) should we skip trying to encode the files we did produce?" },
	};
#endif
	void Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_bSkipEncodeOnRenderCanceled_SetBit(void* Obj)
	{
		((UMoviePipelineCommandLineEncoder*)Obj)->bSkipEncodeOnRenderCanceled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_bSkipEncodeOnRenderCanceled = { "bSkipEncodeOnRenderCanceled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMoviePipelineCommandLineEncoder), &Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_bSkipEncodeOnRenderCanceled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_bSkipEncodeOnRenderCanceled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_bSkipEncodeOnRenderCanceled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_FileNameFormatOverride,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_Quality_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_Quality,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_AdditionalCommandLineArgs,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_bDeleteSourceFiles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::NewProp_bSkipEncodeOnRenderCanceled,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineCommandLineEncoder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::ClassParams = {
		&UMoviePipelineCommandLineEncoder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineCommandLineEncoder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineCommandLineEncoder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineCommandLineEncoder, 3984808630);
	template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<UMoviePipelineCommandLineEncoder>()
	{
		return UMoviePipelineCommandLineEncoder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineCommandLineEncoder(Z_Construct_UClass_UMoviePipelineCommandLineEncoder, &UMoviePipelineCommandLineEncoder::StaticClass, TEXT("/Script/MovieRenderPipelineCore"), TEXT("UMoviePipelineCommandLineEncoder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineCommandLineEncoder);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
