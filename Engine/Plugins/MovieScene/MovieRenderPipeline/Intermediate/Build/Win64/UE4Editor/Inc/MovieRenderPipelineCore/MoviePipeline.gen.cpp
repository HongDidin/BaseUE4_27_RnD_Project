// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MovieRenderPipelineCore/Public/MoviePipeline.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePipeline() {}
// Cross Module References
	MOVIERENDERPIPELINECORE_API UFunction* Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature();
	UPackage* Z_Construct_UPackage__Script_MovieRenderPipelineCore();
	MOVIERENDERPIPELINECORE_API UScriptStruct* Z_Construct_UScriptStruct_FMoviePipelineOutputData();
	MOVIERENDERPIPELINECORE_API UFunction* Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipeline_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipeline();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineMasterConfig_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineCustomTimeStep_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UEngineCustomTimeStep_NoRegister();
	LEVELSEQUENCE_API UClass* Z_Construct_UClass_ULevelSequence_NoRegister();
	LEVELSEQUENCE_API UClass* Z_Construct_UClass_ALevelSequenceActor_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMovieRenderDebugWidget_NoRegister();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineCustomTimeStep();
	ENGINE_API UClass* Z_Construct_UClass_UEngineCustomTimeStep();
// End Cross Module References
	struct Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature_Statics
	{
		struct _Script_MovieRenderPipelineCore_eventMoviePipelineWorkFinished_Parms
		{
			FMoviePipelineOutputData Results;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Results;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature_Statics::NewProp_Results = { "Results", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MovieRenderPipelineCore_eventMoviePipelineWorkFinished_Parms, Results), Z_Construct_UScriptStruct_FMoviePipelineOutputData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature_Statics::NewProp_Results,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore, nullptr, "MoviePipelineWorkFinished__DelegateSignature", nullptr, nullptr, sizeof(_Script_MovieRenderPipelineCore_eventMoviePipelineWorkFinished_Parms), Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature_Statics
	{
		struct _Script_MovieRenderPipelineCore_eventMoviePipelineFinished_Parms
		{
			UMoviePipeline* MoviePipeline;
			bool bFatalError;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MoviePipeline;
		static void NewProp_bFatalError_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFatalError;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature_Statics::NewProp_MoviePipeline = { "MoviePipeline", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_Script_MovieRenderPipelineCore_eventMoviePipelineFinished_Parms, MoviePipeline), Z_Construct_UClass_UMoviePipeline_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature_Statics::NewProp_bFatalError_SetBit(void* Obj)
	{
		((_Script_MovieRenderPipelineCore_eventMoviePipelineFinished_Parms*)Obj)->bFatalError = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature_Statics::NewProp_bFatalError = { "bFatalError", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(_Script_MovieRenderPipelineCore_eventMoviePipelineFinished_Parms), &Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature_Statics::NewProp_bFatalError_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature_Statics::NewProp_MoviePipeline,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature_Statics::NewProp_bFatalError,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature_Statics::FuncParams = { (UObject*(*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore, nullptr, "MoviePipelineFinished__DelegateSignature", nullptr, nullptr, sizeof(_Script_MovieRenderPipelineCore_eventMoviePipelineFinished_Parms), Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00130000, 0, 0, METADATA_PARAMS(Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	DEFINE_FUNCTION(UMoviePipeline::execOnMoviePipelineFinishedImpl)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnMoviePipelineFinishedImpl();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipeline::execGetPreviewTexture)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UTexture**)Z_Param__Result=P_THIS->GetPreviewTexture();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipeline::execGetPipelineMasterConfig)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UMoviePipelineMasterConfig**)Z_Param__Result=P_THIS->GetPipelineMasterConfig();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipeline::execIsShutdownRequested)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->IsShutdownRequested();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipeline::execShutdown)
	{
		P_GET_UBOOL(Z_Param_bError);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Shutdown(Z_Param_bError);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipeline::execRequestShutdown)
	{
		P_GET_UBOOL(Z_Param_bIsError);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->RequestShutdown(Z_Param_bIsError);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(UMoviePipeline::execInitialize)
	{
		P_GET_OBJECT(UMoviePipelineExecutorJob,Z_Param_InJob);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->Initialize(Z_Param_InJob);
		P_NATIVE_END;
	}
	void UMoviePipeline::StaticRegisterNativesUMoviePipeline()
	{
		UClass* Class = UMoviePipeline::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetPipelineMasterConfig", &UMoviePipeline::execGetPipelineMasterConfig },
			{ "GetPreviewTexture", &UMoviePipeline::execGetPreviewTexture },
			{ "Initialize", &UMoviePipeline::execInitialize },
			{ "IsShutdownRequested", &UMoviePipeline::execIsShutdownRequested },
			{ "OnMoviePipelineFinishedImpl", &UMoviePipeline::execOnMoviePipelineFinishedImpl },
			{ "RequestShutdown", &UMoviePipeline::execRequestShutdown },
			{ "Shutdown", &UMoviePipeline::execShutdown },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMoviePipeline_GetPipelineMasterConfig_Statics
	{
		struct MoviePipeline_eventGetPipelineMasterConfig_Parms
		{
			UMoviePipelineMasterConfig* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipeline_GetPipelineMasterConfig_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipeline_eventGetPipelineMasterConfig_Parms, ReturnValue), Z_Construct_UClass_UMoviePipelineMasterConfig_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipeline_GetPipelineMasterConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipeline_GetPipelineMasterConfig_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipeline_GetPipelineMasterConfig_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* Get the Master Configuration used to render this shot. This contains the global settings for the shot, as well as per-shot\n\x09* configurations which can contain their own settings.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
		{ "ToolTip", "Get the Master Configuration used to render this shot. This contains the global settings for the shot, as well as per-shot\nconfigurations which can contain their own settings." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipeline_GetPipelineMasterConfig_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipeline, nullptr, "GetPipelineMasterConfig", nullptr, nullptr, sizeof(MoviePipeline_eventGetPipelineMasterConfig_Parms), Z_Construct_UFunction_UMoviePipeline_GetPipelineMasterConfig_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipeline_GetPipelineMasterConfig_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipeline_GetPipelineMasterConfig_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipeline_GetPipelineMasterConfig_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipeline_GetPipelineMasterConfig()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipeline_GetPipelineMasterConfig_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipeline_GetPreviewTexture_Statics
	{
		struct MoviePipeline_eventGetPreviewTexture_Parms
		{
			UTexture* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipeline_GetPreviewTexture_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipeline_eventGetPreviewTexture_Parms, ReturnValue), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipeline_GetPreviewTexture_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipeline_GetPreviewTexture_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipeline_GetPreviewTexture_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipeline_GetPreviewTexture_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipeline, nullptr, "GetPreviewTexture", nullptr, nullptr, sizeof(MoviePipeline_eventGetPreviewTexture_Parms), Z_Construct_UFunction_UMoviePipeline_GetPreviewTexture_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipeline_GetPreviewTexture_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipeline_GetPreviewTexture_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipeline_GetPreviewTexture_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipeline_GetPreviewTexture()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipeline_GetPreviewTexture_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipeline_Initialize_Statics
	{
		struct MoviePipeline_eventInitialize_Parms
		{
			UMoviePipelineExecutorJob* InJob;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InJob;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_UMoviePipeline_Initialize_Statics::NewProp_InJob = { "InJob", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipeline_eventInitialize_Parms, InJob), Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipeline_Initialize_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipeline_Initialize_Statics::NewProp_InJob,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipeline_Initialize_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** \n\x09* Initialize the movie pipeline with the specified settings. This kicks off the rendering process. \n\x09* @param InJob\x09- This contains settings and sequence to render this Movie Pipeline with.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
		{ "ToolTip", "Initialize the movie pipeline with the specified settings. This kicks off the rendering process.\n@param InJob  - This contains settings and sequence to render this Movie Pipeline with." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipeline_Initialize_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipeline, nullptr, "Initialize", nullptr, nullptr, sizeof(MoviePipeline_eventInitialize_Parms), Z_Construct_UFunction_UMoviePipeline_Initialize_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipeline_Initialize_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipeline_Initialize_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipeline_Initialize_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipeline_Initialize()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipeline_Initialize_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipeline_IsShutdownRequested_Statics
	{
		struct MoviePipeline_eventIsShutdownRequested_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMoviePipeline_IsShutdownRequested_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((MoviePipeline_eventIsShutdownRequested_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMoviePipeline_IsShutdownRequested_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MoviePipeline_eventIsShutdownRequested_Parms), &Z_Construct_UFunction_UMoviePipeline_IsShutdownRequested_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipeline_IsShutdownRequested_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipeline_IsShutdownRequested_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipeline_IsShutdownRequested_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* Has RequestShutdown() been called?\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
		{ "ToolTip", "Has RequestShutdown() been called?" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipeline_IsShutdownRequested_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipeline, nullptr, "IsShutdownRequested", nullptr, nullptr, sizeof(MoviePipeline_eventIsShutdownRequested_Parms), Z_Construct_UFunction_UMoviePipeline_IsShutdownRequested_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipeline_IsShutdownRequested_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipeline_IsShutdownRequested_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipeline_IsShutdownRequested_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipeline_IsShutdownRequested()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipeline_IsShutdownRequested_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipeline_OnMoviePipelineFinishedImpl_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipeline_OnMoviePipelineFinishedImpl_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* This function should be called by the Executor when execution has finished (this should still be called in the event of an error)\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
		{ "ToolTip", "This function should be called by the Executor when execution has finished (this should still be called in the event of an error)" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipeline_OnMoviePipelineFinishedImpl_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipeline, nullptr, "OnMoviePipelineFinishedImpl", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04080400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipeline_OnMoviePipelineFinishedImpl_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipeline_OnMoviePipelineFinishedImpl_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipeline_OnMoviePipelineFinishedImpl()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipeline_OnMoviePipelineFinishedImpl_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipeline_RequestShutdown_Statics
	{
		struct MoviePipeline_eventRequestShutdown_Parms
		{
			bool bIsError;
		};
		static void NewProp_bIsError_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsError;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMoviePipeline_RequestShutdown_Statics::NewProp_bIsError_SetBit(void* Obj)
	{
		((MoviePipeline_eventRequestShutdown_Parms*)Obj)->bIsError = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMoviePipeline_RequestShutdown_Statics::NewProp_bIsError = { "bIsError", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MoviePipeline_eventRequestShutdown_Parms), &Z_Construct_UFunction_UMoviePipeline_RequestShutdown_Statics::NewProp_bIsError_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipeline_RequestShutdown_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipeline_RequestShutdown_Statics::NewProp_bIsError,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipeline_RequestShutdown_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* Request the movie pipeline to shut down at the next available time. The pipeline will attempt to abandon\n\x09* the current frame (such as if there are more temporal samples pending) but may be forced into finishing if\n\x09* there are spatial samples already submitted to the GPU. The shutdown flow will be run to ensure already\n\x09* completed work is written to disk. This is a non-blocking operation, use Shutdown() instead if you need to\n\x09* block until it is fully shut down.\n\x09*\n\x09* @param bError - Whether this is a request for early shut down due to an error\n\x09* \n\x09* This function is thread safe.\n\x09*/" },
		{ "CPP_Default_bIsError", "false" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
		{ "ToolTip", "Request the movie pipeline to shut down at the next available time. The pipeline will attempt to abandon\nthe current frame (such as if there are more temporal samples pending) but may be forced into finishing if\nthere are spatial samples already submitted to the GPU. The shutdown flow will be run to ensure already\ncompleted work is written to disk. This is a non-blocking operation, use Shutdown() instead if you need to\nblock until it is fully shut down.\n\n@param bError - Whether this is a request for early shut down due to an error\n\nThis function is thread safe." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipeline_RequestShutdown_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipeline, nullptr, "RequestShutdown", nullptr, nullptr, sizeof(MoviePipeline_eventRequestShutdown_Parms), Z_Construct_UFunction_UMoviePipeline_RequestShutdown_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipeline_RequestShutdown_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipeline_RequestShutdown_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipeline_RequestShutdown_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipeline_RequestShutdown()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipeline_RequestShutdown_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_UMoviePipeline_Shutdown_Statics
	{
		struct MoviePipeline_eventShutdown_Parms
		{
			bool bError;
		};
		static void NewProp_bError_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bError;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_UMoviePipeline_Shutdown_Statics::NewProp_bError_SetBit(void* Obj)
	{
		((MoviePipeline_eventShutdown_Parms*)Obj)->bError = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UMoviePipeline_Shutdown_Statics::NewProp_bError = { "bError", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(MoviePipeline_eventShutdown_Parms), &Z_Construct_UFunction_UMoviePipeline_Shutdown_Statics::NewProp_bError_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipeline_Shutdown_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipeline_Shutdown_Statics::NewProp_bError,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipeline_Shutdown_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/** \n\x09* Abandons any future work on this Movie Pipeline and runs through the shutdown flow to ensure already\n\x09* completed work is written to disk. This is a blocking-operation and will not return until all outstanding\n\x09* work has been completed.\n\x09*\n\x09* @param bError - Whether this is an early shut down due to an error\n\x09*\n\x09* This function should only be called from the game thread.\n\x09*/" },
		{ "CPP_Default_bError", "false" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
		{ "ToolTip", "Abandons any future work on this Movie Pipeline and runs through the shutdown flow to ensure already\ncompleted work is written to disk. This is a blocking-operation and will not return until all outstanding\nwork has been completed.\n\n@param bError - Whether this is an early shut down due to an error\n\nThis function should only be called from the game thread." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipeline_Shutdown_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipeline, nullptr, "Shutdown", nullptr, nullptr, sizeof(MoviePipeline_eventShutdown_Parms), Z_Construct_UFunction_UMoviePipeline_Shutdown_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipeline_Shutdown_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipeline_Shutdown_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipeline_Shutdown_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipeline_Shutdown()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipeline_Shutdown_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMoviePipeline_NoRegister()
	{
		return UMoviePipeline::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipeline_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnMoviePipelineFinishedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnMoviePipelineFinishedDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnMoviePipelineWorkFinishedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnMoviePipelineWorkFinishedDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OnMoviePipelineShotWorkFinishedDelegate_MetaData[];
#endif
		static const UE4CodeGen_Private::FMulticastDelegatePropertyParams NewProp_OnMoviePipelineShotWorkFinishedDelegate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CustomTimeStep_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CustomTimeStep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedPrevCustomTimeStep_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedPrevCustomTimeStep;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TargetSequence_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TargetSequence;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LevelSequenceActor_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LevelSequenceActor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugWidget_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DebugWidget;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PreviewTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PreviewTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DebugWidgetClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_DebugWidgetClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentJob_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CurrentJob;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipeline_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMoviePipeline_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMoviePipeline_GetPipelineMasterConfig, "GetPipelineMasterConfig" }, // 3441500561
		{ &Z_Construct_UFunction_UMoviePipeline_GetPreviewTexture, "GetPreviewTexture" }, // 2938427846
		{ &Z_Construct_UFunction_UMoviePipeline_Initialize, "Initialize" }, // 832893026
		{ &Z_Construct_UFunction_UMoviePipeline_IsShutdownRequested, "IsShutdownRequested" }, // 2468072671
		{ &Z_Construct_UFunction_UMoviePipeline_OnMoviePipelineFinishedImpl, "OnMoviePipelineFinishedImpl" }, // 665638921
		{ &Z_Construct_UFunction_UMoviePipeline_RequestShutdown, "RequestShutdown" }, // 169304741
		{ &Z_Construct_UFunction_UMoviePipeline_Shutdown, "Shutdown" }, // 2667114400
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipeline_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "MoviePipeline.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipeline_Statics::NewProp_OnMoviePipelineFinishedDelegate_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMoviePipeline_Statics::NewProp_OnMoviePipelineFinishedDelegate = { "OnMoviePipelineFinishedDelegate", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipeline, OnMoviePipelineFinishedDelegate), Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineFinished__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_OnMoviePipelineFinishedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_OnMoviePipelineFinishedDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipeline_Statics::NewProp_OnMoviePipelineWorkFinishedDelegate_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* Called when we have completely finished this pipeline. This means that all frames have been rendered,\n\x09* all files written to disk, and any post-finalize exports have finished. This Pipeline will call\n\x09* Shutdown() on itself before calling this delegate to ensure we've unregistered from all delegates\n\x09* and are no longer trying to do anything (even if we still exist).\n\x09*\n\x09* The params struct in the return will have metadata about files written to disk for each shot.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
		{ "ToolTip", "Called when we have completely finished this pipeline. This means that all frames have been rendered,\nall files written to disk, and any post-finalize exports have finished. This Pipeline will call\nShutdown() on itself before calling this delegate to ensure we've unregistered from all delegates\nand are no longer trying to do anything (even if we still exist).\n\nThe params struct in the return will have metadata about files written to disk for each shot." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMoviePipeline_Statics::NewProp_OnMoviePipelineWorkFinishedDelegate = { "OnMoviePipelineWorkFinishedDelegate", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipeline, OnMoviePipelineWorkFinishedDelegate), Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_OnMoviePipelineWorkFinishedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_OnMoviePipelineWorkFinishedDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipeline_Statics::NewProp_OnMoviePipelineShotWorkFinishedDelegate_MetaData[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* Only called if `IsFlushDiskWritesPerShot()` is set!\n\x09* Called after each shot is finished and files have been flushed to disk. The returned data in\n\x09* the params struct will have only the per-shot metadata for the just finished shot. Use\n\x09* OnMoviePipelineFinished() if you need all ot the metadata.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
		{ "ToolTip", "Only called if `IsFlushDiskWritesPerShot()` is set!\nCalled after each shot is finished and files have been flushed to disk. The returned data in\nthe params struct will have only the per-shot metadata for the just finished shot. Use\nOnMoviePipelineFinished() if you need all ot the metadata." },
	};
#endif
	const UE4CodeGen_Private::FMulticastDelegatePropertyParams Z_Construct_UClass_UMoviePipeline_Statics::NewProp_OnMoviePipelineShotWorkFinishedDelegate = { "OnMoviePipelineShotWorkFinishedDelegate", nullptr, (EPropertyFlags)0x0010000010080000, UE4CodeGen_Private::EPropertyGenFlags::InlineMulticastDelegate, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipeline, OnMoviePipelineShotWorkFinishedDelegate), Z_Construct_UDelegateFunction_MovieRenderPipelineCore_MoviePipelineWorkFinished__DelegateSignature, METADATA_PARAMS(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_OnMoviePipelineShotWorkFinishedDelegate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_OnMoviePipelineShotWorkFinishedDelegate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipeline_Statics::NewProp_CustomTimeStep_MetaData[] = {
		{ "Comment", "/** Custom TimeStep used to drive the engine while rendering. */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
		{ "ToolTip", "Custom TimeStep used to drive the engine while rendering." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMoviePipeline_Statics::NewProp_CustomTimeStep = { "CustomTimeStep", nullptr, (EPropertyFlags)0x0042000000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipeline, CustomTimeStep), Z_Construct_UClass_UMoviePipelineCustomTimeStep_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_CustomTimeStep_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_CustomTimeStep_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipeline_Statics::NewProp_CachedPrevCustomTimeStep_MetaData[] = {
		{ "Comment", "/** Hold a reference to the existing custom time step (if any) so we can restore it after we're done using our custom one. */" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
		{ "ToolTip", "Hold a reference to the existing custom time step (if any) so we can restore it after we're done using our custom one." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMoviePipeline_Statics::NewProp_CachedPrevCustomTimeStep = { "CachedPrevCustomTimeStep", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipeline, CachedPrevCustomTimeStep), Z_Construct_UClass_UEngineCustomTimeStep_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_CachedPrevCustomTimeStep_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_CachedPrevCustomTimeStep_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipeline_Statics::NewProp_TargetSequence_MetaData[] = {
		{ "Comment", "/** This is our duplicated sequence that we're rendering. This will get modified throughout the rendering process. */" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
		{ "ToolTip", "This is our duplicated sequence that we're rendering. This will get modified throughout the rendering process." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMoviePipeline_Statics::NewProp_TargetSequence = { "TargetSequence", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipeline, TargetSequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_TargetSequence_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_TargetSequence_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipeline_Statics::NewProp_LevelSequenceActor_MetaData[] = {
		{ "Comment", "/** The Level Sequence Actor we spawned to play our TargetSequence. */" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
		{ "ToolTip", "The Level Sequence Actor we spawned to play our TargetSequence." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMoviePipeline_Statics::NewProp_LevelSequenceActor = { "LevelSequenceActor", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipeline, LevelSequenceActor), Z_Construct_UClass_ALevelSequenceActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_LevelSequenceActor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_LevelSequenceActor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipeline_Statics::NewProp_DebugWidget_MetaData[] = {
		{ "Comment", "/** The Debug UI Widget that is spawned and placed on the player UI */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
		{ "ToolTip", "The Debug UI Widget that is spawned and placed on the player UI" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMoviePipeline_Statics::NewProp_DebugWidget = { "DebugWidget", nullptr, (EPropertyFlags)0x0040000000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipeline, DebugWidget), Z_Construct_UClass_UMovieRenderDebugWidget_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_DebugWidget_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_DebugWidget_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipeline_Statics::NewProp_PreviewTexture_MetaData[] = {
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMoviePipeline_Statics::NewProp_PreviewTexture = { "PreviewTexture", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipeline, PreviewTexture), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_PreviewTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_PreviewTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipeline_Statics::NewProp_DebugWidgetClass_MetaData[] = {
		{ "Comment", "/** Optional widget for feedback during render */" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
		{ "ToolTip", "Optional widget for feedback during render" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UMoviePipeline_Statics::NewProp_DebugWidgetClass = { "DebugWidgetClass", nullptr, (EPropertyFlags)0x0014000000002000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipeline, DebugWidgetClass), Z_Construct_UClass_UMovieRenderDebugWidget_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_DebugWidgetClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_DebugWidgetClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipeline_Statics::NewProp_CurrentJob_MetaData[] = {
		{ "Comment", "/** Keep track of which job we're working on. This holds our Configuration + which shots we're supposed to render from it. */" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
		{ "ToolTip", "Keep track of which job we're working on. This holds our Configuration + which shots we're supposed to render from it." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMoviePipeline_Statics::NewProp_CurrentJob = { "CurrentJob", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipeline, CurrentJob), Z_Construct_UClass_UMoviePipelineExecutorJob_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_CurrentJob_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipeline_Statics::NewProp_CurrentJob_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMoviePipeline_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipeline_Statics::NewProp_OnMoviePipelineFinishedDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipeline_Statics::NewProp_OnMoviePipelineWorkFinishedDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipeline_Statics::NewProp_OnMoviePipelineShotWorkFinishedDelegate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipeline_Statics::NewProp_CustomTimeStep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipeline_Statics::NewProp_CachedPrevCustomTimeStep,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipeline_Statics::NewProp_TargetSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipeline_Statics::NewProp_LevelSequenceActor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipeline_Statics::NewProp_DebugWidget,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipeline_Statics::NewProp_PreviewTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipeline_Statics::NewProp_DebugWidgetClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipeline_Statics::NewProp_CurrentJob,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipeline_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipeline>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipeline_Statics::ClassParams = {
		&UMoviePipeline::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMoviePipeline_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipeline_Statics::PropPointers),
		0,
		0x009000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipeline_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipeline_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipeline()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipeline_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipeline, 808608881);
	template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<UMoviePipeline>()
	{
		return UMoviePipeline::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipeline(Z_Construct_UClass_UMoviePipeline, &UMoviePipeline::StaticClass, TEXT("/Script/MovieRenderPipelineCore"), TEXT("UMoviePipeline"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipeline);
	void UMoviePipelineCustomTimeStep::StaticRegisterNativesUMoviePipelineCustomTimeStep()
	{
	}
	UClass* Z_Construct_UClass_UMoviePipelineCustomTimeStep_NoRegister()
	{
		return UMoviePipelineCustomTimeStep::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineCustomTimeStep_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineCustomTimeStep_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEngineCustomTimeStep,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineCustomTimeStep_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MoviePipeline.h" },
		{ "ModuleRelativePath", "Public/MoviePipeline.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineCustomTimeStep_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineCustomTimeStep>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineCustomTimeStep_Statics::ClassParams = {
		&UMoviePipelineCustomTimeStep::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineCustomTimeStep_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineCustomTimeStep_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineCustomTimeStep()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineCustomTimeStep_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineCustomTimeStep, 3907928501);
	template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<UMoviePipelineCustomTimeStep>()
	{
		return UMoviePipelineCustomTimeStep::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineCustomTimeStep(Z_Construct_UClass_UMoviePipelineCustomTimeStep, &UMoviePipelineCustomTimeStep::StaticClass, TEXT("/Script/MovieRenderPipelineCore"), TEXT("UMoviePipelineCustomTimeStep"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineCustomTimeStep);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
