// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMoviePipeline;
#ifdef MOVIERENDERPIPELINESETTINGS_MoviePipelineBurnInWidget_generated_h
#error "MoviePipelineBurnInWidget.generated.h already included, missing '#pragma once' in MoviePipelineBurnInWidget.h"
#endif
#define MOVIERENDERPIPELINESETTINGS_MoviePipelineBurnInWidget_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_EVENT_PARMS \
	struct MoviePipelineBurnInWidget_eventOnOutputFrameStarted_Parms \
	{ \
		UMoviePipeline* ForPipeline; \
	};


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_CALLBACK_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineBurnInWidget(); \
	friend struct Z_Construct_UClass_UMoviePipelineBurnInWidget_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineBurnInWidget, UUserWidget, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineSettings"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineBurnInWidget)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineBurnInWidget(); \
	friend struct Z_Construct_UClass_UMoviePipelineBurnInWidget_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineBurnInWidget, UUserWidget, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineSettings"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineBurnInWidget)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineBurnInWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineBurnInWidget) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineBurnInWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineBurnInWidget); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineBurnInWidget(UMoviePipelineBurnInWidget&&); \
	NO_API UMoviePipelineBurnInWidget(const UMoviePipelineBurnInWidget&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineBurnInWidget(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineBurnInWidget(UMoviePipelineBurnInWidget&&); \
	NO_API UMoviePipelineBurnInWidget(const UMoviePipelineBurnInWidget&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineBurnInWidget); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineBurnInWidget); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineBurnInWidget)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_11_PROLOG \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_EVENT_PARMS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_CALLBACK_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_CALLBACK_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h_15_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINESETTINGS_API UClass* StaticClass<class UMoviePipelineBurnInWidget>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineBurnInWidget_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
