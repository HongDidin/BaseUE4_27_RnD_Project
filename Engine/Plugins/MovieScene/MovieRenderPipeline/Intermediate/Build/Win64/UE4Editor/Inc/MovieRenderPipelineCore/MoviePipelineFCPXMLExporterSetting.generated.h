// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MOVIERENDERPIPELINECORE_MoviePipelineFCPXMLExporterSetting_generated_h
#error "MoviePipelineFCPXMLExporterSetting.generated.h already included, missing '#pragma once' in MoviePipelineFCPXMLExporterSetting.h"
#endif
#define MOVIERENDERPIPELINECORE_MoviePipelineFCPXMLExporterSetting_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineFCPXMLExporter(); \
	friend struct Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineFCPXMLExporter, UMoviePipelineOutputBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineFCPXMLExporter)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineFCPXMLExporter(); \
	friend struct Z_Construct_UClass_UMoviePipelineFCPXMLExporter_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineFCPXMLExporter, UMoviePipelineOutputBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineFCPXMLExporter)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineFCPXMLExporter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineFCPXMLExporter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineFCPXMLExporter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineFCPXMLExporter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineFCPXMLExporter(UMoviePipelineFCPXMLExporter&&); \
	NO_API UMoviePipelineFCPXMLExporter(const UMoviePipelineFCPXMLExporter&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineFCPXMLExporter(UMoviePipelineFCPXMLExporter&&); \
	NO_API UMoviePipelineFCPXMLExporter(const UMoviePipelineFCPXMLExporter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineFCPXMLExporter); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineFCPXMLExporter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMoviePipelineFCPXMLExporter)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_15_PROLOG
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<class UMoviePipelineFCPXMLExporter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineFCPXMLExporterSetting_h


#define FOREACH_ENUM_FCPXMLEXPORTDATASOURCE(op) \
	op(FCPXMLExportDataSource::OutputMetadata) \
	op(FCPXMLExportDataSource::SequenceData) 

enum class FCPXMLExportDataSource : uint8;
template<> MOVIERENDERPIPELINECORE_API UEnum* StaticEnum<FCPXMLExportDataSource>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
