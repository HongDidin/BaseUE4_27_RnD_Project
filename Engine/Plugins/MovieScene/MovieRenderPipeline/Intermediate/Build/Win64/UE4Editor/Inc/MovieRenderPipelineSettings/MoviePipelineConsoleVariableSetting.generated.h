// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MOVIERENDERPIPELINESETTINGS_MoviePipelineConsoleVariableSetting_generated_h
#error "MoviePipelineConsoleVariableSetting.generated.h already included, missing '#pragma once' in MoviePipelineConsoleVariableSetting.h"
#endif
#define MOVIERENDERPIPELINESETTINGS_MoviePipelineConsoleVariableSetting_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineConsoleVariableSetting(); \
	friend struct Z_Construct_UClass_UMoviePipelineConsoleVariableSetting_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineConsoleVariableSetting, UMoviePipelineSetting, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineSettings"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineConsoleVariableSetting)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineConsoleVariableSetting(); \
	friend struct Z_Construct_UClass_UMoviePipelineConsoleVariableSetting_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineConsoleVariableSetting, UMoviePipelineSetting, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineSettings"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineConsoleVariableSetting)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineConsoleVariableSetting(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineConsoleVariableSetting) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineConsoleVariableSetting); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineConsoleVariableSetting); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineConsoleVariableSetting(UMoviePipelineConsoleVariableSetting&&); \
	NO_API UMoviePipelineConsoleVariableSetting(const UMoviePipelineConsoleVariableSetting&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineConsoleVariableSetting() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineConsoleVariableSetting(UMoviePipelineConsoleVariableSetting&&); \
	NO_API UMoviePipelineConsoleVariableSetting(const UMoviePipelineConsoleVariableSetting&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineConsoleVariableSetting); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineConsoleVariableSetting); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMoviePipelineConsoleVariableSetting)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_7_PROLOG
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h_10_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINESETTINGS_API UClass* StaticClass<class UMoviePipelineConsoleVariableSetting>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineSettings_Public_MoviePipelineConsoleVariableSetting_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
