// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UMoviePipelineExecutorJob;
class UMoviePipelineQueue;
class ULevelSequence;
class UMoviePipelineMasterConfig;
#ifdef MOVIERENDERPIPELINEEDITOR_MoviePipelineEditorBlueprintLibrary_generated_h
#error "MoviePipelineEditorBlueprintLibrary.generated.h already included, missing '#pragma once' in MoviePipelineEditorBlueprintLibrary.h"
#endif
#define MOVIERENDERPIPELINEEDITOR_MoviePipelineEditorBlueprintLibrary_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execEnsureJobHasDefaultSettings); \
	DECLARE_FUNCTION(execCreateJobFromSequence); \
	DECLARE_FUNCTION(execConvertManifestFileToString); \
	DECLARE_FUNCTION(execSaveQueueToManifestFile); \
	DECLARE_FUNCTION(execWarnUserOfUnsavedMap); \
	DECLARE_FUNCTION(execIsMapValidForRemoteRender); \
	DECLARE_FUNCTION(execExportConfigToAsset);


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execEnsureJobHasDefaultSettings); \
	DECLARE_FUNCTION(execCreateJobFromSequence); \
	DECLARE_FUNCTION(execConvertManifestFileToString); \
	DECLARE_FUNCTION(execSaveQueueToManifestFile); \
	DECLARE_FUNCTION(execWarnUserOfUnsavedMap); \
	DECLARE_FUNCTION(execIsMapValidForRemoteRender); \
	DECLARE_FUNCTION(execExportConfigToAsset);


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineEditorBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineEditorBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineEditor"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineEditorBlueprintLibrary)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineEditorBlueprintLibrary(); \
	friend struct Z_Construct_UClass_UMoviePipelineEditorBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineEditorBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineEditor"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineEditorBlueprintLibrary)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineEditorBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineEditorBlueprintLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineEditorBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineEditorBlueprintLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineEditorBlueprintLibrary(UMoviePipelineEditorBlueprintLibrary&&); \
	NO_API UMoviePipelineEditorBlueprintLibrary(const UMoviePipelineEditorBlueprintLibrary&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineEditorBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineEditorBlueprintLibrary(UMoviePipelineEditorBlueprintLibrary&&); \
	NO_API UMoviePipelineEditorBlueprintLibrary(const UMoviePipelineEditorBlueprintLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineEditorBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineEditorBlueprintLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineEditorBlueprintLibrary)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_19_PROLOG
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINEEDITOR_API UClass* StaticClass<class UMoviePipelineEditorBlueprintLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Public_MoviePipelineEditorBlueprintLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
