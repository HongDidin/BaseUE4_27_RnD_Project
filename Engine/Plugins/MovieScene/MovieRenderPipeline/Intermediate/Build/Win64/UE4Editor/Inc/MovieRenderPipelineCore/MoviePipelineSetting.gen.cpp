// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MovieRenderPipelineCore/Public/MoviePipelineSetting.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePipelineSetting() {}
// Cross Module References
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineSetting_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineSetting();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_MovieRenderPipelineCore();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipeline_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UMoviePipelineSetting::execBuildNewProcessCommandLine)
	{
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_InOutUnrealURLParams);
		P_GET_PROPERTY_REF(FStrProperty,Z_Param_Out_InOutCommandLineArgs);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->BuildNewProcessCommandLine(Z_Param_Out_InOutUnrealURLParams,Z_Param_Out_InOutCommandLineArgs);
		P_NATIVE_END;
	}
	void UMoviePipelineSetting::StaticRegisterNativesUMoviePipelineSetting()
	{
		UClass* Class = UMoviePipelineSetting::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "BuildNewProcessCommandLine", &UMoviePipelineSetting::execBuildNewProcessCommandLine },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UMoviePipelineSetting_BuildNewProcessCommandLine_Statics
	{
		struct MoviePipelineSetting_eventBuildNewProcessCommandLine_Parms
		{
			FString InOutUnrealURLParams;
			FString InOutCommandLineArgs;
		};
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InOutUnrealURLParams;
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_InOutCommandLineArgs;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMoviePipelineSetting_BuildNewProcessCommandLine_Statics::NewProp_InOutUnrealURLParams = { "InOutUnrealURLParams", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineSetting_eventBuildNewProcessCommandLine_Parms, InOutUnrealURLParams), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UFunction_UMoviePipelineSetting_BuildNewProcessCommandLine_Statics::NewProp_InOutCommandLineArgs = { "InOutCommandLineArgs", nullptr, (EPropertyFlags)0x0010000008000180, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(MoviePipelineSetting_eventBuildNewProcessCommandLine_Parms, InOutCommandLineArgs), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UMoviePipelineSetting_BuildNewProcessCommandLine_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineSetting_BuildNewProcessCommandLine_Statics::NewProp_InOutUnrealURLParams,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UMoviePipelineSetting_BuildNewProcessCommandLine_Statics::NewProp_InOutCommandLineArgs,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UMoviePipelineSetting_BuildNewProcessCommandLine_Statics::Function_MetaDataParams[] = {
		{ "Category", "Movie Render Pipeline" },
		{ "Comment", "/**\n\x09* When rendering in a new process some settings may need to provide command line arguments\n\x09* to affect engine settings that need to be set before most of the engine boots up. This function\n\x09* allows a setting to provide these when the user wants to run in a separate process. This won't\n\x09* be used when running in the current process because it is too late to modify the command line.\n\x09*/" },
		{ "ModuleRelativePath", "Public/MoviePipelineSetting.h" },
		{ "ToolTip", "When rendering in a new process some settings may need to provide command line arguments\nto affect engine settings that need to be set before most of the engine boots up. This function\nallows a setting to provide these when the user wants to run in a separate process. This won't\nbe used when running in the current process because it is too late to modify the command line." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UMoviePipelineSetting_BuildNewProcessCommandLine_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UMoviePipelineSetting, nullptr, "BuildNewProcessCommandLine", nullptr, nullptr, sizeof(MoviePipelineSetting_eventBuildNewProcessCommandLine_Parms), Z_Construct_UFunction_UMoviePipelineSetting_BuildNewProcessCommandLine_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineSetting_BuildNewProcessCommandLine_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x54420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UMoviePipelineSetting_BuildNewProcessCommandLine_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UMoviePipelineSetting_BuildNewProcessCommandLine_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UMoviePipelineSetting_BuildNewProcessCommandLine()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UMoviePipelineSetting_BuildNewProcessCommandLine_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UMoviePipelineSetting_NoRegister()
	{
		return UMoviePipelineSetting::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineSetting_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedPipeline_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_CachedPipeline;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnabled_MetaData[];
#endif
		static void NewProp_bEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineSetting_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UMoviePipelineSetting_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UMoviePipelineSetting_BuildNewProcessCommandLine, "BuildNewProcessCommandLine" }, // 4113807798
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineSetting_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n* A base class for all Movie Render Pipeline settings.\n*/" },
		{ "IncludePath", "MoviePipelineSetting.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MoviePipelineSetting.h" },
		{ "ToolTip", "A base class for all Movie Render Pipeline settings." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineSetting_Statics::NewProp_CachedPipeline_MetaData[] = {
		{ "ModuleRelativePath", "Public/MoviePipelineSetting.h" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UClass_UMoviePipelineSetting_Statics::NewProp_CachedPipeline = { "CachedPipeline", nullptr, (EPropertyFlags)0x0044000000002000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineSetting, CachedPipeline), Z_Construct_UClass_UMoviePipeline_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineSetting_Statics::NewProp_CachedPipeline_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineSetting_Statics::NewProp_CachedPipeline_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineSetting_Statics::NewProp_bEnabled_MetaData[] = {
		{ "Comment", "/** Is this setting currently enabled? Disabled settings are like they never existed. */" },
		{ "ModuleRelativePath", "Public/MoviePipelineSetting.h" },
		{ "ToolTip", "Is this setting currently enabled? Disabled settings are like they never existed." },
	};
#endif
	void Z_Construct_UClass_UMoviePipelineSetting_Statics::NewProp_bEnabled_SetBit(void* Obj)
	{
		((UMoviePipelineSetting*)Obj)->bEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMoviePipelineSetting_Statics::NewProp_bEnabled = { "bEnabled", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMoviePipelineSetting), &Z_Construct_UClass_UMoviePipelineSetting_Statics::NewProp_bEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineSetting_Statics::NewProp_bEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineSetting_Statics::NewProp_bEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMoviePipelineSetting_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineSetting_Statics::NewProp_CachedPipeline,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineSetting_Statics::NewProp_bEnabled,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineSetting_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineSetting>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineSetting_Statics::ClassParams = {
		&UMoviePipelineSetting::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UMoviePipelineSetting_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineSetting_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineSetting_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineSetting_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineSetting()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineSetting_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineSetting, 3489302441);
	template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<UMoviePipelineSetting>()
	{
		return UMoviePipelineSetting::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineSetting(Z_Construct_UClass_UMoviePipelineSetting, &UMoviePipelineSetting::StaticClass, TEXT("/Script/MovieRenderPipelineCore"), TEXT("UMoviePipelineSetting"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineSetting);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
