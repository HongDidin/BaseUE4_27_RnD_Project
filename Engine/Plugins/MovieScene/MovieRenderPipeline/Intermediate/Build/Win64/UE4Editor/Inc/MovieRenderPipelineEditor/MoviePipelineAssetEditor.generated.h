// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MOVIERENDERPIPELINEEDITOR_MoviePipelineAssetEditor_generated_h
#error "MoviePipelineAssetEditor.generated.h already included, missing '#pragma once' in MoviePipelineAssetEditor.h"
#endif
#define MOVIERENDERPIPELINEEDITOR_MoviePipelineAssetEditor_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineAssetEditor(); \
	friend struct Z_Construct_UClass_UMoviePipelineAssetEditor_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineAssetEditor, UAssetEditor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineEditor"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineAssetEditor)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineAssetEditor(); \
	friend struct Z_Construct_UClass_UMoviePipelineAssetEditor_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineAssetEditor, UAssetEditor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineEditor"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineAssetEditor)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineAssetEditor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineAssetEditor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineAssetEditor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineAssetEditor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineAssetEditor(UMoviePipelineAssetEditor&&); \
	NO_API UMoviePipelineAssetEditor(const UMoviePipelineAssetEditor&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineAssetEditor() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineAssetEditor(UMoviePipelineAssetEditor&&); \
	NO_API UMoviePipelineAssetEditor(const UMoviePipelineAssetEditor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineAssetEditor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineAssetEditor); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMoviePipelineAssetEditor)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ObjectToEdit() { return STRUCT_OFFSET(UMoviePipelineAssetEditor, ObjectToEdit); }


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_10_PROLOG
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINEEDITOR_API UClass* StaticClass<class UMoviePipelineAssetEditor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineEditor_Private_MoviePipelineAssetEditor_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
