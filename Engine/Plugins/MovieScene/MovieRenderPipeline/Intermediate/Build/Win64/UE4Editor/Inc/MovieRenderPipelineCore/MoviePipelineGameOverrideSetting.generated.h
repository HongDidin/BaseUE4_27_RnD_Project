// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MOVIERENDERPIPELINECORE_MoviePipelineGameOverrideSetting_generated_h
#error "MoviePipelineGameOverrideSetting.generated.h already included, missing '#pragma once' in MoviePipelineGameOverrideSetting.h"
#endif
#define MOVIERENDERPIPELINECORE_MoviePipelineGameOverrideSetting_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_SPARSE_DATA
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineGameOverrideSetting(); \
	friend struct Z_Construct_UClass_UMoviePipelineGameOverrideSetting_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineGameOverrideSetting, UMoviePipelineSetting, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineGameOverrideSetting)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineGameOverrideSetting(); \
	friend struct Z_Construct_UClass_UMoviePipelineGameOverrideSetting_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineGameOverrideSetting, UMoviePipelineSetting, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MovieRenderPipelineCore"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineGameOverrideSetting)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineGameOverrideSetting(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineGameOverrideSetting) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineGameOverrideSetting); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineGameOverrideSetting); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineGameOverrideSetting(UMoviePipelineGameOverrideSetting&&); \
	NO_API UMoviePipelineGameOverrideSetting(const UMoviePipelineGameOverrideSetting&); \
public:


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineGameOverrideSetting(UMoviePipelineGameOverrideSetting&&); \
	NO_API UMoviePipelineGameOverrideSetting(const UMoviePipelineGameOverrideSetting&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineGameOverrideSetting); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineGameOverrideSetting); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMoviePipelineGameOverrideSetting)


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_22_PROLOG
#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_INCLASS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_SPARSE_DATA \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<class UMoviePipelineGameOverrideSetting>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MoviePipelineGameOverrideSetting_h


#define FOREACH_ENUM_EMOVIEPIPELINETEXTURESTREAMINGMETHOD(op) \
	op(EMoviePipelineTextureStreamingMethod::None) \
	op(EMoviePipelineTextureStreamingMethod::Disabled) \
	op(EMoviePipelineTextureStreamingMethod::FullyLoad) 

enum class EMoviePipelineTextureStreamingMethod : uint8;
template<> MOVIERENDERPIPELINECORE_API UEnum* StaticEnum<EMoviePipelineTextureStreamingMethod>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
