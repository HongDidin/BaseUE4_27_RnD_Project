// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MOVIERENDERPIPELINECORE_MovieRenderPipelineDataTypes_generated_h
#error "MovieRenderPipelineDataTypes.generated.h already included, missing '#pragma once' in MovieRenderPipelineDataTypes.h"
#endif
#define MOVIERENDERPIPELINECORE_MovieRenderPipelineDataTypes_generated_h

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MovieRenderPipelineDataTypes_h_1146_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMoviePipelineOutputData_Statics; \
	MOVIERENDERPIPELINECORE_API static class UScriptStruct* StaticStruct();


template<> MOVIERENDERPIPELINECORE_API UScriptStruct* StaticStruct<struct FMoviePipelineOutputData>();

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MovieRenderPipelineDataTypes_h_1109_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMoviePipelineShotOutputData_Statics; \
	MOVIERENDERPIPELINECORE_API static class UScriptStruct* StaticStruct();


template<> MOVIERENDERPIPELINECORE_API UScriptStruct* StaticStruct<struct FMoviePipelineShotOutputData>();

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MovieRenderPipelineDataTypes_h_1100_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMoviePipelineRenderPassOutputData_Statics; \
	MOVIERENDERPIPELINECORE_API static class UScriptStruct* StaticStruct();


template<> MOVIERENDERPIPELINECORE_API UScriptStruct* StaticStruct<struct FMoviePipelineRenderPassOutputData>();

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MovieRenderPipelineDataTypes_h_760_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMoviePipelineFilenameResolveParams_Statics; \
	MOVIERENDERPIPELINECORE_API static class UScriptStruct* StaticStruct();


template<> MOVIERENDERPIPELINECORE_API UScriptStruct* StaticStruct<struct FMoviePipelineFilenameResolveParams>();

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MovieRenderPipelineDataTypes_h_737_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMoviePipelineFormatArgs_Statics; \
	MOVIERENDERPIPELINECORE_API static class UScriptStruct* StaticStruct();


template<> MOVIERENDERPIPELINECORE_API UScriptStruct* StaticStruct<struct FMoviePipelineFormatArgs>();

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MovieRenderPipelineDataTypes_h_460_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMoviePipelineCameraCutInfo_Statics; \
	MOVIERENDERPIPELINECORE_API static class UScriptStruct* StaticStruct();


template<> MOVIERENDERPIPELINECORE_API UScriptStruct* StaticStruct<struct FMoviePipelineCameraCutInfo>();

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MovieRenderPipelineDataTypes_h_403_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMoviePipelineSegmentWorkMetrics_Statics; \
	MOVIERENDERPIPELINECORE_API static class UScriptStruct* StaticStruct();


template<> MOVIERENDERPIPELINECORE_API UScriptStruct* StaticStruct<struct FMoviePipelineSegmentWorkMetrics>();

#define Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MovieRenderPipelineDataTypes_h_93_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FMoviePipelinePassIdentifier_Statics; \
	MOVIERENDERPIPELINECORE_API static class UScriptStruct* StaticStruct();


template<> MOVIERENDERPIPELINECORE_API UScriptStruct* StaticStruct<struct FMoviePipelinePassIdentifier>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MovieRenderPipeline_Source_MovieRenderPipelineCore_Public_MovieRenderPipelineDataTypes_h


#define FOREACH_ENUM_EMOVIEPIPELINESHUTTERTIMING(op) \
	op(EMoviePipelineShutterTiming::FrameOpen) \
	op(EMoviePipelineShutterTiming::FrameCenter) \
	op(EMoviePipelineShutterTiming::FrameClose) 

enum class EMoviePipelineShutterTiming : uint8;
template<> MOVIERENDERPIPELINECORE_API UEnum* StaticEnum<EMoviePipelineShutterTiming>();

#define FOREACH_ENUM_EMOVIERENDERSHOTSTATE(op) \
	op(EMovieRenderShotState::Uninitialized) \
	op(EMovieRenderShotState::WarmingUp) \
	op(EMovieRenderShotState::MotionBlur) \
	op(EMovieRenderShotState::Rendering) \
	op(EMovieRenderShotState::Finished) 

enum class EMovieRenderShotState : uint8;
template<> MOVIERENDERPIPELINECORE_API UEnum* StaticEnum<EMovieRenderShotState>();

#define FOREACH_ENUM_EMOVIERENDERPIPELINESTATE(op) \
	op(EMovieRenderPipelineState::Uninitialized) \
	op(EMovieRenderPipelineState::ProducingFrames) \
	op(EMovieRenderPipelineState::Finalize) \
	op(EMovieRenderPipelineState::Export) \
	op(EMovieRenderPipelineState::Finished) 

enum class EMovieRenderPipelineState : uint8;
template<> MOVIERENDERPIPELINECORE_API UEnum* StaticEnum<EMovieRenderPipelineState>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
