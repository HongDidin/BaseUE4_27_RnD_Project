// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MovieRenderPipelineCore/Public/MoviePipelineVideoOutputBase.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePipelineVideoOutputBase() {}
// Cross Module References
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineVideoOutputBase_NoRegister();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineVideoOutputBase();
	MOVIERENDERPIPELINECORE_API UClass* Z_Construct_UClass_UMoviePipelineOutputBase();
	UPackage* Z_Construct_UPackage__Script_MovieRenderPipelineCore();
// End Cross Module References
	void UMoviePipelineVideoOutputBase::StaticRegisterNativesUMoviePipelineVideoOutputBase()
	{
	}
	UClass* Z_Construct_UClass_UMoviePipelineVideoOutputBase_NoRegister()
	{
		return UMoviePipelineVideoOutputBase::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineVideoOutputBase_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineVideoOutputBase_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMoviePipelineOutputBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MovieRenderPipelineCore,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineVideoOutputBase_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n* A base class for video codec outputs for the Movie Pipeline system. To simplify encoder implementations\n* this handles multi-threading for you and will call all of the encoding functions on a dedicated thread.\n* This allows an encoder to do more expensive operations (such as image quantization) without implementing\n* threading yourself, nor having to worry about blocking the game thread.\n*/" },
		{ "IncludePath", "MoviePipelineVideoOutputBase.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/MoviePipelineVideoOutputBase.h" },
		{ "ToolTip", "A base class for video codec outputs for the Movie Pipeline system. To simplify encoder implementations\nthis handles multi-threading for you and will call all of the encoding functions on a dedicated thread.\nThis allows an encoder to do more expensive operations (such as image quantization) without implementing\nthreading yourself, nor having to worry about blocking the game thread." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineVideoOutputBase_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineVideoOutputBase>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineVideoOutputBase_Statics::ClassParams = {
		&UMoviePipelineVideoOutputBase::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineVideoOutputBase_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineVideoOutputBase_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineVideoOutputBase()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineVideoOutputBase_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineVideoOutputBase, 1684680489);
	template<> MOVIERENDERPIPELINECORE_API UClass* StaticClass<UMoviePipelineVideoOutputBase>()
	{
		return UMoviePipelineVideoOutputBase::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineVideoOutputBase(Z_Construct_UClass_UMoviePipelineVideoOutputBase, &UMoviePipelineVideoOutputBase::StaticClass, TEXT("/Script/MovieRenderPipelineCore"), TEXT("UMoviePipelineVideoOutputBase"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineVideoOutputBase);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
