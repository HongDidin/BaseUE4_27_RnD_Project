// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSequenceEditor/Public/LevelSequenceEditorBlueprintLibrary.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLevelSequenceEditorBlueprintLibrary() {}
// Cross Module References
	LEVELSEQUENCEEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FSequencerChannelProxy();
	UPackage* Z_Construct_UPackage__Script_LevelSequenceEditor();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneSection_NoRegister();
	LEVELSEQUENCEEDITOR_API UClass* Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary_NoRegister();
	LEVELSEQUENCEEDITOR_API UClass* Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneObjectBindingID();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
	LEVELSEQUENCE_API UClass* Z_Construct_UClass_ULevelSequence_NoRegister();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneFolder_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FGuid();
	MOVIESCENE_API UClass* Z_Construct_UClass_UMovieSceneTrack_NoRegister();
	MOVIESCENE_API UScriptStruct* Z_Construct_UScriptStruct_FMovieSceneSequencePlaybackParams();
// End Cross Module References
class UScriptStruct* FSequencerChannelProxy::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSEQUENCEEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FSequencerChannelProxy_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FSequencerChannelProxy, Z_Construct_UPackage__Script_LevelSequenceEditor(), TEXT("SequencerChannelProxy"), sizeof(FSequencerChannelProxy), Get_Z_Construct_UScriptStruct_FSequencerChannelProxy_Hash());
	}
	return Singleton;
}
template<> LEVELSEQUENCEEDITOR_API UScriptStruct* StaticStruct<FSequencerChannelProxy>()
{
	return FSequencerChannelProxy::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FSequencerChannelProxy(FSequencerChannelProxy::StaticStruct, TEXT("/Script/LevelSequenceEditor"), TEXT("SequencerChannelProxy"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSequenceEditor_StaticRegisterNativesFSequencerChannelProxy
{
	FScriptStruct_LevelSequenceEditor_StaticRegisterNativesFSequencerChannelProxy()
	{
		UScriptStruct::DeferCppStructOps<FSequencerChannelProxy>(FName(TEXT("SequencerChannelProxy")));
	}
} ScriptStruct_LevelSequenceEditor_StaticRegisterNativesFSequencerChannelProxy;
	struct Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ChannelName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_ChannelName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Section_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Section;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::Struct_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FSequencerChannelProxy>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::NewProp_ChannelName_MetaData[] = {
		{ "Category", "Channel" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::NewProp_ChannelName = { "ChannelName", nullptr, (EPropertyFlags)0x0010000000000004, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSequencerChannelProxy, ChannelName), METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::NewProp_ChannelName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::NewProp_ChannelName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::NewProp_Section_MetaData[] = {
		{ "Category", "Channel" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::NewProp_Section = { "Section", nullptr, (EPropertyFlags)0x001000000008000c, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FSequencerChannelProxy, Section), Z_Construct_UClass_UMovieSceneSection_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::NewProp_Section_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::NewProp_Section_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::NewProp_ChannelName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::NewProp_Section,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSequenceEditor,
		nullptr,
		&NewStructOps,
		"SequencerChannelProxy",
		sizeof(FSequencerChannelProxy),
		alignof(FSequencerChannelProxy),
		Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000005),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FSequencerChannelProxy()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FSequencerChannelProxy_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSequenceEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("SequencerChannelProxy"), sizeof(FSequencerChannelProxy), Get_Z_Construct_UScriptStruct_FSequencerChannelProxy_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FSequencerChannelProxy_Hash() { return 206123915U; }
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execSetLockLevelSequence)
	{
		P_GET_UBOOL(Z_Param_bLock);
		P_FINISH;
		P_NATIVE_BEGIN;
		ULevelSequenceEditorBlueprintLibrary::SetLockLevelSequence(Z_Param_bLock);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execIsLevelSequenceLocked)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=ULevelSequenceEditorBlueprintLibrary::IsLevelSequenceLocked();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execGetBoundObjects)
	{
		P_GET_STRUCT(FMovieSceneObjectBindingID,Z_Param_ObjectBinding);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UObject*>*)Z_Param__Result=ULevelSequenceEditorBlueprintLibrary::GetBoundObjects(Z_Param_ObjectBinding);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execRefreshCurrentLevelSequence)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		ULevelSequenceEditorBlueprintLibrary::RefreshCurrentLevelSequence();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execEmptySelection)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		ULevelSequenceEditorBlueprintLibrary::EmptySelection();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execSelectObjects)
	{
		P_GET_TARRAY(FGuid,Z_Param_ObjectBinding);
		P_FINISH;
		P_NATIVE_BEGIN;
		ULevelSequenceEditorBlueprintLibrary::SelectObjects(Z_Param_ObjectBinding);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execSelectFolders)
	{
		P_GET_TARRAY_REF(UMovieSceneFolder*,Z_Param_Out_Folders);
		P_FINISH;
		P_NATIVE_BEGIN;
		ULevelSequenceEditorBlueprintLibrary::SelectFolders(Z_Param_Out_Folders);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execSelectChannels)
	{
		P_GET_TARRAY_REF(FSequencerChannelProxy,Z_Param_Out_Channels);
		P_FINISH;
		P_NATIVE_BEGIN;
		ULevelSequenceEditorBlueprintLibrary::SelectChannels(Z_Param_Out_Channels);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execSelectSections)
	{
		P_GET_TARRAY_REF(UMovieSceneSection*,Z_Param_Out_Sections);
		P_FINISH;
		P_NATIVE_BEGIN;
		ULevelSequenceEditorBlueprintLibrary::SelectSections(Z_Param_Out_Sections);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execSelectTracks)
	{
		P_GET_TARRAY_REF(UMovieSceneTrack*,Z_Param_Out_Tracks);
		P_FINISH;
		P_NATIVE_BEGIN;
		ULevelSequenceEditorBlueprintLibrary::SelectTracks(Z_Param_Out_Tracks);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execGetSelectedObjects)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FGuid>*)Z_Param__Result=ULevelSequenceEditorBlueprintLibrary::GetSelectedObjects();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execGetSelectedFolders)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UMovieSceneFolder*>*)Z_Param__Result=ULevelSequenceEditorBlueprintLibrary::GetSelectedFolders();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execGetSelectedChannels)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<FSequencerChannelProxy>*)Z_Param__Result=ULevelSequenceEditorBlueprintLibrary::GetSelectedChannels();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execGetSelectedSections)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UMovieSceneSection*>*)Z_Param__Result=ULevelSequenceEditorBlueprintLibrary::GetSelectedSections();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execGetSelectedTracks)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(TArray<UMovieSceneTrack*>*)Z_Param__Result=ULevelSequenceEditorBlueprintLibrary::GetSelectedTracks();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execIsPlaying)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=ULevelSequenceEditorBlueprintLibrary::IsPlaying();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execPlayTo)
	{
		P_GET_STRUCT(FMovieSceneSequencePlaybackParams,Z_Param_PlaybackParams);
		P_FINISH;
		P_NATIVE_BEGIN;
		ULevelSequenceEditorBlueprintLibrary::PlayTo(Z_Param_PlaybackParams);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execGetCurrentLocalTime)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=ULevelSequenceEditorBlueprintLibrary::GetCurrentLocalTime();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execSetCurrentLocalTime)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_NewFrame);
		P_FINISH;
		P_NATIVE_BEGIN;
		ULevelSequenceEditorBlueprintLibrary::SetCurrentLocalTime(Z_Param_NewFrame);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execGetCurrentTime)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(int32*)Z_Param__Result=ULevelSequenceEditorBlueprintLibrary::GetCurrentTime();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execSetCurrentTime)
	{
		P_GET_PROPERTY(FIntProperty,Z_Param_NewFrame);
		P_FINISH;
		P_NATIVE_BEGIN;
		ULevelSequenceEditorBlueprintLibrary::SetCurrentTime(Z_Param_NewFrame);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execPause)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		ULevelSequenceEditorBlueprintLibrary::Pause();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execPlay)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		ULevelSequenceEditorBlueprintLibrary::Play();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execCloseLevelSequence)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		ULevelSequenceEditorBlueprintLibrary::CloseLevelSequence();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execGetFocusedLevelSequence)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ULevelSequence**)Z_Param__Result=ULevelSequenceEditorBlueprintLibrary::GetFocusedLevelSequence();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execGetCurrentLevelSequence)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(ULevelSequence**)Z_Param__Result=ULevelSequenceEditorBlueprintLibrary::GetCurrentLevelSequence();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ULevelSequenceEditorBlueprintLibrary::execOpenLevelSequence)
	{
		P_GET_OBJECT(ULevelSequence,Z_Param_LevelSequence);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=ULevelSequenceEditorBlueprintLibrary::OpenLevelSequence(Z_Param_LevelSequence);
		P_NATIVE_END;
	}
	void ULevelSequenceEditorBlueprintLibrary::StaticRegisterNativesULevelSequenceEditorBlueprintLibrary()
	{
		UClass* Class = ULevelSequenceEditorBlueprintLibrary::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "CloseLevelSequence", &ULevelSequenceEditorBlueprintLibrary::execCloseLevelSequence },
			{ "EmptySelection", &ULevelSequenceEditorBlueprintLibrary::execEmptySelection },
			{ "GetBoundObjects", &ULevelSequenceEditorBlueprintLibrary::execGetBoundObjects },
			{ "GetCurrentLevelSequence", &ULevelSequenceEditorBlueprintLibrary::execGetCurrentLevelSequence },
			{ "GetCurrentLocalTime", &ULevelSequenceEditorBlueprintLibrary::execGetCurrentLocalTime },
			{ "GetCurrentTime", &ULevelSequenceEditorBlueprintLibrary::execGetCurrentTime },
			{ "GetFocusedLevelSequence", &ULevelSequenceEditorBlueprintLibrary::execGetFocusedLevelSequence },
			{ "GetSelectedChannels", &ULevelSequenceEditorBlueprintLibrary::execGetSelectedChannels },
			{ "GetSelectedFolders", &ULevelSequenceEditorBlueprintLibrary::execGetSelectedFolders },
			{ "GetSelectedObjects", &ULevelSequenceEditorBlueprintLibrary::execGetSelectedObjects },
			{ "GetSelectedSections", &ULevelSequenceEditorBlueprintLibrary::execGetSelectedSections },
			{ "GetSelectedTracks", &ULevelSequenceEditorBlueprintLibrary::execGetSelectedTracks },
			{ "IsLevelSequenceLocked", &ULevelSequenceEditorBlueprintLibrary::execIsLevelSequenceLocked },
			{ "IsPlaying", &ULevelSequenceEditorBlueprintLibrary::execIsPlaying },
			{ "OpenLevelSequence", &ULevelSequenceEditorBlueprintLibrary::execOpenLevelSequence },
			{ "Pause", &ULevelSequenceEditorBlueprintLibrary::execPause },
			{ "Play", &ULevelSequenceEditorBlueprintLibrary::execPlay },
			{ "PlayTo", &ULevelSequenceEditorBlueprintLibrary::execPlayTo },
			{ "RefreshCurrentLevelSequence", &ULevelSequenceEditorBlueprintLibrary::execRefreshCurrentLevelSequence },
			{ "SelectChannels", &ULevelSequenceEditorBlueprintLibrary::execSelectChannels },
			{ "SelectFolders", &ULevelSequenceEditorBlueprintLibrary::execSelectFolders },
			{ "SelectObjects", &ULevelSequenceEditorBlueprintLibrary::execSelectObjects },
			{ "SelectSections", &ULevelSequenceEditorBlueprintLibrary::execSelectSections },
			{ "SelectTracks", &ULevelSequenceEditorBlueprintLibrary::execSelectTracks },
			{ "SetCurrentLocalTime", &ULevelSequenceEditorBlueprintLibrary::execSetCurrentLocalTime },
			{ "SetCurrentTime", &ULevelSequenceEditorBlueprintLibrary::execSetCurrentTime },
			{ "SetLockLevelSequence", &ULevelSequenceEditorBlueprintLibrary::execSetLockLevelSequence },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_CloseLevelSequence_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_CloseLevelSequence_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/*\n\x09 * Close\n\x09 */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "* Close" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_CloseLevelSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "CloseLevelSequence", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_CloseLevelSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_CloseLevelSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_CloseLevelSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_CloseLevelSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_EmptySelection_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_EmptySelection_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/** Empties the current selection. */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Empties the current selection." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_EmptySelection_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "EmptySelection", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_EmptySelection_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_EmptySelection_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_EmptySelection()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_EmptySelection_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventGetBoundObjects_Parms
		{
			FMovieSceneObjectBindingID ObjectBinding;
			TArray<UObject*> ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ObjectBinding;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects_Statics::NewProp_ObjectBinding = { "ObjectBinding", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventGetBoundObjects_Parms, ObjectBinding), Z_Construct_UScriptStruct_FMovieSceneObjectBindingID, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UObject_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventGetBoundObjects_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects_Statics::NewProp_ObjectBinding,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/** Get the object bound to the given binding ID with the current level sequence editor */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Get the object bound to the given binding ID with the current level sequence editor" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "GetBoundObjects", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventGetBoundObjects_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLevelSequence_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventGetCurrentLevelSequence_Parms
		{
			ULevelSequence* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLevelSequence_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventGetCurrentLevelSequence_Parms, ReturnValue), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLevelSequence_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLevelSequence_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLevelSequence_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/*\n\x09 * Get the currently opened root/master level sequence asset\n\x09 */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "* Get the currently opened root/master level sequence asset" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLevelSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "GetCurrentLevelSequence", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventGetCurrentLevelSequence_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLevelSequence_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLevelSequence_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLevelSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLevelSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLevelSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLevelSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLocalTime_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventGetCurrentLocalTime_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLocalTime_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventGetCurrentLocalTime_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLocalTime_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLocalTime_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLocalTime_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/**\n\x09 * Get the current local playback position in frames\n\x09 */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Get the current local playback position in frames" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLocalTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "GetCurrentLocalTime", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventGetCurrentLocalTime_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLocalTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLocalTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLocalTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLocalTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLocalTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLocalTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentTime_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventGetCurrentTime_Parms
		{
			int32 ReturnValue;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentTime_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventGetCurrentTime_Parms, ReturnValue), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentTime_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentTime_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentTime_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/**\n\x09 * Get the current global playback position in frames\n\x09 */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Get the current global playback position in frames" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "GetCurrentTime", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventGetCurrentTime_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetFocusedLevelSequence_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventGetFocusedLevelSequence_Parms
		{
			ULevelSequence* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetFocusedLevelSequence_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventGetFocusedLevelSequence_Parms, ReturnValue), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetFocusedLevelSequence_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetFocusedLevelSequence_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetFocusedLevelSequence_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/*\n\x09 * Get the currently focused/viewed level sequence asset if there is a hierarchy of sequences.\n\x09 */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "* Get the currently focused/viewed level sequence asset if there is a hierarchy of sequences." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetFocusedLevelSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "GetFocusedLevelSequence", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventGetFocusedLevelSequence_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetFocusedLevelSequence_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetFocusedLevelSequence_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetFocusedLevelSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetFocusedLevelSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetFocusedLevelSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetFocusedLevelSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedChannels_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventGetSelectedChannels_Parms
		{
			TArray<FSequencerChannelProxy> ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedChannels_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000008000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSequencerChannelProxy, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedChannels_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010008000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventGetSelectedChannels_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedChannels_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedChannels_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedChannels_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedChannels_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/** Gets the currently selected channels. */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Gets the currently selected channels." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedChannels_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "GetSelectedChannels", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventGetSelectedChannels_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedChannels_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedChannels_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedChannels_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedChannels_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedChannels()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedChannels_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventGetSelectedFolders_Parms
		{
			TArray<UMovieSceneFolder*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMovieSceneFolder_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010008000000588, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventGetSelectedFolders_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/** Gets the currently selected folders. */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Gets the currently selected folders." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "GetSelectedFolders", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventGetSelectedFolders_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedObjects_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventGetSelectedObjects_Parms
		{
			TArray<FGuid> ReturnValue;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ReturnValue_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedObjects_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedObjects_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventGetSelectedObjects_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedObjects_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedObjects_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedObjects_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedObjects_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/** Gets the currently selected Object Guids*/" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Gets the currently selected Object Guids" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedObjects_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "GetSelectedObjects", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventGetSelectedObjects_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedObjects_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedObjects_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedObjects_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedObjects_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedObjects()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedObjects_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventGetSelectedSections_Parms
		{
			TArray<UMovieSceneSection*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMovieSceneSection_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010008000000588, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventGetSelectedSections_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/** Gets the currently selected sections. */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Gets the currently selected sections." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "GetSelectedSections", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventGetSelectedSections_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventGetSelectedTracks_Parms
		{
			TArray<UMovieSceneTrack*> ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks_Statics::NewProp_ReturnValue_Inner = { "ReturnValue", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMovieSceneTrack_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010008000000588, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventGetSelectedTracks_Parms, ReturnValue), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks_Statics::NewProp_ReturnValue_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/** Gets the currently selected tracks. */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Gets the currently selected tracks." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "GetSelectedTracks", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventGetSelectedTracks_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsLevelSequenceLocked_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventIsLevelSequenceLocked_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsLevelSequenceLocked_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LevelSequenceEditorBlueprintLibrary_eventIsLevelSequenceLocked_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsLevelSequenceLocked_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LevelSequenceEditorBlueprintLibrary_eventIsLevelSequenceLocked_Parms), &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsLevelSequenceLocked_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsLevelSequenceLocked_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsLevelSequenceLocked_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsLevelSequenceLocked_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/** Check whether the current level sequence and its descendants are locked for editing. */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Check whether the current level sequence and its descendants are locked for editing." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsLevelSequenceLocked_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "IsLevelSequenceLocked", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventIsLevelSequenceLocked_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsLevelSequenceLocked_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsLevelSequenceLocked_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsLevelSequenceLocked_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsLevelSequenceLocked_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsLevelSequenceLocked()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsLevelSequenceLocked_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsPlaying_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventIsPlaying_Parms
		{
			bool ReturnValue;
		};
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsPlaying_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LevelSequenceEditorBlueprintLibrary_eventIsPlaying_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsPlaying_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LevelSequenceEditorBlueprintLibrary_eventIsPlaying_Parms), &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsPlaying_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsPlaying_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsPlaying_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsPlaying_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/** Check whether the sequence is actively playing. */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Check whether the sequence is actively playing." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsPlaying_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "IsPlaying", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventIsPlaying_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsPlaying_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsPlaying_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x14022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsPlaying_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsPlaying_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsPlaying()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsPlaying_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventOpenLevelSequence_Parms
		{
			ULevelSequence* LevelSequence;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LevelSequence;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence_Statics::NewProp_LevelSequence = { "LevelSequence", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventOpenLevelSequence_Parms, LevelSequence), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((LevelSequenceEditorBlueprintLibrary_eventOpenLevelSequence_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LevelSequenceEditorBlueprintLibrary_eventOpenLevelSequence_Parms), &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence_Statics::NewProp_LevelSequence,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/*\n\x09 * Open a level sequence asset\n\x09 */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "* Open a level sequence asset" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "OpenLevelSequence", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventOpenLevelSequence_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_Pause_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_Pause_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/**\n\x09 * Pause the current level sequence\n\x09 */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Pause the current level sequence" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_Pause_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "Pause", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_Pause_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_Pause_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_Pause()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_Pause_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_Play_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_Play_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/**\n\x09 * Play the current level sequence\n\x09 */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Play the current level sequence" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_Play_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "Play", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_Play_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_Play_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_Play()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_Play_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_PlayTo_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventPlayTo_Parms
		{
			FMovieSceneSequencePlaybackParams PlaybackParams;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PlaybackParams;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_PlayTo_Statics::NewProp_PlaybackParams = { "PlaybackParams", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventPlayTo_Parms, PlaybackParams), Z_Construct_UScriptStruct_FMovieSceneSequencePlaybackParams, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_PlayTo_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_PlayTo_Statics::NewProp_PlaybackParams,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_PlayTo_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/**\n\x09 * Play from the current time to the requested time in frames\n\x09 */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Play from the current time to the requested time in frames" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_PlayTo_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "PlayTo", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventPlayTo_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_PlayTo_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_PlayTo_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_PlayTo_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_PlayTo_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_PlayTo()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_PlayTo_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_RefreshCurrentLevelSequence_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_RefreshCurrentLevelSequence_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/** Refresh Sequencer UI. */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Refresh Sequencer UI." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_RefreshCurrentLevelSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "RefreshCurrentLevelSequence", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_RefreshCurrentLevelSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_RefreshCurrentLevelSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_RefreshCurrentLevelSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_RefreshCurrentLevelSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventSelectChannels_Parms
		{
			TArray<FSequencerChannelProxy> Channels;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Channels_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Channels_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Channels;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels_Statics::NewProp_Channels_Inner = { "Channels", nullptr, (EPropertyFlags)0x0000008000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSequencerChannelProxy, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels_Statics::NewProp_Channels_MetaData[] = {
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels_Statics::NewProp_Channels = { "Channels", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventSelectChannels_Parms, Channels), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels_Statics::NewProp_Channels_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels_Statics::NewProp_Channels_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels_Statics::NewProp_Channels_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels_Statics::NewProp_Channels,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/** Select channels */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Select channels" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "SelectChannels", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventSelectChannels_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventSelectFolders_Parms
		{
			TArray<UMovieSceneFolder*> Folders;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Folders_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Folders_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Folders;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders_Statics::NewProp_Folders_Inner = { "Folders", nullptr, (EPropertyFlags)0x0000000000080000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMovieSceneFolder_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders_Statics::NewProp_Folders_MetaData[] = {
		{ "EditInline", "true" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders_Statics::NewProp_Folders = { "Folders", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventSelectFolders_Parms, Folders), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders_Statics::NewProp_Folders_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders_Statics::NewProp_Folders_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders_Statics::NewProp_Folders_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders_Statics::NewProp_Folders,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/** Select folders */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Select folders" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "SelectFolders", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventSelectFolders_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectObjects_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventSelectObjects_Parms
		{
			TArray<FGuid> ObjectBinding;
		};
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ObjectBinding_Inner;
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ObjectBinding;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectObjects_Statics::NewProp_ObjectBinding_Inner = { "ObjectBinding", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FGuid, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectObjects_Statics::NewProp_ObjectBinding = { "ObjectBinding", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventSelectObjects_Parms, ObjectBinding), EArrayPropertyFlags::None, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectObjects_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectObjects_Statics::NewProp_ObjectBinding_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectObjects_Statics::NewProp_ObjectBinding,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectObjects_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/** Select objects by GUID */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Select objects by GUID" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectObjects_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "SelectObjects", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventSelectObjects_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectObjects_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectObjects_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectObjects_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectObjects_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectObjects()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectObjects_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventSelectSections_Parms
		{
			TArray<UMovieSceneSection*> Sections;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Sections_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sections_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Sections;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections_Statics::NewProp_Sections_Inner = { "Sections", nullptr, (EPropertyFlags)0x0000000000080000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMovieSceneSection_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections_Statics::NewProp_Sections_MetaData[] = {
		{ "EditInline", "true" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections_Statics::NewProp_Sections = { "Sections", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventSelectSections_Parms, Sections), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections_Statics::NewProp_Sections_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections_Statics::NewProp_Sections_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections_Statics::NewProp_Sections_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections_Statics::NewProp_Sections,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/** Select sections */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Select sections" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "SelectSections", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventSelectSections_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventSelectTracks_Parms
		{
			TArray<UMovieSceneTrack*> Tracks;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Tracks_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Tracks_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Tracks;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks_Statics::NewProp_Tracks_Inner = { "Tracks", nullptr, (EPropertyFlags)0x0000000000080000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMovieSceneTrack_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks_Statics::NewProp_Tracks_MetaData[] = {
		{ "EditInline", "true" },
		{ "NativeConst", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks_Statics::NewProp_Tracks = { "Tracks", nullptr, (EPropertyFlags)0x0010008008000182, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventSelectTracks_Parms, Tracks), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks_Statics::NewProp_Tracks_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks_Statics::NewProp_Tracks_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks_Statics::NewProp_Tracks_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks_Statics::NewProp_Tracks,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/** Select tracks */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Select tracks" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "SelectTracks", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventSelectTracks_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04422401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentLocalTime_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventSetCurrentLocalTime_Parms
		{
			int32 NewFrame;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NewFrame;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentLocalTime_Statics::NewProp_NewFrame = { "NewFrame", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventSetCurrentLocalTime_Parms, NewFrame), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentLocalTime_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentLocalTime_Statics::NewProp_NewFrame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentLocalTime_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/**\n\x09 * Set local playback position for the current level sequence in frames\n\x09 */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Set local playback position for the current level sequence in frames" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentLocalTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "SetCurrentLocalTime", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventSetCurrentLocalTime_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentLocalTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentLocalTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentLocalTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentLocalTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentLocalTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentLocalTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentTime_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventSetCurrentTime_Parms
		{
			int32 NewFrame;
		};
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NewFrame;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentTime_Statics::NewProp_NewFrame = { "NewFrame", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(LevelSequenceEditorBlueprintLibrary_eventSetCurrentTime_Parms, NewFrame), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentTime_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentTime_Statics::NewProp_NewFrame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentTime_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/**\n\x09 * Set global playback position for the current level sequence in frames\n\x09 */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Set global playback position for the current level sequence in frames" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentTime_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "SetCurrentTime", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventSetCurrentTime_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentTime_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentTime_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentTime_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentTime_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentTime()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentTime_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetLockLevelSequence_Statics
	{
		struct LevelSequenceEditorBlueprintLibrary_eventSetLockLevelSequence_Parms
		{
			bool bLock;
		};
		static void NewProp_bLock_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bLock;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetLockLevelSequence_Statics::NewProp_bLock_SetBit(void* Obj)
	{
		((LevelSequenceEditorBlueprintLibrary_eventSetLockLevelSequence_Parms*)Obj)->bLock = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetLockLevelSequence_Statics::NewProp_bLock = { "bLock", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(LevelSequenceEditorBlueprintLibrary_eventSetLockLevelSequence_Parms), &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetLockLevelSequence_Statics::NewProp_bLock_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetLockLevelSequence_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetLockLevelSequence_Statics::NewProp_bLock,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetLockLevelSequence_Statics::Function_MetaDataParams[] = {
		{ "Category", "Level Sequence Editor" },
		{ "Comment", "/** Sets the lock for the current level sequence and its descendants for editing. */" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
		{ "ToolTip", "Sets the lock for the current level sequence and its descendants for editing." },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetLockLevelSequence_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, nullptr, "SetLockLevelSequence", nullptr, nullptr, sizeof(LevelSequenceEditorBlueprintLibrary_eventSetLockLevelSequence_Parms), Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetLockLevelSequence_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetLockLevelSequence_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04022401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetLockLevelSequence_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetLockLevelSequence_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetLockLevelSequence()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetLockLevelSequence_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary_NoRegister()
	{
		return ULevelSequenceEditorBlueprintLibrary::StaticClass();
	}
	struct Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSequenceEditor,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_CloseLevelSequence, "CloseLevelSequence" }, // 690386982
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_EmptySelection, "EmptySelection" }, // 3083775083
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetBoundObjects, "GetBoundObjects" }, // 2193281745
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLevelSequence, "GetCurrentLevelSequence" }, // 1019672023
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentLocalTime, "GetCurrentLocalTime" }, // 3456338896
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetCurrentTime, "GetCurrentTime" }, // 3141615249
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetFocusedLevelSequence, "GetFocusedLevelSequence" }, // 2449599957
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedChannels, "GetSelectedChannels" }, // 1591320915
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedFolders, "GetSelectedFolders" }, // 1840245036
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedObjects, "GetSelectedObjects" }, // 2582611477
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedSections, "GetSelectedSections" }, // 2652600237
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_GetSelectedTracks, "GetSelectedTracks" }, // 960021403
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsLevelSequenceLocked, "IsLevelSequenceLocked" }, // 3386704528
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_IsPlaying, "IsPlaying" }, // 1691258746
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_OpenLevelSequence, "OpenLevelSequence" }, // 1793524037
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_Pause, "Pause" }, // 4225643786
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_Play, "Play" }, // 123471577
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_PlayTo, "PlayTo" }, // 262065602
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_RefreshCurrentLevelSequence, "RefreshCurrentLevelSequence" }, // 2566009429
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectChannels, "SelectChannels" }, // 1592651523
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectFolders, "SelectFolders" }, // 619032408
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectObjects, "SelectObjects" }, // 3400135276
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectSections, "SelectSections" }, // 4181374794
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SelectTracks, "SelectTracks" }, // 1889878470
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentLocalTime, "SetCurrentLocalTime" }, // 3037327390
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetCurrentTime, "SetCurrentTime" }, // 4161551297
		{ &Z_Construct_UFunction_ULevelSequenceEditorBlueprintLibrary_SetLockLevelSequence, "SetLockLevelSequence" }, // 530629677
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "LevelSequenceEditorBlueprintLibrary.h" },
		{ "ModuleRelativePath", "Public/LevelSequenceEditorBlueprintLibrary.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULevelSequenceEditorBlueprintLibrary>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary_Statics::ClassParams = {
		&ULevelSequenceEditorBlueprintLibrary::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULevelSequenceEditorBlueprintLibrary, 372893295);
	template<> LEVELSEQUENCEEDITOR_API UClass* StaticClass<ULevelSequenceEditorBlueprintLibrary>()
	{
		return ULevelSequenceEditorBlueprintLibrary::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULevelSequenceEditorBlueprintLibrary(Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary, &ULevelSequenceEditorBlueprintLibrary::StaticClass, TEXT("/Script/LevelSequenceEditor"), TEXT("ULevelSequenceEditorBlueprintLibrary"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULevelSequenceEditorBlueprintLibrary);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
