// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSEQUENCEEDITOR_LevelSequenceFactoryNew_generated_h
#error "LevelSequenceFactoryNew.generated.h already included, missing '#pragma once' in LevelSequenceFactoryNew.h"
#endif
#define LEVELSEQUENCEEDITOR_LevelSequenceFactoryNew_generated_h

#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_SPARSE_DATA
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelSequenceFactoryNew(); \
	friend struct Z_Construct_UClass_ULevelSequenceFactoryNew_Statics; \
public: \
	DECLARE_CLASS(ULevelSequenceFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSequenceEditor"), NO_API) \
	DECLARE_SERIALIZER(ULevelSequenceFactoryNew)


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_INCLASS \
private: \
	static void StaticRegisterNativesULevelSequenceFactoryNew(); \
	friend struct Z_Construct_UClass_ULevelSequenceFactoryNew_Statics; \
public: \
	DECLARE_CLASS(ULevelSequenceFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSequenceEditor"), NO_API) \
	DECLARE_SERIALIZER(ULevelSequenceFactoryNew)


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSequenceFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSequenceFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSequenceFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSequenceFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSequenceFactoryNew(ULevelSequenceFactoryNew&&); \
	NO_API ULevelSequenceFactoryNew(const ULevelSequenceFactoryNew&); \
public:


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSequenceFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSequenceFactoryNew(ULevelSequenceFactoryNew&&); \
	NO_API ULevelSequenceFactoryNew(const ULevelSequenceFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSequenceFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSequenceFactoryNew); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSequenceFactoryNew)


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_13_PROLOG
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_SPARSE_DATA \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_INCLASS \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_SPARSE_DATA \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LevelSequenceFactoryNew."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSEQUENCEEDITOR_API UClass* StaticClass<class ULevelSequenceFactoryNew>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Factories_LevelSequenceFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
