// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FMovieSceneObjectBindingID;
class UObject;
struct FGuid;
class UMovieSceneFolder;
struct FSequencerChannelProxy;
class UMovieSceneSection;
class UMovieSceneTrack;
struct FMovieSceneSequencePlaybackParams;
class ULevelSequence;
#ifdef LEVELSEQUENCEEDITOR_LevelSequenceEditorBlueprintLibrary_generated_h
#error "LevelSequenceEditorBlueprintLibrary.generated.h already included, missing '#pragma once' in LevelSequenceEditorBlueprintLibrary.h"
#endif
#define LEVELSEQUENCEEDITOR_LevelSequenceEditorBlueprintLibrary_generated_h

#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_20_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FSequencerChannelProxy_Statics; \
	LEVELSEQUENCEEDITOR_API static class UScriptStruct* StaticStruct();


template<> LEVELSEQUENCEEDITOR_API UScriptStruct* StaticStruct<struct FSequencerChannelProxy>();

#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_SPARSE_DATA
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSetLockLevelSequence); \
	DECLARE_FUNCTION(execIsLevelSequenceLocked); \
	DECLARE_FUNCTION(execGetBoundObjects); \
	DECLARE_FUNCTION(execRefreshCurrentLevelSequence); \
	DECLARE_FUNCTION(execEmptySelection); \
	DECLARE_FUNCTION(execSelectObjects); \
	DECLARE_FUNCTION(execSelectFolders); \
	DECLARE_FUNCTION(execSelectChannels); \
	DECLARE_FUNCTION(execSelectSections); \
	DECLARE_FUNCTION(execSelectTracks); \
	DECLARE_FUNCTION(execGetSelectedObjects); \
	DECLARE_FUNCTION(execGetSelectedFolders); \
	DECLARE_FUNCTION(execGetSelectedChannels); \
	DECLARE_FUNCTION(execGetSelectedSections); \
	DECLARE_FUNCTION(execGetSelectedTracks); \
	DECLARE_FUNCTION(execIsPlaying); \
	DECLARE_FUNCTION(execPlayTo); \
	DECLARE_FUNCTION(execGetCurrentLocalTime); \
	DECLARE_FUNCTION(execSetCurrentLocalTime); \
	DECLARE_FUNCTION(execGetCurrentTime); \
	DECLARE_FUNCTION(execSetCurrentTime); \
	DECLARE_FUNCTION(execPause); \
	DECLARE_FUNCTION(execPlay); \
	DECLARE_FUNCTION(execCloseLevelSequence); \
	DECLARE_FUNCTION(execGetFocusedLevelSequence); \
	DECLARE_FUNCTION(execGetCurrentLevelSequence); \
	DECLARE_FUNCTION(execOpenLevelSequence);


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSetLockLevelSequence); \
	DECLARE_FUNCTION(execIsLevelSequenceLocked); \
	DECLARE_FUNCTION(execGetBoundObjects); \
	DECLARE_FUNCTION(execRefreshCurrentLevelSequence); \
	DECLARE_FUNCTION(execEmptySelection); \
	DECLARE_FUNCTION(execSelectObjects); \
	DECLARE_FUNCTION(execSelectFolders); \
	DECLARE_FUNCTION(execSelectChannels); \
	DECLARE_FUNCTION(execSelectSections); \
	DECLARE_FUNCTION(execSelectTracks); \
	DECLARE_FUNCTION(execGetSelectedObjects); \
	DECLARE_FUNCTION(execGetSelectedFolders); \
	DECLARE_FUNCTION(execGetSelectedChannels); \
	DECLARE_FUNCTION(execGetSelectedSections); \
	DECLARE_FUNCTION(execGetSelectedTracks); \
	DECLARE_FUNCTION(execIsPlaying); \
	DECLARE_FUNCTION(execPlayTo); \
	DECLARE_FUNCTION(execGetCurrentLocalTime); \
	DECLARE_FUNCTION(execSetCurrentLocalTime); \
	DECLARE_FUNCTION(execGetCurrentTime); \
	DECLARE_FUNCTION(execSetCurrentTime); \
	DECLARE_FUNCTION(execPause); \
	DECLARE_FUNCTION(execPlay); \
	DECLARE_FUNCTION(execCloseLevelSequence); \
	DECLARE_FUNCTION(execGetFocusedLevelSequence); \
	DECLARE_FUNCTION(execGetCurrentLevelSequence); \
	DECLARE_FUNCTION(execOpenLevelSequence);


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelSequenceEditorBlueprintLibrary(); \
	friend struct Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(ULevelSequenceEditorBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSequenceEditor"), NO_API) \
	DECLARE_SERIALIZER(ULevelSequenceEditorBlueprintLibrary)


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_INCLASS \
private: \
	static void StaticRegisterNativesULevelSequenceEditorBlueprintLibrary(); \
	friend struct Z_Construct_UClass_ULevelSequenceEditorBlueprintLibrary_Statics; \
public: \
	DECLARE_CLASS(ULevelSequenceEditorBlueprintLibrary, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/LevelSequenceEditor"), NO_API) \
	DECLARE_SERIALIZER(ULevelSequenceEditorBlueprintLibrary)


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSequenceEditorBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSequenceEditorBlueprintLibrary) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSequenceEditorBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSequenceEditorBlueprintLibrary); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSequenceEditorBlueprintLibrary(ULevelSequenceEditorBlueprintLibrary&&); \
	NO_API ULevelSequenceEditorBlueprintLibrary(const ULevelSequenceEditorBlueprintLibrary&); \
public:


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSequenceEditorBlueprintLibrary(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSequenceEditorBlueprintLibrary(ULevelSequenceEditorBlueprintLibrary&&); \
	NO_API ULevelSequenceEditorBlueprintLibrary(const ULevelSequenceEditorBlueprintLibrary&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSequenceEditorBlueprintLibrary); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSequenceEditorBlueprintLibrary); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSequenceEditorBlueprintLibrary)


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_39_PROLOG
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_SPARSE_DATA \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_INCLASS \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_SPARSE_DATA \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h_44_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSEQUENCEEDITOR_API UClass* StaticClass<class ULevelSequenceEditorBlueprintLibrary>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Public_LevelSequenceEditorBlueprintLibrary_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
