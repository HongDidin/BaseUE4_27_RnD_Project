// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSequenceEditor/Private/Factories/LevelSequenceFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLevelSequenceFactoryNew() {}
// Cross Module References
	LEVELSEQUENCEEDITOR_API UClass* Z_Construct_UClass_ULevelSequenceFactoryNew_NoRegister();
	LEVELSEQUENCEEDITOR_API UClass* Z_Construct_UClass_ULevelSequenceFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_LevelSequenceEditor();
// End Cross Module References
	void ULevelSequenceFactoryNew::StaticRegisterNativesULevelSequenceFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_ULevelSequenceFactoryNew_NoRegister()
	{
		return ULevelSequenceFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_ULevelSequenceFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULevelSequenceFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSequenceEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSequenceFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/**\n * Implements a factory for ULevelSequence objects.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/LevelSequenceFactoryNew.h" },
		{ "ModuleRelativePath", "Private/Factories/LevelSequenceFactoryNew.h" },
		{ "ToolTip", "Implements a factory for ULevelSequence objects." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULevelSequenceFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULevelSequenceFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULevelSequenceFactoryNew_Statics::ClassParams = {
		&ULevelSequenceFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_ULevelSequenceFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSequenceFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULevelSequenceFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULevelSequenceFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULevelSequenceFactoryNew, 1059004166);
	template<> LEVELSEQUENCEEDITOR_API UClass* StaticClass<ULevelSequenceFactoryNew>()
	{
		return ULevelSequenceFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULevelSequenceFactoryNew(Z_Construct_UClass_ULevelSequenceFactoryNew, &ULevelSequenceFactoryNew::StaticClass, TEXT("/Script/LevelSequenceEditor"), TEXT("ULevelSequenceFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULevelSequenceFactoryNew);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
