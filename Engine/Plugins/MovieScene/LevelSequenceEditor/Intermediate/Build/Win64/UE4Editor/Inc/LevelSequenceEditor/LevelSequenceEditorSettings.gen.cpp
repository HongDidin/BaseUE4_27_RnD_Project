// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "LevelSequenceEditor/Private/Misc/LevelSequenceEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeLevelSequenceEditorSettings() {}
// Cross Module References
	LEVELSEQUENCEEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FLevelSequenceTrackSettings();
	UPackage* Z_Construct_UPackage__Script_LevelSequenceEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftClassPath();
	LEVELSEQUENCEEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings();
	LEVELSEQUENCEEDITOR_API UClass* Z_Construct_UClass_ULevelSequenceEditorSettings_NoRegister();
	LEVELSEQUENCEEDITOR_API UClass* Z_Construct_UClass_ULevelSequenceEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	LEVELSEQUENCEEDITOR_API UClass* Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_NoRegister();
	LEVELSEQUENCEEDITOR_API UClass* Z_Construct_UClass_ULevelSequenceMasterSequenceSettings();
	ENGINE_API UScriptStruct* Z_Construct_UScriptStruct_FDirectoryPath();
	LEVELSEQUENCE_API UClass* Z_Construct_UClass_ULevelSequence_NoRegister();
// End Cross Module References
class UScriptStruct* FLevelSequenceTrackSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSEQUENCEEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLevelSequenceTrackSettings, Z_Construct_UPackage__Script_LevelSequenceEditor(), TEXT("LevelSequenceTrackSettings"), sizeof(FLevelSequenceTrackSettings), Get_Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Hash());
	}
	return Singleton;
}
template<> LEVELSEQUENCEEDITOR_API UScriptStruct* StaticStruct<FLevelSequenceTrackSettings>()
{
	return FLevelSequenceTrackSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLevelSequenceTrackSettings(FLevelSequenceTrackSettings::StaticStruct, TEXT("/Script/LevelSequenceEditor"), TEXT("LevelSequenceTrackSettings"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSequenceEditor_StaticRegisterNativesFLevelSequenceTrackSettings
{
	FScriptStruct_LevelSequenceEditor_StaticRegisterNativesFLevelSequenceTrackSettings()
	{
		UScriptStruct::DeferCppStructOps<FLevelSequenceTrackSettings>(FName(TEXT("LevelSequenceTrackSettings")));
	}
} ScriptStruct_LevelSequenceEditor_StaticRegisterNativesFLevelSequenceTrackSettings;
	struct Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MatchingActorClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MatchingActorClass;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultTracks_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultTracks_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DefaultTracks;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExcludeDefaultTracks_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExcludeDefaultTracks_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExcludeDefaultTracks;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultPropertyTracks_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultPropertyTracks_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_DefaultPropertyTracks;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ExcludeDefaultPropertyTracks_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExcludeDefaultPropertyTracks_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ExcludeDefaultPropertyTracks;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLevelSequenceTrackSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_MatchingActorClass_MetaData[] = {
		{ "Category", "TrackSettings" },
		{ "Comment", "/** The Actor class to create movie scene tracks for. */" },
		{ "MetaClass", "Actor" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "The Actor class to create movie scene tracks for." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_MatchingActorClass = { "MatchingActorClass", nullptr, (EPropertyFlags)0x0010000002004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLevelSequenceTrackSettings, MatchingActorClass), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_MatchingActorClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_MatchingActorClass_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_DefaultTracks_Inner = { "DefaultTracks", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_DefaultTracks_MetaData[] = {
		{ "Category", "TrackSettings" },
		{ "Comment", "/** List of movie scene track classes to be added automatically. */" },
		{ "MetaClass", "MovieSceneTrack" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "List of movie scene track classes to be added automatically." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_DefaultTracks = { "DefaultTracks", nullptr, (EPropertyFlags)0x0010000002004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLevelSequenceTrackSettings, DefaultTracks), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_DefaultTracks_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_DefaultTracks_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_ExcludeDefaultTracks_Inner = { "ExcludeDefaultTracks", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_ExcludeDefaultTracks_MetaData[] = {
		{ "Category", "TrackSettings" },
		{ "Comment", "/** List of movie scene track classes not to be added automatically. */" },
		{ "MetaClass", "MovieSceneTrack" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "List of movie scene track classes not to be added automatically." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_ExcludeDefaultTracks = { "ExcludeDefaultTracks", nullptr, (EPropertyFlags)0x0010000002004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLevelSequenceTrackSettings, ExcludeDefaultTracks), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_ExcludeDefaultTracks_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_ExcludeDefaultTracks_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_DefaultPropertyTracks_Inner = { "DefaultPropertyTracks", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_DefaultPropertyTracks_MetaData[] = {
		{ "Category", "TrackSettings" },
		{ "Comment", "/** List of property names for which movie scene tracks will be created automatically. */" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "List of property names for which movie scene tracks will be created automatically." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_DefaultPropertyTracks = { "DefaultPropertyTracks", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLevelSequenceTrackSettings, DefaultPropertyTracks), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_DefaultPropertyTracks_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_DefaultPropertyTracks_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_ExcludeDefaultPropertyTracks_Inner = { "ExcludeDefaultPropertyTracks", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_ExcludeDefaultPropertyTracks_MetaData[] = {
		{ "Category", "TrackSettings" },
		{ "Comment", "/** List of property names for which movie scene tracks will not be created automatically. */" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "List of property names for which movie scene tracks will not be created automatically." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_ExcludeDefaultPropertyTracks = { "ExcludeDefaultPropertyTracks", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLevelSequenceTrackSettings, ExcludeDefaultPropertyTracks), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_ExcludeDefaultPropertyTracks_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_ExcludeDefaultPropertyTracks_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_MatchingActorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_DefaultTracks_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_DefaultTracks,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_ExcludeDefaultTracks_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_ExcludeDefaultTracks,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_DefaultPropertyTracks_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_DefaultPropertyTracks,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_ExcludeDefaultPropertyTracks_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::NewProp_ExcludeDefaultPropertyTracks,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSequenceEditor,
		nullptr,
		&NewStructOps,
		"LevelSequenceTrackSettings",
		sizeof(FLevelSequenceTrackSettings),
		alignof(FLevelSequenceTrackSettings),
		Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLevelSequenceTrackSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSequenceEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LevelSequenceTrackSettings"), sizeof(FLevelSequenceTrackSettings), Get_Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Hash() { return 3677577547U; }
class UScriptStruct* FLevelSequencePropertyTrackSettings::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern LEVELSEQUENCEEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings, Z_Construct_UPackage__Script_LevelSequenceEditor(), TEXT("LevelSequencePropertyTrackSettings"), sizeof(FLevelSequencePropertyTrackSettings), Get_Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Hash());
	}
	return Singleton;
}
template<> LEVELSEQUENCEEDITOR_API UScriptStruct* StaticStruct<FLevelSequencePropertyTrackSettings>()
{
	return FLevelSequencePropertyTrackSettings::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FLevelSequencePropertyTrackSettings(FLevelSequencePropertyTrackSettings::StaticStruct, TEXT("/Script/LevelSequenceEditor"), TEXT("LevelSequencePropertyTrackSettings"), false, nullptr, nullptr);
static struct FScriptStruct_LevelSequenceEditor_StaticRegisterNativesFLevelSequencePropertyTrackSettings
{
	FScriptStruct_LevelSequenceEditor_StaticRegisterNativesFLevelSequencePropertyTrackSettings()
	{
		UScriptStruct::DeferCppStructOps<FLevelSequencePropertyTrackSettings>(FName(TEXT("LevelSequencePropertyTrackSettings")));
	}
} ScriptStruct_LevelSequenceEditor_StaticRegisterNativesFLevelSequencePropertyTrackSettings;
	struct Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ComponentPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_ComponentPath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PropertyPath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_PropertyPath;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FLevelSequencePropertyTrackSettings>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::NewProp_ComponentPath_MetaData[] = {
		{ "Category", "PropertyTrack" },
		{ "Comment", "/** Optional ActorComponent tag (when keying a component property). */" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "Optional ActorComponent tag (when keying a component property)." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::NewProp_ComponentPath = { "ComponentPath", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLevelSequencePropertyTrackSettings, ComponentPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::NewProp_ComponentPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::NewProp_ComponentPath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::NewProp_PropertyPath_MetaData[] = {
		{ "Category", "PropertyTrack" },
		{ "Comment", "/** Path to the keyed property within the Actor or ActorComponent. */" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "Path to the keyed property within the Actor or ActorComponent." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::NewProp_PropertyPath = { "PropertyPath", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FLevelSequencePropertyTrackSettings, PropertyPath), METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::NewProp_PropertyPath_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::NewProp_PropertyPath_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::NewProp_ComponentPath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::NewProp_PropertyPath,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSequenceEditor,
		nullptr,
		&NewStructOps,
		"LevelSequencePropertyTrackSettings",
		sizeof(FLevelSequencePropertyTrackSettings),
		alignof(FLevelSequencePropertyTrackSettings),
		Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_LevelSequenceEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("LevelSequencePropertyTrackSettings"), sizeof(FLevelSequencePropertyTrackSettings), Get_Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Hash() { return 1264583436U; }
	void ULevelSequenceEditorSettings::StaticRegisterNativesULevelSequenceEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_ULevelSequenceEditorSettings_NoRegister()
	{
		return ULevelSequenceEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_ULevelSequenceEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TrackSettings_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TrackSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TrackSettings;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoBindToPIE_MetaData[];
#endif
		static void NewProp_bAutoBindToPIE_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoBindToPIE;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoBindToSimulate_MetaData[];
#endif
		static void NewProp_bAutoBindToSimulate_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoBindToSimulate;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSequenceEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Level Sequence Editor settings.\n */" },
		{ "IncludePath", "Misc/LevelSequenceEditorSettings.h" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Level Sequence Editor settings." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_TrackSettings_Inner = { "TrackSettings", nullptr, (EPropertyFlags)0x0000000000004000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FLevelSequenceTrackSettings, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_TrackSettings_MetaData[] = {
		{ "Category", "Tracks" },
		{ "Comment", "/** Specifies class properties for which movie scene tracks will be created automatically. */" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "Specifies class properties for which movie scene tracks will be created automatically." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_TrackSettings = { "TrackSettings", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSequenceEditorSettings, TrackSettings), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_TrackSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_TrackSettings_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_bAutoBindToPIE_MetaData[] = {
		{ "Category", "Playback" },
		{ "Comment", "/** Specifies whether to automatically bind an active sequencer UI to PIE worlds. */" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "Specifies whether to automatically bind an active sequencer UI to PIE worlds." },
	};
#endif
	void Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_bAutoBindToPIE_SetBit(void* Obj)
	{
		((ULevelSequenceEditorSettings*)Obj)->bAutoBindToPIE = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_bAutoBindToPIE = { "bAutoBindToPIE", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULevelSequenceEditorSettings), &Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_bAutoBindToPIE_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_bAutoBindToPIE_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_bAutoBindToPIE_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_bAutoBindToSimulate_MetaData[] = {
		{ "Category", "Playback" },
		{ "Comment", "/** Specifies whether to automatically bind an active sequencer UI to simulate worlds. */" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "Specifies whether to automatically bind an active sequencer UI to simulate worlds." },
	};
#endif
	void Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_bAutoBindToSimulate_SetBit(void* Obj)
	{
		((ULevelSequenceEditorSettings*)Obj)->bAutoBindToSimulate = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_bAutoBindToSimulate = { "bAutoBindToSimulate", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULevelSequenceEditorSettings), &Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_bAutoBindToSimulate_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_bAutoBindToSimulate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_bAutoBindToSimulate_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_TrackSettings_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_TrackSettings,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_bAutoBindToPIE,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::NewProp_bAutoBindToSimulate,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULevelSequenceEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::ClassParams = {
		&ULevelSequenceEditorSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULevelSequenceEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULevelSequenceEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULevelSequenceEditorSettings, 1233602522);
	template<> LEVELSEQUENCEEDITOR_API UClass* StaticClass<ULevelSequenceEditorSettings>()
	{
		return ULevelSequenceEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULevelSequenceEditorSettings(Z_Construct_UClass_ULevelSequenceEditorSettings, &ULevelSequenceEditorSettings::StaticClass, TEXT("/Script/LevelSequenceEditor"), TEXT("ULevelSequenceEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULevelSequenceEditorSettings);
	void ULevelSequenceMasterSequenceSettings::StaticRegisterNativesULevelSequenceMasterSequenceSettings()
	{
	}
	UClass* Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_NoRegister()
	{
		return ULevelSequenceMasterSequenceSettings::StaticClass();
	}
	struct Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MasterSequenceName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MasterSequenceName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MasterSequenceSuffix_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_MasterSequenceSuffix;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MasterSequenceBasePath_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MasterSequenceBasePath;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MasterSequenceNumShots_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_MasterSequenceNumShots;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MasterSequenceLevelSequenceToDuplicate_MetaData[];
#endif
		static const UE4CodeGen_Private::FLazyObjectPropertyParams NewProp_MasterSequenceLevelSequenceToDuplicate;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_SubSequenceNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SubSequenceNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SubSequenceNames;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bInstanceSubSequences_MetaData[];
#endif
		static void NewProp_bInstanceSubSequences_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bInstanceSubSequences;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_LevelSequenceEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Level Sequence Master Sequence settings.\n */" },
		{ "IncludePath", "Misc/LevelSequenceEditorSettings.h" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "Level Sequence Master Sequence settings." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceName_MetaData[] = {
		{ "Category", "MasterSequence" },
		{ "Comment", "/** Master sequence name. */" },
		{ "DisplayName", "Name" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "Master sequence name." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceName = { "MasterSequenceName", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSequenceMasterSequenceSettings, MasterSequenceName), METADATA_PARAMS(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceSuffix_MetaData[] = {
		{ "Category", "MasterSequence" },
		{ "Comment", "/** Master sequence suffix. */" },
		{ "DisplayName", "Suffix" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "Master sequence suffix." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceSuffix = { "MasterSequenceSuffix", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSequenceMasterSequenceSettings, MasterSequenceSuffix), METADATA_PARAMS(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceSuffix_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceSuffix_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceBasePath_MetaData[] = {
		{ "Category", "MasterSequence" },
		{ "Comment", "/** Master sequence path. */" },
		{ "ContentDir", "" },
		{ "DisplayName", "Base Path" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "Master sequence path." },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceBasePath = { "MasterSequenceBasePath", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSequenceMasterSequenceSettings, MasterSequenceBasePath), Z_Construct_UScriptStruct_FDirectoryPath, METADATA_PARAMS(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceBasePath_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceBasePath_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceNumShots_MetaData[] = {
		{ "Category", "MasterSequence" },
		{ "Comment", "/** Master sequence number of shots. */" },
		{ "DisplayName", "Number of Shots" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "Master sequence number of shots." },
		{ "UIMax", "100" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceNumShots = { "MasterSequenceNumShots", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSequenceMasterSequenceSettings, MasterSequenceNumShots), METADATA_PARAMS(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceNumShots_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceNumShots_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceLevelSequenceToDuplicate_MetaData[] = {
		{ "Category", "MasterSequence" },
		{ "Comment", "/** Master sequence level sequence to duplicate when creating shots. */" },
		{ "DisplayName", "Sequence to Duplicate" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "Master sequence level sequence to duplicate when creating shots." },
	};
#endif
	const UE4CodeGen_Private::FLazyObjectPropertyParams Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceLevelSequenceToDuplicate = { "MasterSequenceLevelSequenceToDuplicate", nullptr, (EPropertyFlags)0x0014000000002001, UE4CodeGen_Private::EPropertyGenFlags::LazyObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSequenceMasterSequenceSettings, MasterSequenceLevelSequenceToDuplicate), Z_Construct_UClass_ULevelSequence_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceLevelSequenceToDuplicate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceLevelSequenceToDuplicate_MetaData)) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_SubSequenceNames_Inner = { "SubSequenceNames", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_SubSequenceNames_MetaData[] = {
		{ "Category", "MasterSequence" },
		{ "Comment", "/** Array of sub sequence names, each will result in a level sequence asset in the shot. */" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "Array of sub sequence names, each will result in a level sequence asset in the shot." },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_SubSequenceNames = { "SubSequenceNames", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ULevelSequenceMasterSequenceSettings, SubSequenceNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_SubSequenceNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_SubSequenceNames_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_bInstanceSubSequences_MetaData[] = {
		{ "Category", "MasterSequence" },
		{ "Comment", "/** Whether to instance sub sequences based on the first created sub sequences. */" },
		{ "DisplayName", "Instance Sub Sequences" },
		{ "ModuleRelativePath", "Private/Misc/LevelSequenceEditorSettings.h" },
		{ "ToolTip", "Whether to instance sub sequences based on the first created sub sequences." },
	};
#endif
	void Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_bInstanceSubSequences_SetBit(void* Obj)
	{
		((ULevelSequenceMasterSequenceSettings*)Obj)->bInstanceSubSequences = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_bInstanceSubSequences = { "bInstanceSubSequences", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ULevelSequenceMasterSequenceSettings), &Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_bInstanceSubSequences_SetBit, METADATA_PARAMS(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_bInstanceSubSequences_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_bInstanceSubSequences_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceSuffix,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceBasePath,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceNumShots,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_MasterSequenceLevelSequenceToDuplicate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_SubSequenceNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_SubSequenceNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::NewProp_bInstanceSubSequences,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ULevelSequenceMasterSequenceSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::ClassParams = {
		&ULevelSequenceMasterSequenceSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ULevelSequenceMasterSequenceSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ULevelSequenceMasterSequenceSettings, 1687193644);
	template<> LEVELSEQUENCEEDITOR_API UClass* StaticClass<ULevelSequenceMasterSequenceSettings>()
	{
		return ULevelSequenceMasterSequenceSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ULevelSequenceMasterSequenceSettings(Z_Construct_UClass_ULevelSequenceMasterSequenceSettings, &ULevelSequenceMasterSequenceSettings::StaticClass, TEXT("/Script/LevelSequenceEditor"), TEXT("ULevelSequenceMasterSequenceSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ULevelSequenceMasterSequenceSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
