// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef LEVELSEQUENCEEDITOR_LevelSequenceEditorSettings_generated_h
#error "LevelSequenceEditorSettings.generated.h already included, missing '#pragma once' in LevelSequenceEditorSettings.h"
#endif
#define LEVELSEQUENCEEDITOR_LevelSequenceEditorSettings_generated_h

#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_31_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLevelSequenceTrackSettings_Statics; \
	LEVELSEQUENCEEDITOR_API static class UScriptStruct* StaticStruct();


template<> LEVELSEQUENCEEDITOR_API UScriptStruct* StaticStruct<struct FLevelSequenceTrackSettings>();

#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_16_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_FLevelSequencePropertyTrackSettings_Statics; \
	LEVELSEQUENCEEDITOR_API static class UScriptStruct* StaticStruct();


template<> LEVELSEQUENCEEDITOR_API UScriptStruct* StaticStruct<struct FLevelSequencePropertyTrackSettings>();

#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_SPARSE_DATA
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelSequenceEditorSettings(); \
	friend struct Z_Construct_UClass_ULevelSequenceEditorSettings_Statics; \
public: \
	DECLARE_CLASS(ULevelSequenceEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LevelSequenceEditor"), NO_API) \
	DECLARE_SERIALIZER(ULevelSequenceEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_INCLASS \
private: \
	static void StaticRegisterNativesULevelSequenceEditorSettings(); \
	friend struct Z_Construct_UClass_ULevelSequenceEditorSettings_Statics; \
public: \
	DECLARE_CLASS(ULevelSequenceEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LevelSequenceEditor"), NO_API) \
	DECLARE_SERIALIZER(ULevelSequenceEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSequenceEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSequenceEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSequenceEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSequenceEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSequenceEditorSettings(ULevelSequenceEditorSettings&&); \
	NO_API ULevelSequenceEditorSettings(const ULevelSequenceEditorSettings&); \
public:


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSequenceEditorSettings(ULevelSequenceEditorSettings&&); \
	NO_API ULevelSequenceEditorSettings(const ULevelSequenceEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSequenceEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSequenceEditorSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSequenceEditorSettings)


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_58_PROLOG
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_SPARSE_DATA \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_INCLASS \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_SPARSE_DATA \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_62_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSEQUENCEEDITOR_API UClass* StaticClass<class ULevelSequenceEditorSettings>();

#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_SPARSE_DATA
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesULevelSequenceMasterSequenceSettings(); \
	friend struct Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics; \
public: \
	DECLARE_CLASS(ULevelSequenceMasterSequenceSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LevelSequenceEditor"), NO_API) \
	DECLARE_SERIALIZER(ULevelSequenceMasterSequenceSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_INCLASS \
private: \
	static void StaticRegisterNativesULevelSequenceMasterSequenceSettings(); \
	friend struct Z_Construct_UClass_ULevelSequenceMasterSequenceSettings_Statics; \
public: \
	DECLARE_CLASS(ULevelSequenceMasterSequenceSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/LevelSequenceEditor"), NO_API) \
	DECLARE_SERIALIZER(ULevelSequenceMasterSequenceSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSequenceMasterSequenceSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSequenceMasterSequenceSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSequenceMasterSequenceSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSequenceMasterSequenceSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSequenceMasterSequenceSettings(ULevelSequenceMasterSequenceSettings&&); \
	NO_API ULevelSequenceMasterSequenceSettings(const ULevelSequenceMasterSequenceSettings&); \
public:


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ULevelSequenceMasterSequenceSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ULevelSequenceMasterSequenceSettings(ULevelSequenceMasterSequenceSettings&&); \
	NO_API ULevelSequenceMasterSequenceSettings(const ULevelSequenceMasterSequenceSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ULevelSequenceMasterSequenceSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ULevelSequenceMasterSequenceSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ULevelSequenceMasterSequenceSettings)


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_84_PROLOG
#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_SPARSE_DATA \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_INCLASS \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_SPARSE_DATA \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h_88_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class LevelSequenceMasterSequenceSettings."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> LEVELSEQUENCEEDITOR_API UClass* StaticClass<class ULevelSequenceMasterSequenceSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_LevelSequenceEditor_Source_LevelSequenceEditor_Private_Misc_LevelSequenceEditorSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
