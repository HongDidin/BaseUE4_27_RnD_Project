// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MoviePipelineMaskRenderPass/Private/MoviePipelineObjectIdPass.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMoviePipelineObjectIdPass() {}
// Cross Module References
	MOVIEPIPELINEMASKRENDERPASS_API UEnum* Z_Construct_UEnum_MoviePipelineMaskRenderPass_EMoviePipelineObjectIdPassIdType();
	UPackage* Z_Construct_UPackage__Script_MoviePipelineMaskRenderPass();
	MOVIEPIPELINEMASKRENDERPASS_API UClass* Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_NoRegister();
	MOVIEPIPELINEMASKRENDERPASS_API UClass* Z_Construct_UClass_UMoviePipelineObjectIdRenderPass();
	MOVIERENDERPIPELINERENDERPASSES_API UClass* Z_Construct_UClass_UMoviePipelineImagePassBase();
// End Cross Module References
	static UEnum* EMoviePipelineObjectIdPassIdType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MoviePipelineMaskRenderPass_EMoviePipelineObjectIdPassIdType, Z_Construct_UPackage__Script_MoviePipelineMaskRenderPass(), TEXT("EMoviePipelineObjectIdPassIdType"));
		}
		return Singleton;
	}
	template<> MOVIEPIPELINEMASKRENDERPASS_API UEnum* StaticEnum<EMoviePipelineObjectIdPassIdType>()
	{
		return EMoviePipelineObjectIdPassIdType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMoviePipelineObjectIdPassIdType(EMoviePipelineObjectIdPassIdType_StaticEnum, TEXT("/Script/MoviePipelineMaskRenderPass"), TEXT("EMoviePipelineObjectIdPassIdType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MoviePipelineMaskRenderPass_EMoviePipelineObjectIdPassIdType_Hash() { return 1661236570U; }
	UEnum* Z_Construct_UEnum_MoviePipelineMaskRenderPass_EMoviePipelineObjectIdPassIdType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MoviePipelineMaskRenderPass();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMoviePipelineObjectIdPassIdType"), 0, Get_Z_Construct_UEnum_MoviePipelineMaskRenderPass_EMoviePipelineObjectIdPassIdType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMoviePipelineObjectIdPassIdType::Full", (int64)EMoviePipelineObjectIdPassIdType::Full },
				{ "EMoviePipelineObjectIdPassIdType::Material", (int64)EMoviePipelineObjectIdPassIdType::Material },
				{ "EMoviePipelineObjectIdPassIdType::Actor", (int64)EMoviePipelineObjectIdPassIdType::Actor },
				{ "EMoviePipelineObjectIdPassIdType::ActorWithHierarchy", (int64)EMoviePipelineObjectIdPassIdType::ActorWithHierarchy },
				{ "EMoviePipelineObjectIdPassIdType::Folder", (int64)EMoviePipelineObjectIdPassIdType::Folder },
				{ "EMoviePipelineObjectIdPassIdType::Layer", (int64)EMoviePipelineObjectIdPassIdType::Layer },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Actor.Comment", "/** Grouped by Actor Name, all materials for a given actor are merged together, and all actors with that name are merged together as well. */" },
				{ "Actor.Name", "EMoviePipelineObjectIdPassIdType::Actor" },
				{ "Actor.ToolTip", "Grouped by Actor Name, all materials for a given actor are merged together, and all actors with that name are merged together as well." },
				{ "ActorWithHierarchy.Comment", "/** Grouped by Actor Name and Folder Hierarchy. This means actors with the same name in different folders will not be merged together. */" },
				{ "ActorWithHierarchy.Name", "EMoviePipelineObjectIdPassIdType::ActorWithHierarchy" },
				{ "ActorWithHierarchy.ToolTip", "Grouped by Actor Name and Folder Hierarchy. This means actors with the same name in different folders will not be merged together." },
				{ "BlueprintType", "true" },
				{ "Folder.Comment", "/** Grouped by Folder Name. All actors within a given folder hierarchy in the World Outliner are merged together. */" },
				{ "Folder.Name", "EMoviePipelineObjectIdPassIdType::Folder" },
				{ "Folder.ToolTip", "Grouped by Folder Name. All actors within a given folder hierarchy in the World Outliner are merged together." },
				{ "Full.Comment", "/** As much information as the renderer can provide - unique per material per primitive in the world. */" },
				{ "Full.Name", "EMoviePipelineObjectIdPassIdType::Full" },
				{ "Full.ToolTip", "As much information as the renderer can provide - unique per material per primitive in the world." },
				{ "Layer.Comment", "/** Primary Layer. This is the first layer found in the AActor::Layers array. May not do what you expect if an actor belongs to multiple layers. */" },
				{ "Layer.Name", "EMoviePipelineObjectIdPassIdType::Layer" },
				{ "Layer.ToolTip", "Primary Layer. This is the first layer found in the AActor::Layers array. May not do what you expect if an actor belongs to multiple layers." },
				{ "Material.Comment", "/** Grouped by material name. This means different objects that use the same material will be merged. */" },
				{ "Material.Name", "EMoviePipelineObjectIdPassIdType::Material" },
				{ "Material.ToolTip", "Grouped by material name. This means different objects that use the same material will be merged." },
				{ "ModuleRelativePath", "Private/MoviePipelineObjectIdPass.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MoviePipelineMaskRenderPass,
				nullptr,
				"EMoviePipelineObjectIdPassIdType",
				"EMoviePipelineObjectIdPassIdType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMoviePipelineObjectIdRenderPass::StaticRegisterNativesUMoviePipelineObjectIdRenderPass()
	{
	}
	UClass* Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_NoRegister()
	{
		return UMoviePipelineObjectIdRenderPass::StaticClass();
	}
	struct Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_IdType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_IdType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_IdType;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMoviePipelineImagePassBase,
		(UObject* (*)())Z_Construct_UPackage__Script_MoviePipelineMaskRenderPass,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "MoviePipelineObjectIdPass.h" },
		{ "ModuleRelativePath", "Private/MoviePipelineObjectIdPass.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::NewProp_IdType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::NewProp_IdType_MetaData[] = {
		{ "Category", "Settings" },
		{ "ModuleRelativePath", "Private/MoviePipelineObjectIdPass.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::NewProp_IdType = { "IdType", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMoviePipelineObjectIdRenderPass, IdType), Z_Construct_UEnum_MoviePipelineMaskRenderPass_EMoviePipelineObjectIdPassIdType, METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::NewProp_IdType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::NewProp_IdType_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::NewProp_IdType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::NewProp_IdType,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMoviePipelineObjectIdRenderPass>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::ClassParams = {
		&UMoviePipelineObjectIdRenderPass::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMoviePipelineObjectIdRenderPass()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMoviePipelineObjectIdRenderPass, 2157177132);
	template<> MOVIEPIPELINEMASKRENDERPASS_API UClass* StaticClass<UMoviePipelineObjectIdRenderPass>()
	{
		return UMoviePipelineObjectIdRenderPass::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMoviePipelineObjectIdRenderPass(Z_Construct_UClass_UMoviePipelineObjectIdRenderPass, &UMoviePipelineObjectIdRenderPass::StaticClass, TEXT("/Script/MoviePipelineMaskRenderPass"), TEXT("UMoviePipelineObjectIdRenderPass"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMoviePipelineObjectIdRenderPass);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
