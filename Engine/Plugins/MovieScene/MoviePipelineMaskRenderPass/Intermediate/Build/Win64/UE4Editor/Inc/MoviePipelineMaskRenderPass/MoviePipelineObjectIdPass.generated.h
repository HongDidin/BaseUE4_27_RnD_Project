// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MOVIEPIPELINEMASKRENDERPASS_MoviePipelineObjectIdPass_generated_h
#error "MoviePipelineObjectIdPass.generated.h already included, missing '#pragma once' in MoviePipelineObjectIdPass.h"
#endif
#define MOVIEPIPELINEMASKRENDERPASS_MoviePipelineObjectIdPass_generated_h

#define Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_SPARSE_DATA
#define Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMoviePipelineObjectIdRenderPass(); \
	friend struct Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineObjectIdRenderPass, UMoviePipelineImagePassBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MoviePipelineMaskRenderPass"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineObjectIdRenderPass)


#define Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_INCLASS \
private: \
	static void StaticRegisterNativesUMoviePipelineObjectIdRenderPass(); \
	friend struct Z_Construct_UClass_UMoviePipelineObjectIdRenderPass_Statics; \
public: \
	DECLARE_CLASS(UMoviePipelineObjectIdRenderPass, UMoviePipelineImagePassBase, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MoviePipelineMaskRenderPass"), NO_API) \
	DECLARE_SERIALIZER(UMoviePipelineObjectIdRenderPass)


#define Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMoviePipelineObjectIdRenderPass(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMoviePipelineObjectIdRenderPass) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineObjectIdRenderPass); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineObjectIdRenderPass); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineObjectIdRenderPass(UMoviePipelineObjectIdRenderPass&&); \
	NO_API UMoviePipelineObjectIdRenderPass(const UMoviePipelineObjectIdRenderPass&); \
public:


#define Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMoviePipelineObjectIdRenderPass(UMoviePipelineObjectIdRenderPass&&); \
	NO_API UMoviePipelineObjectIdRenderPass(const UMoviePipelineObjectIdRenderPass&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMoviePipelineObjectIdRenderPass); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMoviePipelineObjectIdRenderPass); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMoviePipelineObjectIdRenderPass)


#define Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_25_PROLOG
#define Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_SPARSE_DATA \
	Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_INCLASS \
	Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_SPARSE_DATA \
	Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h_28_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MOVIEPIPELINEMASKRENDERPASS_API UClass* StaticClass<class UMoviePipelineObjectIdRenderPass>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_MoviePipelineMaskRenderPass_Source_MoviePipelineMaskRenderPass_Private_MoviePipelineObjectIdPass_h


#define FOREACH_ENUM_EMOVIEPIPELINEOBJECTIDPASSIDTYPE(op) \
	op(EMoviePipelineObjectIdPassIdType::Full) \
	op(EMoviePipelineObjectIdPassIdType::Material) \
	op(EMoviePipelineObjectIdPassIdType::Actor) \
	op(EMoviePipelineObjectIdPassIdType::ActorWithHierarchy) \
	op(EMoviePipelineObjectIdPassIdType::Folder) \
	op(EMoviePipelineObjectIdPassIdType::Layer) 

enum class EMoviePipelineObjectIdPassIdType : uint8;
template<> MOVIEPIPELINEMASKRENDERPASS_API UEnum* StaticEnum<EMoviePipelineObjectIdPassIdType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
