// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TEMPLATESEQUENCEEDITOR_TemplateSequenceFactoryNew_generated_h
#error "TemplateSequenceFactoryNew.generated.h already included, missing '#pragma once' in TemplateSequenceFactoryNew.h"
#endif
#define TEMPLATESEQUENCEEDITOR_TemplateSequenceFactoryNew_generated_h

#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_SPARSE_DATA
#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTemplateSequenceFactoryNew(); \
	friend struct Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UTemplateSequenceFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TemplateSequenceEditor"), NO_API) \
	DECLARE_SERIALIZER(UTemplateSequenceFactoryNew)


#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUTemplateSequenceFactoryNew(); \
	friend struct Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics; \
public: \
	DECLARE_CLASS(UTemplateSequenceFactoryNew, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/TemplateSequenceEditor"), NO_API) \
	DECLARE_SERIALIZER(UTemplateSequenceFactoryNew)


#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTemplateSequenceFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTemplateSequenceFactoryNew) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTemplateSequenceFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTemplateSequenceFactoryNew); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTemplateSequenceFactoryNew(UTemplateSequenceFactoryNew&&); \
	NO_API UTemplateSequenceFactoryNew(const UTemplateSequenceFactoryNew&); \
public:


#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTemplateSequenceFactoryNew(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTemplateSequenceFactoryNew(UTemplateSequenceFactoryNew&&); \
	NO_API UTemplateSequenceFactoryNew(const UTemplateSequenceFactoryNew&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTemplateSequenceFactoryNew); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTemplateSequenceFactoryNew); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTemplateSequenceFactoryNew)


#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_15_PROLOG
#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_SPARSE_DATA \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_INCLASS \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_SPARSE_DATA \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class TemplateSequenceFactoryNew."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TEMPLATESEQUENCEEDITOR_API UClass* StaticClass<class UTemplateSequenceFactoryNew>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Factories_TemplateSequenceFactoryNew_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
