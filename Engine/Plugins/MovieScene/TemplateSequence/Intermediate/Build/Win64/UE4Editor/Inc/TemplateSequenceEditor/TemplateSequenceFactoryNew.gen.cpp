// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TemplateSequenceEditor/Private/Factories/TemplateSequenceFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTemplateSequenceFactoryNew() {}
// Cross Module References
	TEMPLATESEQUENCEEDITOR_API UClass* Z_Construct_UClass_UTemplateSequenceFactoryNew_NoRegister();
	TEMPLATESEQUENCEEDITOR_API UClass* Z_Construct_UClass_UTemplateSequenceFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_TemplateSequenceEditor();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject_NoRegister();
// End Cross Module References
	void UTemplateSequenceFactoryNew::StaticRegisterNativesUTemplateSequenceFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UTemplateSequenceFactoryNew_NoRegister()
	{
		return UTemplateSequenceFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BoundActorClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_BoundActorClass;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_TemplateSequenceEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements a factory for UTemplateSequence objects.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/TemplateSequenceFactoryNew.h" },
		{ "ModuleRelativePath", "Private/Factories/TemplateSequenceFactoryNew.h" },
		{ "ToolTip", "Implements a factory for UTemplateSequence objects." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics::NewProp_BoundActorClass_MetaData[] = {
		{ "Category", "TemplateSequenceFactory" },
		{ "Comment", "// The root object binding class of the created template sequence.\n" },
		{ "ModuleRelativePath", "Private/Factories/TemplateSequenceFactoryNew.h" },
		{ "ToolTip", "The root object binding class of the created template sequence." },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics::NewProp_BoundActorClass = { "BoundActorClass", nullptr, (EPropertyFlags)0x0014000000000001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTemplateSequenceFactoryNew, BoundActorClass), Z_Construct_UClass_UObject_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics::NewProp_BoundActorClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics::NewProp_BoundActorClass_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics::NewProp_BoundActorClass,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTemplateSequenceFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics::ClassParams = {
		&UTemplateSequenceFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTemplateSequenceFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTemplateSequenceFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTemplateSequenceFactoryNew, 1808606350);
	template<> TEMPLATESEQUENCEEDITOR_API UClass* StaticClass<UTemplateSequenceFactoryNew>()
	{
		return UTemplateSequenceFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTemplateSequenceFactoryNew(Z_Construct_UClass_UTemplateSequenceFactoryNew, &UTemplateSequenceFactoryNew::StaticClass, TEXT("/Script/TemplateSequenceEditor"), TEXT("UTemplateSequenceFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTemplateSequenceFactoryNew);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
