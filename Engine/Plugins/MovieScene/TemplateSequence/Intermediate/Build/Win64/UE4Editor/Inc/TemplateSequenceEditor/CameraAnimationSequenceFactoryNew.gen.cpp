// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TemplateSequenceEditor/Private/Factories/CameraAnimationSequenceFactoryNew.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCameraAnimationSequenceFactoryNew() {}
// Cross Module References
	TEMPLATESEQUENCEEDITOR_API UClass* Z_Construct_UClass_UCameraAnimationSequenceFactoryNew_NoRegister();
	TEMPLATESEQUENCEEDITOR_API UClass* Z_Construct_UClass_UCameraAnimationSequenceFactoryNew();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_TemplateSequenceEditor();
// End Cross Module References
	void UCameraAnimationSequenceFactoryNew::StaticRegisterNativesUCameraAnimationSequenceFactoryNew()
	{
	}
	UClass* Z_Construct_UClass_UCameraAnimationSequenceFactoryNew_NoRegister()
	{
		return UCameraAnimationSequenceFactoryNew::StaticClass();
	}
	struct Z_Construct_UClass_UCameraAnimationSequenceFactoryNew_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UCameraAnimationSequenceFactoryNew_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_TemplateSequenceEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UCameraAnimationSequenceFactoryNew_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements a factory for UCameraAnimationSequence objects.\n */" },
		{ "HideCategories", "Object" },
		{ "IncludePath", "Factories/CameraAnimationSequenceFactoryNew.h" },
		{ "ModuleRelativePath", "Private/Factories/CameraAnimationSequenceFactoryNew.h" },
		{ "ToolTip", "Implements a factory for UCameraAnimationSequence objects." },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UCameraAnimationSequenceFactoryNew_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UCameraAnimationSequenceFactoryNew>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UCameraAnimationSequenceFactoryNew_Statics::ClassParams = {
		&UCameraAnimationSequenceFactoryNew::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UCameraAnimationSequenceFactoryNew_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UCameraAnimationSequenceFactoryNew_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UCameraAnimationSequenceFactoryNew()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UCameraAnimationSequenceFactoryNew_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UCameraAnimationSequenceFactoryNew, 3069096917);
	template<> TEMPLATESEQUENCEEDITOR_API UClass* StaticClass<UCameraAnimationSequenceFactoryNew>()
	{
		return UCameraAnimationSequenceFactoryNew::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UCameraAnimationSequenceFactoryNew(Z_Construct_UClass_UCameraAnimationSequenceFactoryNew, &UCameraAnimationSequenceFactoryNew::StaticClass, TEXT("/Script/TemplateSequenceEditor"), TEXT("UCameraAnimationSequenceFactoryNew"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UCameraAnimationSequenceFactoryNew);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
