// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TemplateSequenceEditor/Private/Misc/TemplateSequenceEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTemplateSequenceEditorSettings() {}
// Cross Module References
	TEMPLATESEQUENCEEDITOR_API UClass* Z_Construct_UClass_UTemplateSequenceEditorSettings_NoRegister();
	TEMPLATESEQUENCEEDITOR_API UClass* Z_Construct_UClass_UTemplateSequenceEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_TemplateSequenceEditor();
// End Cross Module References
	void UTemplateSequenceEditorSettings::StaticRegisterNativesUTemplateSequenceEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UTemplateSequenceEditorSettings_NoRegister()
	{
		return UTemplateSequenceEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowOutdatedAssetsInCameraAnimationTrackEditor_MetaData[];
#endif
		static void NewProp_bShowOutdatedAssetsInCameraAnimationTrackEditor_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowOutdatedAssetsInCameraAnimationTrackEditor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_TemplateSequenceEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Template Sequence Editor settings.\n */" },
		{ "IncludePath", "Misc/TemplateSequenceEditorSettings.h" },
		{ "ModuleRelativePath", "Private/Misc/TemplateSequenceEditorSettings.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Template Sequence Editor settings." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::NewProp_bShowOutdatedAssetsInCameraAnimationTrackEditor_MetaData[] = {
		{ "Comment", "/** Whether to show \"outdated assets\" by default in the camera animation track's asset picker. */" },
		{ "ModuleRelativePath", "Private/Misc/TemplateSequenceEditorSettings.h" },
		{ "ToolTip", "Whether to show \"outdated assets\" by default in the camera animation track's asset picker." },
	};
#endif
	void Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::NewProp_bShowOutdatedAssetsInCameraAnimationTrackEditor_SetBit(void* Obj)
	{
		((UTemplateSequenceEditorSettings*)Obj)->bShowOutdatedAssetsInCameraAnimationTrackEditor = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::NewProp_bShowOutdatedAssetsInCameraAnimationTrackEditor = { "bShowOutdatedAssetsInCameraAnimationTrackEditor", nullptr, (EPropertyFlags)0x0010000000004000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTemplateSequenceEditorSettings), &Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::NewProp_bShowOutdatedAssetsInCameraAnimationTrackEditor_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::NewProp_bShowOutdatedAssetsInCameraAnimationTrackEditor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::NewProp_bShowOutdatedAssetsInCameraAnimationTrackEditor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::NewProp_bShowOutdatedAssetsInCameraAnimationTrackEditor,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTemplateSequenceEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::ClassParams = {
		&UTemplateSequenceEditorSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTemplateSequenceEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTemplateSequenceEditorSettings, 3096764712);
	template<> TEMPLATESEQUENCEEDITOR_API UClass* StaticClass<UTemplateSequenceEditorSettings>()
	{
		return UTemplateSequenceEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTemplateSequenceEditorSettings(Z_Construct_UClass_UTemplateSequenceEditorSettings, &UTemplateSequenceEditorSettings::StaticClass, TEXT("/Script/TemplateSequenceEditor"), TEXT("UTemplateSequenceEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTemplateSequenceEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
