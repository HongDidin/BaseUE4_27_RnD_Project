// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef TEMPLATESEQUENCEEDITOR_TemplateSequenceEditorSettings_generated_h
#error "TemplateSequenceEditorSettings.generated.h already included, missing '#pragma once' in TemplateSequenceEditorSettings.h"
#endif
#define TEMPLATESEQUENCEEDITOR_TemplateSequenceEditorSettings_generated_h

#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_SPARSE_DATA
#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_RPC_WRAPPERS
#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTemplateSequenceEditorSettings(); \
	friend struct Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UTemplateSequenceEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TemplateSequenceEditor"), NO_API) \
	DECLARE_SERIALIZER(UTemplateSequenceEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUTemplateSequenceEditorSettings(); \
	friend struct Z_Construct_UClass_UTemplateSequenceEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UTemplateSequenceEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TemplateSequenceEditor"), NO_API) \
	DECLARE_SERIALIZER(UTemplateSequenceEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTemplateSequenceEditorSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTemplateSequenceEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTemplateSequenceEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTemplateSequenceEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTemplateSequenceEditorSettings(UTemplateSequenceEditorSettings&&); \
	NO_API UTemplateSequenceEditorSettings(const UTemplateSequenceEditorSettings&); \
public:


#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTemplateSequenceEditorSettings(UTemplateSequenceEditorSettings&&); \
	NO_API UTemplateSequenceEditorSettings(const UTemplateSequenceEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTemplateSequenceEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTemplateSequenceEditorSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTemplateSequenceEditorSettings)


#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_16_PROLOG
#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_SPARSE_DATA \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_RPC_WRAPPERS \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_INCLASS \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_SPARSE_DATA \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TEMPLATESEQUENCEEDITOR_API UClass* StaticClass<class UTemplateSequenceEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MovieScene_TemplateSequence_Source_TemplateSequenceEditor_Private_Misc_TemplateSequenceEditorSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
