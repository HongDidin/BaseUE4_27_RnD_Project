// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshPaintEditorMode/Private/MeshPaintMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshPaintMode() {}
// Cross Module References
	MESHPAINTEDITORMODE_API UClass* Z_Construct_UClass_UMeshPaintMode_NoRegister();
	MESHPAINTEDITORMODE_API UClass* Z_Construct_UClass_UMeshPaintMode();
	UNREALED_API UClass* Z_Construct_UClass_UEdMode();
	UPackage* Z_Construct_UPackage__Script_MeshPaintEditorMode();
	MESHPAINTEDITORMODE_API UClass* Z_Construct_UClass_UMeshPaintModeSettings_NoRegister();
// End Cross Module References
	void UMeshPaintMode::StaticRegisterNativesUMeshPaintMode()
	{
	}
	UClass* Z_Construct_UClass_UMeshPaintMode_NoRegister()
	{
		return UMeshPaintMode::StaticClass();
	}
	struct Z_Construct_UClass_UMeshPaintMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ModeSettings_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ModeSettings;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshPaintMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdMode,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshPaintMode_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Mesh paint Mode.  Extends editor viewports with the ability to paint data on meshes\n */" },
		{ "IncludePath", "MeshPaintMode.h" },
		{ "ModuleRelativePath", "Private/MeshPaintMode.h" },
		{ "ToolTip", "Mesh paint Mode.  Extends editor viewports with the ability to paint data on meshes" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshPaintMode_Statics::NewProp_ModeSettings_MetaData[] = {
		{ "ModuleRelativePath", "Private/MeshPaintMode.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshPaintMode_Statics::NewProp_ModeSettings = { "ModeSettings", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshPaintMode, ModeSettings), Z_Construct_UClass_UMeshPaintModeSettings_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshPaintMode_Statics::NewProp_ModeSettings_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshPaintMode_Statics::NewProp_ModeSettings_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshPaintMode_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshPaintMode_Statics::NewProp_ModeSettings,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshPaintMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshPaintMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshPaintMode_Statics::ClassParams = {
		&UMeshPaintMode::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshPaintMode_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshPaintMode_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshPaintMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshPaintMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshPaintMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshPaintMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshPaintMode, 2849124226);
	template<> MESHPAINTEDITORMODE_API UClass* StaticClass<UMeshPaintMode>()
	{
		return UMeshPaintMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshPaintMode(Z_Construct_UClass_UMeshPaintMode, &UMeshPaintMode::StaticClass, TEXT("/Script/MeshPaintEditorMode"), TEXT("UMeshPaintMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshPaintMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
