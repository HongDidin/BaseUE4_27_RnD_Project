// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshPaintingToolset/Public/MeshPaintingToolsetTypes.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshPaintingToolsetTypes() {}
// Cross Module References
	MESHPAINTINGTOOLSET_API UEnum* Z_Construct_UEnum_MeshPaintingToolset_EMeshVertexPaintModeTarget();
	UPackage* Z_Construct_UPackage__Script_MeshPaintingToolset();
	MESHPAINTINGTOOLSET_API UEnum* Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintDataColorViewMode();
	MESHPAINTINGTOOLSET_API UScriptStruct* Z_Construct_UScriptStruct_FPaintTexture2DData();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
// End Cross Module References
	static UEnum* EMeshVertexPaintModeTarget_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshPaintingToolset_EMeshVertexPaintModeTarget, Z_Construct_UPackage__Script_MeshPaintingToolset(), TEXT("EMeshVertexPaintModeTarget"));
		}
		return Singleton;
	}
	template<> MESHPAINTINGTOOLSET_API UEnum* StaticEnum<EMeshVertexPaintModeTarget>()
	{
		return EMeshVertexPaintModeTarget_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshVertexPaintModeTarget(EMeshVertexPaintModeTarget_StaticEnum, TEXT("/Script/MeshPaintingToolset"), TEXT("EMeshVertexPaintModeTarget"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshPaintingToolset_EMeshVertexPaintModeTarget_Hash() { return 1436267741U; }
	UEnum* Z_Construct_UEnum_MeshPaintingToolset_EMeshVertexPaintModeTarget()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshPaintingToolset();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshVertexPaintModeTarget"), 0, Get_Z_Construct_UEnum_MeshPaintingToolset_EMeshVertexPaintModeTarget_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshVertexPaintModeTarget::ComponentInstance", (int64)EMeshVertexPaintModeTarget::ComponentInstance },
				{ "EMeshVertexPaintModeTarget::Mesh", (int64)EMeshVertexPaintModeTarget::Mesh },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Comment", "/** Vertex paint target */" },
				{ "ComponentInstance.Comment", "/** Paint the static mesh component instance in the level */" },
				{ "ComponentInstance.Name", "EMeshVertexPaintModeTarget::ComponentInstance" },
				{ "ComponentInstance.ToolTip", "Paint the static mesh component instance in the level" },
				{ "Mesh.Comment", "/** Paint the actual static mesh asset */" },
				{ "Mesh.Name", "EMeshVertexPaintModeTarget::Mesh" },
				{ "Mesh.ToolTip", "Paint the actual static mesh asset" },
				{ "ModuleRelativePath", "Public/MeshPaintingToolsetTypes.h" },
				{ "ToolTip", "Vertex paint target" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
				nullptr,
				"EMeshVertexPaintModeTarget",
				"EMeshVertexPaintModeTarget",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMeshPaintDataColorViewMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintDataColorViewMode, Z_Construct_UPackage__Script_MeshPaintingToolset(), TEXT("EMeshPaintDataColorViewMode"));
		}
		return Singleton;
	}
	template<> MESHPAINTINGTOOLSET_API UEnum* StaticEnum<EMeshPaintDataColorViewMode>()
	{
		return EMeshPaintDataColorViewMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshPaintDataColorViewMode(EMeshPaintDataColorViewMode_StaticEnum, TEXT("/Script/MeshPaintingToolset"), TEXT("EMeshPaintDataColorViewMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintDataColorViewMode_Hash() { return 1910602388U; }
	UEnum* Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintDataColorViewMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshPaintingToolset();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshPaintDataColorViewMode"), 0, Get_Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintDataColorViewMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshPaintDataColorViewMode::Normal", (int64)EMeshPaintDataColorViewMode::Normal },
				{ "EMeshPaintDataColorViewMode::RGB", (int64)EMeshPaintDataColorViewMode::RGB },
				{ "EMeshPaintDataColorViewMode::Alpha", (int64)EMeshPaintDataColorViewMode::Alpha },
				{ "EMeshPaintDataColorViewMode::Red", (int64)EMeshPaintDataColorViewMode::Red },
				{ "EMeshPaintDataColorViewMode::Green", (int64)EMeshPaintDataColorViewMode::Green },
				{ "EMeshPaintDataColorViewMode::Blue", (int64)EMeshPaintDataColorViewMode::Blue },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Alpha.Comment", "/** Alpha only */" },
				{ "Alpha.DisplayName", "Alpha Channel" },
				{ "Alpha.Name", "EMeshPaintDataColorViewMode::Alpha" },
				{ "Alpha.ToolTip", "Alpha only" },
				{ "Blue.Comment", "/** Blue only */" },
				{ "Blue.DisplayName", "Blue Channel" },
				{ "Blue.Name", "EMeshPaintDataColorViewMode::Blue" },
				{ "Blue.ToolTip", "Blue only" },
				{ "Comment", "/** Mesh paint color view modes (somewhat maps to EVertexColorViewMode engine enum.) */" },
				{ "Green.Comment", "/** Green only */" },
				{ "Green.DisplayName", "Green Channel" },
				{ "Green.Name", "EMeshPaintDataColorViewMode::Green" },
				{ "Green.ToolTip", "Green only" },
				{ "ModuleRelativePath", "Public/MeshPaintingToolsetTypes.h" },
				{ "Normal.Comment", "/** Normal view mode (vertex color visualization off) */" },
				{ "Normal.DisplayName", "Off" },
				{ "Normal.Name", "EMeshPaintDataColorViewMode::Normal" },
				{ "Normal.ToolTip", "Normal view mode (vertex color visualization off)" },
				{ "Red.Comment", "/** Red only */" },
				{ "Red.DisplayName", "Red Channel" },
				{ "Red.Name", "EMeshPaintDataColorViewMode::Red" },
				{ "Red.ToolTip", "Red only" },
				{ "RGB.Comment", "/** RGB only */" },
				{ "RGB.DisplayName", "RGB Channels" },
				{ "RGB.Name", "EMeshPaintDataColorViewMode::RGB" },
				{ "RGB.ToolTip", "RGB only" },
				{ "ToolTip", "Mesh paint color view modes (somewhat maps to EVertexColorViewMode engine enum.)" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
				nullptr,
				"EMeshPaintDataColorViewMode",
				"EMeshPaintDataColorViewMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
class UScriptStruct* FPaintTexture2DData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MESHPAINTINGTOOLSET_API uint32 Get_Z_Construct_UScriptStruct_FPaintTexture2DData_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FPaintTexture2DData, Z_Construct_UPackage__Script_MeshPaintingToolset(), TEXT("PaintTexture2DData"), sizeof(FPaintTexture2DData), Get_Z_Construct_UScriptStruct_FPaintTexture2DData_Hash());
	}
	return Singleton;
}
template<> MESHPAINTINGTOOLSET_API UScriptStruct* StaticStruct<FPaintTexture2DData>()
{
	return FPaintTexture2DData::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FPaintTexture2DData(FPaintTexture2DData::StaticStruct, TEXT("/Script/MeshPaintingToolset"), TEXT("PaintTexture2DData"), false, nullptr, nullptr);
static struct FScriptStruct_MeshPaintingToolset_StaticRegisterNativesFPaintTexture2DData
{
	FScriptStruct_MeshPaintingToolset_StaticRegisterNativesFPaintTexture2DData()
	{
		UScriptStruct::DeferCppStructOps<FPaintTexture2DData>(FName(TEXT("PaintTexture2DData")));
	}
} ScriptStruct_MeshPaintingToolset_StaticRegisterNativesFPaintTexture2DData;
	struct Z_Construct_UScriptStruct_FPaintTexture2DData_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PaintingTexture2D_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PaintingTexture2D;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PaintingTexture2DDuplicate_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PaintingTexture2DDuplicate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PaintRenderTargetTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PaintRenderTargetTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CloneRenderTargetTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CloneRenderTargetTexture;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PaintingMaterials_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PaintingMaterials_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_PaintingMaterials;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MeshPaintingToolsetTypes.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FPaintTexture2DData>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingTexture2D_MetaData[] = {
		{ "Comment", "/** The original texture that we're painting */" },
		{ "ModuleRelativePath", "Public/MeshPaintingToolsetTypes.h" },
		{ "ToolTip", "The original texture that we're painting" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingTexture2D = { "PaintingTexture2D", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPaintTexture2DData, PaintingTexture2D), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingTexture2D_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingTexture2D_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingTexture2DDuplicate_MetaData[] = {
		{ "Comment", "/** A copy of the original texture we're painting, used for restoration. */" },
		{ "ModuleRelativePath", "Public/MeshPaintingToolsetTypes.h" },
		{ "ToolTip", "A copy of the original texture we're painting, used for restoration." },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingTexture2DDuplicate = { "PaintingTexture2DDuplicate", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPaintTexture2DData, PaintingTexture2DDuplicate), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingTexture2DDuplicate_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingTexture2DDuplicate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintRenderTargetTexture_MetaData[] = {
		{ "Comment", "/** Render target texture for painting */" },
		{ "ModuleRelativePath", "Public/MeshPaintingToolsetTypes.h" },
		{ "ToolTip", "Render target texture for painting" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintRenderTargetTexture = { "PaintRenderTargetTexture", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPaintTexture2DData, PaintRenderTargetTexture), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintRenderTargetTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintRenderTargetTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_CloneRenderTargetTexture_MetaData[] = {
		{ "Comment", "/** Render target texture used as an input while painting that contains a clone of the original image */" },
		{ "ModuleRelativePath", "Public/MeshPaintingToolsetTypes.h" },
		{ "ToolTip", "Render target texture used as an input while painting that contains a clone of the original image" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_CloneRenderTargetTexture = { "CloneRenderTargetTexture", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPaintTexture2DData, CloneRenderTargetTexture), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_CloneRenderTargetTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_CloneRenderTargetTexture_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingMaterials_Inner = { "PaintingMaterials", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingMaterials_MetaData[] = {
		{ "Comment", "/** List of materials we are painting on */" },
		{ "ModuleRelativePath", "Public/MeshPaintingToolsetTypes.h" },
		{ "ToolTip", "List of materials we are painting on" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingMaterials = { "PaintingMaterials", nullptr, (EPropertyFlags)0x0010000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FPaintTexture2DData, PaintingMaterials), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingMaterials_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingMaterials_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingTexture2D,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingTexture2DDuplicate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintRenderTargetTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_CloneRenderTargetTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingMaterials_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::NewProp_PaintingMaterials,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
		nullptr,
		&NewStructOps,
		"PaintTexture2DData",
		sizeof(FPaintTexture2DData),
		alignof(FPaintTexture2DData),
		Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FPaintTexture2DData()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FPaintTexture2DData_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_MeshPaintingToolset();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("PaintTexture2DData"), sizeof(FPaintTexture2DData), Get_Z_Construct_UScriptStruct_FPaintTexture2DData_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FPaintTexture2DData_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FPaintTexture2DData_Hash() { return 1607997739U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
