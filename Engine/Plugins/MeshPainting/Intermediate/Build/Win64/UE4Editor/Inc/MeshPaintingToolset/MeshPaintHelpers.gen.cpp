// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshPaintingToolset/Public/MeshPaintHelpers.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshPaintHelpers() {}
// Cross Module References
	MESHPAINTINGTOOLSET_API UEnum* Z_Construct_UEnum_MeshPaintingToolset_ETexturePaintWeightIndex();
	UPackage* Z_Construct_UPackage__Script_MeshPaintingToolset();
	MESHPAINTINGTOOLSET_API UEnum* Z_Construct_UEnum_MeshPaintingToolset_ETexturePaintWeightTypes();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshPaintingToolset_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshPaintingToolset();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshToolsContext_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshToolsContext();
	EDITORINTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UEdModeInteractiveToolsContext();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshToolManager_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshToolManager();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolManager();
// End Cross Module References
	static UEnum* ETexturePaintWeightIndex_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshPaintingToolset_ETexturePaintWeightIndex, Z_Construct_UPackage__Script_MeshPaintingToolset(), TEXT("ETexturePaintWeightIndex"));
		}
		return Singleton;
	}
	template<> MESHPAINTINGTOOLSET_API UEnum* StaticEnum<ETexturePaintWeightIndex>()
	{
		return ETexturePaintWeightIndex_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETexturePaintWeightIndex(ETexturePaintWeightIndex_StaticEnum, TEXT("/Script/MeshPaintingToolset"), TEXT("ETexturePaintWeightIndex"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshPaintingToolset_ETexturePaintWeightIndex_Hash() { return 588650038U; }
	UEnum* Z_Construct_UEnum_MeshPaintingToolset_ETexturePaintWeightIndex()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshPaintingToolset();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETexturePaintWeightIndex"), 0, Get_Z_Construct_UEnum_MeshPaintingToolset_ETexturePaintWeightIndex_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETexturePaintWeightIndex::TextureOne", (int64)ETexturePaintWeightIndex::TextureOne },
				{ "ETexturePaintWeightIndex::TextureTwo", (int64)ETexturePaintWeightIndex::TextureTwo },
				{ "ETexturePaintWeightIndex::TextureThree", (int64)ETexturePaintWeightIndex::TextureThree },
				{ "ETexturePaintWeightIndex::TextureFour", (int64)ETexturePaintWeightIndex::TextureFour },
				{ "ETexturePaintWeightIndex::TextureFive", (int64)ETexturePaintWeightIndex::TextureFive },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/MeshPaintHelpers.h" },
				{ "TextureFive.Name", "ETexturePaintWeightIndex::TextureFive" },
				{ "TextureFour.Name", "ETexturePaintWeightIndex::TextureFour" },
				{ "TextureOne.Name", "ETexturePaintWeightIndex::TextureOne" },
				{ "TextureThree.Name", "ETexturePaintWeightIndex::TextureThree" },
				{ "TextureTwo.Name", "ETexturePaintWeightIndex::TextureTwo" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
				nullptr,
				"ETexturePaintWeightIndex",
				"ETexturePaintWeightIndex",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* ETexturePaintWeightTypes_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshPaintingToolset_ETexturePaintWeightTypes, Z_Construct_UPackage__Script_MeshPaintingToolset(), TEXT("ETexturePaintWeightTypes"));
		}
		return Singleton;
	}
	template<> MESHPAINTINGTOOLSET_API UEnum* StaticEnum<ETexturePaintWeightTypes>()
	{
		return ETexturePaintWeightTypes_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ETexturePaintWeightTypes(ETexturePaintWeightTypes_StaticEnum, TEXT("/Script/MeshPaintingToolset"), TEXT("ETexturePaintWeightTypes"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshPaintingToolset_ETexturePaintWeightTypes_Hash() { return 3113029349U; }
	UEnum* Z_Construct_UEnum_MeshPaintingToolset_ETexturePaintWeightTypes()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshPaintingToolset();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ETexturePaintWeightTypes"), 0, Get_Z_Construct_UEnum_MeshPaintingToolset_ETexturePaintWeightTypes_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ETexturePaintWeightTypes::AlphaLerp", (int64)ETexturePaintWeightTypes::AlphaLerp },
				{ "ETexturePaintWeightTypes::RGB", (int64)ETexturePaintWeightTypes::RGB },
				{ "ETexturePaintWeightTypes::ARGB", (int64)ETexturePaintWeightTypes::ARGB },
				{ "ETexturePaintWeightTypes::OneMinusARGB", (int64)ETexturePaintWeightTypes::OneMinusARGB },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AlphaLerp.Comment", "/** Lerp Between Two Textures using Alpha Value */" },
				{ "AlphaLerp.DisplayName", "Alpha (Two Textures)" },
				{ "AlphaLerp.Name", "ETexturePaintWeightTypes::AlphaLerp" },
				{ "AlphaLerp.ToolTip", "Lerp Between Two Textures using Alpha Value" },
				{ "ARGB.Comment", "/**  Weighting Four Textures according to Channels*/" },
				{ "ARGB.DisplayName", "ARGB (Four Textures)" },
				{ "ARGB.Name", "ETexturePaintWeightTypes::ARGB" },
				{ "ARGB.ToolTip", "Weighting Four Textures according to Channels" },
				{ "ModuleRelativePath", "Public/MeshPaintHelpers.h" },
				{ "OneMinusARGB.Comment", "/**  Weighting Five Textures according to Channels */" },
				{ "OneMinusARGB.DisplayName", "ARGB - 1 (Five Textures)" },
				{ "OneMinusARGB.Name", "ETexturePaintWeightTypes::OneMinusARGB" },
				{ "OneMinusARGB.ToolTip", "Weighting Five Textures according to Channels" },
				{ "RGB.Comment", "/** Weighting Three Textures according to Channels*/" },
				{ "RGB.DisplayName", "RGB (Three Textures)" },
				{ "RGB.Name", "ETexturePaintWeightTypes::RGB" },
				{ "RGB.ToolTip", "Weighting Three Textures according to Channels" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
				nullptr,
				"ETexturePaintWeightTypes",
				"ETexturePaintWeightTypes",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMeshPaintingToolset::StaticRegisterNativesUMeshPaintingToolset()
	{
	}
	UClass* Z_Construct_UClass_UMeshPaintingToolset_NoRegister()
	{
		return UMeshPaintingToolset::StaticClass();
	}
	struct Z_Construct_UClass_UMeshPaintingToolset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshPaintingToolset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshPaintingToolset_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshPaintHelpers.h" },
		{ "ModuleRelativePath", "Public/MeshPaintHelpers.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshPaintingToolset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshPaintingToolset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshPaintingToolset_Statics::ClassParams = {
		&UMeshPaintingToolset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshPaintingToolset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshPaintingToolset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshPaintingToolset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshPaintingToolset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshPaintingToolset, 1922755772);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshPaintingToolset>()
	{
		return UMeshPaintingToolset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshPaintingToolset(Z_Construct_UClass_UMeshPaintingToolset, &UMeshPaintingToolset::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshPaintingToolset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshPaintingToolset);
	void UMeshToolsContext::StaticRegisterNativesUMeshToolsContext()
	{
	}
	UClass* Z_Construct_UClass_UMeshToolsContext_NoRegister()
	{
		return UMeshToolsContext::StaticClass();
	}
	struct Z_Construct_UClass_UMeshToolsContext_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshToolsContext_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UEdModeInteractiveToolsContext,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshToolsContext_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshPaintHelpers.h" },
		{ "ModuleRelativePath", "Public/MeshPaintHelpers.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshToolsContext_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshToolsContext>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshToolsContext_Statics::ClassParams = {
		&UMeshToolsContext::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshToolsContext_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshToolsContext_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshToolsContext()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshToolsContext_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshToolsContext, 1566813989);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshToolsContext>()
	{
		return UMeshToolsContext::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshToolsContext(Z_Construct_UClass_UMeshToolsContext, &UMeshToolsContext::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshToolsContext"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshToolsContext);
	void UMeshToolManager::StaticRegisterNativesUMeshToolManager()
	{
	}
	UClass* Z_Construct_UClass_UMeshToolManager_NoRegister()
	{
		return UMeshToolManager::StaticClass();
	}
	struct Z_Construct_UClass_UMeshToolManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshToolManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolManager,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshToolManager_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshPaintHelpers.h" },
		{ "ModuleRelativePath", "Public/MeshPaintHelpers.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshToolManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshToolManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshToolManager_Statics::ClassParams = {
		&UMeshToolManager::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshToolManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshToolManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshToolManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshToolManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshToolManager, 2219794578);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshToolManager>()
	{
		return UMeshToolManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshToolManager(Z_Construct_UClass_UMeshToolManager, &UMeshToolManager::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshToolManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshToolManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
