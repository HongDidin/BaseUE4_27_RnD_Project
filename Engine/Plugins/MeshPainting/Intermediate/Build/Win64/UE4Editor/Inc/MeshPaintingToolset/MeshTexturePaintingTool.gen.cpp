// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshPaintingToolset/Public/MeshTexturePaintingTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshTexturePaintingTool() {}
// Cross Module References
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshTexturePaintingToolBuilder_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshTexturePaintingToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshPaintingToolset();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshTexturePaintingToolProperties_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshTexturePaintingToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UBrushBaseProperties();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshTexturePaintingTool_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshTexturePaintingTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UBaseBrushTool();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	MESHPAINTINGTOOLSET_API UScriptStruct* Z_Construct_UScriptStruct_FPaintTexture2DData();
	ENGINE_API UClass* Z_Construct_UClass_UMeshComponent_NoRegister();
// End Cross Module References
	void UMeshTexturePaintingToolBuilder::StaticRegisterNativesUMeshTexturePaintingToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UMeshTexturePaintingToolBuilder_NoRegister()
	{
		return UMeshTexturePaintingToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UMeshTexturePaintingToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshTexturePaintingToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "MeshTexturePaintingTool.h" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshTexturePaintingToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshTexturePaintingToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshTexturePaintingToolBuilder_Statics::ClassParams = {
		&UMeshTexturePaintingToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshTexturePaintingToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshTexturePaintingToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshTexturePaintingToolBuilder, 2020595573);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshTexturePaintingToolBuilder>()
	{
		return UMeshTexturePaintingToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshTexturePaintingToolBuilder(Z_Construct_UClass_UMeshTexturePaintingToolBuilder, &UMeshTexturePaintingToolBuilder::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshTexturePaintingToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshTexturePaintingToolBuilder);
	void UMeshTexturePaintingToolProperties::StaticRegisterNativesUMeshTexturePaintingToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UMeshTexturePaintingToolProperties_NoRegister()
	{
		return UMeshTexturePaintingToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PaintColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PaintColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EraseColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EraseColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWriteRed_MetaData[];
#endif
		static void NewProp_bWriteRed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWriteRed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWriteGreen_MetaData[];
#endif
		static void NewProp_bWriteGreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWriteGreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWriteBlue_MetaData[];
#endif
		static void NewProp_bWriteBlue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWriteBlue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWriteAlpha_MetaData[];
#endif
		static void NewProp_bWriteAlpha_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWriteAlpha;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_UVChannel_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_UVChannel;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableSeamPainting_MetaData[];
#endif
		static void NewProp_bEnableSeamPainting_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableSeamPainting;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PaintTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PaintTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableFlow_MetaData[];
#endif
		static void NewProp_bEnableFlow_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableFlow;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlyFrontFacingTriangles_MetaData[];
#endif
		static void NewProp_bOnlyFrontFacingTriangles_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlyFrontFacingTriangles;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBrushBaseProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshTexturePaintingTool.h" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_PaintColor_MetaData[] = {
		{ "Category", "TexturePainting" },
		{ "Comment", "/** Color used for Applying Texture Color Painting */" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "Color used for Applying Texture Color Painting" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_PaintColor = { "PaintColor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTexturePaintingToolProperties, PaintColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_PaintColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_PaintColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_EraseColor_MetaData[] = {
		{ "Category", "TexturePainting" },
		{ "Comment", "/** Color used for Erasing Texture Color Painting */" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "Color used for Erasing Texture Color Painting" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_EraseColor = { "EraseColor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTexturePaintingToolProperties, EraseColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_EraseColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_EraseColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteRed_MetaData[] = {
		{ "Category", "TexturePainting" },
		{ "Comment", "/** Whether or not to apply Texture Color Painting to the Red Channel */" },
		{ "DisplayName", "Red" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "Whether or not to apply Texture Color Painting to the Red Channel" },
	};
#endif
	void Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteRed_SetBit(void* Obj)
	{
		((UMeshTexturePaintingToolProperties*)Obj)->bWriteRed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteRed = { "bWriteRed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshTexturePaintingToolProperties), &Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteRed_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteRed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteRed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteGreen_MetaData[] = {
		{ "Category", "TexturePainting" },
		{ "Comment", "/** Whether or not to apply Texture Color Painting to the Green Channel */" },
		{ "DisplayName", "Green" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "Whether or not to apply Texture Color Painting to the Green Channel" },
	};
#endif
	void Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteGreen_SetBit(void* Obj)
	{
		((UMeshTexturePaintingToolProperties*)Obj)->bWriteGreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteGreen = { "bWriteGreen", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshTexturePaintingToolProperties), &Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteGreen_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteGreen_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteGreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteBlue_MetaData[] = {
		{ "Category", "TexturePainting" },
		{ "Comment", "/** Whether or not to apply Texture Color Painting to the Blue Channel */" },
		{ "DisplayName", "Blue" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "Whether or not to apply Texture Color Painting to the Blue Channel" },
	};
#endif
	void Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteBlue_SetBit(void* Obj)
	{
		((UMeshTexturePaintingToolProperties*)Obj)->bWriteBlue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteBlue = { "bWriteBlue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshTexturePaintingToolProperties), &Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteBlue_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteBlue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteBlue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteAlpha_MetaData[] = {
		{ "Category", "TexturePainting" },
		{ "Comment", "/** Whether or not to apply Texture Color Painting to the Alpha Channel */" },
		{ "DisplayName", "Alpha" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "Whether or not to apply Texture Color Painting to the Alpha Channel" },
	};
#endif
	void Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteAlpha_SetBit(void* Obj)
	{
		((UMeshTexturePaintingToolProperties*)Obj)->bWriteAlpha = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteAlpha = { "bWriteAlpha", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshTexturePaintingToolProperties), &Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteAlpha_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteAlpha_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteAlpha_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_UVChannel_MetaData[] = {
		{ "Category", "TexturePainting" },
		{ "Comment", "/** UV channel which should be used for paint textures */" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "UV channel which should be used for paint textures" },
		{ "TransientToolProperty", "" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_UVChannel = { "UVChannel", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTexturePaintingToolProperties, UVChannel), METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_UVChannel_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_UVChannel_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bEnableSeamPainting_MetaData[] = {
		{ "Category", "TexturePainting" },
		{ "Comment", "/** Seam painting flag, True if we should enable dilation to allow the painting of texture seams */" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "Seam painting flag, True if we should enable dilation to allow the painting of texture seams" },
	};
#endif
	void Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bEnableSeamPainting_SetBit(void* Obj)
	{
		((UMeshTexturePaintingToolProperties*)Obj)->bEnableSeamPainting = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bEnableSeamPainting = { "bEnableSeamPainting", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshTexturePaintingToolProperties), &Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bEnableSeamPainting_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bEnableSeamPainting_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bEnableSeamPainting_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_PaintTexture_MetaData[] = {
		{ "Category", "TexturePainting" },
		{ "Comment", "/** Texture to which Painting should be Applied */" },
		{ "DisplayThumbnail", "true" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "Texture to which Painting should be Applied" },
		{ "TransientToolProperty", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_PaintTexture = { "PaintTexture", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTexturePaintingToolProperties, PaintTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_PaintTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_PaintTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bEnableFlow_MetaData[] = {
		{ "Category", "Brush" },
		{ "Comment", "/** Enables \"Flow\" painting where paint is continually applied from the brush every tick */" },
		{ "DisplayName", "Enable Brush Flow" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "Enables \"Flow\" painting where paint is continually applied from the brush every tick" },
	};
#endif
	void Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bEnableFlow_SetBit(void* Obj)
	{
		((UMeshTexturePaintingToolProperties*)Obj)->bEnableFlow = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bEnableFlow = { "bEnableFlow", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshTexturePaintingToolProperties), &Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bEnableFlow_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bEnableFlow_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bEnableFlow_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bOnlyFrontFacingTriangles_MetaData[] = {
		{ "Category", "Brush" },
		{ "Comment", "/** Whether back-facing triangles should be ignored */" },
		{ "DisplayName", "Ignore Back-Facing" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "Whether back-facing triangles should be ignored" },
	};
#endif
	void Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bOnlyFrontFacingTriangles_SetBit(void* Obj)
	{
		((UMeshTexturePaintingToolProperties*)Obj)->bOnlyFrontFacingTriangles = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bOnlyFrontFacingTriangles = { "bOnlyFrontFacingTriangles", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshTexturePaintingToolProperties), &Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bOnlyFrontFacingTriangles_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bOnlyFrontFacingTriangles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bOnlyFrontFacingTriangles_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_PaintColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_EraseColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteRed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteGreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteBlue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bWriteAlpha,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_UVChannel,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bEnableSeamPainting,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_PaintTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bEnableFlow,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::NewProp_bOnlyFrontFacingTriangles,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshTexturePaintingToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::ClassParams = {
		&UMeshTexturePaintingToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshTexturePaintingToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshTexturePaintingToolProperties, 3446761810);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshTexturePaintingToolProperties>()
	{
		return UMeshTexturePaintingToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshTexturePaintingToolProperties(Z_Construct_UClass_UMeshTexturePaintingToolProperties, &UMeshTexturePaintingToolProperties::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshTexturePaintingToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshTexturePaintingToolProperties);
	void UMeshTexturePaintingTool::StaticRegisterNativesUMeshTexturePaintingTool()
	{
	}
	UClass* Z_Construct_UClass_UMeshTexturePaintingTool_NoRegister()
	{
		return UMeshTexturePaintingTool::StaticClass();
	}
	struct Z_Construct_UClass_UMeshTexturePaintingTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TextureProperties;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Textures_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Textures_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Textures;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushRenderTargetTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrushRenderTargetTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BrushMaskRenderTargetTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_BrushMaskRenderTargetTexture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SeamMaskRenderTargetTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SeamMaskRenderTargetTexture;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PaintTargetData_ValueProp;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PaintTargetData_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PaintTargetData_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_PaintTargetData;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TexturePaintingCurrentMeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TexturePaintingCurrentMeshComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PaintingTexture2D_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PaintingTexture2D;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshTexturePaintingTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseBrushTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshTexturePaintingTool.h" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_TextureProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_TextureProperties = { "TextureProperties", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTexturePaintingTool, TextureProperties), Z_Construct_UClass_UMeshTexturePaintingToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_TextureProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_TextureProperties_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_Textures_Inner = { "Textures", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_Textures_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "NativeConstTemplateArg", "" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_Textures = { "Textures", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTexturePaintingTool, Textures), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_Textures_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_Textures_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_BrushRenderTargetTexture_MetaData[] = {
		{ "Comment", "/** Temporary render target used to draw incremental paint to */" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "Temporary render target used to draw incremental paint to" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_BrushRenderTargetTexture = { "BrushRenderTargetTexture", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTexturePaintingTool, BrushRenderTargetTexture), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_BrushRenderTargetTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_BrushRenderTargetTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_BrushMaskRenderTargetTexture_MetaData[] = {
		{ "Comment", "/** Temporary render target used to store a mask of the affected paint region, updated every time we add incremental texture paint */" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "Temporary render target used to store a mask of the affected paint region, updated every time we add incremental texture paint" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_BrushMaskRenderTargetTexture = { "BrushMaskRenderTargetTexture", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTexturePaintingTool, BrushMaskRenderTargetTexture), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_BrushMaskRenderTargetTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_BrushMaskRenderTargetTexture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_SeamMaskRenderTargetTexture_MetaData[] = {
		{ "Comment", "/** Temporary render target used to store generated mask for texture seams, we create this by projecting object triangles into texture space using the selected UV channel */" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "Temporary render target used to store generated mask for texture seams, we create this by projecting object triangles into texture space using the selected UV channel" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_SeamMaskRenderTargetTexture = { "SeamMaskRenderTargetTexture", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTexturePaintingTool, SeamMaskRenderTargetTexture), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_SeamMaskRenderTargetTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_SeamMaskRenderTargetTexture_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_PaintTargetData_ValueProp = { "PaintTargetData", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_FPaintTexture2DData, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_PaintTargetData_Key_KeyProp = { "PaintTargetData_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_PaintTargetData_MetaData[] = {
		{ "Comment", "/** Stores data associated with our paint target textures */" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "Stores data associated with our paint target textures" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_PaintTargetData = { "PaintTargetData", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTexturePaintingTool, PaintTargetData), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_PaintTargetData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_PaintTargetData_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_TexturePaintingCurrentMeshComponent_MetaData[] = {
		{ "Comment", "/** Texture paint: The mesh components that we're currently painting */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "Texture paint: The mesh components that we're currently painting" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_TexturePaintingCurrentMeshComponent = { "TexturePaintingCurrentMeshComponent", nullptr, (EPropertyFlags)0x0040000000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTexturePaintingTool, TexturePaintingCurrentMeshComponent), Z_Construct_UClass_UMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_TexturePaintingCurrentMeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_TexturePaintingCurrentMeshComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_PaintingTexture2D_MetaData[] = {
		{ "Comment", "/** The original texture that we're painting */" },
		{ "ModuleRelativePath", "Public/MeshTexturePaintingTool.h" },
		{ "ToolTip", "The original texture that we're painting" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_PaintingTexture2D = { "PaintingTexture2D", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshTexturePaintingTool, PaintingTexture2D), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_PaintingTexture2D_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_PaintingTexture2D_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshTexturePaintingTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_TextureProperties,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_Textures_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_Textures,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_BrushRenderTargetTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_BrushMaskRenderTargetTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_SeamMaskRenderTargetTexture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_PaintTargetData_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_PaintTargetData_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_PaintTargetData,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_TexturePaintingCurrentMeshComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshTexturePaintingTool_Statics::NewProp_PaintingTexture2D,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshTexturePaintingTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshTexturePaintingTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshTexturePaintingTool_Statics::ClassParams = {
		&UMeshTexturePaintingTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshTexturePaintingTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshTexturePaintingTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshTexturePaintingTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshTexturePaintingTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshTexturePaintingTool, 3146839802);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshTexturePaintingTool>()
	{
		return UMeshTexturePaintingTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshTexturePaintingTool(Z_Construct_UClass_UMeshTexturePaintingTool, &UMeshTexturePaintingTool::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshTexturePaintingTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshTexturePaintingTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
