// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHPAINTINGTOOLSET_MeshSelect_generated_h
#error "MeshSelect.generated.h already included, missing '#pragma once' in MeshSelect.h"
#endif
#define MESHPAINTINGTOOLSET_MeshSelect_generated_h

#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVertexAdapterClickToolBuilder(); \
	friend struct Z_Construct_UClass_UVertexAdapterClickToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UVertexAdapterClickToolBuilder, USingleClickToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UVertexAdapterClickToolBuilder)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_INCLASS \
private: \
	static void StaticRegisterNativesUVertexAdapterClickToolBuilder(); \
	friend struct Z_Construct_UClass_UVertexAdapterClickToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UVertexAdapterClickToolBuilder, USingleClickToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UVertexAdapterClickToolBuilder)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVertexAdapterClickToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVertexAdapterClickToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVertexAdapterClickToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVertexAdapterClickToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVertexAdapterClickToolBuilder(UVertexAdapterClickToolBuilder&&); \
	NO_API UVertexAdapterClickToolBuilder(const UVertexAdapterClickToolBuilder&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVertexAdapterClickToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVertexAdapterClickToolBuilder(UVertexAdapterClickToolBuilder&&); \
	NO_API UVertexAdapterClickToolBuilder(const UVertexAdapterClickToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVertexAdapterClickToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVertexAdapterClickToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVertexAdapterClickToolBuilder)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_18_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_INCLASS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_21_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<class UVertexAdapterClickToolBuilder>();

#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTextureAdapterClickToolBuilder(); \
	friend struct Z_Construct_UClass_UTextureAdapterClickToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UTextureAdapterClickToolBuilder, USingleClickToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UTextureAdapterClickToolBuilder)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_INCLASS \
private: \
	static void StaticRegisterNativesUTextureAdapterClickToolBuilder(); \
	friend struct Z_Construct_UClass_UTextureAdapterClickToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UTextureAdapterClickToolBuilder, USingleClickToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UTextureAdapterClickToolBuilder)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTextureAdapterClickToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTextureAdapterClickToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTextureAdapterClickToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTextureAdapterClickToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTextureAdapterClickToolBuilder(UTextureAdapterClickToolBuilder&&); \
	NO_API UTextureAdapterClickToolBuilder(const UTextureAdapterClickToolBuilder&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTextureAdapterClickToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTextureAdapterClickToolBuilder(UTextureAdapterClickToolBuilder&&); \
	NO_API UTextureAdapterClickToolBuilder(const UTextureAdapterClickToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTextureAdapterClickToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTextureAdapterClickToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTextureAdapterClickToolBuilder)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_32_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_INCLASS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_35_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<class UTextureAdapterClickToolBuilder>();

#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshClickTool(); \
	friend struct Z_Construct_UClass_UMeshClickTool_Statics; \
public: \
	DECLARE_CLASS(UMeshClickTool, USingleClickTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshClickTool) \
	virtual UObject* _getUObject() const override { return const_cast<UMeshClickTool*>(this); }


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_INCLASS \
private: \
	static void StaticRegisterNativesUMeshClickTool(); \
	friend struct Z_Construct_UClass_UMeshClickTool_Statics; \
public: \
	DECLARE_CLASS(UMeshClickTool, USingleClickTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshClickTool) \
	virtual UObject* _getUObject() const override { return const_cast<UMeshClickTool*>(this); }


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshClickTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshClickTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshClickTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshClickTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshClickTool(UMeshClickTool&&); \
	NO_API UMeshClickTool(const UMeshClickTool&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshClickTool(UMeshClickTool&&); \
	NO_API UMeshClickTool(const UMeshClickTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshClickTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshClickTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshClickTool)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SelectionMechanic() { return STRUCT_OFFSET(UMeshClickTool, SelectionMechanic); }


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_56_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_INCLASS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_59_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<class UMeshClickTool>();

#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUVertexAdapterClickTool(); \
	friend struct Z_Construct_UClass_UVertexAdapterClickTool_Statics; \
public: \
	DECLARE_CLASS(UVertexAdapterClickTool, UMeshClickTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UVertexAdapterClickTool)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_INCLASS \
private: \
	static void StaticRegisterNativesUVertexAdapterClickTool(); \
	friend struct Z_Construct_UClass_UVertexAdapterClickTool_Statics; \
public: \
	DECLARE_CLASS(UVertexAdapterClickTool, UMeshClickTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UVertexAdapterClickTool)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UVertexAdapterClickTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UVertexAdapterClickTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVertexAdapterClickTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVertexAdapterClickTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVertexAdapterClickTool(UVertexAdapterClickTool&&); \
	NO_API UVertexAdapterClickTool(const UVertexAdapterClickTool&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UVertexAdapterClickTool(UVertexAdapterClickTool&&); \
	NO_API UVertexAdapterClickTool(const UVertexAdapterClickTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UVertexAdapterClickTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UVertexAdapterClickTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UVertexAdapterClickTool)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_87_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_INCLASS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_90_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<class UVertexAdapterClickTool>();

#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTextureAdapterClickTool(); \
	friend struct Z_Construct_UClass_UTextureAdapterClickTool_Statics; \
public: \
	DECLARE_CLASS(UTextureAdapterClickTool, UMeshClickTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UTextureAdapterClickTool)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_INCLASS \
private: \
	static void StaticRegisterNativesUTextureAdapterClickTool(); \
	friend struct Z_Construct_UClass_UTextureAdapterClickTool_Statics; \
public: \
	DECLARE_CLASS(UTextureAdapterClickTool, UMeshClickTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UTextureAdapterClickTool)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTextureAdapterClickTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTextureAdapterClickTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTextureAdapterClickTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTextureAdapterClickTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTextureAdapterClickTool(UTextureAdapterClickTool&&); \
	NO_API UTextureAdapterClickTool(const UTextureAdapterClickTool&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTextureAdapterClickTool(UTextureAdapterClickTool&&); \
	NO_API UTextureAdapterClickTool(const UTextureAdapterClickTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTextureAdapterClickTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTextureAdapterClickTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UTextureAdapterClickTool)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_100_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_INCLASS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h_103_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<class UTextureAdapterClickTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshSelect_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
