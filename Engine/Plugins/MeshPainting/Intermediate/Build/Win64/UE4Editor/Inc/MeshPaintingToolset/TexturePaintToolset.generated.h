// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHPAINTINGTOOLSET_TexturePaintToolset_generated_h
#error "TexturePaintToolset.generated.h already included, missing '#pragma once' in TexturePaintToolset.h"
#endif
#define MESHPAINTINGTOOLSET_TexturePaintToolset_generated_h

#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTexturePaintToolset(); \
	friend struct Z_Construct_UClass_UTexturePaintToolset_Statics; \
public: \
	DECLARE_CLASS(UTexturePaintToolset, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UTexturePaintToolset)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_INCLASS \
private: \
	static void StaticRegisterNativesUTexturePaintToolset(); \
	friend struct Z_Construct_UClass_UTexturePaintToolset_Statics; \
public: \
	DECLARE_CLASS(UTexturePaintToolset, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UTexturePaintToolset)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTexturePaintToolset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTexturePaintToolset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTexturePaintToolset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTexturePaintToolset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTexturePaintToolset(UTexturePaintToolset&&); \
	NO_API UTexturePaintToolset(const UTexturePaintToolset&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTexturePaintToolset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTexturePaintToolset(UTexturePaintToolset&&); \
	NO_API UTexturePaintToolset(const UTexturePaintToolset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTexturePaintToolset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTexturePaintToolset); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTexturePaintToolset)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_61_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_INCLASS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h_64_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<class UTexturePaintToolset>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_TexturePaintToolset_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
