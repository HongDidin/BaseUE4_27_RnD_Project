// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshPaintEditorMode/Private/MeshPaintModeSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshPaintModeSettings() {}
// Cross Module References
	MESHPAINTEDITORMODE_API UEnum* Z_Construct_UEnum_MeshPaintEditorMode_EMeshPaintColorView();
	UPackage* Z_Construct_UPackage__Script_MeshPaintEditorMode();
	MESHPAINTEDITORMODE_API UClass* Z_Construct_UClass_UMeshPaintModeSettings_NoRegister();
	MESHPAINTEDITORMODE_API UClass* Z_Construct_UClass_UMeshPaintModeSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	MESHPAINTINGTOOLSET_API UEnum* Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintDataColorViewMode();
// End Cross Module References
	static UEnum* EMeshPaintColorView_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshPaintEditorMode_EMeshPaintColorView, Z_Construct_UPackage__Script_MeshPaintEditorMode(), TEXT("EMeshPaintColorView"));
		}
		return Singleton;
	}
	template<> MESHPAINTEDITORMODE_API UEnum* StaticEnum<EMeshPaintColorView>()
	{
		return EMeshPaintColorView_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshPaintColorView(EMeshPaintColorView_StaticEnum, TEXT("/Script/MeshPaintEditorMode"), TEXT("EMeshPaintColorView"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshPaintEditorMode_EMeshPaintColorView_Hash() { return 3102753462U; }
	UEnum* Z_Construct_UEnum_MeshPaintEditorMode_EMeshPaintColorView()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshPaintEditorMode();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshPaintColorView"), 0, Get_Z_Construct_UEnum_MeshPaintEditorMode_EMeshPaintColorView_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshPaintColorView::Normal", (int64)EMeshPaintColorView::Normal },
				{ "EMeshPaintColorView::RGB", (int64)EMeshPaintColorView::RGB },
				{ "EMeshPaintColorView::Alpha", (int64)EMeshPaintColorView::Alpha },
				{ "EMeshPaintColorView::Red", (int64)EMeshPaintColorView::Red },
				{ "EMeshPaintColorView::Green", (int64)EMeshPaintColorView::Green },
				{ "EMeshPaintColorView::Blue", (int64)EMeshPaintColorView::Blue },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Alpha.Comment", "/** Alpha only */" },
				{ "Alpha.DisplayName", "Alpha Channel" },
				{ "Alpha.Name", "EMeshPaintColorView::Alpha" },
				{ "Alpha.ToolTip", "Alpha only" },
				{ "Blue.Comment", "/** Blue only */" },
				{ "Blue.DisplayName", "Blue Channel" },
				{ "Blue.Name", "EMeshPaintColorView::Blue" },
				{ "Blue.ToolTip", "Blue only" },
				{ "Comment", "/** Mesh paint color view modes (somewhat maps to EVertexColorViewMode engine enum.) */" },
				{ "Green.Comment", "/** Green only */" },
				{ "Green.DisplayName", "Green Channel" },
				{ "Green.Name", "EMeshPaintColorView::Green" },
				{ "Green.ToolTip", "Green only" },
				{ "ModuleRelativePath", "Private/MeshPaintModeSettings.h" },
				{ "Normal.Comment", "/** Normal view mode (vertex color visualization off) */" },
				{ "Normal.DisplayName", "Off" },
				{ "Normal.Name", "EMeshPaintColorView::Normal" },
				{ "Normal.ToolTip", "Normal view mode (vertex color visualization off)" },
				{ "Red.Comment", "/** Red only */" },
				{ "Red.DisplayName", "Red Channel" },
				{ "Red.Name", "EMeshPaintColorView::Red" },
				{ "Red.ToolTip", "Red only" },
				{ "RGB.Comment", "/** RGB only */" },
				{ "RGB.DisplayName", "RGB Channels" },
				{ "RGB.Name", "EMeshPaintColorView::RGB" },
				{ "RGB.ToolTip", "RGB only" },
				{ "ToolTip", "Mesh paint color view modes (somewhat maps to EVertexColorViewMode engine enum.)" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshPaintEditorMode,
				nullptr,
				"EMeshPaintColorView",
				"EMeshPaintColorView",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMeshPaintModeSettings::StaticRegisterNativesUMeshPaintModeSettings()
	{
	}
	UClass* Z_Construct_UClass_UMeshPaintModeSettings_NoRegister()
	{
		return UMeshPaintModeSettings::StaticClass();
	}
	struct Z_Construct_UClass_UMeshPaintModeSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_ColorViewMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorViewMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_ColorViewMode;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshPaintModeSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintEditorMode,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshPaintModeSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements the Mesh Editor's settings.\n */" },
		{ "IncludePath", "MeshPaintModeSettings.h" },
		{ "ModuleRelativePath", "Private/MeshPaintModeSettings.h" },
		{ "ToolTip", "Implements the Mesh Editor's settings." },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMeshPaintModeSettings_Statics::NewProp_ColorViewMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshPaintModeSettings_Statics::NewProp_ColorViewMode_MetaData[] = {
		{ "Category", "Visualization" },
		{ "Comment", "/** Color view mode used to display Vertex Colors */" },
		{ "ModuleRelativePath", "Private/MeshPaintModeSettings.h" },
		{ "ToolTip", "Color view mode used to display Vertex Colors" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMeshPaintModeSettings_Statics::NewProp_ColorViewMode = { "ColorViewMode", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshPaintModeSettings, ColorViewMode), Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintDataColorViewMode, METADATA_PARAMS(Z_Construct_UClass_UMeshPaintModeSettings_Statics::NewProp_ColorViewMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshPaintModeSettings_Statics::NewProp_ColorViewMode_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshPaintModeSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshPaintModeSettings_Statics::NewProp_ColorViewMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshPaintModeSettings_Statics::NewProp_ColorViewMode,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshPaintModeSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshPaintModeSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshPaintModeSettings_Statics::ClassParams = {
		&UMeshPaintModeSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshPaintModeSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshPaintModeSettings_Statics::PropPointers),
		0,
		0x001000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshPaintModeSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshPaintModeSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshPaintModeSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshPaintModeSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshPaintModeSettings, 732071711);
	template<> MESHPAINTEDITORMODE_API UClass* StaticClass<UMeshPaintModeSettings>()
	{
		return UMeshPaintModeSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshPaintModeSettings(Z_Construct_UClass_UMeshPaintModeSettings, &UMeshPaintModeSettings::StaticClass, TEXT("/Script/MeshPaintEditorMode"), TEXT("UMeshPaintModeSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshPaintModeSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
