// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHPAINTEDITORMODE_MeshPaintModeSettings_generated_h
#error "MeshPaintModeSettings.generated.h already included, missing '#pragma once' in MeshPaintModeSettings.h"
#endif
#define MESHPAINTEDITORMODE_MeshPaintModeSettings_generated_h

#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshPaintModeSettings(); \
	friend struct Z_Construct_UClass_UMeshPaintModeSettings_Statics; \
public: \
	DECLARE_CLASS(UMeshPaintModeSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MeshPaintEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UMeshPaintModeSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_INCLASS \
private: \
	static void StaticRegisterNativesUMeshPaintModeSettings(); \
	friend struct Z_Construct_UClass_UMeshPaintModeSettings_Statics; \
public: \
	DECLARE_CLASS(UMeshPaintModeSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/MeshPaintEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UMeshPaintModeSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshPaintModeSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshPaintModeSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshPaintModeSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshPaintModeSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshPaintModeSettings(UMeshPaintModeSettings&&); \
	NO_API UMeshPaintModeSettings(const UMeshPaintModeSettings&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshPaintModeSettings(UMeshPaintModeSettings&&); \
	NO_API UMeshPaintModeSettings(const UMeshPaintModeSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshPaintModeSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshPaintModeSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshPaintModeSettings)


#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_35_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_INCLASS \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h_39_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTEDITORMODE_API UClass* StaticClass<class UMeshPaintModeSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintModeSettings_h


#define FOREACH_ENUM_EMESHPAINTCOLORVIEW(op) \
	op(EMeshPaintColorView::Normal) \
	op(EMeshPaintColorView::RGB) \
	op(EMeshPaintColorView::Alpha) \
	op(EMeshPaintColorView::Red) \
	op(EMeshPaintColorView::Green) \
	op(EMeshPaintColorView::Blue) 

enum class EMeshPaintColorView : uint8;
template<> MESHPAINTEDITORMODE_API UEnum* StaticEnum<EMeshPaintColorView>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
