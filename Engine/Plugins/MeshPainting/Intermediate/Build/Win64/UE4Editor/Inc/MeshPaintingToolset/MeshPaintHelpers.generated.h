// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHPAINTINGTOOLSET_MeshPaintHelpers_generated_h
#error "MeshPaintHelpers.generated.h already included, missing '#pragma once' in MeshPaintHelpers.h"
#endif
#define MESHPAINTINGTOOLSET_MeshPaintHelpers_generated_h

#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshPaintingToolset(); \
	friend struct Z_Construct_UClass_UMeshPaintingToolset_Statics; \
public: \
	DECLARE_CLASS(UMeshPaintingToolset, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshPaintingToolset)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_INCLASS \
private: \
	static void StaticRegisterNativesUMeshPaintingToolset(); \
	friend struct Z_Construct_UClass_UMeshPaintingToolset_Statics; \
public: \
	DECLARE_CLASS(UMeshPaintingToolset, UBlueprintFunctionLibrary, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshPaintingToolset)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshPaintingToolset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshPaintingToolset) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshPaintingToolset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshPaintingToolset); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshPaintingToolset(UMeshPaintingToolset&&); \
	NO_API UMeshPaintingToolset(const UMeshPaintingToolset&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshPaintingToolset(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshPaintingToolset(UMeshPaintingToolset&&); \
	NO_API UMeshPaintingToolset(const UMeshPaintingToolset&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshPaintingToolset); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshPaintingToolset); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshPaintingToolset)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_124_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_INCLASS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_128_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<class UMeshPaintingToolset>();

#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshToolsContext(); \
	friend struct Z_Construct_UClass_UMeshToolsContext_Statics; \
public: \
	DECLARE_CLASS(UMeshToolsContext, UEdModeInteractiveToolsContext, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshToolsContext)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_INCLASS \
private: \
	static void StaticRegisterNativesUMeshToolsContext(); \
	friend struct Z_Construct_UClass_UMeshToolsContext_Statics; \
public: \
	DECLARE_CLASS(UMeshToolsContext, UEdModeInteractiveToolsContext, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshToolsContext)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshToolsContext(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshToolsContext) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshToolsContext); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshToolsContext); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshToolsContext(UMeshToolsContext&&); \
	NO_API UMeshToolsContext(const UMeshToolsContext&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshToolsContext(UMeshToolsContext&&); \
	NO_API UMeshToolsContext(const UMeshToolsContext&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshToolsContext); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshToolsContext); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshToolsContext)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_257_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_INCLASS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_260_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<class UMeshToolsContext>();

#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshToolManager(); \
	friend struct Z_Construct_UClass_UMeshToolManager_Statics; \
public: \
	DECLARE_CLASS(UMeshToolManager, UInteractiveToolManager, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshToolManager)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_INCLASS \
private: \
	static void StaticRegisterNativesUMeshToolManager(); \
	friend struct Z_Construct_UClass_UMeshToolManager_Statics; \
public: \
	DECLARE_CLASS(UMeshToolManager, UInteractiveToolManager, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshToolManager)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshToolManager(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshToolManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshToolManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshToolManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshToolManager(UMeshToolManager&&); \
	NO_API UMeshToolManager(const UMeshToolManager&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshToolManager(UMeshToolManager&&); \
	NO_API UMeshToolManager(const UMeshToolManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshToolManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshToolManager); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshToolManager)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_266_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_INCLASS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h_269_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<class UMeshToolManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintHelpers_h


#define FOREACH_ENUM_ETEXTUREPAINTWEIGHTINDEX(op) \
	op(ETexturePaintWeightIndex::TextureOne) \
	op(ETexturePaintWeightIndex::TextureTwo) \
	op(ETexturePaintWeightIndex::TextureThree) \
	op(ETexturePaintWeightIndex::TextureFour) \
	op(ETexturePaintWeightIndex::TextureFive) 

enum class ETexturePaintWeightIndex : uint8;
template<> MESHPAINTINGTOOLSET_API UEnum* StaticEnum<ETexturePaintWeightIndex>();

#define FOREACH_ENUM_ETEXTUREPAINTWEIGHTTYPES(op) \
	op(ETexturePaintWeightTypes::AlphaLerp) \
	op(ETexturePaintWeightTypes::RGB) \
	op(ETexturePaintWeightTypes::ARGB) \
	op(ETexturePaintWeightTypes::OneMinusARGB) 

enum class ETexturePaintWeightTypes : uint8;
template<> MESHPAINTINGTOOLSET_API UEnum* StaticEnum<ETexturePaintWeightTypes>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
