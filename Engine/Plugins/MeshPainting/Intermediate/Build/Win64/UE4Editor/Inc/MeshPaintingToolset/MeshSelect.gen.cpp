// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshPaintingToolset/Public/MeshSelect.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshSelect() {}
// Cross Module References
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UVertexAdapterClickToolBuilder_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UVertexAdapterClickToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleClickToolBuilder();
	UPackage* Z_Construct_UPackage__Script_MeshPaintingToolset();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UTextureAdapterClickToolBuilder_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UTextureAdapterClickToolBuilder();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshClickTool_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshClickTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_USingleClickTool();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshPaintSelectionMechanic_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshPaintSelectionInterface_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UVertexAdapterClickTool_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UVertexAdapterClickTool();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UTextureAdapterClickTool_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UTextureAdapterClickTool();
// End Cross Module References
	void UVertexAdapterClickToolBuilder::StaticRegisterNativesUVertexAdapterClickToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UVertexAdapterClickToolBuilder_NoRegister()
	{
		return UVertexAdapterClickToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UVertexAdapterClickToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVertexAdapterClickToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleClickToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVertexAdapterClickToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Builder for UVertexAdapterClickTool\n */" },
		{ "IncludePath", "MeshSelect.h" },
		{ "ModuleRelativePath", "Public/MeshSelect.h" },
		{ "ToolTip", "Builder for UVertexAdapterClickTool" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVertexAdapterClickToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVertexAdapterClickToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVertexAdapterClickToolBuilder_Statics::ClassParams = {
		&UVertexAdapterClickToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVertexAdapterClickToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVertexAdapterClickToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVertexAdapterClickToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVertexAdapterClickToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVertexAdapterClickToolBuilder, 2607029964);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UVertexAdapterClickToolBuilder>()
	{
		return UVertexAdapterClickToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVertexAdapterClickToolBuilder(Z_Construct_UClass_UVertexAdapterClickToolBuilder, &UVertexAdapterClickToolBuilder::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UVertexAdapterClickToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVertexAdapterClickToolBuilder);
	void UTextureAdapterClickToolBuilder::StaticRegisterNativesUTextureAdapterClickToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UTextureAdapterClickToolBuilder_NoRegister()
	{
		return UTextureAdapterClickToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UTextureAdapterClickToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTextureAdapterClickToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleClickToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTextureAdapterClickToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Builder for UTextureAdapterClickTool\n */" },
		{ "IncludePath", "MeshSelect.h" },
		{ "ModuleRelativePath", "Public/MeshSelect.h" },
		{ "ToolTip", "Builder for UTextureAdapterClickTool" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTextureAdapterClickToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTextureAdapterClickToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTextureAdapterClickToolBuilder_Statics::ClassParams = {
		&UTextureAdapterClickToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UTextureAdapterClickToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTextureAdapterClickToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTextureAdapterClickToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTextureAdapterClickToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTextureAdapterClickToolBuilder, 1725214550);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UTextureAdapterClickToolBuilder>()
	{
		return UTextureAdapterClickToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTextureAdapterClickToolBuilder(Z_Construct_UClass_UTextureAdapterClickToolBuilder, &UTextureAdapterClickToolBuilder::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UTextureAdapterClickToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTextureAdapterClickToolBuilder);
	void UMeshClickTool::StaticRegisterNativesUMeshClickTool()
	{
	}
	UClass* Z_Construct_UClass_UMeshClickTool_NoRegister()
	{
		return UMeshClickTool::StaticClass();
	}
	struct Z_Construct_UClass_UMeshClickTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectionMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectionMechanic;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshClickTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_USingleClickTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshClickTool_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * USingleClickTool is perhaps the simplest possible interactive tool. It simply\n * reacts to default primary button clicks for the active device (eg left-mouse clicks).\n *\n * The function ::IsHitByClick() determines what is clickable by this Tool. The default is\n * to return true, which means the click will activate anywhere (the Tool itself has no\n * notion of Actors, Components, etc). You can override this function to, for example,\n * filter out clicks that don't hit a target object, etc.\n *\n * The function ::OnClicked() implements the action that will occur when a click happens.\n * You must override this to implement any kind of useful behavior.\n */" },
		{ "IncludePath", "MeshSelect.h" },
		{ "ModuleRelativePath", "Public/MeshSelect.h" },
		{ "ToolTip", "USingleClickTool is perhaps the simplest possible interactive tool. It simply\nreacts to default primary button clicks for the active device (eg left-mouse clicks).\n\nThe function ::IsHitByClick() determines what is clickable by this Tool. The default is\nto return true, which means the click will activate anywhere (the Tool itself has no\nnotion of Actors, Components, etc). You can override this function to, for example,\nfilter out clicks that don't hit a target object, etc.\n\nThe function ::OnClicked() implements the action that will occur when a click happens.\nYou must override this to implement any kind of useful behavior." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshClickTool_Statics::NewProp_SelectionMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshSelect.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshClickTool_Statics::NewProp_SelectionMechanic = { "SelectionMechanic", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshClickTool, SelectionMechanic), Z_Construct_UClass_UMeshPaintSelectionMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshClickTool_Statics::NewProp_SelectionMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshClickTool_Statics::NewProp_SelectionMechanic_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshClickTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshClickTool_Statics::NewProp_SelectionMechanic,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UMeshClickTool_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UMeshPaintSelectionInterface_NoRegister, (int32)VTABLE_OFFSET(UMeshClickTool, IMeshPaintSelectionInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshClickTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshClickTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshClickTool_Statics::ClassParams = {
		&UMeshClickTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshClickTool_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshClickTool_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshClickTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshClickTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshClickTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshClickTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshClickTool, 4199843698);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshClickTool>()
	{
		return UMeshClickTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshClickTool(Z_Construct_UClass_UMeshClickTool, &UMeshClickTool::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshClickTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshClickTool);
	void UVertexAdapterClickTool::StaticRegisterNativesUVertexAdapterClickTool()
	{
	}
	UClass* Z_Construct_UClass_UVertexAdapterClickTool_NoRegister()
	{
		return UVertexAdapterClickTool::StaticClass();
	}
	struct Z_Construct_UClass_UVertexAdapterClickTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UVertexAdapterClickTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshClickTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UVertexAdapterClickTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshSelect.h" },
		{ "ModuleRelativePath", "Public/MeshSelect.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UVertexAdapterClickTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UVertexAdapterClickTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UVertexAdapterClickTool_Statics::ClassParams = {
		&UVertexAdapterClickTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UVertexAdapterClickTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UVertexAdapterClickTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UVertexAdapterClickTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UVertexAdapterClickTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UVertexAdapterClickTool, 898243324);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UVertexAdapterClickTool>()
	{
		return UVertexAdapterClickTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UVertexAdapterClickTool(Z_Construct_UClass_UVertexAdapterClickTool, &UVertexAdapterClickTool::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UVertexAdapterClickTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UVertexAdapterClickTool);
	void UTextureAdapterClickTool::StaticRegisterNativesUTextureAdapterClickTool()
	{
	}
	UClass* Z_Construct_UClass_UTextureAdapterClickTool_NoRegister()
	{
		return UTextureAdapterClickTool::StaticClass();
	}
	struct Z_Construct_UClass_UTextureAdapterClickTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTextureAdapterClickTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshClickTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTextureAdapterClickTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshSelect.h" },
		{ "ModuleRelativePath", "Public/MeshSelect.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTextureAdapterClickTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTextureAdapterClickTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTextureAdapterClickTool_Statics::ClassParams = {
		&UTextureAdapterClickTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UTextureAdapterClickTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTextureAdapterClickTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTextureAdapterClickTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTextureAdapterClickTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTextureAdapterClickTool, 805111118);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UTextureAdapterClickTool>()
	{
		return UTextureAdapterClickTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTextureAdapterClickTool(Z_Construct_UClass_UTextureAdapterClickTool, &UTextureAdapterClickTool::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UTextureAdapterClickTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTextureAdapterClickTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
