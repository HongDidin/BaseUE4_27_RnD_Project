// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHPAINTINGTOOLSET_MeshTexturePaintingTool_generated_h
#error "MeshTexturePaintingTool.generated.h already included, missing '#pragma once' in MeshTexturePaintingTool.h"
#endif
#define MESHPAINTINGTOOLSET_MeshTexturePaintingTool_generated_h

#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshTexturePaintingToolBuilder(); \
	friend struct Z_Construct_UClass_UMeshTexturePaintingToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UMeshTexturePaintingToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshTexturePaintingToolBuilder)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUMeshTexturePaintingToolBuilder(); \
	friend struct Z_Construct_UClass_UMeshTexturePaintingToolBuilder_Statics; \
public: \
	DECLARE_CLASS(UMeshTexturePaintingToolBuilder, UInteractiveToolBuilder, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshTexturePaintingToolBuilder)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshTexturePaintingToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshTexturePaintingToolBuilder) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshTexturePaintingToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshTexturePaintingToolBuilder); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshTexturePaintingToolBuilder(UMeshTexturePaintingToolBuilder&&); \
	NO_API UMeshTexturePaintingToolBuilder(const UMeshTexturePaintingToolBuilder&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshTexturePaintingToolBuilder(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshTexturePaintingToolBuilder(UMeshTexturePaintingToolBuilder&&); \
	NO_API UMeshTexturePaintingToolBuilder(const UMeshTexturePaintingToolBuilder&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshTexturePaintingToolBuilder); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshTexturePaintingToolBuilder); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshTexturePaintingToolBuilder)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_30_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_INCLASS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_33_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<class UMeshTexturePaintingToolBuilder>();

#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshTexturePaintingToolProperties(); \
	friend struct Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics; \
public: \
	DECLARE_CLASS(UMeshTexturePaintingToolProperties, UBrushBaseProperties, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshTexturePaintingToolProperties)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_INCLASS \
private: \
	static void StaticRegisterNativesUMeshTexturePaintingToolProperties(); \
	friend struct Z_Construct_UClass_UMeshTexturePaintingToolProperties_Statics; \
public: \
	DECLARE_CLASS(UMeshTexturePaintingToolProperties, UBrushBaseProperties, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshTexturePaintingToolProperties)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshTexturePaintingToolProperties(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshTexturePaintingToolProperties) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshTexturePaintingToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshTexturePaintingToolProperties); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshTexturePaintingToolProperties(UMeshTexturePaintingToolProperties&&); \
	NO_API UMeshTexturePaintingToolProperties(const UMeshTexturePaintingToolProperties&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshTexturePaintingToolProperties(UMeshTexturePaintingToolProperties&&); \
	NO_API UMeshTexturePaintingToolProperties(const UMeshTexturePaintingToolProperties&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshTexturePaintingToolProperties); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshTexturePaintingToolProperties); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshTexturePaintingToolProperties)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_41_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_INCLASS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_44_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<class UMeshTexturePaintingToolProperties>();

#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshTexturePaintingTool(); \
	friend struct Z_Construct_UClass_UMeshTexturePaintingTool_Statics; \
public: \
	DECLARE_CLASS(UMeshTexturePaintingTool, UBaseBrushTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshTexturePaintingTool)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_INCLASS \
private: \
	static void StaticRegisterNativesUMeshTexturePaintingTool(); \
	friend struct Z_Construct_UClass_UMeshTexturePaintingTool_Statics; \
public: \
	DECLARE_CLASS(UMeshTexturePaintingTool, UBaseBrushTool, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshTexturePaintingTool)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshTexturePaintingTool(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshTexturePaintingTool) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshTexturePaintingTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshTexturePaintingTool); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshTexturePaintingTool(UMeshTexturePaintingTool&&); \
	NO_API UMeshTexturePaintingTool(const UMeshTexturePaintingTool&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshTexturePaintingTool(UMeshTexturePaintingTool&&); \
	NO_API UMeshTexturePaintingTool(const UMeshTexturePaintingTool&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshTexturePaintingTool); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshTexturePaintingTool); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshTexturePaintingTool)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TextureProperties() { return STRUCT_OFFSET(UMeshTexturePaintingTool, TextureProperties); } \
	FORCEINLINE static uint32 __PPO__Textures() { return STRUCT_OFFSET(UMeshTexturePaintingTool, Textures); } \
	FORCEINLINE static uint32 __PPO__BrushRenderTargetTexture() { return STRUCT_OFFSET(UMeshTexturePaintingTool, BrushRenderTargetTexture); } \
	FORCEINLINE static uint32 __PPO__BrushMaskRenderTargetTexture() { return STRUCT_OFFSET(UMeshTexturePaintingTool, BrushMaskRenderTargetTexture); } \
	FORCEINLINE static uint32 __PPO__SeamMaskRenderTargetTexture() { return STRUCT_OFFSET(UMeshTexturePaintingTool, SeamMaskRenderTargetTexture); } \
	FORCEINLINE static uint32 __PPO__PaintTargetData() { return STRUCT_OFFSET(UMeshTexturePaintingTool, PaintTargetData); } \
	FORCEINLINE static uint32 __PPO__TexturePaintingCurrentMeshComponent() { return STRUCT_OFFSET(UMeshTexturePaintingTool, TexturePaintingCurrentMeshComponent); } \
	FORCEINLINE static uint32 __PPO__PaintingTexture2D() { return STRUCT_OFFSET(UMeshTexturePaintingTool, PaintingTexture2D); }


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_96_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_INCLASS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h_99_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<class UMeshTexturePaintingTool>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshTexturePaintingTool_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
