// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshPaintingToolset/Public/MeshVertexPaintingTool.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshVertexPaintingTool() {}
// Cross Module References
	MESHPAINTINGTOOLSET_API UEnum* Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintTextureIndex();
	UPackage* Z_Construct_UPackage__Script_MeshPaintingToolset();
	MESHPAINTINGTOOLSET_API UEnum* Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintWeightTypes();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshColorPaintingToolBuilder_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshColorPaintingToolBuilder();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractiveToolBuilder();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshWeightPaintingToolBuilder_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshWeightPaintingToolBuilder();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshVertexPaintingToolProperties_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshVertexPaintingToolProperties();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UBrushBaseProperties();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshColorPaintingToolProperties_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshColorPaintingToolProperties();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshWeightPaintingToolProperties_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshWeightPaintingToolProperties();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshVertexPaintingTool_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshVertexPaintingTool();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UBaseBrushTool();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshPaintSelectionMechanic_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshPaintSelectionInterface_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshColorPaintingTool_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshColorPaintingTool();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshWeightPaintingTool_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshWeightPaintingTool();
// End Cross Module References
	static UEnum* EMeshPaintTextureIndex_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintTextureIndex, Z_Construct_UPackage__Script_MeshPaintingToolset(), TEXT("EMeshPaintTextureIndex"));
		}
		return Singleton;
	}
	template<> MESHPAINTINGTOOLSET_API UEnum* StaticEnum<EMeshPaintTextureIndex>()
	{
		return EMeshPaintTextureIndex_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshPaintTextureIndex(EMeshPaintTextureIndex_StaticEnum, TEXT("/Script/MeshPaintingToolset"), TEXT("EMeshPaintTextureIndex"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintTextureIndex_Hash() { return 596804978U; }
	UEnum* Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintTextureIndex()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshPaintingToolset();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshPaintTextureIndex"), 0, Get_Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintTextureIndex_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshPaintTextureIndex::TextureOne", (int64)EMeshPaintTextureIndex::TextureOne },
				{ "EMeshPaintTextureIndex::TextureTwo", (int64)EMeshPaintTextureIndex::TextureTwo },
				{ "EMeshPaintTextureIndex::TextureThree", (int64)EMeshPaintTextureIndex::TextureThree },
				{ "EMeshPaintTextureIndex::TextureFour", (int64)EMeshPaintTextureIndex::TextureFour },
				{ "EMeshPaintTextureIndex::TextureFive", (int64)EMeshPaintTextureIndex::TextureFive },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
				{ "TextureFive.Name", "EMeshPaintTextureIndex::TextureFive" },
				{ "TextureFour.Name", "EMeshPaintTextureIndex::TextureFour" },
				{ "TextureOne.Name", "EMeshPaintTextureIndex::TextureOne" },
				{ "TextureThree.Name", "EMeshPaintTextureIndex::TextureThree" },
				{ "TextureTwo.Name", "EMeshPaintTextureIndex::TextureTwo" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
				nullptr,
				"EMeshPaintTextureIndex",
				"EMeshPaintTextureIndex",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	static UEnum* EMeshPaintWeightTypes_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintWeightTypes, Z_Construct_UPackage__Script_MeshPaintingToolset(), TEXT("EMeshPaintWeightTypes"));
		}
		return Singleton;
	}
	template<> MESHPAINTINGTOOLSET_API UEnum* StaticEnum<EMeshPaintWeightTypes>()
	{
		return EMeshPaintWeightTypes_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EMeshPaintWeightTypes(EMeshPaintWeightTypes_StaticEnum, TEXT("/Script/MeshPaintingToolset"), TEXT("EMeshPaintWeightTypes"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintWeightTypes_Hash() { return 2476898718U; }
	UEnum* Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintWeightTypes()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_MeshPaintingToolset();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EMeshPaintWeightTypes"), 0, Get_Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintWeightTypes_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EMeshPaintWeightTypes::AlphaLerp", (int64)EMeshPaintWeightTypes::AlphaLerp },
				{ "EMeshPaintWeightTypes::RGB", (int64)EMeshPaintWeightTypes::RGB },
				{ "EMeshPaintWeightTypes::ARGB", (int64)EMeshPaintWeightTypes::ARGB },
				{ "EMeshPaintWeightTypes::OneMinusARGB", (int64)EMeshPaintWeightTypes::OneMinusARGB },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AlphaLerp.Comment", "/** Lerp Between Two Textures using Alpha Value */" },
				{ "AlphaLerp.DisplayName", "Alpha (Two Textures)" },
				{ "AlphaLerp.Name", "EMeshPaintWeightTypes::AlphaLerp" },
				{ "AlphaLerp.ToolTip", "Lerp Between Two Textures using Alpha Value" },
				{ "ARGB.Comment", "/**  Weighting Four Textures according to Channels*/" },
				{ "ARGB.DisplayName", "ARGB (Four Textures)" },
				{ "ARGB.Name", "EMeshPaintWeightTypes::ARGB" },
				{ "ARGB.ToolTip", "Weighting Four Textures according to Channels" },
				{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
				{ "OneMinusARGB.Comment", "/**  Weighting Five Textures according to Channels */" },
				{ "OneMinusARGB.DisplayName", "ARGB - 1 (Five Textures)" },
				{ "OneMinusARGB.Name", "EMeshPaintWeightTypes::OneMinusARGB" },
				{ "OneMinusARGB.ToolTip", "Weighting Five Textures according to Channels" },
				{ "RGB.Comment", "/** Weighting Three Textures according to Channels*/" },
				{ "RGB.DisplayName", "RGB (Three Textures)" },
				{ "RGB.Name", "EMeshPaintWeightTypes::RGB" },
				{ "RGB.ToolTip", "Weighting Three Textures according to Channels" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
				nullptr,
				"EMeshPaintWeightTypes",
				"EMeshPaintWeightTypes",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UMeshColorPaintingToolBuilder::StaticRegisterNativesUMeshColorPaintingToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UMeshColorPaintingToolBuilder_NoRegister()
	{
		return UMeshColorPaintingToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UMeshColorPaintingToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshColorPaintingToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshColorPaintingToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n *\n */" },
		{ "IncludePath", "MeshVertexPaintingTool.h" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshColorPaintingToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshColorPaintingToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshColorPaintingToolBuilder_Statics::ClassParams = {
		&UMeshColorPaintingToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshColorPaintingToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshColorPaintingToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshColorPaintingToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshColorPaintingToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshColorPaintingToolBuilder, 3340698657);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshColorPaintingToolBuilder>()
	{
		return UMeshColorPaintingToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshColorPaintingToolBuilder(Z_Construct_UClass_UMeshColorPaintingToolBuilder, &UMeshColorPaintingToolBuilder::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshColorPaintingToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshColorPaintingToolBuilder);
	void UMeshWeightPaintingToolBuilder::StaticRegisterNativesUMeshWeightPaintingToolBuilder()
	{
	}
	UClass* Z_Construct_UClass_UMeshWeightPaintingToolBuilder_NoRegister()
	{
		return UMeshWeightPaintingToolBuilder::StaticClass();
	}
	struct Z_Construct_UClass_UMeshWeightPaintingToolBuilder_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshWeightPaintingToolBuilder_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractiveToolBuilder,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshWeightPaintingToolBuilder_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshVertexPaintingTool.h" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshWeightPaintingToolBuilder_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshWeightPaintingToolBuilder>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshWeightPaintingToolBuilder_Statics::ClassParams = {
		&UMeshWeightPaintingToolBuilder::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshWeightPaintingToolBuilder_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshWeightPaintingToolBuilder_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshWeightPaintingToolBuilder()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshWeightPaintingToolBuilder_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshWeightPaintingToolBuilder, 1127022795);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshWeightPaintingToolBuilder>()
	{
		return UMeshWeightPaintingToolBuilder::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshWeightPaintingToolBuilder(Z_Construct_UClass_UMeshWeightPaintingToolBuilder, &UMeshWeightPaintingToolBuilder::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshWeightPaintingToolBuilder"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshWeightPaintingToolBuilder);
	void UMeshVertexPaintingToolProperties::StaticRegisterNativesUMeshVertexPaintingToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UMeshVertexPaintingToolProperties_NoRegister()
	{
		return UMeshVertexPaintingToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PaintColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_PaintColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EraseColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_EraseColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bEnableFlow_MetaData[];
#endif
		static void NewProp_bEnableFlow_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bEnableFlow;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bOnlyFrontFacingTriangles_MetaData[];
#endif
		static void NewProp_bOnlyFrontFacingTriangles_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bOnlyFrontFacingTriangles;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VertexPreviewSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_VertexPreviewSize;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBrushBaseProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshVertexPaintingTool.h" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_PaintColor_MetaData[] = {
		{ "Category", "VertexPainting" },
		{ "Comment", "/** Color used for Applying Vertex Color Painting */" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
		{ "ToolTip", "Color used for Applying Vertex Color Painting" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_PaintColor = { "PaintColor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshVertexPaintingToolProperties, PaintColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_PaintColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_PaintColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_EraseColor_MetaData[] = {
		{ "Category", "VertexPainting" },
		{ "Comment", "/** Color used for Erasing Vertex Color Painting */" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
		{ "ToolTip", "Color used for Erasing Vertex Color Painting" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_EraseColor = { "EraseColor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshVertexPaintingToolProperties, EraseColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_EraseColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_EraseColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_bEnableFlow_MetaData[] = {
		{ "Category", "Brush" },
		{ "Comment", "/** Enables \"Flow\" painting where paint is continually applied from the brush every tick */" },
		{ "DisplayName", "Enable Brush Flow" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
		{ "ToolTip", "Enables \"Flow\" painting where paint is continually applied from the brush every tick" },
	};
#endif
	void Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_bEnableFlow_SetBit(void* Obj)
	{
		((UMeshVertexPaintingToolProperties*)Obj)->bEnableFlow = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_bEnableFlow = { "bEnableFlow", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshVertexPaintingToolProperties), &Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_bEnableFlow_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_bEnableFlow_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_bEnableFlow_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_bOnlyFrontFacingTriangles_MetaData[] = {
		{ "Category", "Brush" },
		{ "Comment", "/** Whether back-facing triangles should be ignored */" },
		{ "DisplayName", "Ignore Back-Facing" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
		{ "ToolTip", "Whether back-facing triangles should be ignored" },
	};
#endif
	void Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_bOnlyFrontFacingTriangles_SetBit(void* Obj)
	{
		((UMeshVertexPaintingToolProperties*)Obj)->bOnlyFrontFacingTriangles = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_bOnlyFrontFacingTriangles = { "bOnlyFrontFacingTriangles", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshVertexPaintingToolProperties), &Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_bOnlyFrontFacingTriangles_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_bOnlyFrontFacingTriangles_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_bOnlyFrontFacingTriangles_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_VertexPreviewSize_MetaData[] = {
		{ "Category", "VertexPainting|Visualization" },
		{ "Comment", "/** Size of vertex points drawn when mesh painting is active. */" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
		{ "ToolTip", "Size of vertex points drawn when mesh painting is active." },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_VertexPreviewSize = { "VertexPreviewSize", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshVertexPaintingToolProperties, VertexPreviewSize), METADATA_PARAMS(Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_VertexPreviewSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_VertexPreviewSize_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_PaintColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_EraseColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_bEnableFlow,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_bOnlyFrontFacingTriangles,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::NewProp_VertexPreviewSize,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshVertexPaintingToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::ClassParams = {
		&UMeshVertexPaintingToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshVertexPaintingToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshVertexPaintingToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshVertexPaintingToolProperties, 895213984);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshVertexPaintingToolProperties>()
	{
		return UMeshVertexPaintingToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshVertexPaintingToolProperties(Z_Construct_UClass_UMeshVertexPaintingToolProperties, &UMeshVertexPaintingToolProperties::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshVertexPaintingToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshVertexPaintingToolProperties);
	void UMeshColorPaintingToolProperties::StaticRegisterNativesUMeshColorPaintingToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UMeshColorPaintingToolProperties_NoRegister()
	{
		return UMeshColorPaintingToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWriteRed_MetaData[];
#endif
		static void NewProp_bWriteRed_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWriteRed;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWriteGreen_MetaData[];
#endif
		static void NewProp_bWriteGreen_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWriteGreen;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWriteBlue_MetaData[];
#endif
		static void NewProp_bWriteBlue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWriteBlue;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bWriteAlpha_MetaData[];
#endif
		static void NewProp_bWriteAlpha_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bWriteAlpha;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPaintOnSpecificLOD_MetaData[];
#endif
		static void NewProp_bPaintOnSpecificLOD_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPaintOnSpecificLOD;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LODIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_LODIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshVertexPaintingToolProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshVertexPaintingTool.h" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteRed_MetaData[] = {
		{ "Category", "ColorPainting" },
		{ "Comment", "/** Whether or not to apply Vertex Color Painting to the Red Channel */" },
		{ "DisplayName", "Red" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
		{ "ToolTip", "Whether or not to apply Vertex Color Painting to the Red Channel" },
	};
#endif
	void Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteRed_SetBit(void* Obj)
	{
		((UMeshColorPaintingToolProperties*)Obj)->bWriteRed = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteRed = { "bWriteRed", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshColorPaintingToolProperties), &Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteRed_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteRed_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteRed_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteGreen_MetaData[] = {
		{ "Category", "ColorPainting" },
		{ "Comment", "/** Whether or not to apply Vertex Color Painting to the Green Channel */" },
		{ "DisplayName", "Green" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
		{ "ToolTip", "Whether or not to apply Vertex Color Painting to the Green Channel" },
	};
#endif
	void Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteGreen_SetBit(void* Obj)
	{
		((UMeshColorPaintingToolProperties*)Obj)->bWriteGreen = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteGreen = { "bWriteGreen", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshColorPaintingToolProperties), &Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteGreen_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteGreen_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteGreen_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteBlue_MetaData[] = {
		{ "Category", "ColorPainting" },
		{ "Comment", "/** Whether or not to apply Vertex Color Painting to the Blue Channel */" },
		{ "DisplayName", "Blue" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
		{ "ToolTip", "Whether or not to apply Vertex Color Painting to the Blue Channel" },
	};
#endif
	void Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteBlue_SetBit(void* Obj)
	{
		((UMeshColorPaintingToolProperties*)Obj)->bWriteBlue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteBlue = { "bWriteBlue", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshColorPaintingToolProperties), &Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteBlue_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteBlue_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteBlue_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteAlpha_MetaData[] = {
		{ "Category", "ColorPainting" },
		{ "Comment", "/** Whether or not to apply Vertex Color Painting to the Alpha Channel */" },
		{ "DisplayName", "Alpha" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
		{ "ToolTip", "Whether or not to apply Vertex Color Painting to the Alpha Channel" },
	};
#endif
	void Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteAlpha_SetBit(void* Obj)
	{
		((UMeshColorPaintingToolProperties*)Obj)->bWriteAlpha = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteAlpha = { "bWriteAlpha", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshColorPaintingToolProperties), &Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteAlpha_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteAlpha_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteAlpha_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bPaintOnSpecificLOD_MetaData[] = {
		{ "Category", "Painting" },
		{ "Comment", "/** When unchecked the painting on the base LOD will be propagate automatically to all other LODs when exiting the mode or changing the selection */" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
		{ "ToolTip", "When unchecked the painting on the base LOD will be propagate automatically to all other LODs when exiting the mode or changing the selection" },
		{ "TransientToolProperty", "" },
	};
#endif
	void Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bPaintOnSpecificLOD_SetBit(void* Obj)
	{
		((UMeshColorPaintingToolProperties*)Obj)->bPaintOnSpecificLOD = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bPaintOnSpecificLOD = { "bPaintOnSpecificLOD", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UMeshColorPaintingToolProperties), &Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bPaintOnSpecificLOD_SetBit, METADATA_PARAMS(Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bPaintOnSpecificLOD_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bPaintOnSpecificLOD_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_LODIndex_MetaData[] = {
		{ "Category", "Painting" },
		{ "ClampMin", "0" },
		{ "Comment", "/** LOD Index to which should specifically be painted */" },
		{ "EditCondition", "bPaintOnSpecificLOD" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
		{ "ToolTip", "LOD Index to which should specifically be painted" },
		{ "TransientToolProperty", "" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_LODIndex = { "LODIndex", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshColorPaintingToolProperties, LODIndex), METADATA_PARAMS(Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_LODIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_LODIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteRed,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteGreen,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteBlue,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bWriteAlpha,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_bPaintOnSpecificLOD,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::NewProp_LODIndex,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshColorPaintingToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::ClassParams = {
		&UMeshColorPaintingToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshColorPaintingToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshColorPaintingToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshColorPaintingToolProperties, 1609376507);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshColorPaintingToolProperties>()
	{
		return UMeshColorPaintingToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshColorPaintingToolProperties(Z_Construct_UClass_UMeshColorPaintingToolProperties, &UMeshColorPaintingToolProperties::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshColorPaintingToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshColorPaintingToolProperties);
	void UMeshWeightPaintingToolProperties::StaticRegisterNativesUMeshWeightPaintingToolProperties()
	{
	}
	UClass* Z_Construct_UClass_UMeshWeightPaintingToolProperties_NoRegister()
	{
		return UMeshWeightPaintingToolProperties::StaticClass();
	}
	struct Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TextureWeightType_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureWeightType_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TextureWeightType;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_PaintTextureWeightIndex_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PaintTextureWeightIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_PaintTextureWeightIndex;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_EraseTextureWeightIndex_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_EraseTextureWeightIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_EraseTextureWeightIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshVertexPaintingToolProperties,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshVertexPaintingTool.h" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_TextureWeightType_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_TextureWeightType_MetaData[] = {
		{ "Category", "WeightPainting" },
		{ "Comment", "/** Texture Blend Weight Painting Mode */" },
		{ "EnumCondition", "1" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
		{ "ToolTip", "Texture Blend Weight Painting Mode" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_TextureWeightType = { "TextureWeightType", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshWeightPaintingToolProperties, TextureWeightType), Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintWeightTypes, METADATA_PARAMS(Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_TextureWeightType_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_TextureWeightType_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_PaintTextureWeightIndex_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_PaintTextureWeightIndex_MetaData[] = {
		{ "Category", "WeightPainting" },
		{ "Comment", "/** Texture Blend Weight index which should be applied during Painting */" },
		{ "EnumCondition", "1" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
		{ "ToolTip", "Texture Blend Weight index which should be applied during Painting" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_PaintTextureWeightIndex = { "PaintTextureWeightIndex", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshWeightPaintingToolProperties, PaintTextureWeightIndex), Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintTextureIndex, METADATA_PARAMS(Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_PaintTextureWeightIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_PaintTextureWeightIndex_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_EraseTextureWeightIndex_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_EraseTextureWeightIndex_MetaData[] = {
		{ "Category", "WeightPainting" },
		{ "Comment", "/** Texture Blend Weight index which should be erased during Painting */" },
		{ "EnumCondition", "1" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
		{ "ToolTip", "Texture Blend Weight index which should be erased during Painting" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_EraseTextureWeightIndex = { "EraseTextureWeightIndex", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshWeightPaintingToolProperties, EraseTextureWeightIndex), Z_Construct_UEnum_MeshPaintingToolset_EMeshPaintTextureIndex, METADATA_PARAMS(Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_EraseTextureWeightIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_EraseTextureWeightIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_TextureWeightType_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_TextureWeightType,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_PaintTextureWeightIndex_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_PaintTextureWeightIndex,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_EraseTextureWeightIndex_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::NewProp_EraseTextureWeightIndex,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshWeightPaintingToolProperties>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::ClassParams = {
		&UMeshWeightPaintingToolProperties::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshWeightPaintingToolProperties()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshWeightPaintingToolProperties_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshWeightPaintingToolProperties, 3150547222);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshWeightPaintingToolProperties>()
	{
		return UMeshWeightPaintingToolProperties::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshWeightPaintingToolProperties(Z_Construct_UClass_UMeshWeightPaintingToolProperties, &UMeshWeightPaintingToolProperties::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshWeightPaintingToolProperties"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshWeightPaintingToolProperties);
	void UMeshVertexPaintingTool::StaticRegisterNativesUMeshVertexPaintingTool()
	{
	}
	UClass* Z_Construct_UClass_UMeshVertexPaintingTool_NoRegister()
	{
		return UMeshVertexPaintingTool::StaticClass();
	}
	struct Z_Construct_UClass_UMeshVertexPaintingTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SelectionMechanic_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SelectionMechanic;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_VertexProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_VertexProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshVertexPaintingTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBaseBrushTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshVertexPaintingTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshVertexPaintingTool.h" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshVertexPaintingTool_Statics::NewProp_SelectionMechanic_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshVertexPaintingTool_Statics::NewProp_SelectionMechanic = { "SelectionMechanic", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshVertexPaintingTool, SelectionMechanic), Z_Construct_UClass_UMeshPaintSelectionMechanic_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshVertexPaintingTool_Statics::NewProp_SelectionMechanic_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexPaintingTool_Statics::NewProp_SelectionMechanic_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshVertexPaintingTool_Statics::NewProp_VertexProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshVertexPaintingTool_Statics::NewProp_VertexProperties = { "VertexProperties", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshVertexPaintingTool, VertexProperties), Z_Construct_UClass_UMeshVertexPaintingToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshVertexPaintingTool_Statics::NewProp_VertexProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexPaintingTool_Statics::NewProp_VertexProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshVertexPaintingTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshVertexPaintingTool_Statics::NewProp_SelectionMechanic,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshVertexPaintingTool_Statics::NewProp_VertexProperties,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_UMeshVertexPaintingTool_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UMeshPaintSelectionInterface_NoRegister, (int32)VTABLE_OFFSET(UMeshVertexPaintingTool, IMeshPaintSelectionInterface), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshVertexPaintingTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshVertexPaintingTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshVertexPaintingTool_Statics::ClassParams = {
		&UMeshVertexPaintingTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshVertexPaintingTool_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexPaintingTool_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x001000A9u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshVertexPaintingTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshVertexPaintingTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshVertexPaintingTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshVertexPaintingTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshVertexPaintingTool, 953765775);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshVertexPaintingTool>()
	{
		return UMeshVertexPaintingTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshVertexPaintingTool(Z_Construct_UClass_UMeshVertexPaintingTool, &UMeshVertexPaintingTool::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshVertexPaintingTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshVertexPaintingTool);
	void UMeshColorPaintingTool::StaticRegisterNativesUMeshColorPaintingTool()
	{
	}
	UClass* Z_Construct_UClass_UMeshColorPaintingTool_NoRegister()
	{
		return UMeshColorPaintingTool::StaticClass();
	}
	struct Z_Construct_UClass_UMeshColorPaintingTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ColorProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ColorProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshColorPaintingTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshVertexPaintingTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshColorPaintingTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshVertexPaintingTool.h" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshColorPaintingTool_Statics::NewProp_ColorProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshColorPaintingTool_Statics::NewProp_ColorProperties = { "ColorProperties", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshColorPaintingTool, ColorProperties), Z_Construct_UClass_UMeshColorPaintingToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshColorPaintingTool_Statics::NewProp_ColorProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshColorPaintingTool_Statics::NewProp_ColorProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshColorPaintingTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshColorPaintingTool_Statics::NewProp_ColorProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshColorPaintingTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshColorPaintingTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshColorPaintingTool_Statics::ClassParams = {
		&UMeshColorPaintingTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshColorPaintingTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshColorPaintingTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshColorPaintingTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshColorPaintingTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshColorPaintingTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshColorPaintingTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshColorPaintingTool, 2415625709);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshColorPaintingTool>()
	{
		return UMeshColorPaintingTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshColorPaintingTool(Z_Construct_UClass_UMeshColorPaintingTool, &UMeshColorPaintingTool::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshColorPaintingTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshColorPaintingTool);
	void UMeshWeightPaintingTool::StaticRegisterNativesUMeshWeightPaintingTool()
	{
	}
	UClass* Z_Construct_UClass_UMeshWeightPaintingTool_NoRegister()
	{
		return UMeshWeightPaintingTool::StaticClass();
	}
	struct Z_Construct_UClass_UMeshWeightPaintingTool_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeightProperties_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WeightProperties;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshWeightPaintingTool_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UMeshVertexPaintingTool,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshWeightPaintingTool_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshVertexPaintingTool.h" },
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshWeightPaintingTool_Statics::NewProp_WeightProperties_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshVertexPaintingTool.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshWeightPaintingTool_Statics::NewProp_WeightProperties = { "WeightProperties", nullptr, (EPropertyFlags)0x0040000000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshWeightPaintingTool, WeightProperties), Z_Construct_UClass_UMeshWeightPaintingToolProperties_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UMeshWeightPaintingTool_Statics::NewProp_WeightProperties_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshWeightPaintingTool_Statics::NewProp_WeightProperties_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshWeightPaintingTool_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshWeightPaintingTool_Statics::NewProp_WeightProperties,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshWeightPaintingTool_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshWeightPaintingTool>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshWeightPaintingTool_Statics::ClassParams = {
		&UMeshWeightPaintingTool::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshWeightPaintingTool_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshWeightPaintingTool_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshWeightPaintingTool_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshWeightPaintingTool_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshWeightPaintingTool()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshWeightPaintingTool_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshWeightPaintingTool, 145458810);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshWeightPaintingTool>()
	{
		return UMeshWeightPaintingTool::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshWeightPaintingTool(Z_Construct_UClass_UMeshWeightPaintingTool, &UMeshWeightPaintingTool::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshWeightPaintingTool"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshWeightPaintingTool);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
