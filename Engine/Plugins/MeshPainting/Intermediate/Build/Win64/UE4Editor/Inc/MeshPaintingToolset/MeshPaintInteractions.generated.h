// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHPAINTINGTOOLSET_MeshPaintInteractions_generated_h
#error "MeshPaintInteractions.generated.h already included, missing '#pragma once' in MeshPaintInteractions.h"
#endif
#define MESHPAINTINGTOOLSET_MeshPaintInteractions_generated_h

#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshPaintSelectionInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshPaintSelectionInterface) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshPaintSelectionInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshPaintSelectionInterface); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshPaintSelectionInterface(UMeshPaintSelectionInterface&&); \
	NO_API UMeshPaintSelectionInterface(const UMeshPaintSelectionInterface&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshPaintSelectionInterface(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshPaintSelectionInterface(UMeshPaintSelectionInterface&&); \
	NO_API UMeshPaintSelectionInterface(const UMeshPaintSelectionInterface&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshPaintSelectionInterface); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshPaintSelectionInterface); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshPaintSelectionInterface)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_GENERATED_UINTERFACE_BODY() \
private: \
	static void StaticRegisterNativesUMeshPaintSelectionInterface(); \
	friend struct Z_Construct_UClass_UMeshPaintSelectionInterface_Statics; \
public: \
	DECLARE_CLASS(UMeshPaintSelectionInterface, UInterface, COMPILED_IN_FLAGS(CLASS_Abstract | CLASS_Interface), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshPaintSelectionInterface)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_GENERATED_BODY_LEGACY \
		PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_STANDARD_CONSTRUCTORS \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_GENERATED_BODY \
	PRAGMA_DISABLE_DEPRECATION_WARNINGS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_GENERATED_UINTERFACE_BODY() \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_ENHANCED_CONSTRUCTORS \
private: \
	PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_INCLASS_IINTERFACE_NO_PURE_DECLS \
protected: \
	virtual ~IMeshPaintSelectionInterface() {} \
public: \
	typedef UMeshPaintSelectionInterface UClassType; \
	typedef IMeshPaintSelectionInterface ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_INCLASS_IINTERFACE \
protected: \
	virtual ~IMeshPaintSelectionInterface() {} \
public: \
	typedef UMeshPaintSelectionInterface UClassType; \
	typedef IMeshPaintSelectionInterface ThisClass; \
	virtual UObject* _getUObject() const { check(0 && "Missing required implementation."); return nullptr; }


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_14_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_INCLASS_IINTERFACE \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_17_INCLASS_IINTERFACE_NO_PURE_DECLS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<class UMeshPaintSelectionInterface>();

#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshPaintSelectionMechanic(); \
	friend struct Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics; \
public: \
	DECLARE_CLASS(UMeshPaintSelectionMechanic, UInteractionMechanic, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshPaintSelectionMechanic)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_INCLASS \
private: \
	static void StaticRegisterNativesUMeshPaintSelectionMechanic(); \
	friend struct Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics; \
public: \
	DECLARE_CLASS(UMeshPaintSelectionMechanic, UInteractionMechanic, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/MeshPaintingToolset"), NO_API) \
	DECLARE_SERIALIZER(UMeshPaintSelectionMechanic)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshPaintSelectionMechanic(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshPaintSelectionMechanic) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshPaintSelectionMechanic); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshPaintSelectionMechanic); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshPaintSelectionMechanic(UMeshPaintSelectionMechanic&&); \
	NO_API UMeshPaintSelectionMechanic(const UMeshPaintSelectionMechanic&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshPaintSelectionMechanic() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshPaintSelectionMechanic(UMeshPaintSelectionMechanic&&); \
	NO_API UMeshPaintSelectionMechanic(const UMeshPaintSelectionMechanic&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshPaintSelectionMechanic); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshPaintSelectionMechanic); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshPaintSelectionMechanic)


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CachedClickedComponents() { return STRUCT_OFFSET(UMeshPaintSelectionMechanic, CachedClickedComponents); } \
	FORCEINLINE static uint32 __PPO__CachedClickedActors() { return STRUCT_OFFSET(UMeshPaintSelectionMechanic, CachedClickedActors); }


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_29_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_INCLASS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h_32_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<class UMeshPaintSelectionMechanic>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MeshPainting_Source_MeshPaintingToolset_Public_MeshPaintInteractions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
