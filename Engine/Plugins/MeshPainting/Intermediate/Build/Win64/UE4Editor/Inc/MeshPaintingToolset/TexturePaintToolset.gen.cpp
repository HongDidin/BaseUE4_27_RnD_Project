// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshPaintingToolset/Public/TexturePaintToolset.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTexturePaintToolset() {}
// Cross Module References
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UTexturePaintToolset_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UTexturePaintToolset();
	ENGINE_API UClass* Z_Construct_UClass_UBlueprintFunctionLibrary();
	UPackage* Z_Construct_UPackage__Script_MeshPaintingToolset();
// End Cross Module References
	void UTexturePaintToolset::StaticRegisterNativesUTexturePaintToolset()
	{
	}
	UClass* Z_Construct_UClass_UTexturePaintToolset_NoRegister()
	{
		return UTexturePaintToolset::StaticClass();
	}
	struct Z_Construct_UClass_UTexturePaintToolset_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTexturePaintToolset_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBlueprintFunctionLibrary,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTexturePaintToolset_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/** Helpers functions for texture painting functionality */" },
		{ "IncludePath", "TexturePaintToolset.h" },
		{ "ModuleRelativePath", "Public/TexturePaintToolset.h" },
		{ "ToolTip", "Helpers functions for texture painting functionality" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTexturePaintToolset_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTexturePaintToolset>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTexturePaintToolset_Statics::ClassParams = {
		&UTexturePaintToolset::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTexturePaintToolset_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTexturePaintToolset_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTexturePaintToolset()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTexturePaintToolset_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTexturePaintToolset, 3295818011);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UTexturePaintToolset>()
	{
		return UTexturePaintToolset::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTexturePaintToolset(Z_Construct_UClass_UTexturePaintToolset, &UTexturePaintToolset::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UTexturePaintToolset"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTexturePaintToolset);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
