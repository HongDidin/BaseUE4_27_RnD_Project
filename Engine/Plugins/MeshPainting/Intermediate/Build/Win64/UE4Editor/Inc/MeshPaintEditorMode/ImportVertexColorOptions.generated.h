// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHPAINTEDITORMODE_ImportVertexColorOptions_generated_h
#error "ImportVertexColorOptions.generated.h already included, missing '#pragma once' in ImportVertexColorOptions.h"
#endif
#define MESHPAINTEDITORMODE_ImportVertexColorOptions_generated_h

#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUImportVertexColorOptions(); \
	friend struct Z_Construct_UClass_UImportVertexColorOptions_Statics; \
public: \
	DECLARE_CLASS(UImportVertexColorOptions, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshPaintEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UImportVertexColorOptions)


#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUImportVertexColorOptions(); \
	friend struct Z_Construct_UClass_UImportVertexColorOptions_Statics; \
public: \
	DECLARE_CLASS(UImportVertexColorOptions, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshPaintEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UImportVertexColorOptions)


#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UImportVertexColorOptions(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UImportVertexColorOptions) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UImportVertexColorOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImportVertexColorOptions); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UImportVertexColorOptions(UImportVertexColorOptions&&); \
	NO_API UImportVertexColorOptions(const UImportVertexColorOptions&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UImportVertexColorOptions(UImportVertexColorOptions&&); \
	NO_API UImportVertexColorOptions(const UImportVertexColorOptions&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UImportVertexColorOptions); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UImportVertexColorOptions); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UImportVertexColorOptions)


#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_14_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_INCLASS \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTEDITORMODE_API UClass* StaticClass<class UImportVertexColorOptions>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_ImportVertexColorOptions_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
