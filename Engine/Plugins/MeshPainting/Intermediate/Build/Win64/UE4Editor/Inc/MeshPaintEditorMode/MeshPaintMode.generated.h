// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MESHPAINTEDITORMODE_MeshPaintMode_generated_h
#error "MeshPaintMode.generated.h already included, missing '#pragma once' in MeshPaintMode.h"
#endif
#define MESHPAINTEDITORMODE_MeshPaintMode_generated_h

#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_SPARSE_DATA
#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_RPC_WRAPPERS
#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMeshPaintMode(); \
	friend struct Z_Construct_UClass_UMeshPaintMode_Statics; \
public: \
	DECLARE_CLASS(UMeshPaintMode, UEdMode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshPaintEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UMeshPaintMode)


#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_INCLASS \
private: \
	static void StaticRegisterNativesUMeshPaintMode(); \
	friend struct Z_Construct_UClass_UMeshPaintMode_Statics; \
public: \
	DECLARE_CLASS(UMeshPaintMode, UEdMode, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/MeshPaintEditorMode"), NO_API) \
	DECLARE_SERIALIZER(UMeshPaintMode)


#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMeshPaintMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMeshPaintMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshPaintMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshPaintMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshPaintMode(UMeshPaintMode&&); \
	NO_API UMeshPaintMode(const UMeshPaintMode&); \
public:


#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMeshPaintMode(UMeshPaintMode&&); \
	NO_API UMeshPaintMode(const UMeshPaintMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMeshPaintMode); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMeshPaintMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UMeshPaintMode)


#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ModeSettings() { return STRUCT_OFFSET(UMeshPaintMode, ModeSettings); }


#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_29_PROLOG
#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_RPC_WRAPPERS \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_INCLASS \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_SPARSE_DATA \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h_33_ENHANCED_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> MESHPAINTEDITORMODE_API UClass* StaticClass<class UMeshPaintMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_MeshPainting_Source_MeshPaintEditorMode_Private_MeshPaintMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
