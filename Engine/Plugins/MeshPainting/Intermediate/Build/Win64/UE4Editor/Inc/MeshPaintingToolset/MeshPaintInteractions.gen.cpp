// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "MeshPaintingToolset/Public/MeshPaintInteractions.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMeshPaintInteractions() {}
// Cross Module References
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshPaintSelectionInterface_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshPaintSelectionInterface();
	COREUOBJECT_API UClass* Z_Construct_UClass_UInterface();
	UPackage* Z_Construct_UPackage__Script_MeshPaintingToolset();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshPaintSelectionMechanic_NoRegister();
	MESHPAINTINGTOOLSET_API UClass* Z_Construct_UClass_UMeshPaintSelectionMechanic();
	INTERACTIVETOOLSFRAMEWORK_API UClass* Z_Construct_UClass_UInteractionMechanic();
	ENGINE_API UClass* Z_Construct_UClass_UMeshComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
// End Cross Module References
	void UMeshPaintSelectionInterface::StaticRegisterNativesUMeshPaintSelectionInterface()
	{
	}
	UClass* Z_Construct_UClass_UMeshPaintSelectionInterface_NoRegister()
	{
		return UMeshPaintSelectionInterface::StaticClass();
	}
	struct Z_Construct_UClass_UMeshPaintSelectionInterface_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshPaintSelectionInterface_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInterface,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshPaintSelectionInterface_Statics::Class_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/MeshPaintInteractions.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshPaintSelectionInterface_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<IMeshPaintSelectionInterface>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshPaintSelectionInterface_Statics::ClassParams = {
		&UMeshPaintSelectionInterface::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001040A1u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshPaintSelectionInterface_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshPaintSelectionInterface_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshPaintSelectionInterface()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshPaintSelectionInterface_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshPaintSelectionInterface, 1150279531);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshPaintSelectionInterface>()
	{
		return UMeshPaintSelectionInterface::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshPaintSelectionInterface(Z_Construct_UClass_UMeshPaintSelectionInterface, &UMeshPaintSelectionInterface::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshPaintSelectionInterface"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshPaintSelectionInterface);
	void UMeshPaintSelectionMechanic::StaticRegisterNativesUMeshPaintSelectionMechanic()
	{
	}
	UClass* Z_Construct_UClass_UMeshPaintSelectionMechanic_NoRegister()
	{
		return UMeshPaintSelectionMechanic::StaticClass();
	}
	struct Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedClickedComponents_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedClickedComponents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CachedClickedComponents;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedClickedActors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedClickedActors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_CachedClickedActors;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UInteractionMechanic,
		(UObject* (*)())Z_Construct_UPackage__Script_MeshPaintingToolset,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "MeshPaintInteractions.h" },
		{ "ModuleRelativePath", "Public/MeshPaintInteractions.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::NewProp_CachedClickedComponents_Inner = { "CachedClickedComponents", nullptr, (EPropertyFlags)0x0000000000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UMeshComponent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::NewProp_CachedClickedComponents_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/MeshPaintInteractions.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::NewProp_CachedClickedComponents = { "CachedClickedComponents", nullptr, (EPropertyFlags)0x0020088000002008, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshPaintSelectionMechanic, CachedClickedComponents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::NewProp_CachedClickedComponents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::NewProp_CachedClickedComponents_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::NewProp_CachedClickedActors_Inner = { "CachedClickedActors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::NewProp_CachedClickedActors_MetaData[] = {
		{ "ModuleRelativePath", "Public/MeshPaintInteractions.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::NewProp_CachedClickedActors = { "CachedClickedActors", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UMeshPaintSelectionMechanic, CachedClickedActors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::NewProp_CachedClickedActors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::NewProp_CachedClickedActors_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::NewProp_CachedClickedComponents_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::NewProp_CachedClickedComponents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::NewProp_CachedClickedActors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::NewProp_CachedClickedActors,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMeshPaintSelectionMechanic>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::ClassParams = {
		&UMeshPaintSelectionMechanic::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMeshPaintSelectionMechanic()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMeshPaintSelectionMechanic_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMeshPaintSelectionMechanic, 3105294718);
	template<> MESHPAINTINGTOOLSET_API UClass* StaticClass<UMeshPaintSelectionMechanic>()
	{
		return UMeshPaintSelectionMechanic::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMeshPaintSelectionMechanic(Z_Construct_UClass_UMeshPaintSelectionMechanic, &UMeshPaintSelectionMechanic::StaticClass, TEXT("/Script/MeshPaintingToolset"), TEXT("UMeshPaintSelectionMechanic"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMeshPaintSelectionMechanic);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
