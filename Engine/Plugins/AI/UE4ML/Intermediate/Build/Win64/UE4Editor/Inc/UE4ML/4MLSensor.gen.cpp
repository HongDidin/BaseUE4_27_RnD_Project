// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4ML/Public/Sensors/4MLSensor.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode4MLSensor() {}
// Cross Module References
	UE4ML_API UEnum* Z_Construct_UEnum_UE4ML_E4MLTickPolicy();
	UPackage* Z_Construct_UPackage__Script_UE4ML();
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor();
	UE4ML_API UClass* Z_Construct_UClass_U4MLAgentElement();
// End Cross Module References
	static UEnum* E4MLTickPolicy_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_UE4ML_E4MLTickPolicy, Z_Construct_UPackage__Script_UE4ML(), TEXT("E4MLTickPolicy"));
		}
		return Singleton;
	}
	template<> UE4ML_API UEnum* StaticEnum<E4MLTickPolicy>()
	{
		return E4MLTickPolicy_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_E4MLTickPolicy(E4MLTickPolicy_StaticEnum, TEXT("/Script/UE4ML"), TEXT("E4MLTickPolicy"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_UE4ML_E4MLTickPolicy_Hash() { return 2063516033U; }
	UEnum* Z_Construct_UEnum_UE4ML_E4MLTickPolicy()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_UE4ML();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("E4MLTickPolicy"), 0, Get_Z_Construct_UEnum_UE4ML_E4MLTickPolicy_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "E4MLTickPolicy::EveryTick", (int64)E4MLTickPolicy::EveryTick },
				{ "E4MLTickPolicy::EveryXSeconds", (int64)E4MLTickPolicy::EveryXSeconds },
				{ "E4MLTickPolicy::EveryNTicks", (int64)E4MLTickPolicy::EveryNTicks },
				{ "E4MLTickPolicy::Never", (int64)E4MLTickPolicy::Never },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "EveryNTicks.Name", "E4MLTickPolicy::EveryNTicks" },
				{ "EveryTick.Name", "E4MLTickPolicy::EveryTick" },
				{ "EveryXSeconds.Name", "E4MLTickPolicy::EveryXSeconds" },
				{ "ModuleRelativePath", "Public/Sensors/4MLSensor.h" },
				{ "Never.Name", "E4MLTickPolicy::Never" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_UE4ML,
				nullptr,
				"E4MLTickPolicy",
				"E4MLTickPolicy",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void U4MLSensor::StaticRegisterNativesU4MLSensor()
	{
	}
	UClass* Z_Construct_UClass_U4MLSensor_NoRegister()
	{
		return U4MLSensor::StaticClass();
	}
	struct Z_Construct_UClass_U4MLSensor_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRequiresPawn_MetaData[];
#endif
		static void NewProp_bRequiresPawn_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRequiresPawn;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bIsPolling_MetaData[];
#endif
		static void NewProp_bIsPolling_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsPolling;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_TickPolicy_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TickPolicy_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_TickPolicy;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_U4MLSensor_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_U4MLAgentElement,
		(UObject* (*)())Z_Construct_UPackage__Script_UE4ML,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Sensors/4MLSensor.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Statics::NewProp_bRequiresPawn_MetaData[] = {
		{ "Category", "UE4ML" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor.h" },
	};
#endif
	void Z_Construct_UClass_U4MLSensor_Statics::NewProp_bRequiresPawn_SetBit(void* Obj)
	{
		((U4MLSensor*)Obj)->bRequiresPawn = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_U4MLSensor_Statics::NewProp_bRequiresPawn = { "bRequiresPawn", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(U4MLSensor), &Z_Construct_UClass_U4MLSensor_Statics::NewProp_bRequiresPawn_SetBit, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Statics::NewProp_bRequiresPawn_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Statics::NewProp_bRequiresPawn_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Statics::NewProp_bIsPolling_MetaData[] = {
		{ "Category", "UE4ML" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor.h" },
	};
#endif
	void Z_Construct_UClass_U4MLSensor_Statics::NewProp_bIsPolling_SetBit(void* Obj)
	{
		((U4MLSensor*)Obj)->bIsPolling = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_U4MLSensor_Statics::NewProp_bIsPolling = { "bIsPolling", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(U4MLSensor), &Z_Construct_UClass_U4MLSensor_Statics::NewProp_bIsPolling_SetBit, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Statics::NewProp_bIsPolling_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Statics::NewProp_bIsPolling_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_U4MLSensor_Statics::NewProp_TickPolicy_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Statics::NewProp_TickPolicy_MetaData[] = {
		{ "Category", "UE4ML" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_U4MLSensor_Statics::NewProp_TickPolicy = { "TickPolicy", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSensor, TickPolicy), Z_Construct_UEnum_UE4ML_E4MLTickPolicy, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Statics::NewProp_TickPolicy_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Statics::NewProp_TickPolicy_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_U4MLSensor_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Statics::NewProp_bRequiresPawn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Statics::NewProp_bIsPolling,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Statics::NewProp_TickPolicy_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Statics::NewProp_TickPolicy,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_U4MLSensor_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<U4MLSensor>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_U4MLSensor_Statics::ClassParams = {
		&U4MLSensor::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_U4MLSensor_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Statics::PropPointers),
		0,
		0x001010A1u,
		METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_U4MLSensor()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_U4MLSensor_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(U4MLSensor, 861708957);
	template<> UE4ML_API UClass* StaticClass<U4MLSensor>()
	{
		return U4MLSensor::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_U4MLSensor(Z_Construct_UClass_U4MLSensor, &U4MLSensor::StaticClass, TEXT("/Script/UE4ML"), TEXT("U4MLSensor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(U4MLSensor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
