// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4ML/Public/Sensors/4MLSensor_Movement.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode4MLSensor_Movement() {}
// Cross Module References
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor_Movement_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor_Movement();
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor();
	UPackage* Z_Construct_UPackage__Script_UE4ML();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
// End Cross Module References
	void U4MLSensor_Movement::StaticRegisterNativesU4MLSensor_Movement()
	{
	}
	UClass* Z_Construct_UClass_U4MLSensor_Movement_NoRegister()
	{
		return U4MLSensor_Movement::StaticClass();
	}
	struct Z_Construct_UClass_U4MLSensor_Movement_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAbsoluteLocation_MetaData[];
#endif
		static void NewProp_bAbsoluteLocation_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAbsoluteLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAbsoluteVelocity_MetaData[];
#endif
		static void NewProp_bAbsoluteVelocity_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAbsoluteVelocity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RefLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RefLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RefVelocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_RefVelocity;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentLocation_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurrentLocation;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CurrentVelocity_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CurrentVelocity;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_U4MLSensor_Movement_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_U4MLSensor,
		(UObject* (*)())Z_Construct_UPackage__Script_UE4ML,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Movement_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Sensors/4MLSensor_Movement.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Movement.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_bAbsoluteLocation_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Movement.h" },
	};
#endif
	void Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_bAbsoluteLocation_SetBit(void* Obj)
	{
		((U4MLSensor_Movement*)Obj)->bAbsoluteLocation = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_bAbsoluteLocation = { "bAbsoluteLocation", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(U4MLSensor_Movement), &Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_bAbsoluteLocation_SetBit, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_bAbsoluteLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_bAbsoluteLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_bAbsoluteVelocity_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Movement.h" },
	};
#endif
	void Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_bAbsoluteVelocity_SetBit(void* Obj)
	{
		((U4MLSensor_Movement*)Obj)->bAbsoluteVelocity = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_bAbsoluteVelocity = { "bAbsoluteVelocity", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(U4MLSensor_Movement), &Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_bAbsoluteVelocity_SetBit, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_bAbsoluteVelocity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_bAbsoluteVelocity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_RefLocation_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Movement.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_RefLocation = { "RefLocation", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSensor_Movement, RefLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_RefLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_RefLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_RefVelocity_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Movement.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_RefVelocity = { "RefVelocity", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSensor_Movement, RefVelocity), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_RefVelocity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_RefVelocity_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_CurrentLocation_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Movement.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_CurrentLocation = { "CurrentLocation", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSensor_Movement, CurrentLocation), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_CurrentLocation_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_CurrentLocation_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_CurrentVelocity_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Movement.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_CurrentVelocity = { "CurrentVelocity", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSensor_Movement, CurrentVelocity), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_CurrentVelocity_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_CurrentVelocity_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_U4MLSensor_Movement_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_bAbsoluteLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_bAbsoluteVelocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_RefLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_RefVelocity,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_CurrentLocation,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Movement_Statics::NewProp_CurrentVelocity,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_U4MLSensor_Movement_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<U4MLSensor_Movement>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_U4MLSensor_Movement_Statics::ClassParams = {
		&U4MLSensor_Movement::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_U4MLSensor_Movement_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Movement_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Movement_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Movement_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_U4MLSensor_Movement()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_U4MLSensor_Movement_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(U4MLSensor_Movement, 1249552033);
	template<> UE4ML_API UClass* StaticClass<U4MLSensor_Movement>()
	{
		return U4MLSensor_Movement::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_U4MLSensor_Movement(Z_Construct_UClass_U4MLSensor_Movement, &U4MLSensor_Movement::StaticClass, TEXT("/Script/UE4ML"), TEXT("U4MLSensor_Movement"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(U4MLSensor_Movement);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
