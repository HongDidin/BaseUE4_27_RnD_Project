// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4ML/Public/4MLSession.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode4MLSession() {}
// Cross Module References
	UE4ML_API UClass* Z_Construct_UClass_U4MLSession_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLSession();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_UE4ML();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UWorld_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLAgent_NoRegister();
// End Cross Module References
	void U4MLSession::StaticRegisterNativesU4MLSession()
	{
	}
	UClass* Z_Construct_UClass_U4MLSession_NoRegister()
	{
		return U4MLSession::StaticClass();
	}
	struct Z_Construct_UClass_U4MLSession_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedGameMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedGameMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedWorld_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedWorld;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AvatarToAgent_ValueProp;
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_AvatarToAgent_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AvatarToAgent_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_AvatarToAgent;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Agents_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Agents_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Agents;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AwaitingAvatar_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AwaitingAvatar_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_AwaitingAvatar;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_U4MLSession_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_UE4ML,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSession_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "4MLSession.h" },
		{ "ModuleRelativePath", "Public/4MLSession.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSession_Statics::NewProp_CachedGameMode_MetaData[] = {
		{ "ModuleRelativePath", "Public/4MLSession.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLSession_Statics::NewProp_CachedGameMode = { "CachedGameMode", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSession, CachedGameMode), Z_Construct_UClass_AGameModeBase_NoRegister, METADATA_PARAMS(Z_Construct_UClass_U4MLSession_Statics::NewProp_CachedGameMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSession_Statics::NewProp_CachedGameMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSession_Statics::NewProp_CachedWorld_MetaData[] = {
		{ "ModuleRelativePath", "Public/4MLSession.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLSession_Statics::NewProp_CachedWorld = { "CachedWorld", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSession, CachedWorld), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(Z_Construct_UClass_U4MLSession_Statics::NewProp_CachedWorld_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSession_Statics::NewProp_CachedWorld_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLSession_Statics::NewProp_AvatarToAgent_ValueProp = { "AvatarToAgent", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_U4MLAgent_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_U4MLSession_Statics::NewProp_AvatarToAgent_Key_KeyProp = { "AvatarToAgent_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSession_Statics::NewProp_AvatarToAgent_MetaData[] = {
		{ "Comment", "/** @see HashAvatar */" },
		{ "ModuleRelativePath", "Public/4MLSession.h" },
		{ "ToolTip", "@see HashAvatar" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UClass_U4MLSession_Statics::NewProp_AvatarToAgent = { "AvatarToAgent", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSession, AvatarToAgent), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_U4MLSession_Statics::NewProp_AvatarToAgent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSession_Statics::NewProp_AvatarToAgent_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLSession_Statics::NewProp_Agents_Inner = { "Agents", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_U4MLAgent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSession_Statics::NewProp_Agents_MetaData[] = {
		{ "ModuleRelativePath", "Public/4MLSession.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_U4MLSession_Statics::NewProp_Agents = { "Agents", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSession, Agents), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_U4MLSession_Statics::NewProp_Agents_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSession_Statics::NewProp_Agents_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLSession_Statics::NewProp_AwaitingAvatar_Inner = { "AwaitingAvatar", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_U4MLAgent_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSession_Statics::NewProp_AwaitingAvatar_MetaData[] = {
		{ "ModuleRelativePath", "Public/4MLSession.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_U4MLSession_Statics::NewProp_AwaitingAvatar = { "AwaitingAvatar", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSession, AwaitingAvatar), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_U4MLSession_Statics::NewProp_AwaitingAvatar_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSession_Statics::NewProp_AwaitingAvatar_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_U4MLSession_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSession_Statics::NewProp_CachedGameMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSession_Statics::NewProp_CachedWorld,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSession_Statics::NewProp_AvatarToAgent_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSession_Statics::NewProp_AvatarToAgent_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSession_Statics::NewProp_AvatarToAgent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSession_Statics::NewProp_Agents_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSession_Statics::NewProp_Agents,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSession_Statics::NewProp_AwaitingAvatar_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSession_Statics::NewProp_AwaitingAvatar,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_U4MLSession_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<U4MLSession>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_U4MLSession_Statics::ClassParams = {
		&U4MLSession::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_U4MLSession_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSession_Statics::PropPointers),
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_U4MLSession_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSession_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_U4MLSession()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_U4MLSession_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(U4MLSession, 994166007);
	template<> UE4ML_API UClass* StaticClass<U4MLSession>()
	{
		return U4MLSession::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_U4MLSession(Z_Construct_UClass_U4MLSession, &U4MLSession::StaticClass, TEXT("/Script/UE4ML"), TEXT("U4MLSession"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(U4MLSession);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
