// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4ML/Public/4MLSpace.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode4MLSpace() {}
// Cross Module References
	UE4ML_API UEnum* Z_Construct_UEnum_UE4ML_E4MLSpaceType();
	UPackage* Z_Construct_UPackage__Script_UE4ML();
// End Cross Module References
	static UEnum* E4MLSpaceType_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_UE4ML_E4MLSpaceType, Z_Construct_UPackage__Script_UE4ML(), TEXT("E4MLSpaceType"));
		}
		return Singleton;
	}
	template<> UE4ML_API UEnum* StaticEnum<E4MLSpaceType>()
	{
		return E4MLSpaceType_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_E4MLSpaceType(E4MLSpaceType_StaticEnum, TEXT("/Script/UE4ML"), TEXT("E4MLSpaceType"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_UE4ML_E4MLSpaceType_Hash() { return 2184942043U; }
	UEnum* Z_Construct_UEnum_UE4ML_E4MLSpaceType()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_UE4ML();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("E4MLSpaceType"), 0, Get_Z_Construct_UEnum_UE4ML_E4MLSpaceType_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "E4MLSpaceType::Discrete", (int64)E4MLSpaceType::Discrete },
				{ "E4MLSpaceType::MultiDiscrete", (int64)E4MLSpaceType::MultiDiscrete },
				{ "E4MLSpaceType::Box", (int64)E4MLSpaceType::Box },
				{ "E4MLSpaceType::Tuple", (int64)E4MLSpaceType::Tuple },
				{ "E4MLSpaceType::MAX", (int64)E4MLSpaceType::MAX },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Box.Name", "E4MLSpaceType::Box" },
				{ "Discrete.Name", "E4MLSpaceType::Discrete" },
				{ "MAX.Name", "E4MLSpaceType::MAX" },
				{ "ModuleRelativePath", "Public/4MLSpace.h" },
				{ "MultiDiscrete.Name", "E4MLSpaceType::MultiDiscrete" },
				{ "Tuple.Name", "E4MLSpaceType::Tuple" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_UE4ML,
				nullptr,
				"E4MLSpaceType",
				"E4MLSpaceType",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
