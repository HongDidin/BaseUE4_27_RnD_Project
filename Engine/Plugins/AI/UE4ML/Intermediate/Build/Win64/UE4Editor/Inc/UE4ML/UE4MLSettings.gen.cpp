// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4ML/Public/UE4MLSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeUE4MLSettings() {}
// Cross Module References
	UE4ML_API UClass* Z_Construct_UClass_UUE4MLSettings_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_UUE4MLSettings();
	DEVELOPERSETTINGS_API UClass* Z_Construct_UClass_UDeveloperSettings();
	UPackage* Z_Construct_UPackage__Script_UE4ML();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FSoftClassPath();
// End Cross Module References
	void UUE4MLSettings::StaticRegisterNativesUUE4MLSettings()
	{
	}
	UClass* Z_Construct_UClass_UUE4MLSettings_NoRegister()
	{
		return UUE4MLSettings::StaticClass();
	}
	struct Z_Construct_UClass_UUE4MLSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ManagerClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ManagerClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SessionClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_SessionClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefautAgentClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefautAgentClass;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultRPCServerPort_MetaData[];
#endif
		static const UE4CodeGen_Private::FFInt16PropertyParams NewProp_DefaultRPCServerPort;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UUE4MLSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UDeveloperSettings,
		(UObject* (*)())Z_Construct_UPackage__Script_UE4ML,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUE4MLSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Implements the settings for the UE4ML plugin.\n */" },
		{ "DisplayName", "UE4ML" },
		{ "IncludePath", "UE4MLSettings.h" },
		{ "ModuleRelativePath", "Public/UE4MLSettings.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Implements the settings for the UE4ML plugin." },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_ManagerClass_MetaData[] = {
		{ "Category", "UE4ML" },
		{ "MetaClass", "4MLManager" },
		{ "ModuleRelativePath", "Public/UE4MLSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_ManagerClass = { "ManagerClass", nullptr, (EPropertyFlags)0x0020080000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUE4MLSettings, ManagerClass), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_ManagerClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_ManagerClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_SessionClass_MetaData[] = {
		{ "Category", "UE4ML" },
		{ "MetaClass", "4MLSession" },
		{ "ModuleRelativePath", "Public/UE4MLSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_SessionClass = { "SessionClass", nullptr, (EPropertyFlags)0x0020080000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUE4MLSettings, SessionClass), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_SessionClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_SessionClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_DefautAgentClass_MetaData[] = {
		{ "Category", "UE4ML" },
		{ "MetaClass", "4MLAgent" },
		{ "ModuleRelativePath", "Public/UE4MLSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_DefautAgentClass = { "DefautAgentClass", nullptr, (EPropertyFlags)0x0020080000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUE4MLSettings, DefautAgentClass), Z_Construct_UScriptStruct_FSoftClassPath, METADATA_PARAMS(Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_DefautAgentClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_DefautAgentClass_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_DefaultRPCServerPort_MetaData[] = {
		{ "Category", "UE4ML" },
		{ "ModuleRelativePath", "Public/UE4MLSettings.h" },
	};
#endif
	const UE4CodeGen_Private::FFInt16PropertyParams Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_DefaultRPCServerPort = { "DefaultRPCServerPort", nullptr, (EPropertyFlags)0x0020080000004001, UE4CodeGen_Private::EPropertyGenFlags::UInt16, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UUE4MLSettings, DefaultRPCServerPort), METADATA_PARAMS(Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_DefaultRPCServerPort_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_DefaultRPCServerPort_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UUE4MLSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_ManagerClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_SessionClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_DefautAgentClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UUE4MLSettings_Statics::NewProp_DefaultRPCServerPort,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UUE4MLSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UUE4MLSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UUE4MLSettings_Statics::ClassParams = {
		&UUE4MLSettings::StaticClass,
		"Plugins",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UUE4MLSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UUE4MLSettings_Statics::PropPointers),
		0,
		0x001000A6u,
		METADATA_PARAMS(Z_Construct_UClass_UUE4MLSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UUE4MLSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UUE4MLSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UUE4MLSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UUE4MLSettings, 4219101579);
	template<> UE4ML_API UClass* StaticClass<UUE4MLSettings>()
	{
		return UUE4MLSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UUE4MLSettings(Z_Construct_UClass_UUE4MLSettings, &UUE4MLSettings::StaticClass, TEXT("/Script/UE4ML"), TEXT("UUE4MLSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UUE4MLSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
