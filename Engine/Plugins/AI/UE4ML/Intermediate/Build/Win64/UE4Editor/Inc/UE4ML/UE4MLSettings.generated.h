// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UE4ML_UE4MLSettings_generated_h
#error "UE4MLSettings.generated.h already included, missing '#pragma once' in UE4MLSettings.h"
#endif
#define UE4ML_UE4MLSettings_generated_h

#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_SPARSE_DATA
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_RPC_WRAPPERS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUUE4MLSettings(); \
	friend struct Z_Construct_UClass_UUE4MLSettings_Statics; \
public: \
	DECLARE_CLASS(UUE4MLSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(UUE4MLSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Plugins");} \



#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_INCLASS \
private: \
	static void StaticRegisterNativesUUE4MLSettings(); \
	friend struct Z_Construct_UClass_UUE4MLSettings_Statics; \
public: \
	DECLARE_CLASS(UUE4MLSettings, UDeveloperSettings, COMPILED_IN_FLAGS(0 | CLASS_DefaultConfig | CLASS_Config), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(UUE4MLSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("Plugins");} \



#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UUE4MLSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUE4MLSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUE4MLSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUE4MLSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUE4MLSettings(UUE4MLSettings&&); \
	NO_API UUE4MLSettings(const UUE4MLSettings&); \
public:


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UUE4MLSettings(UUE4MLSettings&&); \
	NO_API UUE4MLSettings(const UUE4MLSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UUE4MLSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UUE4MLSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UUE4MLSettings)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ManagerClass() { return STRUCT_OFFSET(UUE4MLSettings, ManagerClass); } \
	FORCEINLINE static uint32 __PPO__SessionClass() { return STRUCT_OFFSET(UUE4MLSettings, SessionClass); } \
	FORCEINLINE static uint32 __PPO__DefautAgentClass() { return STRUCT_OFFSET(UUE4MLSettings, DefautAgentClass); } \
	FORCEINLINE static uint32 __PPO__DefaultRPCServerPort() { return STRUCT_OFFSET(UUE4MLSettings, DefaultRPCServerPort); }


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_21_PROLOG
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_RPC_WRAPPERS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_INCLASS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h_24_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UE4ML_API UClass* StaticClass<class UUE4MLSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_UE4MLSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
