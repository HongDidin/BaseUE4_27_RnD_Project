// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UE4ML_4MLSensor_Attribute_generated_h
#error "4MLSensor_Attribute.generated.h already included, missing '#pragma once' in 4MLSensor_Attribute.h"
#endif
#define UE4ML_4MLSensor_Attribute_generated_h

#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_SPARSE_DATA
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_RPC_WRAPPERS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesU4MLSensor_Attribute(); \
	friend struct Z_Construct_UClass_U4MLSensor_Attribute_Statics; \
public: \
	DECLARE_CLASS(U4MLSensor_Attribute, U4MLSensor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLSensor_Attribute)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_INCLASS \
private: \
	static void StaticRegisterNativesU4MLSensor_Attribute(); \
	friend struct Z_Construct_UClass_U4MLSensor_Attribute_Statics; \
public: \
	DECLARE_CLASS(U4MLSensor_Attribute, U4MLSensor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLSensor_Attribute)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API U4MLSensor_Attribute(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLSensor_Attribute) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLSensor_Attribute); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLSensor_Attribute); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLSensor_Attribute(U4MLSensor_Attribute&&); \
	NO_API U4MLSensor_Attribute(const U4MLSensor_Attribute&); \
public:


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLSensor_Attribute(U4MLSensor_Attribute&&); \
	NO_API U4MLSensor_Attribute(const U4MLSensor_Attribute&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLSensor_Attribute); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLSensor_Attribute); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLSensor_Attribute)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__AttributeSet() { return STRUCT_OFFSET(U4MLSensor_Attribute, AttributeSet); }


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_14_PROLOG
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_RPC_WRAPPERS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_INCLASS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h_17_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UE4ML_API UClass* StaticClass<class U4MLSensor_Attribute>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Attribute_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
