// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class APawn;
class AController;
class AActor;
#ifdef UE4ML_4MLAgent_generated_h
#error "4MLAgent.generated.h already included, missing '#pragma once' in 4MLAgent.h"
#endif
#define UE4ML_4MLAgent_generated_h

#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_34_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_F4MLAgentConfig_Statics; \
	UE4ML_API static class UScriptStruct* StaticStruct();


template<> UE4ML_API UScriptStruct* StaticStruct<struct F4MLAgentConfig>();

#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_25_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_F4MLParameterMap_Statics; \
	UE4ML_API static class UScriptStruct* StaticStruct();


template<> UE4ML_API UScriptStruct* StaticStruct<struct F4MLParameterMap>();

#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_SPARSE_DATA
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execOnPawnControllerChanged); \
	DECLARE_FUNCTION(execOnAvatarDestroyed);


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execOnPawnControllerChanged); \
	DECLARE_FUNCTION(execOnAvatarDestroyed);


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesU4MLAgent(); \
	friend struct Z_Construct_UClass_U4MLAgent_Statics; \
public: \
	DECLARE_CLASS(U4MLAgent, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLAgent)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_INCLASS \
private: \
	static void StaticRegisterNativesU4MLAgent(); \
	friend struct Z_Construct_UClass_U4MLAgent_Statics; \
public: \
	DECLARE_CLASS(U4MLAgent, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLAgent)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API U4MLAgent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLAgent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLAgent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLAgent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLAgent(U4MLAgent&&); \
	NO_API U4MLAgent(const U4MLAgent&); \
public:


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLAgent(U4MLAgent&&); \
	NO_API U4MLAgent(const U4MLAgent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLAgent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLAgent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLAgent)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Sensors() { return STRUCT_OFFSET(U4MLAgent, Sensors); } \
	FORCEINLINE static uint32 __PPO__Actuators() { return STRUCT_OFFSET(U4MLAgent, Actuators); } \
	FORCEINLINE static uint32 __PPO__Avatar() { return STRUCT_OFFSET(U4MLAgent, Avatar); } \
	FORCEINLINE static uint32 __PPO__Controller() { return STRUCT_OFFSET(U4MLAgent, Controller); } \
	FORCEINLINE static uint32 __PPO__Pawn() { return STRUCT_OFFSET(U4MLAgent, Pawn); }


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_67_PROLOG
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_RPC_WRAPPERS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_INCLASS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h_70_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UE4ML_API UClass* StaticClass<class U4MLAgent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
