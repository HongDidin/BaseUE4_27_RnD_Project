// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UE4ML_4MLAgentElement_generated_h
#error "4MLAgentElement.generated.h already included, missing '#pragma once' in 4MLAgentElement.h"
#endif
#define UE4ML_4MLAgentElement_generated_h

#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_SPARSE_DATA
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_RPC_WRAPPERS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesU4MLAgentElement(); \
	friend struct Z_Construct_UClass_U4MLAgentElement_Statics; \
public: \
	DECLARE_CLASS(U4MLAgentElement, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLAgentElement)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_INCLASS \
private: \
	static void StaticRegisterNativesU4MLAgentElement(); \
	friend struct Z_Construct_UClass_U4MLAgentElement_Statics; \
public: \
	DECLARE_CLASS(U4MLAgentElement, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLAgentElement)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API U4MLAgentElement(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLAgentElement) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLAgentElement); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLAgentElement); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLAgentElement(U4MLAgentElement&&); \
	NO_API U4MLAgentElement(const U4MLAgentElement&); \
public:


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLAgentElement(U4MLAgentElement&&); \
	NO_API U4MLAgentElement(const U4MLAgentElement&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLAgentElement); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLAgentElement); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLAgentElement)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__ElementID() { return STRUCT_OFFSET(U4MLAgentElement, ElementID); } \
	FORCEINLINE static uint32 __PPO__Nickname() { return STRUCT_OFFSET(U4MLAgentElement, Nickname); }


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_16_PROLOG
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_RPC_WRAPPERS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_INCLASS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UE4ML_API UClass* StaticClass<class U4MLAgentElement>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Agents_4MLAgentElement_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
