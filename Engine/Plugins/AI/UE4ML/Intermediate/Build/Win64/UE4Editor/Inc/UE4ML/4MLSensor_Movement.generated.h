// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UE4ML_4MLSensor_Movement_generated_h
#error "4MLSensor_Movement.generated.h already included, missing '#pragma once' in 4MLSensor_Movement.h"
#endif
#define UE4ML_4MLSensor_Movement_generated_h

#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_SPARSE_DATA
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_RPC_WRAPPERS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesU4MLSensor_Movement(); \
	friend struct Z_Construct_UClass_U4MLSensor_Movement_Statics; \
public: \
	DECLARE_CLASS(U4MLSensor_Movement, U4MLSensor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLSensor_Movement)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_INCLASS \
private: \
	static void StaticRegisterNativesU4MLSensor_Movement(); \
	friend struct Z_Construct_UClass_U4MLSensor_Movement_Statics; \
public: \
	DECLARE_CLASS(U4MLSensor_Movement, U4MLSensor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLSensor_Movement)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API U4MLSensor_Movement(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLSensor_Movement) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLSensor_Movement); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLSensor_Movement); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLSensor_Movement(U4MLSensor_Movement&&); \
	NO_API U4MLSensor_Movement(const U4MLSensor_Movement&); \
public:


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLSensor_Movement(U4MLSensor_Movement&&); \
	NO_API U4MLSensor_Movement(const U4MLSensor_Movement&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLSensor_Movement); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLSensor_Movement); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLSensor_Movement)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RefLocation() { return STRUCT_OFFSET(U4MLSensor_Movement, RefLocation); } \
	FORCEINLINE static uint32 __PPO__RefVelocity() { return STRUCT_OFFSET(U4MLSensor_Movement, RefVelocity); } \
	FORCEINLINE static uint32 __PPO__CurrentLocation() { return STRUCT_OFFSET(U4MLSensor_Movement, CurrentLocation); } \
	FORCEINLINE static uint32 __PPO__CurrentVelocity() { return STRUCT_OFFSET(U4MLSensor_Movement, CurrentVelocity); }


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_10_PROLOG
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_RPC_WRAPPERS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_INCLASS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h_13_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UE4ML_API UClass* StaticClass<class U4MLSensor_Movement>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_Movement_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
