// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UE4ML_4MLLibrarian_generated_h
#error "4MLLibrarian.generated.h already included, missing '#pragma once' in 4MLLibrarian.h"
#endif
#define UE4ML_4MLLibrarian_generated_h

#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLLibrarian_h_17_GENERATED_BODY \
	friend struct Z_Construct_UScriptStruct_F4MLLibrarian_Statics; \
	UE4ML_API static class UScriptStruct* StaticStruct(); \
	FORCEINLINE static uint32 __PPO__KnownSensorClasses() { return STRUCT_OFFSET(F4MLLibrarian, KnownSensorClasses); } \
	FORCEINLINE static uint32 __PPO__KnownActuatorClasses() { return STRUCT_OFFSET(F4MLLibrarian, KnownActuatorClasses); } \
	FORCEINLINE static uint32 __PPO__KnownAgentClasses() { return STRUCT_OFFSET(F4MLLibrarian, KnownAgentClasses); }


template<> UE4ML_API UScriptStruct* StaticStruct<struct F4MLLibrarian>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLLibrarian_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
