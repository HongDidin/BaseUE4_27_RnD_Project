// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4ML/Public/4MLManager.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode4MLManager() {}
// Cross Module References
	UE4ML_API UEnum* Z_Construct_UEnum_UE4ML_EUE4MLServerMode();
	UPackage* Z_Construct_UPackage__Script_UE4ML();
	UE4ML_API UClass* Z_Construct_UClass_U4MLManager_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLManager();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UE4ML_API UClass* Z_Construct_UClass_U4MLSession_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UWorld_NoRegister();
	UE4ML_API UScriptStruct* Z_Construct_UScriptStruct_F4MLLibrarian();
// End Cross Module References
	static UEnum* EUE4MLServerMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_UE4ML_EUE4MLServerMode, Z_Construct_UPackage__Script_UE4ML(), TEXT("EUE4MLServerMode"));
		}
		return Singleton;
	}
	template<> UE4ML_API UEnum* StaticEnum<EUE4MLServerMode>()
	{
		return EUE4MLServerMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EUE4MLServerMode(EUE4MLServerMode_StaticEnum, TEXT("/Script/UE4ML"), TEXT("EUE4MLServerMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_UE4ML_EUE4MLServerMode_Hash() { return 1490595016U; }
	UEnum* Z_Construct_UEnum_UE4ML_EUE4MLServerMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_UE4ML();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EUE4MLServerMode"), 0, Get_Z_Construct_UEnum_UE4ML_EUE4MLServerMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "EUE4MLServerMode::Invalid", (int64)EUE4MLServerMode::Invalid },
				{ "EUE4MLServerMode::Server", (int64)EUE4MLServerMode::Server },
				{ "EUE4MLServerMode::Client", (int64)EUE4MLServerMode::Client },
				{ "EUE4MLServerMode::Standalone", (int64)EUE4MLServerMode::Standalone },
				{ "EUE4MLServerMode::AutoDetect", (int64)EUE4MLServerMode::AutoDetect },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "AutoDetect.Comment", "// this applies both to Standalone games as well as PIE\n" },
				{ "AutoDetect.Name", "EUE4MLServerMode::AutoDetect" },
				{ "AutoDetect.ToolTip", "this applies both to Standalone games as well as PIE" },
				{ "Client.Name", "EUE4MLServerMode::Client" },
				{ "Invalid.Name", "EUE4MLServerMode::Invalid" },
				{ "ModuleRelativePath", "Public/4MLManager.h" },
				{ "Server.Name", "EUE4MLServerMode::Server" },
				{ "Standalone.Name", "EUE4MLServerMode::Standalone" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_UE4ML,
				nullptr,
				"EUE4MLServerMode",
				"EUE4MLServerMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void U4MLManager::StaticRegisterNativesU4MLManager()
	{
	}
	UClass* Z_Construct_UClass_U4MLManager_NoRegister()
	{
		return U4MLManager::StaticClass();
	}
	struct Z_Construct_UClass_U4MLManager_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Session_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Session;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_LastActiveWorld_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_LastActiveWorld;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Librarian_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Librarian;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_U4MLManager_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_UE4ML,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLManager_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "4MLManager.h" },
		{ "ModuleRelativePath", "Public/4MLManager.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLManager_Statics::NewProp_Session_MetaData[] = {
		{ "ModuleRelativePath", "Public/4MLManager.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLManager_Statics::NewProp_Session = { "Session", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLManager, Session), Z_Construct_UClass_U4MLSession_NoRegister, METADATA_PARAMS(Z_Construct_UClass_U4MLManager_Statics::NewProp_Session_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLManager_Statics::NewProp_Session_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLManager_Statics::NewProp_LastActiveWorld_MetaData[] = {
		{ "ModuleRelativePath", "Public/4MLManager.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLManager_Statics::NewProp_LastActiveWorld = { "LastActiveWorld", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLManager, LastActiveWorld), Z_Construct_UClass_UWorld_NoRegister, METADATA_PARAMS(Z_Construct_UClass_U4MLManager_Statics::NewProp_LastActiveWorld_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLManager_Statics::NewProp_LastActiveWorld_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLManager_Statics::NewProp_Librarian_MetaData[] = {
		{ "ModuleRelativePath", "Public/4MLManager.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_U4MLManager_Statics::NewProp_Librarian = { "Librarian", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLManager, Librarian), Z_Construct_UScriptStruct_F4MLLibrarian, METADATA_PARAMS(Z_Construct_UClass_U4MLManager_Statics::NewProp_Librarian_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLManager_Statics::NewProp_Librarian_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_U4MLManager_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLManager_Statics::NewProp_Session,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLManager_Statics::NewProp_LastActiveWorld,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLManager_Statics::NewProp_Librarian,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_U4MLManager_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<U4MLManager>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_U4MLManager_Statics::ClassParams = {
		&U4MLManager::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_U4MLManager_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_U4MLManager_Statics::PropPointers),
		0,
		0x001000A8u,
		METADATA_PARAMS(Z_Construct_UClass_U4MLManager_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLManager_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_U4MLManager()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_U4MLManager_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(U4MLManager, 1062930299);
	template<> UE4ML_API UClass* StaticClass<U4MLManager>()
	{
		return U4MLManager::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_U4MLManager(Z_Construct_UClass_U4MLManager, &U4MLManager::StaticClass, TEXT("/Script/UE4ML"), TEXT("U4MLManager"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(U4MLManager);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
