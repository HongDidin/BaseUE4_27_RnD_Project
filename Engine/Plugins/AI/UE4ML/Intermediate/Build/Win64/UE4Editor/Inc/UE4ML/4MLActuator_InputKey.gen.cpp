// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4ML/Public/Actuators/4MLActuator_InputKey.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode4MLActuator_InputKey() {}
// Cross Module References
	UE4ML_API UClass* Z_Construct_UClass_U4MLActuator_InputKey_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLActuator_InputKey();
	UE4ML_API UClass* Z_Construct_UClass_U4MLActuator();
	UPackage* Z_Construct_UPackage__Script_UE4ML();
// End Cross Module References
	void U4MLActuator_InputKey::StaticRegisterNativesU4MLActuator_InputKey()
	{
	}
	UClass* Z_Construct_UClass_U4MLActuator_InputKey_NoRegister()
	{
		return U4MLActuator_InputKey::StaticClass();
	}
	struct Z_Construct_UClass_U4MLActuator_InputKey_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_U4MLActuator_InputKey_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_U4MLActuator,
		(UObject* (*)())Z_Construct_UPackage__Script_UE4ML,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLActuator_InputKey_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Actuators/4MLActuator_InputKey.h" },
		{ "ModuleRelativePath", "Public/Actuators/4MLActuator_InputKey.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_U4MLActuator_InputKey_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<U4MLActuator_InputKey>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_U4MLActuator_InputKey_Statics::ClassParams = {
		&U4MLActuator_InputKey::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_U4MLActuator_InputKey_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLActuator_InputKey_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_U4MLActuator_InputKey()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_U4MLActuator_InputKey_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(U4MLActuator_InputKey, 1551872031);
	template<> UE4ML_API UClass* StaticClass<U4MLActuator_InputKey>()
	{
		return U4MLActuator_InputKey::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_U4MLActuator_InputKey(Z_Construct_UClass_U4MLActuator_InputKey, &U4MLActuator_InputKey::StaticClass, TEXT("/Script/UE4ML"), TEXT("U4MLActuator_InputKey"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(U4MLActuator_InputKey);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
