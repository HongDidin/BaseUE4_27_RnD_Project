// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4ML/Public/Agents/4MLAgentElement.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode4MLAgentElement() {}
// Cross Module References
	UE4ML_API UClass* Z_Construct_UClass_U4MLAgentElement_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLAgentElement();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_UE4ML();
// End Cross Module References
	void U4MLAgentElement::StaticRegisterNativesU4MLAgentElement()
	{
	}
	UClass* Z_Construct_UClass_U4MLAgentElement_NoRegister()
	{
		return U4MLAgentElement::StaticClass();
	}
	struct Z_Construct_UClass_U4MLAgentElement_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ElementID_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_ElementID;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Nickname_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Nickname;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_U4MLAgentElement_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_UE4ML,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLAgentElement_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Agents/4MLAgentElement.h" },
		{ "ModuleRelativePath", "Public/Agents/4MLAgentElement.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLAgentElement_Statics::NewProp_ElementID_MetaData[] = {
		{ "Comment", "/** @note this is not a common counter, meaning Sensors and Actuators (for \n\x09 *\x09""example) track the ID separately */" },
		{ "ModuleRelativePath", "Public/Agents/4MLAgentElement.h" },
		{ "ToolTip", "@note this is not a common counter, meaning Sensors and Actuators (for\n    example) track the ID separately" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_U4MLAgentElement_Statics::NewProp_ElementID = { "ElementID", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLAgentElement, ElementID), METADATA_PARAMS(Z_Construct_UClass_U4MLAgentElement_Statics::NewProp_ElementID_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLAgentElement_Statics::NewProp_ElementID_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLAgentElement_Statics::NewProp_Nickname_MetaData[] = {
		{ "Comment", "/** User-configured name for this element, mostly for debugging purposes but \n\x09 *\x09""comes in handy when fetching observation/action spaces descriptions.\n\x09 *\x09""Defaults to UE4 instance name*/" },
		{ "ModuleRelativePath", "Public/Agents/4MLAgentElement.h" },
		{ "ToolTip", "User-configured name for this element, mostly for debugging purposes but\n    comes in handy when fetching observation/action spaces descriptions.\n    Defaults to UE4 instance name" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_U4MLAgentElement_Statics::NewProp_Nickname = { "Nickname", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLAgentElement, Nickname), METADATA_PARAMS(Z_Construct_UClass_U4MLAgentElement_Statics::NewProp_Nickname_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLAgentElement_Statics::NewProp_Nickname_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_U4MLAgentElement_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLAgentElement_Statics::NewProp_ElementID,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLAgentElement_Statics::NewProp_Nickname,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_U4MLAgentElement_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<U4MLAgentElement>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_U4MLAgentElement_Statics::ClassParams = {
		&U4MLAgentElement::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_U4MLAgentElement_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_U4MLAgentElement_Statics::PropPointers),
		0,
		0x001000A1u,
		METADATA_PARAMS(Z_Construct_UClass_U4MLAgentElement_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLAgentElement_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_U4MLAgentElement()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_U4MLAgentElement_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(U4MLAgentElement, 1574641463);
	template<> UE4ML_API UClass* StaticClass<U4MLAgentElement>()
	{
		return U4MLAgentElement::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_U4MLAgentElement(Z_Construct_UClass_U4MLAgentElement, &U4MLAgentElement::StaticClass, TEXT("/Script/UE4ML"), TEXT("U4MLAgentElement"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(U4MLAgentElement);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
