// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UE4ML_4MLManager_generated_h
#error "4MLManager.generated.h already included, missing '#pragma once' in 4MLManager.h"
#endif
#define UE4ML_4MLManager_generated_h

#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_SPARSE_DATA
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_RPC_WRAPPERS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesU4MLManager(); \
	friend struct Z_Construct_UClass_U4MLManager_Statics; \
public: \
	DECLARE_CLASS(U4MLManager, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLManager)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_INCLASS \
private: \
	static void StaticRegisterNativesU4MLManager(); \
	friend struct Z_Construct_UClass_U4MLManager_Statics; \
public: \
	DECLARE_CLASS(U4MLManager, UObject, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLManager)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API U4MLManager(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLManager) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLManager); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLManager(U4MLManager&&); \
	NO_API U4MLManager(const U4MLManager&); \
public:


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLManager(U4MLManager&&); \
	NO_API U4MLManager(const U4MLManager&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLManager); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLManager); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLManager)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__Session() { return STRUCT_OFFSET(U4MLManager, Session); } \
	FORCEINLINE static uint32 __PPO__LastActiveWorld() { return STRUCT_OFFSET(U4MLManager, LastActiveWorld); } \
	FORCEINLINE static uint32 __PPO__Librarian() { return STRUCT_OFFSET(U4MLManager, Librarian); }


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_35_PROLOG
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_RPC_WRAPPERS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_INCLASS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h_38_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UE4ML_API UClass* StaticClass<class U4MLManager>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLManager_h


#define FOREACH_ENUM_EUE4MLSERVERMODE(op) \
	op(EUE4MLServerMode::Invalid) \
	op(EUE4MLServerMode::Server) \
	op(EUE4MLServerMode::Client) \
	op(EUE4MLServerMode::Standalone) \
	op(EUE4MLServerMode::AutoDetect) 

enum class EUE4MLServerMode;
template<> UE4ML_API UEnum* StaticEnum<EUE4MLServerMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
