// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UE4ML_4MLSensor_generated_h
#error "4MLSensor.generated.h already included, missing '#pragma once' in 4MLSensor.h"
#endif
#define UE4ML_4MLSensor_generated_h

#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_SPARSE_DATA
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_RPC_WRAPPERS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesU4MLSensor(); \
	friend struct Z_Construct_UClass_U4MLSensor_Statics; \
public: \
	DECLARE_CLASS(U4MLSensor, U4MLAgentElement, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLSensor)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_INCLASS \
private: \
	static void StaticRegisterNativesU4MLSensor(); \
	friend struct Z_Construct_UClass_U4MLSensor_Statics; \
public: \
	DECLARE_CLASS(U4MLSensor, U4MLAgentElement, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLSensor)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API U4MLSensor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLSensor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLSensor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLSensor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLSensor(U4MLSensor&&); \
	NO_API U4MLSensor(const U4MLSensor&); \
public:


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLSensor(U4MLSensor&&); \
	NO_API U4MLSensor(const U4MLSensor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLSensor); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLSensor); \
	DEFINE_ABSTRACT_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLSensor)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TickPolicy() { return STRUCT_OFFSET(U4MLSensor, TickPolicy); }


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_22_PROLOG
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_RPC_WRAPPERS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_INCLASS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UE4ML_API UClass* StaticClass<class U4MLSensor>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_h


#define FOREACH_ENUM_E4MLTICKPOLICY(op) \
	op(E4MLTickPolicy::EveryTick) \
	op(E4MLTickPolicy::EveryXSeconds) \
	op(E4MLTickPolicy::EveryNTicks) \
	op(E4MLTickPolicy::Never) 

enum class E4MLTickPolicy : uint8;
template<> UE4ML_API UEnum* StaticEnum<E4MLTickPolicy>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
