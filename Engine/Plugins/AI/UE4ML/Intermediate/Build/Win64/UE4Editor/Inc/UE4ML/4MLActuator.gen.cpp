// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4ML/Public/Actuators/4MLActuator.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode4MLActuator() {}
// Cross Module References
	UE4ML_API UClass* Z_Construct_UClass_U4MLActuator_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLActuator();
	UE4ML_API UClass* Z_Construct_UClass_U4MLAgentElement();
	UPackage* Z_Construct_UPackage__Script_UE4ML();
// End Cross Module References
	void U4MLActuator::StaticRegisterNativesU4MLActuator()
	{
	}
	UClass* Z_Construct_UClass_U4MLActuator_NoRegister()
	{
		return U4MLActuator::StaticClass();
	}
	struct Z_Construct_UClass_U4MLActuator_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_U4MLActuator_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_U4MLAgentElement,
		(UObject* (*)())Z_Construct_UPackage__Script_UE4ML,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLActuator_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Actuators/4MLActuator.h" },
		{ "ModuleRelativePath", "Public/Actuators/4MLActuator.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_U4MLActuator_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<U4MLActuator>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_U4MLActuator_Statics::ClassParams = {
		&U4MLActuator::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A1u,
		METADATA_PARAMS(Z_Construct_UClass_U4MLActuator_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLActuator_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_U4MLActuator()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_U4MLActuator_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(U4MLActuator, 77428072);
	template<> UE4ML_API UClass* StaticClass<U4MLActuator>()
	{
		return U4MLActuator::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_U4MLActuator(Z_Construct_UClass_U4MLActuator, &U4MLActuator::StaticClass, TEXT("/Script/UE4ML"), TEXT("U4MLActuator"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(U4MLActuator);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
