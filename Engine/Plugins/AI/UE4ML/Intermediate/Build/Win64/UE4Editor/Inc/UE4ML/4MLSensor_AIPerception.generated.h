// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UE4ML_4MLSensor_AIPerception_generated_h
#error "4MLSensor_AIPerception.generated.h already included, missing '#pragma once' in 4MLSensor_AIPerception.h"
#endif
#define UE4ML_4MLSensor_AIPerception_generated_h

#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_SPARSE_DATA
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_RPC_WRAPPERS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesU4MLSensor_AIPerception(); \
	friend struct Z_Construct_UClass_U4MLSensor_AIPerception_Statics; \
public: \
	DECLARE_CLASS(U4MLSensor_AIPerception, U4MLSensor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLSensor_AIPerception)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_INCLASS \
private: \
	static void StaticRegisterNativesU4MLSensor_AIPerception(); \
	friend struct Z_Construct_UClass_U4MLSensor_AIPerception_Statics; \
public: \
	DECLARE_CLASS(U4MLSensor_AIPerception, U4MLSensor, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLSensor_AIPerception)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API U4MLSensor_AIPerception(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLSensor_AIPerception) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLSensor_AIPerception); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLSensor_AIPerception); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLSensor_AIPerception(U4MLSensor_AIPerception&&); \
	NO_API U4MLSensor_AIPerception(const U4MLSensor_AIPerception&); \
public:


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLSensor_AIPerception(U4MLSensor_AIPerception&&); \
	NO_API U4MLSensor_AIPerception(const U4MLSensor_AIPerception&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLSensor_AIPerception); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLSensor_AIPerception); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLSensor_AIPerception)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__PerceptionComponent() { return STRUCT_OFFSET(U4MLSensor_AIPerception, PerceptionComponent); } \
	FORCEINLINE static uint32 __PPO__bSenseOnlyChanges() { return STRUCT_OFFSET(U4MLSensor_AIPerception, bSenseOnlyChanges); }


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_20_PROLOG
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_RPC_WRAPPERS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_INCLASS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h_23_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UE4ML_API UClass* StaticClass<class U4MLSensor_AIPerception>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Sensors_4MLSensor_AIPerception_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
