// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UE4ML_4MLSession_generated_h
#error "4MLSession.generated.h already included, missing '#pragma once' in 4MLSession.h"
#endif
#define UE4ML_4MLSession_generated_h

#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_SPARSE_DATA
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_RPC_WRAPPERS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesU4MLSession(); \
	friend struct Z_Construct_UClass_U4MLSession_Statics; \
public: \
	DECLARE_CLASS(U4MLSession, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLSession)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_INCLASS \
private: \
	static void StaticRegisterNativesU4MLSession(); \
	friend struct Z_Construct_UClass_U4MLSession_Statics; \
public: \
	DECLARE_CLASS(U4MLSession, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLSession)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API U4MLSession(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLSession) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLSession); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLSession); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLSession(U4MLSession&&); \
	NO_API U4MLSession(const U4MLSession&); \
public:


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API U4MLSession(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLSession(U4MLSession&&); \
	NO_API U4MLSession(const U4MLSession&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLSession); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLSession); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLSession)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CachedGameMode() { return STRUCT_OFFSET(U4MLSession, CachedGameMode); } \
	FORCEINLINE static uint32 __PPO__CachedWorld() { return STRUCT_OFFSET(U4MLSession, CachedWorld); } \
	FORCEINLINE static uint32 __PPO__AvatarToAgent() { return STRUCT_OFFSET(U4MLSession, AvatarToAgent); } \
	FORCEINLINE static uint32 __PPO__Agents() { return STRUCT_OFFSET(U4MLSession, Agents); } \
	FORCEINLINE static uint32 __PPO__AwaitingAvatar() { return STRUCT_OFFSET(U4MLSession, AwaitingAvatar); }


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_33_PROLOG
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_RPC_WRAPPERS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_INCLASS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h_36_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UE4ML_API UClass* StaticClass<class U4MLSession>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSession_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
