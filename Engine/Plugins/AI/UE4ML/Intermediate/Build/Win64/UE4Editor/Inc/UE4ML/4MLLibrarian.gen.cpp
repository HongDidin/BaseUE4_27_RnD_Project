// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4ML/Public/4MLLibrarian.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode4MLLibrarian() {}
// Cross Module References
	UE4ML_API UScriptStruct* Z_Construct_UScriptStruct_F4MLLibrarian();
	UPackage* Z_Construct_UPackage__Script_UE4ML();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLActuator_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLAgent_NoRegister();
// End Cross Module References
class UScriptStruct* F4MLLibrarian::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern UE4ML_API uint32 Get_Z_Construct_UScriptStruct_F4MLLibrarian_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_F4MLLibrarian, Z_Construct_UPackage__Script_UE4ML(), TEXT("4MLLibrarian"), sizeof(F4MLLibrarian), Get_Z_Construct_UScriptStruct_F4MLLibrarian_Hash());
	}
	return Singleton;
}
template<> UE4ML_API UScriptStruct* StaticStruct<F4MLLibrarian>()
{
	return F4MLLibrarian::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_F4MLLibrarian(F4MLLibrarian::StaticStruct, TEXT("/Script/UE4ML"), TEXT("4MLLibrarian"), false, nullptr, nullptr);
static struct FScriptStruct_UE4ML_StaticRegisterNativesF4MLLibrarian
{
	FScriptStruct_UE4ML_StaticRegisterNativesF4MLLibrarian()
	{
		UScriptStruct::DeferCppStructOps<F4MLLibrarian>(FName(TEXT("4MLLibrarian")));
	}
} ScriptStruct_UE4ML_StaticRegisterNativesF4MLLibrarian;
	struct Z_Construct_UScriptStruct_F4MLLibrarian_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_KnownSensorClasses_ValueProp;
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_KnownSensorClasses_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KnownSensorClasses_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_KnownSensorClasses;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_KnownActuatorClasses_ValueProp;
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_KnownActuatorClasses_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KnownActuatorClasses_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_KnownActuatorClasses;
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_KnownAgentClasses_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_KnownAgentClasses_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_KnownAgentClasses;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_F4MLLibrarian_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/4MLLibrarian.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<F4MLLibrarian>();
	}
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownSensorClasses_ValueProp = { "KnownSensorClasses", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_U4MLSensor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownSensorClasses_Key_KeyProp = { "KnownSensorClasses_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownSensorClasses_MetaData[] = {
		{ "ModuleRelativePath", "Public/4MLLibrarian.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownSensorClasses = { "KnownSensorClasses", nullptr, (EPropertyFlags)0x0024080000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(F4MLLibrarian, KnownSensorClasses), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownSensorClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownSensorClasses_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownActuatorClasses_ValueProp = { "KnownActuatorClasses", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UClass_U4MLActuator_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownActuatorClasses_Key_KeyProp = { "KnownActuatorClasses_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownActuatorClasses_MetaData[] = {
		{ "ModuleRelativePath", "Public/4MLLibrarian.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownActuatorClasses = { "KnownActuatorClasses", nullptr, (EPropertyFlags)0x0024080000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(F4MLLibrarian, KnownActuatorClasses), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownActuatorClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownActuatorClasses_MetaData)) };
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownAgentClasses_Inner = { "KnownAgentClasses", nullptr, (EPropertyFlags)0x0004000000000000, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_U4MLAgent_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownAgentClasses_MetaData[] = {
		{ "ModuleRelativePath", "Public/4MLLibrarian.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownAgentClasses = { "KnownAgentClasses", nullptr, (EPropertyFlags)0x0024080000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(F4MLLibrarian, KnownAgentClasses), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownAgentClasses_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownAgentClasses_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_F4MLLibrarian_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownSensorClasses_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownSensorClasses_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownSensorClasses,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownActuatorClasses_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownActuatorClasses_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownActuatorClasses,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownAgentClasses_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLLibrarian_Statics::NewProp_KnownAgentClasses,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_F4MLLibrarian_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_UE4ML,
		nullptr,
		&NewStructOps,
		"4MLLibrarian",
		sizeof(F4MLLibrarian),
		alignof(F4MLLibrarian),
		Z_Construct_UScriptStruct_F4MLLibrarian_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_F4MLLibrarian_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_F4MLLibrarian_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_F4MLLibrarian_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_F4MLLibrarian()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_F4MLLibrarian_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_UE4ML();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("4MLLibrarian"), sizeof(F4MLLibrarian), Get_Z_Construct_UScriptStruct_F4MLLibrarian_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_F4MLLibrarian_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_F4MLLibrarian_Hash() { return 1872243347U; }
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
