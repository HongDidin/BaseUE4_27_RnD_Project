// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4ML/Public/Sensors/4MLSensor_Camera.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode4MLSensor_Camera() {}
// Cross Module References
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor_Camera_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor_Camera();
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor();
	UPackage* Z_Construct_UPackage__Script_UE4ML();
	ENGINE_API UClass* Z_Construct_UClass_USceneCaptureComponent2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTextureRenderTarget2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UGameViewportClient_NoRegister();
// End Cross Module References
	void U4MLSensor_Camera::StaticRegisterNativesU4MLSensor_Camera()
	{
	}
	UClass* Z_Construct_UClass_U4MLSensor_Camera_NoRegister()
	{
		return U4MLSensor_Camera::StaticClass();
	}
	struct Z_Construct_UClass_U4MLSensor_Camera_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Width_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_Width;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Height_MetaData[];
#endif
		static const UE4CodeGen_Private::FUInt32PropertyParams NewProp_Height;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowUI_MetaData[];
#endif
		static void NewProp_bShowUI_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowUI;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CaptureComp_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CaptureComp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_RenderTarget2D_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_RenderTarget2D;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CachedViewportClient_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CachedViewportClient;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_U4MLSensor_Camera_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_U4MLSensor,
		(UObject* (*)())Z_Construct_UPackage__Script_UE4ML,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Camera_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Observing player's camera */" },
		{ "IncludePath", "Sensors/4MLSensor_Camera.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Camera.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Observing player's camera" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_Width_MetaData[] = {
		{ "Category", "UE4ML" },
		{ "ClampMin", "1" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Camera.h" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_Width = { "Width", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSensor_Camera, Width), METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_Width_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_Width_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_Height_MetaData[] = {
		{ "Category", "UE4ML" },
		{ "ClampMin", "1" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Camera.h" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FUInt32PropertyParams Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_Height = { "Height", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::UInt32, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSensor_Camera, Height), METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_Height_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_Height_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_bShowUI_MetaData[] = {
		{ "Category", "UE4ML" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Camera.h" },
	};
#endif
	void Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_bShowUI_SetBit(void* Obj)
	{
		((U4MLSensor_Camera*)Obj)->bShowUI = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_bShowUI = { "bShowUI", nullptr, (EPropertyFlags)0x0020080000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(U4MLSensor_Camera), &Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_bShowUI_SetBit, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_bShowUI_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_bShowUI_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_CaptureComp_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Camera.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_CaptureComp = { "CaptureComp", nullptr, (EPropertyFlags)0x0020080000082008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSensor_Camera, CaptureComp), Z_Construct_UClass_USceneCaptureComponent2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_CaptureComp_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_CaptureComp_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_RenderTarget2D_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Camera.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_RenderTarget2D = { "RenderTarget2D", nullptr, (EPropertyFlags)0x0020080000002000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSensor_Camera, RenderTarget2D), Z_Construct_UClass_UTextureRenderTarget2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_RenderTarget2D_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_RenderTarget2D_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_CachedViewportClient_MetaData[] = {
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Camera.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_CachedViewportClient = { "CachedViewportClient", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSensor_Camera, CachedViewportClient), Z_Construct_UClass_UGameViewportClient_NoRegister, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_CachedViewportClient_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_CachedViewportClient_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_U4MLSensor_Camera_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_Width,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_Height,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_bShowUI,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_CaptureComp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_RenderTarget2D,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Camera_Statics::NewProp_CachedViewportClient,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_U4MLSensor_Camera_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<U4MLSensor_Camera>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_U4MLSensor_Camera_Statics::ClassParams = {
		&U4MLSensor_Camera::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_U4MLSensor_Camera_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Camera_Statics::PropPointers),
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Camera_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Camera_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_U4MLSensor_Camera()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_U4MLSensor_Camera_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(U4MLSensor_Camera, 2125762515);
	template<> UE4ML_API UClass* StaticClass<U4MLSensor_Camera>()
	{
		return U4MLSensor_Camera::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_U4MLSensor_Camera(Z_Construct_UClass_U4MLSensor_Camera, &U4MLSensor_Camera::StaticClass, TEXT("/Script/UE4ML"), TEXT("U4MLSensor_Camera"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(U4MLSensor_Camera);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
