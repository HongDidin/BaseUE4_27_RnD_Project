// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4ML/Public/Sensors/4MLSensor_Attribute.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode4MLSensor_Attribute() {}
// Cross Module References
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor_Attribute_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor_Attribute();
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor();
	UPackage* Z_Construct_UPackage__Script_UE4ML();
	GAMEPLAYABILITIES_API UClass* Z_Construct_UClass_UAttributeSet_NoRegister();
// End Cross Module References
	void U4MLSensor_Attribute::StaticRegisterNativesU4MLSensor_Attribute()
	{
	}
	UClass* Z_Construct_UClass_U4MLSensor_Attribute_NoRegister()
	{
		return U4MLSensor_Attribute::StaticClass();
	}
	struct Z_Construct_UClass_U4MLSensor_Attribute_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AttributeSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AttributeSet;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_U4MLSensor_Attribute_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_U4MLSensor,
		(UObject* (*)())Z_Construct_UPackage__Script_UE4ML,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Attribute_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Sensors/4MLSensor_Attribute.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Attribute.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Attribute_Statics::NewProp_AttributeSet_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Attribute.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLSensor_Attribute_Statics::NewProp_AttributeSet = { "AttributeSet", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSensor_Attribute, AttributeSet), Z_Construct_UClass_UAttributeSet_NoRegister, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Attribute_Statics::NewProp_AttributeSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Attribute_Statics::NewProp_AttributeSet_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_U4MLSensor_Attribute_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Attribute_Statics::NewProp_AttributeSet,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_U4MLSensor_Attribute_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<U4MLSensor_Attribute>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_U4MLSensor_Attribute_Statics::ClassParams = {
		&U4MLSensor_Attribute::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_U4MLSensor_Attribute_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Attribute_Statics::PropPointers),
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Attribute_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Attribute_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_U4MLSensor_Attribute()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_U4MLSensor_Attribute_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(U4MLSensor_Attribute, 466412846);
	template<> UE4ML_API UClass* StaticClass<U4MLSensor_Attribute>()
	{
		return U4MLSensor_Attribute::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_U4MLSensor_Attribute(Z_Construct_UClass_U4MLSensor_Attribute, &U4MLSensor_Attribute::StaticClass, TEXT("/Script/UE4ML"), TEXT("U4MLSensor_Attribute"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(U4MLSensor_Attribute);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
