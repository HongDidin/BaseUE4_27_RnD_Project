// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UE4ML_4MLActuator_Camera_generated_h
#error "4MLActuator_Camera.generated.h already included, missing '#pragma once' in 4MLActuator_Camera.h"
#endif
#define UE4ML_4MLActuator_Camera_generated_h

#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_SPARSE_DATA
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_RPC_WRAPPERS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesU4MLActuator_Camera(); \
	friend struct Z_Construct_UClass_U4MLActuator_Camera_Statics; \
public: \
	DECLARE_CLASS(U4MLActuator_Camera, U4MLActuator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLActuator_Camera)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_INCLASS \
private: \
	static void StaticRegisterNativesU4MLActuator_Camera(); \
	friend struct Z_Construct_UClass_U4MLActuator_Camera_Statics; \
public: \
	DECLARE_CLASS(U4MLActuator_Camera, U4MLActuator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLActuator_Camera)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API U4MLActuator_Camera(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLActuator_Camera) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLActuator_Camera); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLActuator_Camera); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLActuator_Camera(U4MLActuator_Camera&&); \
	NO_API U4MLActuator_Camera(const U4MLActuator_Camera&); \
public:


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLActuator_Camera(U4MLActuator_Camera&&); \
	NO_API U4MLActuator_Camera(const U4MLActuator_Camera&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLActuator_Camera); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLActuator_Camera); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLActuator_Camera)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_11_PROLOG
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_RPC_WRAPPERS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_INCLASS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UE4ML_API UClass* StaticClass<class U4MLActuator_Camera>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_Camera_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
