// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4ML/Public/Sensors/4MLSensor_AIPerception.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode4MLSensor_AIPerception() {}
// Cross Module References
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor_AIPerception_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor_AIPerception();
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor();
	UPackage* Z_Construct_UPackage__Script_UE4ML();
	AIMODULE_API UClass* Z_Construct_UClass_UAIPerceptionComponent_NoRegister();
// End Cross Module References
	void U4MLSensor_AIPerception::StaticRegisterNativesU4MLSensor_AIPerception()
	{
	}
	UClass* Z_Construct_UClass_U4MLSensor_AIPerception_NoRegister()
	{
		return U4MLSensor_AIPerception::StaticClass();
	}
	struct Z_Construct_UClass_U4MLSensor_AIPerception_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_PerceptionComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_PerceptionComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bSenseOnlyChanges_MetaData[];
#endif
		static void NewProp_bSenseOnlyChanges_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bSenseOnlyChanges;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_U4MLSensor_AIPerception_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_U4MLSensor,
		(UObject* (*)())Z_Construct_UPackage__Script_UE4ML,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_AIPerception_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** When applied to a player controller will create an AIPerception component for \n *\x09that player and plug it into the AIPerceptionSystem. The sensor will report \n *\x09information gathered by the perception system on the behalf of this agent.\n *\x09@see UAIPerceptionComponent::ProcessStimuli\n *\n *\x09Note that the world needs to be configured to allow AI Systems to be created\n *\x09@see Server.Configure mz@todo replace with the proper reference\n */" },
		{ "IncludePath", "Sensors/4MLSensor_AIPerception.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_AIPerception.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "When applied to a player controller will create an AIPerception component for\n   that player and plug it into the AIPerceptionSystem. The sensor will report\n   information gathered by the perception system on the behalf of this agent.\n   @see UAIPerceptionComponent::ProcessStimuli\n\n   Note that the world needs to be configured to allow AI Systems to be created\n   @see Server.Configure mz@todo replace with the proper reference" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_AIPerception_Statics::NewProp_PerceptionComponent_MetaData[] = {
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_AIPerception.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLSensor_AIPerception_Statics::NewProp_PerceptionComponent = { "PerceptionComponent", nullptr, (EPropertyFlags)0x0020080000080008, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSensor_AIPerception, PerceptionComponent), Z_Construct_UClass_UAIPerceptionComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_AIPerception_Statics::NewProp_PerceptionComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_AIPerception_Statics::NewProp_PerceptionComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_AIPerception_Statics::NewProp_bSenseOnlyChanges_MetaData[] = {
		{ "Comment", "/** When set to true will only gather perception \"delta\" meaning consecutive\n\x09 *\x09updates will consist of new perception information. Defaults to \"false\" \n\x09 *\x09which means that every update all of data contained by the PerceptionComponent \n\x09 *\x09will be \"sensed\" */" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_AIPerception.h" },
		{ "ToolTip", "When set to true will only gather perception \"delta\" meaning consecutive\n    updates will consist of new perception information. Defaults to \"false\"\n    which means that every update all of data contained by the PerceptionComponent\n    will be \"sensed\"" },
	};
#endif
	void Z_Construct_UClass_U4MLSensor_AIPerception_Statics::NewProp_bSenseOnlyChanges_SetBit(void* Obj)
	{
		((U4MLSensor_AIPerception*)Obj)->bSenseOnlyChanges = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_U4MLSensor_AIPerception_Statics::NewProp_bSenseOnlyChanges = { "bSenseOnlyChanges", nullptr, (EPropertyFlags)0x0020080000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(U4MLSensor_AIPerception), &Z_Construct_UClass_U4MLSensor_AIPerception_Statics::NewProp_bSenseOnlyChanges_SetBit, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_AIPerception_Statics::NewProp_bSenseOnlyChanges_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_AIPerception_Statics::NewProp_bSenseOnlyChanges_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_U4MLSensor_AIPerception_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_AIPerception_Statics::NewProp_PerceptionComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_AIPerception_Statics::NewProp_bSenseOnlyChanges,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_U4MLSensor_AIPerception_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<U4MLSensor_AIPerception>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_U4MLSensor_AIPerception_Statics::ClassParams = {
		&U4MLSensor_AIPerception::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_U4MLSensor_AIPerception_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_AIPerception_Statics::PropPointers),
		0,
		0x009010A0u,
		METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_AIPerception_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_AIPerception_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_U4MLSensor_AIPerception()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_U4MLSensor_AIPerception_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(U4MLSensor_AIPerception, 3085388871);
	template<> UE4ML_API UClass* StaticClass<U4MLSensor_AIPerception>()
	{
		return U4MLSensor_AIPerception::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_U4MLSensor_AIPerception(Z_Construct_UClass_U4MLSensor_AIPerception, &U4MLSensor_AIPerception::StaticClass, TEXT("/Script/UE4ML"), TEXT("U4MLSensor_AIPerception"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(U4MLSensor_AIPerception);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
