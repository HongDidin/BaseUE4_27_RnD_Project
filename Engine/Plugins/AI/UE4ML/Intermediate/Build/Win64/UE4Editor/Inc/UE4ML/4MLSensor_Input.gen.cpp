// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4ML/Public/Sensors/4MLSensor_Input.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode4MLSensor_Input() {}
// Cross Module References
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor_Input_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor_Input();
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor();
	UPackage* Z_Construct_UPackage__Script_UE4ML();
	ENGINE_API UClass* Z_Construct_UClass_UGameViewportClient_NoRegister();
// End Cross Module References
	void U4MLSensor_Input::StaticRegisterNativesU4MLSensor_Input()
	{
	}
	UClass* Z_Construct_UClass_U4MLSensor_Input_NoRegister()
	{
		return U4MLSensor_Input::StaticClass();
	}
	struct Z_Construct_UClass_U4MLSensor_Input_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_GameViewport_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_GameViewport;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bRecordKeyRelease_MetaData[];
#endif
		static void NewProp_bRecordKeyRelease_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bRecordKeyRelease;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_U4MLSensor_Input_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_U4MLSensor,
		(UObject* (*)())Z_Construct_UPackage__Script_UE4ML,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Input_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "Comment", "/** Note that this sensor doesn't buffer input state between GetObservations call\n *\x09@todo a child class could easily do that by overriding OnInputKey/OnInputAxis and \n *\x09\x09GetObservations\n */" },
		{ "IncludePath", "Sensors/4MLSensor_Input.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Input.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
		{ "ToolTip", "Note that this sensor doesn't buffer input state between GetObservations call\n   @todo a child class could easily do that by overriding OnInputKey/OnInputAxis and\n           GetObservations" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Input_Statics::NewProp_GameViewport_MetaData[] = {
		{ "Category", "UE4ML" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Input.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLSensor_Input_Statics::NewProp_GameViewport = { "GameViewport", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLSensor_Input, GameViewport), Z_Construct_UClass_UGameViewportClient_NoRegister, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Input_Statics::NewProp_GameViewport_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Input_Statics::NewProp_GameViewport_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLSensor_Input_Statics::NewProp_bRecordKeyRelease_MetaData[] = {
		{ "Category", "UE4ML" },
		{ "ModuleRelativePath", "Public/Sensors/4MLSensor_Input.h" },
	};
#endif
	void Z_Construct_UClass_U4MLSensor_Input_Statics::NewProp_bRecordKeyRelease_SetBit(void* Obj)
	{
		((U4MLSensor_Input*)Obj)->bRecordKeyRelease = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_U4MLSensor_Input_Statics::NewProp_bRecordKeyRelease = { "bRecordKeyRelease", nullptr, (EPropertyFlags)0x0020080000010001, UE4CodeGen_Private::EPropertyGenFlags::Bool , RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(uint8), sizeof(U4MLSensor_Input), &Z_Construct_UClass_U4MLSensor_Input_Statics::NewProp_bRecordKeyRelease_SetBit, METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Input_Statics::NewProp_bRecordKeyRelease_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Input_Statics::NewProp_bRecordKeyRelease_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_U4MLSensor_Input_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Input_Statics::NewProp_GameViewport,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLSensor_Input_Statics::NewProp_bRecordKeyRelease,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_U4MLSensor_Input_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<U4MLSensor_Input>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_U4MLSensor_Input_Statics::ClassParams = {
		&U4MLSensor_Input::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_U4MLSensor_Input_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Input_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_U4MLSensor_Input_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLSensor_Input_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_U4MLSensor_Input()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_U4MLSensor_Input_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(U4MLSensor_Input, 579290498);
	template<> UE4ML_API UClass* StaticClass<U4MLSensor_Input>()
	{
		return U4MLSensor_Input::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_U4MLSensor_Input(Z_Construct_UClass_U4MLSensor_Input, &U4MLSensor_Input::StaticClass, TEXT("/Script/UE4ML"), TEXT("U4MLSensor_Input"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(U4MLSensor_Input);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
