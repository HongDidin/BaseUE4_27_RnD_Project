// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UE4ML_4MLActuator_InputKey_generated_h
#error "4MLActuator_InputKey.generated.h already included, missing '#pragma once' in 4MLActuator_InputKey.h"
#endif
#define UE4ML_4MLActuator_InputKey_generated_h

#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_SPARSE_DATA
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_RPC_WRAPPERS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesU4MLActuator_InputKey(); \
	friend struct Z_Construct_UClass_U4MLActuator_InputKey_Statics; \
public: \
	DECLARE_CLASS(U4MLActuator_InputKey, U4MLActuator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLActuator_InputKey)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_INCLASS \
private: \
	static void StaticRegisterNativesU4MLActuator_InputKey(); \
	friend struct Z_Construct_UClass_U4MLActuator_InputKey_Statics; \
public: \
	DECLARE_CLASS(U4MLActuator_InputKey, U4MLActuator, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/UE4ML"), NO_API) \
	DECLARE_SERIALIZER(U4MLActuator_InputKey)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API U4MLActuator_InputKey(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLActuator_InputKey) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLActuator_InputKey); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLActuator_InputKey); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLActuator_InputKey(U4MLActuator_InputKey&&); \
	NO_API U4MLActuator_InputKey(const U4MLActuator_InputKey&); \
public:


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API U4MLActuator_InputKey(U4MLActuator_InputKey&&); \
	NO_API U4MLActuator_InputKey(const U4MLActuator_InputKey&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, U4MLActuator_InputKey); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(U4MLActuator_InputKey); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(U4MLActuator_InputKey)


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_11_PROLOG
#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_RPC_WRAPPERS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_INCLASS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_SPARSE_DATA \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> UE4ML_API UClass* StaticClass<class U4MLActuator_InputKey>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_Actuators_4MLActuator_InputKey_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
