// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef UE4ML_4MLSpace_generated_h
#error "4MLSpace.generated.h already included, missing '#pragma once' in 4MLSpace.h"
#endif
#define UE4ML_4MLSpace_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_AI_UE4ML_Source_UE4ML_Public_4MLSpace_h


#define FOREACH_ENUM_E4MLSPACETYPE(op) \
	op(E4MLSpaceType::Discrete) \
	op(E4MLSpaceType::MultiDiscrete) \
	op(E4MLSpaceType::Box) \
	op(E4MLSpaceType::Tuple) 

enum class E4MLSpaceType : uint8;
template<> UE4ML_API UEnum* StaticEnum<E4MLSpaceType>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
