// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "UE4ML/Public/Agents/4MLAgent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCode4MLAgent() {}
// Cross Module References
	UE4ML_API UScriptStruct* Z_Construct_UScriptStruct_F4MLAgentConfig();
	UPackage* Z_Construct_UPackage__Script_UE4ML();
	UE4ML_API UScriptStruct* Z_Construct_UScriptStruct_F4MLParameterMap();
	UE4ML_API UClass* Z_Construct_UClass_U4MLAgent_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLAgent();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_APawn_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_AController_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLSensor_NoRegister();
	UE4ML_API UClass* Z_Construct_UClass_U4MLActuator_NoRegister();
// End Cross Module References
class UScriptStruct* F4MLAgentConfig::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern UE4ML_API uint32 Get_Z_Construct_UScriptStruct_F4MLAgentConfig_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_F4MLAgentConfig, Z_Construct_UPackage__Script_UE4ML(), TEXT("4MLAgentConfig"), sizeof(F4MLAgentConfig), Get_Z_Construct_UScriptStruct_F4MLAgentConfig_Hash());
	}
	return Singleton;
}
template<> UE4ML_API UScriptStruct* StaticStruct<F4MLAgentConfig>()
{
	return F4MLAgentConfig::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_F4MLAgentConfig(F4MLAgentConfig::StaticStruct, TEXT("/Script/UE4ML"), TEXT("4MLAgentConfig"), false, nullptr, nullptr);
static struct FScriptStruct_UE4ML_StaticRegisterNativesF4MLAgentConfig
{
	FScriptStruct_UE4ML_StaticRegisterNativesF4MLAgentConfig()
	{
		UScriptStruct::DeferCppStructOps<F4MLAgentConfig>(FName(TEXT("4MLAgentConfig")));
	}
} ScriptStruct_UE4ML_StaticRegisterNativesF4MLAgentConfig;
	struct Z_Construct_UScriptStruct_F4MLAgentConfig_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Sensors_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Sensors_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sensors_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Sensors;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_Actuators_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Actuators_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actuators_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Actuators;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AvatarClassName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_AvatarClassName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AgentClassName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_AgentClassName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAvatarClassExact_MetaData[];
#endif
		static void NewProp_bAvatarClassExact_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAvatarClassExact;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bAutoRequestNewAvatarUponClearingPrev_MetaData[];
#endif
		static void NewProp_bAutoRequestNewAvatarUponClearingPrev_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bAutoRequestNewAvatarUponClearingPrev;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<F4MLAgentConfig>();
	}
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Sensors_ValueProp = { "Sensors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_F4MLParameterMap, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Sensors_Key_KeyProp = { "Sensors_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Sensors_MetaData[] = {
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Sensors = { "Sensors", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(F4MLAgentConfig, Sensors), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Sensors_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Sensors_MetaData)) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Actuators_ValueProp = { "Actuators", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, Z_Construct_UScriptStruct_F4MLParameterMap, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Actuators_Key_KeyProp = { "Actuators_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Actuators_MetaData[] = {
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Actuators = { "Actuators", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(F4MLAgentConfig, Actuators), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Actuators_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Actuators_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_AvatarClassName_MetaData[] = {
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_AvatarClassName = { "AvatarClassName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(F4MLAgentConfig, AvatarClassName), METADATA_PARAMS(Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_AvatarClassName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_AvatarClassName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_AgentClassName_MetaData[] = {
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_AgentClassName = { "AgentClassName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(F4MLAgentConfig, AgentClassName), METADATA_PARAMS(Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_AgentClassName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_AgentClassName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_bAvatarClassExact_MetaData[] = {
		{ "Comment", "/** if set to true won't accept child classes of AvatarClass */" },
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
		{ "ToolTip", "if set to true won't accept child classes of AvatarClass" },
	};
#endif
	void Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_bAvatarClassExact_SetBit(void* Obj)
	{
		((F4MLAgentConfig*)Obj)->bAvatarClassExact = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_bAvatarClassExact = { "bAvatarClassExact", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(F4MLAgentConfig), &Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_bAvatarClassExact_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_bAvatarClassExact_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_bAvatarClassExact_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_bAutoRequestNewAvatarUponClearingPrev_MetaData[] = {
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
	};
#endif
	void Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_bAutoRequestNewAvatarUponClearingPrev_SetBit(void* Obj)
	{
		((F4MLAgentConfig*)Obj)->bAutoRequestNewAvatarUponClearingPrev = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_bAutoRequestNewAvatarUponClearingPrev = { "bAutoRequestNewAvatarUponClearingPrev", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(F4MLAgentConfig), &Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_bAutoRequestNewAvatarUponClearingPrev_SetBit, METADATA_PARAMS(Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_bAutoRequestNewAvatarUponClearingPrev_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_bAutoRequestNewAvatarUponClearingPrev_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Sensors_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Sensors_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Sensors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Actuators_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Actuators_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_Actuators,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_AvatarClassName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_AgentClassName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_bAvatarClassExact,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::NewProp_bAutoRequestNewAvatarUponClearingPrev,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_UE4ML,
		nullptr,
		&NewStructOps,
		"4MLAgentConfig",
		sizeof(F4MLAgentConfig),
		alignof(F4MLAgentConfig),
		Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_F4MLAgentConfig()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_F4MLAgentConfig_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_UE4ML();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("4MLAgentConfig"), sizeof(F4MLAgentConfig), Get_Z_Construct_UScriptStruct_F4MLAgentConfig_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_F4MLAgentConfig_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_F4MLAgentConfig_Hash() { return 4248703326U; }
class UScriptStruct* F4MLParameterMap::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern UE4ML_API uint32 Get_Z_Construct_UScriptStruct_F4MLParameterMap_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_F4MLParameterMap, Z_Construct_UPackage__Script_UE4ML(), TEXT("4MLParameterMap"), sizeof(F4MLParameterMap), Get_Z_Construct_UScriptStruct_F4MLParameterMap_Hash());
	}
	return Singleton;
}
template<> UE4ML_API UScriptStruct* StaticStruct<F4MLParameterMap>()
{
	return F4MLParameterMap::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_F4MLParameterMap(F4MLParameterMap::StaticStruct, TEXT("/Script/UE4ML"), TEXT("4MLParameterMap"), false, nullptr, nullptr);
static struct FScriptStruct_UE4ML_StaticRegisterNativesF4MLParameterMap
{
	FScriptStruct_UE4ML_StaticRegisterNativesF4MLParameterMap()
	{
		UScriptStruct::DeferCppStructOps<F4MLParameterMap>(FName(TEXT("4MLParameterMap")));
	}
} ScriptStruct_UE4ML_StaticRegisterNativesF4MLParameterMap;
	struct Z_Construct_UScriptStruct_F4MLParameterMap_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_Params_ValueProp;
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_Params_Key_KeyProp;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Params_MetaData[];
#endif
		static const UE4CodeGen_Private::FMapPropertyParams NewProp_Params;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_F4MLParameterMap_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_F4MLParameterMap_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<F4MLParameterMap>();
	}
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_F4MLParameterMap_Statics::NewProp_Params_ValueProp = { "Params", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 1, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UScriptStruct_F4MLParameterMap_Statics::NewProp_Params_Key_KeyProp = { "Params_Key", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_F4MLParameterMap_Statics::NewProp_Params_MetaData[] = {
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
	};
#endif
	const UE4CodeGen_Private::FMapPropertyParams Z_Construct_UScriptStruct_F4MLParameterMap_Statics::NewProp_Params = { "Params", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Map, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(F4MLParameterMap, Params), EMapPropertyFlags::None, METADATA_PARAMS(Z_Construct_UScriptStruct_F4MLParameterMap_Statics::NewProp_Params_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_F4MLParameterMap_Statics::NewProp_Params_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_F4MLParameterMap_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLParameterMap_Statics::NewProp_Params_ValueProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLParameterMap_Statics::NewProp_Params_Key_KeyProp,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_F4MLParameterMap_Statics::NewProp_Params,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_F4MLParameterMap_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_UE4ML,
		nullptr,
		&NewStructOps,
		"4MLParameterMap",
		sizeof(F4MLParameterMap),
		alignof(F4MLParameterMap),
		Z_Construct_UScriptStruct_F4MLParameterMap_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_F4MLParameterMap_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_F4MLParameterMap_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_F4MLParameterMap_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_F4MLParameterMap()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_F4MLParameterMap_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_UE4ML();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("4MLParameterMap"), sizeof(F4MLParameterMap), Get_Z_Construct_UScriptStruct_F4MLParameterMap_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_F4MLParameterMap_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_F4MLParameterMap_Hash() { return 3595933888U; }
	DEFINE_FUNCTION(U4MLAgent::execOnPawnControllerChanged)
	{
		P_GET_OBJECT(APawn,Z_Param_InPawn);
		P_GET_OBJECT(AController,Z_Param_InController);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnPawnControllerChanged(Z_Param_InPawn,Z_Param_InController);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(U4MLAgent::execOnAvatarDestroyed)
	{
		P_GET_OBJECT(AActor,Z_Param_DestroyedActor);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->OnAvatarDestroyed(Z_Param_DestroyedActor);
		P_NATIVE_END;
	}
	void U4MLAgent::StaticRegisterNativesU4MLAgent()
	{
		UClass* Class = U4MLAgent::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "OnAvatarDestroyed", &U4MLAgent::execOnAvatarDestroyed },
			{ "OnPawnControllerChanged", &U4MLAgent::execOnPawnControllerChanged },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_U4MLAgent_OnAvatarDestroyed_Statics
	{
		struct _4MLAgent_eventOnAvatarDestroyed_Parms
		{
			AActor* DestroyedActor;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_DestroyedActor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_U4MLAgent_OnAvatarDestroyed_Statics::NewProp_DestroyedActor = { "DestroyedActor", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_4MLAgent_eventOnAvatarDestroyed_Parms, DestroyedActor), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_U4MLAgent_OnAvatarDestroyed_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_U4MLAgent_OnAvatarDestroyed_Statics::NewProp_DestroyedActor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_U4MLAgent_OnAvatarDestroyed_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_U4MLAgent_OnAvatarDestroyed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_U4MLAgent, nullptr, "OnAvatarDestroyed", nullptr, nullptr, sizeof(_4MLAgent_eventOnAvatarDestroyed_Parms), Z_Construct_UFunction_U4MLAgent_OnAvatarDestroyed_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_U4MLAgent_OnAvatarDestroyed_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080400, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_U4MLAgent_OnAvatarDestroyed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_U4MLAgent_OnAvatarDestroyed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_U4MLAgent_OnAvatarDestroyed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_U4MLAgent_OnAvatarDestroyed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_U4MLAgent_OnPawnControllerChanged_Statics
	{
		struct _4MLAgent_eventOnPawnControllerChanged_Parms
		{
			APawn* InPawn;
			AController* InController;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InPawn;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_InController;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_U4MLAgent_OnPawnControllerChanged_Statics::NewProp_InPawn = { "InPawn", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_4MLAgent_eventOnPawnControllerChanged_Parms, InPawn), Z_Construct_UClass_APawn_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_U4MLAgent_OnPawnControllerChanged_Statics::NewProp_InController = { "InController", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(_4MLAgent_eventOnPawnControllerChanged_Parms, InController), Z_Construct_UClass_AController_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_U4MLAgent_OnPawnControllerChanged_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_U4MLAgent_OnPawnControllerChanged_Statics::NewProp_InPawn,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_U4MLAgent_OnPawnControllerChanged_Statics::NewProp_InController,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_U4MLAgent_OnPawnControllerChanged_Statics::Function_MetaDataParams[] = {
		{ "Comment", "/** will be bound to UGameInstance.OnPawnControllerChanged if current avatar is a pawn or a controller */" },
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
		{ "ToolTip", "will be bound to UGameInstance.OnPawnControllerChanged if current avatar is a pawn or a controller" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_U4MLAgent_OnPawnControllerChanged_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_U4MLAgent, nullptr, "OnPawnControllerChanged", nullptr, nullptr, sizeof(_4MLAgent_eventOnPawnControllerChanged_Parms), Z_Construct_UFunction_U4MLAgent_OnPawnControllerChanged_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_U4MLAgent_OnPawnControllerChanged_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00080401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_U4MLAgent_OnPawnControllerChanged_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_U4MLAgent_OnPawnControllerChanged_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_U4MLAgent_OnPawnControllerChanged()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_U4MLAgent_OnPawnControllerChanged_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_U4MLAgent_NoRegister()
	{
		return U4MLAgent::StaticClass();
	}
	struct Z_Construct_UClass_U4MLAgent_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Sensors_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sensors_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Sensors;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Actuators_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Actuators_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Actuators;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Avatar_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Avatar;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Controller_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Controller;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Pawn_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Pawn;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_U4MLAgent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_UE4ML,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_U4MLAgent_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_U4MLAgent_OnAvatarDestroyed, "OnAvatarDestroyed" }, // 4249772373
		{ &Z_Construct_UFunction_U4MLAgent_OnPawnControllerChanged, "OnPawnControllerChanged" }, // 3251566033
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLAgent_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "IncludePath", "Agents/4MLAgent.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLAgent_Statics::NewProp_Sensors_Inner = { "Sensors", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_U4MLSensor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLAgent_Statics::NewProp_Sensors_MetaData[] = {
		{ "Category", "UE4ML" },
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_U4MLAgent_Statics::NewProp_Sensors = { "Sensors", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLAgent, Sensors), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_U4MLAgent_Statics::NewProp_Sensors_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLAgent_Statics::NewProp_Sensors_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLAgent_Statics::NewProp_Actuators_Inner = { "Actuators", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_U4MLActuator_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLAgent_Statics::NewProp_Actuators_MetaData[] = {
		{ "Category", "UE4ML" },
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_U4MLAgent_Statics::NewProp_Actuators = { "Actuators", nullptr, (EPropertyFlags)0x0020080000000005, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLAgent, Actuators), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_U4MLAgent_Statics::NewProp_Actuators_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLAgent_Statics::NewProp_Actuators_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLAgent_Statics::NewProp_Avatar_MetaData[] = {
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLAgent_Statics::NewProp_Avatar = { "Avatar", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLAgent, Avatar), Z_Construct_UClass_AActor_NoRegister, METADATA_PARAMS(Z_Construct_UClass_U4MLAgent_Statics::NewProp_Avatar_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLAgent_Statics::NewProp_Avatar_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLAgent_Statics::NewProp_Controller_MetaData[] = {
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLAgent_Statics::NewProp_Controller = { "Controller", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLAgent, Controller), Z_Construct_UClass_AController_NoRegister, METADATA_PARAMS(Z_Construct_UClass_U4MLAgent_Statics::NewProp_Controller_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLAgent_Statics::NewProp_Controller_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_U4MLAgent_Statics::NewProp_Pawn_MetaData[] = {
		{ "ModuleRelativePath", "Public/Agents/4MLAgent.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_U4MLAgent_Statics::NewProp_Pawn = { "Pawn", nullptr, (EPropertyFlags)0x0040000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(U4MLAgent, Pawn), Z_Construct_UClass_APawn_NoRegister, METADATA_PARAMS(Z_Construct_UClass_U4MLAgent_Statics::NewProp_Pawn_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLAgent_Statics::NewProp_Pawn_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_U4MLAgent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLAgent_Statics::NewProp_Sensors_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLAgent_Statics::NewProp_Sensors,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLAgent_Statics::NewProp_Actuators_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLAgent_Statics::NewProp_Actuators,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLAgent_Statics::NewProp_Avatar,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLAgent_Statics::NewProp_Controller,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_U4MLAgent_Statics::NewProp_Pawn,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_U4MLAgent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<U4MLAgent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_U4MLAgent_Statics::ClassParams = {
		&U4MLAgent::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_U4MLAgent_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_U4MLAgent_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_U4MLAgent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_U4MLAgent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_U4MLAgent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_U4MLAgent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(U4MLAgent, 3629946995);
	template<> UE4ML_API UClass* StaticClass<U4MLAgent>()
	{
		return U4MLAgent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_U4MLAgent(Z_Construct_UClass_U4MLAgent, &U4MLAgent::StaticClass, TEXT("/Script/UE4ML"), TEXT("U4MLAgent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(U4MLAgent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
