// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "EnvironmentQueryEditor/Public/EdGraphSchema_EnvironmentQuery.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeEdGraphSchema_EnvironmentQuery() {}
// Cross Module References
	ENVIRONMENTQUERYEDITOR_API UClass* Z_Construct_UClass_UEdGraphSchema_EnvironmentQuery_NoRegister();
	ENVIRONMENTQUERYEDITOR_API UClass* Z_Construct_UClass_UEdGraphSchema_EnvironmentQuery();
	AIGRAPH_API UClass* Z_Construct_UClass_UAIGraphSchema();
	UPackage* Z_Construct_UPackage__Script_EnvironmentQueryEditor();
// End Cross Module References
	void UEdGraphSchema_EnvironmentQuery::StaticRegisterNativesUEdGraphSchema_EnvironmentQuery()
	{
	}
	UClass* Z_Construct_UClass_UEdGraphSchema_EnvironmentQuery_NoRegister()
	{
		return UEdGraphSchema_EnvironmentQuery::StaticClass();
	}
	struct Z_Construct_UClass_UEdGraphSchema_EnvironmentQuery_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UEdGraphSchema_EnvironmentQuery_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAIGraphSchema,
		(UObject* (*)())Z_Construct_UPackage__Script_EnvironmentQueryEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UEdGraphSchema_EnvironmentQuery_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "EdGraphSchema_EnvironmentQuery.h" },
		{ "ModuleRelativePath", "Public/EdGraphSchema_EnvironmentQuery.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UEdGraphSchema_EnvironmentQuery_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UEdGraphSchema_EnvironmentQuery>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UEdGraphSchema_EnvironmentQuery_Statics::ClassParams = {
		&UEdGraphSchema_EnvironmentQuery::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000800A0u,
		METADATA_PARAMS(Z_Construct_UClass_UEdGraphSchema_EnvironmentQuery_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UEdGraphSchema_EnvironmentQuery_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UEdGraphSchema_EnvironmentQuery()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UEdGraphSchema_EnvironmentQuery_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UEdGraphSchema_EnvironmentQuery, 702460151);
	template<> ENVIRONMENTQUERYEDITOR_API UClass* StaticClass<UEdGraphSchema_EnvironmentQuery>()
	{
		return UEdGraphSchema_EnvironmentQuery::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UEdGraphSchema_EnvironmentQuery(Z_Construct_UClass_UEdGraphSchema_EnvironmentQuery, &UEdGraphSchema_EnvironmentQuery::StaticClass, TEXT("/Script/EnvironmentQueryEditor"), TEXT("UEdGraphSchema_EnvironmentQuery"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UEdGraphSchema_EnvironmentQuery);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
