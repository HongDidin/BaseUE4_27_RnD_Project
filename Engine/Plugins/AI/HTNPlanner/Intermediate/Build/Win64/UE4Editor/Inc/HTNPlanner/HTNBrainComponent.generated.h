// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HTNPLANNER_HTNBrainComponent_generated_h
#error "HTNBrainComponent.generated.h already included, missing '#pragma once' in HTNBrainComponent.h"
#endif
#define HTNPLANNER_HTNBrainComponent_generated_h

#define Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_SPARSE_DATA
#define Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_RPC_WRAPPERS
#define Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHTNBrainComponent(); \
	friend struct Z_Construct_UClass_UHTNBrainComponent_Statics; \
public: \
	DECLARE_CLASS(UHTNBrainComponent, UBrainComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HTNPlanner"), NO_API) \
	DECLARE_SERIALIZER(UHTNBrainComponent)


#define Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUHTNBrainComponent(); \
	friend struct Z_Construct_UClass_UHTNBrainComponent_Statics; \
public: \
	DECLARE_CLASS(UHTNBrainComponent, UBrainComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HTNPlanner"), NO_API) \
	DECLARE_SERIALIZER(UHTNBrainComponent)


#define Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHTNBrainComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHTNBrainComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHTNBrainComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHTNBrainComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHTNBrainComponent(UHTNBrainComponent&&); \
	NO_API UHTNBrainComponent(const UHTNBrainComponent&); \
public:


#define Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHTNBrainComponent(UHTNBrainComponent&&); \
	NO_API UHTNBrainComponent(const UHTNBrainComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHTNBrainComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHTNBrainComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHTNBrainComponent)


#define Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_12_PROLOG
#define Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_SPARSE_DATA \
	Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_RPC_WRAPPERS \
	Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_INCLASS \
	Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_SPARSE_DATA \
	Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HTNPLANNER_API UClass* StaticClass<class UHTNBrainComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_AI_HTNPlanner_Source_HTNPlanner_Public_AI_HTNBrainComponent_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
