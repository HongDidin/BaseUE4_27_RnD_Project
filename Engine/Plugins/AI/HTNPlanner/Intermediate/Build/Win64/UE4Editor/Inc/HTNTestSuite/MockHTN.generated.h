// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef HTNTESTSUITE_MockHTN_generated_h
#error "MockHTN.generated.h already included, missing '#pragma once' in MockHTN.h"
#endif
#define HTNTESTSUITE_MockHTN_generated_h

#define Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_SPARSE_DATA
#define Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_RPC_WRAPPERS
#define Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUMockHTNComponent(); \
	friend struct Z_Construct_UClass_UMockHTNComponent_Statics; \
public: \
	DECLARE_CLASS(UMockHTNComponent, UHTNBrainComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HTNTestSuite"), NO_API) \
	DECLARE_SERIALIZER(UMockHTNComponent)


#define Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_INCLASS \
private: \
	static void StaticRegisterNativesUMockHTNComponent(); \
	friend struct Z_Construct_UClass_UMockHTNComponent_Statics; \
public: \
	DECLARE_CLASS(UMockHTNComponent, UHTNBrainComponent, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/HTNTestSuite"), NO_API) \
	DECLARE_SERIALIZER(UMockHTNComponent)


#define Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMockHTNComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMockHTNComponent) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMockHTNComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMockHTNComponent); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMockHTNComponent(UMockHTNComponent&&); \
	NO_API UMockHTNComponent(const UMockHTNComponent&); \
public:


#define Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UMockHTNComponent(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UMockHTNComponent(UMockHTNComponent&&); \
	NO_API UMockHTNComponent(const UMockHTNComponent&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UMockHTNComponent); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UMockHTNComponent); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UMockHTNComponent)


#define Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_38_PROLOG
#define Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_SPARSE_DATA \
	Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_RPC_WRAPPERS \
	Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_INCLASS \
	Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_SPARSE_DATA \
	Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h_41_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> HTNTESTSUITE_API UClass* StaticClass<class UMockHTNComponent>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_AI_HTNPlanner_Source_HTNTestSuite_Private_MockHTN_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
