// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HTNPlanner/Public/AI/HTNBrainComponent.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHTNBrainComponent() {}
// Cross Module References
	HTNPLANNER_API UClass* Z_Construct_UClass_UHTNBrainComponent_NoRegister();
	HTNPLANNER_API UClass* Z_Construct_UClass_UHTNBrainComponent();
	AIMODULE_API UClass* Z_Construct_UClass_UBrainComponent();
	UPackage* Z_Construct_UPackage__Script_HTNPlanner();
// End Cross Module References
	void UHTNBrainComponent::StaticRegisterNativesUHTNBrainComponent()
	{
	}
	UClass* Z_Construct_UClass_UHTNBrainComponent_NoRegister()
	{
		return UHTNBrainComponent::StaticClass();
	}
	struct Z_Construct_UClass_UHTNBrainComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHTNBrainComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UBrainComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_HTNPlanner,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHTNBrainComponent_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Sockets Collision" },
		{ "IncludePath", "AI/HTNBrainComponent.h" },
		{ "ModuleRelativePath", "Public/AI/HTNBrainComponent.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHTNBrainComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHTNBrainComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHTNBrainComponent_Statics::ClassParams = {
		&UHTNBrainComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00B000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UHTNBrainComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHTNBrainComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHTNBrainComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHTNBrainComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHTNBrainComponent, 1116779476);
	template<> HTNPLANNER_API UClass* StaticClass<UHTNBrainComponent>()
	{
		return UHTNBrainComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHTNBrainComponent(Z_Construct_UClass_UHTNBrainComponent, &UHTNBrainComponent::StaticClass, TEXT("/Script/HTNPlanner"), TEXT("UHTNBrainComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHTNBrainComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
