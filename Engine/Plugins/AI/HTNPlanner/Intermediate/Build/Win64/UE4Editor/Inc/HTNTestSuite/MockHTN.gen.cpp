// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "HTNTestSuite/Private/MockHTN.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeMockHTN() {}
// Cross Module References
	HTNTESTSUITE_API UClass* Z_Construct_UClass_UMockHTNComponent_NoRegister();
	HTNTESTSUITE_API UClass* Z_Construct_UClass_UMockHTNComponent();
	HTNPLANNER_API UClass* Z_Construct_UClass_UHTNBrainComponent();
	UPackage* Z_Construct_UPackage__Script_HTNTestSuite();
// End Cross Module References
	void UMockHTNComponent::StaticRegisterNativesUMockHTNComponent()
	{
	}
	UClass* Z_Construct_UClass_UMockHTNComponent_NoRegister()
	{
		return UMockHTNComponent::StaticClass();
	}
	struct Z_Construct_UClass_UMockHTNComponent_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UMockHTNComponent_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UHTNBrainComponent,
		(UObject* (*)())Z_Construct_UPackage__Script_HTNTestSuite,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UMockHTNComponent_Statics::Class_MetaDataParams[] = {
		{ "HideCategories", "Sockets Collision" },
		{ "IncludePath", "MockHTN.h" },
		{ "ModuleRelativePath", "Private/MockHTN.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UMockHTNComponent_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UMockHTNComponent>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UMockHTNComponent_Statics::ClassParams = {
		&UMockHTNComponent::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x00A000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UMockHTNComponent_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UMockHTNComponent_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UMockHTNComponent()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UMockHTNComponent_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UMockHTNComponent, 794788515);
	template<> HTNTESTSUITE_API UClass* StaticClass<UMockHTNComponent>()
	{
		return UMockHTNComponent::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UMockHTNComponent(Z_Construct_UClass_UMockHTNComponent, &UMockHTNComponent::StaticClass, TEXT("/Script/HTNTestSuite"), TEXT("UMockHTNComponent"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UMockHTNComponent);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
