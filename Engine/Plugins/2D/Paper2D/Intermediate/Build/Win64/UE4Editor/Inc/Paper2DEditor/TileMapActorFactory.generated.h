// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAPER2DEDITOR_TileMapActorFactory_generated_h
#error "TileMapActorFactory.generated.h already included, missing '#pragma once' in TileMapActorFactory.h"
#endif
#define PAPER2DEDITOR_TileMapActorFactory_generated_h

#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_SPARSE_DATA
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_RPC_WRAPPERS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTileMapActorFactory(); \
	friend struct Z_Construct_UClass_UTileMapActorFactory_Statics; \
public: \
	DECLARE_CLASS(UTileMapActorFactory, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UTileMapActorFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUTileMapActorFactory(); \
	friend struct Z_Construct_UClass_UTileMapActorFactory_Statics; \
public: \
	DECLARE_CLASS(UTileMapActorFactory, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UTileMapActorFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTileMapActorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTileMapActorFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTileMapActorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTileMapActorFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTileMapActorFactory(UTileMapActorFactory&&); \
	NO_API UTileMapActorFactory(const UTileMapActorFactory&); \
public:


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTileMapActorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTileMapActorFactory(UTileMapActorFactory&&); \
	NO_API UTileMapActorFactory(const UTileMapActorFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTileMapActorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTileMapActorFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTileMapActorFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_13_PROLOG
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_RPC_WRAPPERS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_INCLASS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class TileMapActorFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAPER2DEDITOR_API UClass* StaticClass<class UTileMapActorFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_TileMapActorFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
