// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAPERSPRITESHEETIMPORTER_PaperSpriteSheet_generated_h
#error "PaperSpriteSheet.generated.h already included, missing '#pragma once' in PaperSpriteSheet.h"
#endif
#define PAPERSPRITESHEETIMPORTER_PaperSpriteSheet_generated_h

#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_SPARSE_DATA
#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_RPC_WRAPPERS
#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#if WITH_EDITORONLY_DATA
#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_ARCHIVESERIALIZER \
	DECLARE_FSTRUCTUREDARCHIVE_SERIALIZER(UPaperSpriteSheet, NO_API)


#else
#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_ARCHIVESERIALIZER
#endif
#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPaperSpriteSheet(); \
	friend struct Z_Construct_UClass_UPaperSpriteSheet_Statics; \
public: \
	DECLARE_CLASS(UPaperSpriteSheet, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaperSpriteSheetImporter"), NO_API) \
	DECLARE_SERIALIZER(UPaperSpriteSheet) \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_ARCHIVESERIALIZER


#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUPaperSpriteSheet(); \
	friend struct Z_Construct_UClass_UPaperSpriteSheet_Statics; \
public: \
	DECLARE_CLASS(UPaperSpriteSheet, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaperSpriteSheetImporter"), NO_API) \
	DECLARE_SERIALIZER(UPaperSpriteSheet) \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_ARCHIVESERIALIZER


#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperSpriteSheet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperSpriteSheet) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperSpriteSheet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperSpriteSheet); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperSpriteSheet(UPaperSpriteSheet&&); \
	NO_API UPaperSpriteSheet(const UPaperSpriteSheet&); \
public:


#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperSpriteSheet(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperSpriteSheet(UPaperSpriteSheet&&); \
	NO_API UPaperSpriteSheet(const UPaperSpriteSheet&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperSpriteSheet); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperSpriteSheet); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperSpriteSheet)


#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_11_PROLOG
#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_RPC_WRAPPERS \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_INCLASS \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h_14_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class PaperSpriteSheet."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAPERSPRITESHEETIMPORTER_API UClass* StaticClass<class UPaperSpriteSheet>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheet_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
