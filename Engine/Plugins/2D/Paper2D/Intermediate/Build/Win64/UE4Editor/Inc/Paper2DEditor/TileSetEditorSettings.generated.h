// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAPER2DEDITOR_TileSetEditorSettings_generated_h
#error "TileSetEditorSettings.generated.h already included, missing '#pragma once' in TileSetEditorSettings.h"
#endif
#define PAPER2DEDITOR_TileSetEditorSettings_generated_h

#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_SPARSE_DATA
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_RPC_WRAPPERS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTileSetEditorSettings(); \
	friend struct Z_Construct_UClass_UTileSetEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UTileSetEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UTileSetEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUTileSetEditorSettings(); \
	friend struct Z_Construct_UClass_UTileSetEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UTileSetEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UTileSetEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTileSetEditorSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTileSetEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTileSetEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTileSetEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTileSetEditorSettings(UTileSetEditorSettings&&); \
	NO_API UTileSetEditorSettings(const UTileSetEditorSettings&); \
public:


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTileSetEditorSettings(UTileSetEditorSettings&&); \
	NO_API UTileSetEditorSettings(const UTileSetEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTileSetEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTileSetEditorSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UTileSetEditorSettings)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_11_PROLOG
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_RPC_WRAPPERS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_INCLASS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAPER2DEDITOR_API UClass* StaticClass<class UTileSetEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSetEditorSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
