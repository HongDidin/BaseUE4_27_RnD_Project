// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAPER2DEDITOR_SpriteEditorSettings_generated_h
#error "SpriteEditorSettings.generated.h already included, missing '#pragma once' in SpriteEditorSettings.h"
#endif
#define PAPER2DEDITOR_SpriteEditorSettings_generated_h

#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_SPARSE_DATA
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_RPC_WRAPPERS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUSpriteEditorSettings(); \
	friend struct Z_Construct_UClass_USpriteEditorSettings_Statics; \
public: \
	DECLARE_CLASS(USpriteEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(USpriteEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUSpriteEditorSettings(); \
	friend struct Z_Construct_UClass_USpriteEditorSettings_Statics; \
public: \
	DECLARE_CLASS(USpriteEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(USpriteEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API USpriteEditorSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(USpriteEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpriteEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpriteEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpriteEditorSettings(USpriteEditorSettings&&); \
	NO_API USpriteEditorSettings(const USpriteEditorSettings&); \
public:


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API USpriteEditorSettings(USpriteEditorSettings&&); \
	NO_API USpriteEditorSettings(const USpriteEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, USpriteEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(USpriteEditorSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(USpriteEditorSettings)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_11_PROLOG
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_RPC_WRAPPERS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_INCLASS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAPER2DEDITOR_API UClass* StaticClass<class USpriteEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_SpriteEditor_SpriteEditorSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
