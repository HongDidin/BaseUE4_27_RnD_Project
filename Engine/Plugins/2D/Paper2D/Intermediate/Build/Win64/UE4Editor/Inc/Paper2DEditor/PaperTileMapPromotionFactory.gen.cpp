// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Paper2DEditor/Classes/PaperTileMapPromotionFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaperTileMapPromotionFactory() {}
// Cross Module References
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UPaperTileMapPromotionFactory_NoRegister();
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UPaperTileMapPromotionFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_Paper2DEditor();
	PAPER2D_API UClass* Z_Construct_UClass_UPaperTileMap_NoRegister();
// End Cross Module References
	void UPaperTileMapPromotionFactory::StaticRegisterNativesUPaperTileMapPromotionFactory()
	{
	}
	UClass* Z_Construct_UClass_UPaperTileMapPromotionFactory_NoRegister()
	{
		return UPaperTileMapPromotionFactory::StaticClass();
	}
	struct Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetToRename_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AssetToRename;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_Paper2DEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PaperTileMapPromotionFactory.h" },
		{ "ModuleRelativePath", "Classes/PaperTileMapPromotionFactory.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics::NewProp_AssetToRename_MetaData[] = {
		{ "Comment", "// Object being promoted to an asset\n" },
		{ "ModuleRelativePath", "Classes/PaperTileMapPromotionFactory.h" },
		{ "ToolTip", "Object being promoted to an asset" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics::NewProp_AssetToRename = { "AssetToRename", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperTileMapPromotionFactory, AssetToRename), Z_Construct_UClass_UPaperTileMap_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics::NewProp_AssetToRename_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics::NewProp_AssetToRename_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics::NewProp_AssetToRename,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPaperTileMapPromotionFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics::ClassParams = {
		&UPaperTileMapPromotionFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPaperTileMapPromotionFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPaperTileMapPromotionFactory, 2321060826);
	template<> PAPER2DEDITOR_API UClass* StaticClass<UPaperTileMapPromotionFactory>()
	{
		return UPaperTileMapPromotionFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPaperTileMapPromotionFactory(Z_Construct_UClass_UPaperTileMapPromotionFactory, &UPaperTileMapPromotionFactory::StaticClass, TEXT("/Script/Paper2DEditor"), TEXT("UPaperTileMapPromotionFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPaperTileMapPromotionFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
