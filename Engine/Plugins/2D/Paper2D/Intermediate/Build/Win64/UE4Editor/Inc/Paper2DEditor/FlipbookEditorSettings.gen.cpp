// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Paper2DEditor/Private/FlipbookEditor/FlipbookEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFlipbookEditorSettings() {}
// Cross Module References
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UFlipbookEditorSettings_NoRegister();
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UFlipbookEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_Paper2DEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
// End Cross Module References
	void UFlipbookEditorSettings::StaticRegisterNativesUFlipbookEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UFlipbookEditorSettings_NoRegister()
	{
		return UFlipbookEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UFlipbookEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BackgroundColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BackgroundColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowGridByDefault_MetaData[];
#endif
		static void NewProp_bShowGridByDefault_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowGridByDefault;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UFlipbookEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Paper2DEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFlipbookEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Settings for the Paper2D sprite editor\n" },
		{ "IncludePath", "FlipbookEditor/FlipbookEditorSettings.h" },
		{ "ModuleRelativePath", "Private/FlipbookEditor/FlipbookEditorSettings.h" },
		{ "ToolTip", "Settings for the Paper2D sprite editor" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFlipbookEditorSettings_Statics::NewProp_BackgroundColor_MetaData[] = {
		{ "Category", "Background" },
		{ "Comment", "/** Background color in the flipbook editor */" },
		{ "HideAlphaChannel", "" },
		{ "ModuleRelativePath", "Private/FlipbookEditor/FlipbookEditorSettings.h" },
		{ "ToolTip", "Background color in the flipbook editor" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UFlipbookEditorSettings_Statics::NewProp_BackgroundColor = { "BackgroundColor", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UFlipbookEditorSettings, BackgroundColor), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UClass_UFlipbookEditorSettings_Statics::NewProp_BackgroundColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFlipbookEditorSettings_Statics::NewProp_BackgroundColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UFlipbookEditorSettings_Statics::NewProp_bShowGridByDefault_MetaData[] = {
		{ "Category", "Background" },
		{ "Comment", "/** Should the grid be shown by default when the editor is opened? */" },
		{ "ModuleRelativePath", "Private/FlipbookEditor/FlipbookEditorSettings.h" },
		{ "ToolTip", "Should the grid be shown by default when the editor is opened?" },
	};
#endif
	void Z_Construct_UClass_UFlipbookEditorSettings_Statics::NewProp_bShowGridByDefault_SetBit(void* Obj)
	{
		((UFlipbookEditorSettings*)Obj)->bShowGridByDefault = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UFlipbookEditorSettings_Statics::NewProp_bShowGridByDefault = { "bShowGridByDefault", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UFlipbookEditorSettings), &Z_Construct_UClass_UFlipbookEditorSettings_Statics::NewProp_bShowGridByDefault_SetBit, METADATA_PARAMS(Z_Construct_UClass_UFlipbookEditorSettings_Statics::NewProp_bShowGridByDefault_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UFlipbookEditorSettings_Statics::NewProp_bShowGridByDefault_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UFlipbookEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFlipbookEditorSettings_Statics::NewProp_BackgroundColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UFlipbookEditorSettings_Statics::NewProp_bShowGridByDefault,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UFlipbookEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UFlipbookEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UFlipbookEditorSettings_Statics::ClassParams = {
		&UFlipbookEditorSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UFlipbookEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UFlipbookEditorSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UFlipbookEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UFlipbookEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UFlipbookEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UFlipbookEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UFlipbookEditorSettings, 140322045);
	template<> PAPER2DEDITOR_API UClass* StaticClass<UFlipbookEditorSettings>()
	{
		return UFlipbookEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UFlipbookEditorSettings(Z_Construct_UClass_UFlipbookEditorSettings, &UFlipbookEditorSettings::StaticClass, TEXT("/Script/Paper2DEditor"), TEXT("UFlipbookEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UFlipbookEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
