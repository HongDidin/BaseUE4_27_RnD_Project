// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaperSpriteSheetImporter/Private/PaperSpriteSheet.h"
#include "Serialization/ArchiveUObjectFromStructuredArchive.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaperSpriteSheet() {}
// Cross Module References
	PAPERSPRITESHEETIMPORTER_API UClass* Z_Construct_UClass_UPaperSpriteSheet_NoRegister();
	PAPERSPRITESHEETIMPORTER_API UClass* Z_Construct_UClass_UPaperSpriteSheet();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_PaperSpriteSheetImporter();
	PAPER2D_API UClass* Z_Construct_UClass_UPaperSprite_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture2D_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UAssetImportData_NoRegister();
// End Cross Module References
	void UPaperSpriteSheet::StaticRegisterNativesUPaperSpriteSheet()
	{
	}
	UClass* Z_Construct_UClass_UPaperSpriteSheet_NoRegister()
	{
		return UPaperSpriteSheet::StaticClass();
	}
	struct Z_Construct_UClass_UPaperSpriteSheet_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SpriteNames_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpriteNames_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_SpriteNames;
		static const UE4CodeGen_Private::FSoftObjectPropertyParams NewProp_Sprites_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Sprites_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_Sprites;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TextureName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_TextureName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_Texture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Texture;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NormalMapTextureName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NormalMapTextureName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NormalMapTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_NormalMapTexture;
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AssetImportData_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_AssetImportData;
#endif // WITH_EDITORONLY_DATA
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_EDITORONLY_DATA
#endif // WITH_EDITORONLY_DATA
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPaperSpriteSheet_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_PaperSpriteSheetImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperSpriteSheet_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "DisplayThumbnail", "true" },
		{ "IncludePath", "PaperSpriteSheet.h" },
		{ "ModuleRelativePath", "Private/PaperSpriteSheet.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_SpriteNames_Inner = { "SpriteNames", nullptr, (EPropertyFlags)0x0000000000020000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_SpriteNames_MetaData[] = {
		{ "Category", "Data" },
		{ "Comment", "// The names of sprites during import\n" },
		{ "ModuleRelativePath", "Private/PaperSpriteSheet.h" },
		{ "ToolTip", "The names of sprites during import" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_SpriteNames = { "SpriteNames", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperSpriteSheet, SpriteNames), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_SpriteNames_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_SpriteNames_MetaData)) };
	const UE4CodeGen_Private::FSoftObjectPropertyParams Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_Sprites_Inner = { "Sprites", nullptr, (EPropertyFlags)0x0004000000020000, UE4CodeGen_Private::EPropertyGenFlags::SoftObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_UPaperSprite_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_Sprites_MetaData[] = {
		{ "Category", "Data" },
		{ "ModuleRelativePath", "Private/PaperSpriteSheet.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_Sprites = { "Sprites", nullptr, (EPropertyFlags)0x0014000000020001, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperSpriteSheet, Sprites), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_Sprites_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_Sprites_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_TextureName_MetaData[] = {
		{ "Category", "Data" },
		{ "Comment", "// The name of the default or diffuse texture during import\n" },
		{ "ModuleRelativePath", "Private/PaperSpriteSheet.h" },
		{ "ToolTip", "The name of the default or diffuse texture during import" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_TextureName = { "TextureName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperSpriteSheet, TextureName), METADATA_PARAMS(Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_TextureName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_TextureName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_Texture_MetaData[] = {
		{ "Category", "Data" },
		{ "Comment", "// The asset that was created for TextureName\n" },
		{ "ModuleRelativePath", "Private/PaperSpriteSheet.h" },
		{ "ToolTip", "The asset that was created for TextureName" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_Texture = { "Texture", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperSpriteSheet, Texture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_Texture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_Texture_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_NormalMapTextureName_MetaData[] = {
		{ "Category", "Data" },
		{ "Comment", "// The name of the normal map texture during import (if any)\n" },
		{ "ModuleRelativePath", "Private/PaperSpriteSheet.h" },
		{ "ToolTip", "The name of the normal map texture during import (if any)" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_NormalMapTextureName = { "NormalMapTextureName", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperSpriteSheet, NormalMapTextureName), METADATA_PARAMS(Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_NormalMapTextureName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_NormalMapTextureName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_NormalMapTexture_MetaData[] = {
		{ "Category", "Data" },
		{ "Comment", "// The asset that was created for NormalMapTextureName (if any)\n" },
		{ "ModuleRelativePath", "Private/PaperSpriteSheet.h" },
		{ "ToolTip", "The asset that was created for NormalMapTextureName (if any)" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_NormalMapTexture = { "NormalMapTexture", nullptr, (EPropertyFlags)0x0010000000020001, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperSpriteSheet, NormalMapTexture), Z_Construct_UClass_UTexture2D_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_NormalMapTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_NormalMapTexture_MetaData)) };
#if WITH_EDITORONLY_DATA
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_AssetImportData_MetaData[] = {
		{ "Category", "ImportSettings" },
		{ "Comment", "// Import data for this \n" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Private/PaperSpriteSheet.h" },
		{ "ToolTip", "Import data for this" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_AssetImportData = { "AssetImportData", nullptr, (EPropertyFlags)0x00120008000a0009, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperSpriteSheet, AssetImportData), Z_Construct_UClass_UAssetImportData_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_AssetImportData_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_AssetImportData_MetaData)) };
#endif // WITH_EDITORONLY_DATA
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPaperSpriteSheet_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_SpriteNames_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_SpriteNames,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_Sprites_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_Sprites,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_TextureName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_Texture,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_NormalMapTextureName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_NormalMapTexture,
#if WITH_EDITORONLY_DATA
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperSpriteSheet_Statics::NewProp_AssetImportData,
#endif // WITH_EDITORONLY_DATA
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPaperSpriteSheet_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPaperSpriteSheet>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPaperSpriteSheet_Statics::ClassParams = {
		&UPaperSpriteSheet::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPaperSpriteSheet_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPaperSpriteSheet_Statics::PropPointers),
		0,
		0x008000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPaperSpriteSheet_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperSpriteSheet_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPaperSpriteSheet()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPaperSpriteSheet_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPaperSpriteSheet, 2022593748);
	template<> PAPERSPRITESHEETIMPORTER_API UClass* StaticClass<UPaperSpriteSheet>()
	{
		return UPaperSpriteSheet::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPaperSpriteSheet(Z_Construct_UClass_UPaperSpriteSheet, &UPaperSpriteSheet::StaticClass, TEXT("/Script/PaperSpriteSheetImporter"), TEXT("UPaperSpriteSheet"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPaperSpriteSheet);
#if WITH_EDITORONLY_DATA
	IMPLEMENT_FSTRUCTUREDARCHIVE_SERIALIZER(UPaperSpriteSheet)
#endif
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
