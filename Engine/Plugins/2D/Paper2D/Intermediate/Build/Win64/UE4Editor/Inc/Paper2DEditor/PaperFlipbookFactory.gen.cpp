// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Paper2DEditor/Classes/PaperFlipbookFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaperFlipbookFactory() {}
// Cross Module References
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UPaperFlipbookFactory_NoRegister();
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UPaperFlipbookFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_Paper2DEditor();
// End Cross Module References
	void UPaperFlipbookFactory::StaticRegisterNativesUPaperFlipbookFactory()
	{
	}
	UClass* Z_Construct_UClass_UPaperFlipbookFactory_NoRegister()
	{
		return UPaperFlipbookFactory::StaticClass();
	}
	struct Z_Construct_UClass_UPaperFlipbookFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPaperFlipbookFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_Paper2DEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperFlipbookFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Factory for flipbooks\n */" },
		{ "IncludePath", "PaperFlipbookFactory.h" },
		{ "ModuleRelativePath", "Classes/PaperFlipbookFactory.h" },
		{ "ToolTip", "Factory for flipbooks" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPaperFlipbookFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPaperFlipbookFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPaperFlipbookFactory_Statics::ClassParams = {
		&UPaperFlipbookFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x001000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPaperFlipbookFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperFlipbookFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPaperFlipbookFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPaperFlipbookFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPaperFlipbookFactory, 3363358027);
	template<> PAPER2DEDITOR_API UClass* StaticClass<UPaperFlipbookFactory>()
	{
		return UPaperFlipbookFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPaperFlipbookFactory(Z_Construct_UClass_UPaperFlipbookFactory, &UPaperFlipbookFactory::StaticClass, TEXT("/Script/Paper2DEditor"), TEXT("UPaperFlipbookFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPaperFlipbookFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
