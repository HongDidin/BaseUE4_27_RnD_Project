// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAPER2DEDITOR_PaperSpriteActorFactory_generated_h
#error "PaperSpriteActorFactory.generated.h already included, missing '#pragma once' in PaperSpriteActorFactory.h"
#endif
#define PAPER2DEDITOR_PaperSpriteActorFactory_generated_h

#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_SPARSE_DATA
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_RPC_WRAPPERS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPaperSpriteActorFactory(); \
	friend struct Z_Construct_UClass_UPaperSpriteActorFactory_Statics; \
public: \
	DECLARE_CLASS(UPaperSpriteActorFactory, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UPaperSpriteActorFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUPaperSpriteActorFactory(); \
	friend struct Z_Construct_UClass_UPaperSpriteActorFactory_Statics; \
public: \
	DECLARE_CLASS(UPaperSpriteActorFactory, UActorFactory, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UPaperSpriteActorFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperSpriteActorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperSpriteActorFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperSpriteActorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperSpriteActorFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperSpriteActorFactory(UPaperSpriteActorFactory&&); \
	NO_API UPaperSpriteActorFactory(const UPaperSpriteActorFactory&); \
public:


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperSpriteActorFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperSpriteActorFactory(UPaperSpriteActorFactory&&); \
	NO_API UPaperSpriteActorFactory(const UPaperSpriteActorFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperSpriteActorFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperSpriteActorFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperSpriteActorFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_13_PROLOG
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_RPC_WRAPPERS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_INCLASS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h_16_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class PaperSpriteActorFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAPER2DEDITOR_API UClass* StaticClass<class UPaperSpriteActorFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperSpriteActorFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
