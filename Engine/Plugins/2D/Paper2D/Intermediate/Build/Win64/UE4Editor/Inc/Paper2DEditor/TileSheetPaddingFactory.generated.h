// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAPER2DEDITOR_TileSheetPaddingFactory_generated_h
#error "TileSheetPaddingFactory.generated.h already included, missing '#pragma once' in TileSheetPaddingFactory.h"
#endif
#define PAPER2DEDITOR_TileSheetPaddingFactory_generated_h

#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_SPARSE_DATA
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_RPC_WRAPPERS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTileSheetPaddingFactory(); \
	friend struct Z_Construct_UClass_UTileSheetPaddingFactory_Statics; \
public: \
	DECLARE_CLASS(UTileSheetPaddingFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UTileSheetPaddingFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_INCLASS \
private: \
	static void StaticRegisterNativesUTileSheetPaddingFactory(); \
	friend struct Z_Construct_UClass_UTileSheetPaddingFactory_Statics; \
public: \
	DECLARE_CLASS(UTileSheetPaddingFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UTileSheetPaddingFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTileSheetPaddingFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTileSheetPaddingFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTileSheetPaddingFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTileSheetPaddingFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTileSheetPaddingFactory(UTileSheetPaddingFactory&&); \
	NO_API UTileSheetPaddingFactory(const UTileSheetPaddingFactory&); \
public:


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTileSheetPaddingFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTileSheetPaddingFactory(UTileSheetPaddingFactory&&); \
	NO_API UTileSheetPaddingFactory(const UTileSheetPaddingFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTileSheetPaddingFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTileSheetPaddingFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTileSheetPaddingFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_17_PROLOG
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_RPC_WRAPPERS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_INCLASS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h_20_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class TileSheetPaddingFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAPER2DEDITOR_API UClass* StaticClass<class UTileSheetPaddingFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_TileSetEditor_TileSheetPaddingFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
