// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAPERSPRITESHEETIMPORTER_PaperSpriteSheetImportFactory_generated_h
#error "PaperSpriteSheetImportFactory.generated.h already included, missing '#pragma once' in PaperSpriteSheetImportFactory.h"
#endif
#define PAPERSPRITESHEETIMPORTER_PaperSpriteSheetImportFactory_generated_h

#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_SPARSE_DATA
#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_RPC_WRAPPERS
#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPaperSpriteSheetImportFactory(); \
	friend struct Z_Construct_UClass_UPaperSpriteSheetImportFactory_Statics; \
public: \
	DECLARE_CLASS(UPaperSpriteSheetImportFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaperSpriteSheetImporter"), NO_API) \
	DECLARE_SERIALIZER(UPaperSpriteSheetImportFactory)


#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUPaperSpriteSheetImportFactory(); \
	friend struct Z_Construct_UClass_UPaperSpriteSheetImportFactory_Statics; \
public: \
	DECLARE_CLASS(UPaperSpriteSheetImportFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaperSpriteSheetImporter"), NO_API) \
	DECLARE_SERIALIZER(UPaperSpriteSheetImportFactory)


#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperSpriteSheetImportFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperSpriteSheetImportFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperSpriteSheetImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperSpriteSheetImportFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperSpriteSheetImportFactory(UPaperSpriteSheetImportFactory&&); \
	NO_API UPaperSpriteSheetImportFactory(const UPaperSpriteSheetImportFactory&); \
public:


#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperSpriteSheetImportFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperSpriteSheetImportFactory(UPaperSpriteSheetImportFactory&&); \
	NO_API UPaperSpriteSheetImportFactory(const UPaperSpriteSheetImportFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperSpriteSheetImportFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperSpriteSheetImportFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperSpriteSheetImportFactory)


#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_12_PROLOG
#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_RPC_WRAPPERS \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_INCLASS \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h_15_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class PaperSpriteSheetImportFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAPERSPRITESHEETIMPORTER_API UClass* StaticClass<class UPaperSpriteSheetImportFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_2D_Paper2D_Source_PaperSpriteSheetImporter_Private_PaperSpriteSheetImportFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
