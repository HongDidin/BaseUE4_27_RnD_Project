// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "PaperSpriteSheetImporter/Private/PaperSpriteSheetReimportFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaperSpriteSheetReimportFactory() {}
// Cross Module References
	PAPERSPRITESHEETIMPORTER_API UClass* Z_Construct_UClass_UPaperSpriteSheetReimportFactory_NoRegister();
	PAPERSPRITESHEETIMPORTER_API UClass* Z_Construct_UClass_UPaperSpriteSheetReimportFactory();
	PAPERSPRITESHEETIMPORTER_API UClass* Z_Construct_UClass_UPaperSpriteSheetImportFactory();
	UPackage* Z_Construct_UPackage__Script_PaperSpriteSheetImporter();
// End Cross Module References
	void UPaperSpriteSheetReimportFactory::StaticRegisterNativesUPaperSpriteSheetReimportFactory()
	{
	}
	UClass* Z_Construct_UClass_UPaperSpriteSheetReimportFactory_NoRegister()
	{
		return UPaperSpriteSheetReimportFactory::StaticClass();
	}
	struct Z_Construct_UClass_UPaperSpriteSheetReimportFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPaperSpriteSheetReimportFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPaperSpriteSheetImportFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_PaperSpriteSheetImporter,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperSpriteSheetReimportFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Reimports a UPaperSpriteSheet asset\n" },
		{ "IncludePath", "PaperSpriteSheetReimportFactory.h" },
		{ "ModuleRelativePath", "Private/PaperSpriteSheetReimportFactory.h" },
		{ "ToolTip", "Reimports a UPaperSpriteSheet asset" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPaperSpriteSheetReimportFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPaperSpriteSheetReimportFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPaperSpriteSheetReimportFactory_Statics::ClassParams = {
		&UPaperSpriteSheetReimportFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPaperSpriteSheetReimportFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperSpriteSheetReimportFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPaperSpriteSheetReimportFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPaperSpriteSheetReimportFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPaperSpriteSheetReimportFactory, 3558209000);
	template<> PAPERSPRITESHEETIMPORTER_API UClass* StaticClass<UPaperSpriteSheetReimportFactory>()
	{
		return UPaperSpriteSheetReimportFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPaperSpriteSheetReimportFactory(Z_Construct_UClass_UPaperSpriteSheetReimportFactory, &UPaperSpriteSheetReimportFactory::StaticClass, TEXT("/Script/PaperSpriteSheetImporter"), TEXT("UPaperSpriteSheetReimportFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPaperSpriteSheetReimportFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
