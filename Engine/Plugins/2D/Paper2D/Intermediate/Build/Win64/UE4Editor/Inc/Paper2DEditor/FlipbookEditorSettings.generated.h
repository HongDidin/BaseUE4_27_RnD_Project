// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAPER2DEDITOR_FlipbookEditorSettings_generated_h
#error "FlipbookEditorSettings.generated.h already included, missing '#pragma once' in FlipbookEditorSettings.h"
#endif
#define PAPER2DEDITOR_FlipbookEditorSettings_generated_h

#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_SPARSE_DATA
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_RPC_WRAPPERS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUFlipbookEditorSettings(); \
	friend struct Z_Construct_UClass_UFlipbookEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UFlipbookEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UFlipbookEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_INCLASS \
private: \
	static void StaticRegisterNativesUFlipbookEditorSettings(); \
	friend struct Z_Construct_UClass_UFlipbookEditorSettings_Statics; \
public: \
	DECLARE_CLASS(UFlipbookEditorSettings, UObject, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UFlipbookEditorSettings) \
	static const TCHAR* StaticConfigName() {return TEXT("EditorPerProjectUserSettings");} \



#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UFlipbookEditorSettings(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UFlipbookEditorSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFlipbookEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFlipbookEditorSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFlipbookEditorSettings(UFlipbookEditorSettings&&); \
	NO_API UFlipbookEditorSettings(const UFlipbookEditorSettings&); \
public:


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UFlipbookEditorSettings(UFlipbookEditorSettings&&); \
	NO_API UFlipbookEditorSettings(const UFlipbookEditorSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UFlipbookEditorSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UFlipbookEditorSettings); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UFlipbookEditorSettings)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_11_PROLOG
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_RPC_WRAPPERS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_INCLASS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h_14_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAPER2DEDITOR_API UClass* StaticClass<class UFlipbookEditorSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_FlipbookEditor_FlipbookEditorSettings_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
