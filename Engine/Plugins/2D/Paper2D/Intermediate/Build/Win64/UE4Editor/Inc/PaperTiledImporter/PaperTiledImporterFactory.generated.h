// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAPERTILEDIMPORTER_PaperTiledImporterFactory_generated_h
#error "PaperTiledImporterFactory.generated.h already included, missing '#pragma once' in PaperTiledImporterFactory.h"
#endif
#define PAPERTILEDIMPORTER_PaperTiledImporterFactory_generated_h

#define Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_SPARSE_DATA
#define Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_RPC_WRAPPERS
#define Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPaperTiledImporterFactory(); \
	friend struct Z_Construct_UClass_UPaperTiledImporterFactory_Statics; \
public: \
	DECLARE_CLASS(UPaperTiledImporterFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaperTiledImporter"), NO_API) \
	DECLARE_SERIALIZER(UPaperTiledImporterFactory)


#define Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_INCLASS \
private: \
	static void StaticRegisterNativesUPaperTiledImporterFactory(); \
	friend struct Z_Construct_UClass_UPaperTiledImporterFactory_Statics; \
public: \
	DECLARE_CLASS(UPaperTiledImporterFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/PaperTiledImporter"), NO_API) \
	DECLARE_SERIALIZER(UPaperTiledImporterFactory)


#define Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperTiledImporterFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperTiledImporterFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperTiledImporterFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperTiledImporterFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperTiledImporterFactory(UPaperTiledImporterFactory&&); \
	NO_API UPaperTiledImporterFactory(const UPaperTiledImporterFactory&); \
public:


#define Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperTiledImporterFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperTiledImporterFactory(UPaperTiledImporterFactory&&); \
	NO_API UPaperTiledImporterFactory(const UPaperTiledImporterFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperTiledImporterFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperTiledImporterFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperTiledImporterFactory)


#define Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_70_PROLOG
#define Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_RPC_WRAPPERS \
	Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_INCLASS \
	Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h_73_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class PaperTiledImporterFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAPERTILEDIMPORTER_API UClass* StaticClass<class UPaperTiledImporterFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_2D_Paper2D_Source_PaperTiledImporter_Classes_PaperTiledImporterFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
