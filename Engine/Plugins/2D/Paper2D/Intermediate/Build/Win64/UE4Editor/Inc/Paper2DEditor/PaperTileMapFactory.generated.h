// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAPER2DEDITOR_PaperTileMapFactory_generated_h
#error "PaperTileMapFactory.generated.h already included, missing '#pragma once' in PaperTileMapFactory.h"
#endif
#define PAPER2DEDITOR_PaperTileMapFactory_generated_h

#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_SPARSE_DATA
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_RPC_WRAPPERS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPaperTileMapFactory(); \
	friend struct Z_Construct_UClass_UPaperTileMapFactory_Statics; \
public: \
	DECLARE_CLASS(UPaperTileMapFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UPaperTileMapFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUPaperTileMapFactory(); \
	friend struct Z_Construct_UClass_UPaperTileMapFactory_Statics; \
public: \
	DECLARE_CLASS(UPaperTileMapFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UPaperTileMapFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperTileMapFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperTileMapFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperTileMapFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperTileMapFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperTileMapFactory(UPaperTileMapFactory&&); \
	NO_API UPaperTileMapFactory(const UPaperTileMapFactory&); \
public:


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperTileMapFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperTileMapFactory(UPaperTileMapFactory&&); \
	NO_API UPaperTileMapFactory(const UPaperTileMapFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperTileMapFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperTileMapFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperTileMapFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_14_PROLOG
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_RPC_WRAPPERS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_INCLASS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class PaperTileMapFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAPER2DEDITOR_API UClass* StaticClass<class UPaperTileMapFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
