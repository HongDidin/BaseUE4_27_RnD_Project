// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAPER2DEDITOR_PaperFlipbookFactory_generated_h
#error "PaperFlipbookFactory.generated.h already included, missing '#pragma once' in PaperFlipbookFactory.h"
#endif
#define PAPER2DEDITOR_PaperFlipbookFactory_generated_h

#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_SPARSE_DATA
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_RPC_WRAPPERS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPaperFlipbookFactory(); \
	friend struct Z_Construct_UClass_UPaperFlipbookFactory_Statics; \
public: \
	DECLARE_CLASS(UPaperFlipbookFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UPaperFlipbookFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_INCLASS \
private: \
	static void StaticRegisterNativesUPaperFlipbookFactory(); \
	friend struct Z_Construct_UClass_UPaperFlipbookFactory_Statics; \
public: \
	DECLARE_CLASS(UPaperFlipbookFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UPaperFlipbookFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperFlipbookFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperFlipbookFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperFlipbookFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperFlipbookFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperFlipbookFactory(UPaperFlipbookFactory&&); \
	NO_API UPaperFlipbookFactory(const UPaperFlipbookFactory&); \
public:


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperFlipbookFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperFlipbookFactory(UPaperFlipbookFactory&&); \
	NO_API UPaperFlipbookFactory(const UPaperFlipbookFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperFlipbookFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperFlipbookFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperFlipbookFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_15_PROLOG
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_RPC_WRAPPERS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_INCLASS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h_18_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class PaperFlipbookFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAPER2DEDITOR_API UClass* StaticClass<class UPaperFlipbookFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperFlipbookFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
