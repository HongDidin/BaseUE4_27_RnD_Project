// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAPER2DEDITOR_PaperTileSetFactory_generated_h
#error "PaperTileSetFactory.generated.h already included, missing '#pragma once' in PaperTileSetFactory.h"
#endif
#define PAPER2DEDITOR_PaperTileSetFactory_generated_h

#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_SPARSE_DATA
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_RPC_WRAPPERS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPaperTileSetFactory(); \
	friend struct Z_Construct_UClass_UPaperTileSetFactory_Statics; \
public: \
	DECLARE_CLASS(UPaperTileSetFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UPaperTileSetFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUPaperTileSetFactory(); \
	friend struct Z_Construct_UClass_UPaperTileSetFactory_Statics; \
public: \
	DECLARE_CLASS(UPaperTileSetFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UPaperTileSetFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperTileSetFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperTileSetFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperTileSetFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperTileSetFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperTileSetFactory(UPaperTileSetFactory&&); \
	NO_API UPaperTileSetFactory(const UPaperTileSetFactory&); \
public:


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperTileSetFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperTileSetFactory(UPaperTileSetFactory&&); \
	NO_API UPaperTileSetFactory(const UPaperTileSetFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperTileSetFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperTileSetFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperTileSetFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_14_PROLOG
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_RPC_WRAPPERS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_INCLASS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class PaperTileSetFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAPER2DEDITOR_API UClass* StaticClass<class UPaperTileSetFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileSetFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
