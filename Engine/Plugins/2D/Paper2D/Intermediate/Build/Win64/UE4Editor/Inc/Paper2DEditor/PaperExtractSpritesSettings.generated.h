// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAPER2DEDITOR_PaperExtractSpritesSettings_generated_h
#error "PaperExtractSpritesSettings.generated.h already included, missing '#pragma once' in PaperExtractSpritesSettings.h"
#endif
#define PAPER2DEDITOR_PaperExtractSpritesSettings_generated_h

#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_SPARSE_DATA
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_RPC_WRAPPERS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPaperExtractSpritesSettings(); \
	friend struct Z_Construct_UClass_UPaperExtractSpritesSettings_Statics; \
public: \
	DECLARE_CLASS(UPaperExtractSpritesSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UPaperExtractSpritesSettings)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_INCLASS \
private: \
	static void StaticRegisterNativesUPaperExtractSpritesSettings(); \
	friend struct Z_Construct_UClass_UPaperExtractSpritesSettings_Statics; \
public: \
	DECLARE_CLASS(UPaperExtractSpritesSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UPaperExtractSpritesSettings)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperExtractSpritesSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperExtractSpritesSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperExtractSpritesSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperExtractSpritesSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperExtractSpritesSettings(UPaperExtractSpritesSettings&&); \
	NO_API UPaperExtractSpritesSettings(const UPaperExtractSpritesSettings&); \
public:


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperExtractSpritesSettings(UPaperExtractSpritesSettings&&); \
	NO_API UPaperExtractSpritesSettings(const UPaperExtractSpritesSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperExtractSpritesSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperExtractSpritesSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperExtractSpritesSettings)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_26_PROLOG
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_RPC_WRAPPERS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_INCLASS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAPER2DEDITOR_API UClass* StaticClass<class UPaperExtractSpritesSettings>();

#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_SPARSE_DATA
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_RPC_WRAPPERS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPaperExtractSpriteGridSettings(); \
	friend struct Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics; \
public: \
	DECLARE_CLASS(UPaperExtractSpriteGridSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UPaperExtractSpriteGridSettings)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_INCLASS \
private: \
	static void StaticRegisterNativesUPaperExtractSpriteGridSettings(); \
	friend struct Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics; \
public: \
	DECLARE_CLASS(UPaperExtractSpriteGridSettings, UObject, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UPaperExtractSpriteGridSettings)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperExtractSpriteGridSettings(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperExtractSpriteGridSettings) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperExtractSpriteGridSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperExtractSpriteGridSettings); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperExtractSpriteGridSettings(UPaperExtractSpriteGridSettings&&); \
	NO_API UPaperExtractSpriteGridSettings(const UPaperExtractSpriteGridSettings&); \
public:


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperExtractSpriteGridSettings(UPaperExtractSpriteGridSettings&&); \
	NO_API UPaperExtractSpriteGridSettings(const UPaperExtractSpriteGridSettings&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperExtractSpriteGridSettings); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperExtractSpriteGridSettings); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperExtractSpriteGridSettings)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_59_PROLOG
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_RPC_WRAPPERS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_INCLASS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h_62_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAPER2DEDITOR_API UClass* StaticClass<class UPaperExtractSpriteGridSettings>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_ExtractSprites_PaperExtractSpritesSettings_h


#define FOREACH_ENUM_ESPRITEEXTRACTMODE(op) \
	op(ESpriteExtractMode::Auto) \
	op(ESpriteExtractMode::Grid) 

enum class ESpriteExtractMode : uint8;
template<> PAPER2DEDITOR_API UEnum* StaticEnum<ESpriteExtractMode>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
