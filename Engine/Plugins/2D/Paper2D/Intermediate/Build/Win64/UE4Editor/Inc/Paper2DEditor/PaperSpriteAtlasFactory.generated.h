// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAPER2DEDITOR_PaperSpriteAtlasFactory_generated_h
#error "PaperSpriteAtlasFactory.generated.h already included, missing '#pragma once' in PaperSpriteAtlasFactory.h"
#endif
#define PAPER2DEDITOR_PaperSpriteAtlasFactory_generated_h

#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_SPARSE_DATA
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_RPC_WRAPPERS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPaperSpriteAtlasFactory(); \
	friend struct Z_Construct_UClass_UPaperSpriteAtlasFactory_Statics; \
public: \
	DECLARE_CLASS(UPaperSpriteAtlasFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UPaperSpriteAtlasFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_INCLASS \
private: \
	static void StaticRegisterNativesUPaperSpriteAtlasFactory(); \
	friend struct Z_Construct_UClass_UPaperSpriteAtlasFactory_Statics; \
public: \
	DECLARE_CLASS(UPaperSpriteAtlasFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UPaperSpriteAtlasFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperSpriteAtlasFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperSpriteAtlasFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperSpriteAtlasFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperSpriteAtlasFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperSpriteAtlasFactory(UPaperSpriteAtlasFactory&&); \
	NO_API UPaperSpriteAtlasFactory(const UPaperSpriteAtlasFactory&); \
public:


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperSpriteAtlasFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperSpriteAtlasFactory(UPaperSpriteAtlasFactory&&); \
	NO_API UPaperSpriteAtlasFactory(const UPaperSpriteAtlasFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperSpriteAtlasFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperSpriteAtlasFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperSpriteAtlasFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_10_PROLOG
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_RPC_WRAPPERS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_INCLASS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h_13_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class PaperSpriteAtlasFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAPER2DEDITOR_API UClass* StaticClass<class UPaperSpriteAtlasFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Private_Atlasing_PaperSpriteAtlasFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
