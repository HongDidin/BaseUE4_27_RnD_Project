// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Paper2DEditor/Classes/PaperFlipbookThumbnailRenderer.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaperFlipbookThumbnailRenderer() {}
// Cross Module References
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UPaperFlipbookThumbnailRenderer_NoRegister();
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UPaperFlipbookThumbnailRenderer();
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UPaperSpriteThumbnailRenderer();
	UPackage* Z_Construct_UPackage__Script_Paper2DEditor();
// End Cross Module References
	void UPaperFlipbookThumbnailRenderer::StaticRegisterNativesUPaperFlipbookThumbnailRenderer()
	{
	}
	UClass* Z_Construct_UClass_UPaperFlipbookThumbnailRenderer_NoRegister()
	{
		return UPaperFlipbookThumbnailRenderer::StaticClass();
	}
	struct Z_Construct_UClass_UPaperFlipbookThumbnailRenderer_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPaperFlipbookThumbnailRenderer_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UPaperSpriteThumbnailRenderer,
		(UObject* (*)())Z_Construct_UPackage__Script_Paper2DEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperFlipbookThumbnailRenderer_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "PaperFlipbookThumbnailRenderer.h" },
		{ "ModuleRelativePath", "Classes/PaperFlipbookThumbnailRenderer.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPaperFlipbookThumbnailRenderer_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPaperFlipbookThumbnailRenderer>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPaperFlipbookThumbnailRenderer_Statics::ClassParams = {
		&UPaperFlipbookThumbnailRenderer::StaticClass,
		"Editor",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UPaperFlipbookThumbnailRenderer_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperFlipbookThumbnailRenderer_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPaperFlipbookThumbnailRenderer()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPaperFlipbookThumbnailRenderer_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPaperFlipbookThumbnailRenderer, 1964807647);
	template<> PAPER2DEDITOR_API UClass* StaticClass<UPaperFlipbookThumbnailRenderer>()
	{
		return UPaperFlipbookThumbnailRenderer::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPaperFlipbookThumbnailRenderer(Z_Construct_UClass_UPaperFlipbookThumbnailRenderer, &UPaperFlipbookThumbnailRenderer::StaticClass, TEXT("/Script/Paper2DEditor"), TEXT("UPaperFlipbookThumbnailRenderer"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPaperFlipbookThumbnailRenderer);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
