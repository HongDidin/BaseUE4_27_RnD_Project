// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Paper2DEditor/Private/TileSetEditor/TileSheetPaddingFactory.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTileSheetPaddingFactory() {}
// Cross Module References
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UTileSheetPaddingFactory_NoRegister();
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UTileSheetPaddingFactory();
	UNREALED_API UClass* Z_Construct_UClass_UFactory();
	UPackage* Z_Construct_UPackage__Script_Paper2DEditor();
	PAPER2D_API UClass* Z_Construct_UClass_UPaperTileSet_NoRegister();
// End Cross Module References
	void UTileSheetPaddingFactory::StaticRegisterNativesUTileSheetPaddingFactory()
	{
	}
	UClass* Z_Construct_UClass_UTileSheetPaddingFactory_NoRegister()
	{
		return UTileSheetPaddingFactory::StaticClass();
	}
	struct Z_Construct_UClass_UTileSheetPaddingFactory_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceTileSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_SourceTileSet;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ExtrusionAmount_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_ExtrusionAmount;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bPadToPowerOf2_MetaData[];
#endif
		static void NewProp_bPadToPowerOf2_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bPadToPowerOf2;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bFillWithTransparentBlack_MetaData[];
#endif
		static void NewProp_bFillWithTransparentBlack_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bFillWithTransparentBlack;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTileSheetPaddingFactory_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UFactory,
		(UObject* (*)())Z_Construct_UPackage__Script_Paper2DEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileSheetPaddingFactory_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Factory used to pad out each individual tile in a tile sheet texture\n */" },
		{ "IncludePath", "TileSetEditor/TileSheetPaddingFactory.h" },
		{ "ModuleRelativePath", "Private/TileSetEditor/TileSheetPaddingFactory.h" },
		{ "ToolTip", "Factory used to pad out each individual tile in a tile sheet texture" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_SourceTileSet_MetaData[] = {
		{ "Comment", "// Source tile sheet texture\n" },
		{ "ModuleRelativePath", "Private/TileSetEditor/TileSheetPaddingFactory.h" },
		{ "ToolTip", "Source tile sheet texture" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_SourceTileSet = { "SourceTileSet", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTileSheetPaddingFactory, SourceTileSet), Z_Construct_UClass_UPaperTileSet_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_SourceTileSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_SourceTileSet_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_ExtrusionAmount_MetaData[] = {
		{ "Category", "Settings" },
		{ "ClampMin", "1" },
		{ "Comment", "// The amount to extrude out from each tile (in pixels)\n" },
		{ "ModuleRelativePath", "Private/TileSetEditor/TileSheetPaddingFactory.h" },
		{ "ToolTip", "The amount to extrude out from each tile (in pixels)" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_ExtrusionAmount = { "ExtrusionAmount", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTileSheetPaddingFactory, ExtrusionAmount), METADATA_PARAMS(Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_ExtrusionAmount_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_ExtrusionAmount_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_bPadToPowerOf2_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// Should we pad the texture to the next power of 2?\n" },
		{ "ModuleRelativePath", "Private/TileSetEditor/TileSheetPaddingFactory.h" },
		{ "ToolTip", "Should we pad the texture to the next power of 2?" },
	};
#endif
	void Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_bPadToPowerOf2_SetBit(void* Obj)
	{
		((UTileSheetPaddingFactory*)Obj)->bPadToPowerOf2 = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_bPadToPowerOf2 = { "bPadToPowerOf2", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTileSheetPaddingFactory), &Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_bPadToPowerOf2_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_bPadToPowerOf2_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_bPadToPowerOf2_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_bFillWithTransparentBlack_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// Should we use transparent black or white when filling the texture areas that aren't covered by tiles?\n" },
		{ "ModuleRelativePath", "Private/TileSetEditor/TileSheetPaddingFactory.h" },
		{ "ToolTip", "Should we use transparent black or white when filling the texture areas that aren't covered by tiles?" },
	};
#endif
	void Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_bFillWithTransparentBlack_SetBit(void* Obj)
	{
		((UTileSheetPaddingFactory*)Obj)->bFillWithTransparentBlack = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_bFillWithTransparentBlack = { "bFillWithTransparentBlack", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTileSheetPaddingFactory), &Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_bFillWithTransparentBlack_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_bFillWithTransparentBlack_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_bFillWithTransparentBlack_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTileSheetPaddingFactory_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_SourceTileSet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_ExtrusionAmount,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_bPadToPowerOf2,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTileSheetPaddingFactory_Statics::NewProp_bFillWithTransparentBlack,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTileSheetPaddingFactory_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTileSheetPaddingFactory>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTileSheetPaddingFactory_Statics::ClassParams = {
		&UTileSheetPaddingFactory::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTileSheetPaddingFactory_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTileSheetPaddingFactory_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTileSheetPaddingFactory_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTileSheetPaddingFactory_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTileSheetPaddingFactory()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTileSheetPaddingFactory_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTileSheetPaddingFactory, 919269494);
	template<> PAPER2DEDITOR_API UClass* StaticClass<UTileSheetPaddingFactory>()
	{
		return UTileSheetPaddingFactory::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTileSheetPaddingFactory(Z_Construct_UClass_UTileSheetPaddingFactory, &UTileSheetPaddingFactory::StaticClass, TEXT("/Script/Paper2DEditor"), TEXT("UTileSheetPaddingFactory"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTileSheetPaddingFactory);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
