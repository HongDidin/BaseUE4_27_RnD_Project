// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef PAPER2DEDITOR_PaperTileMapPromotionFactory_generated_h
#error "PaperTileMapPromotionFactory.generated.h already included, missing '#pragma once' in PaperTileMapPromotionFactory.h"
#endif
#define PAPER2DEDITOR_PaperTileMapPromotionFactory_generated_h

#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_SPARSE_DATA
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_RPC_WRAPPERS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUPaperTileMapPromotionFactory(); \
	friend struct Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics; \
public: \
	DECLARE_CLASS(UPaperTileMapPromotionFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UPaperTileMapPromotionFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_INCLASS \
private: \
	static void StaticRegisterNativesUPaperTileMapPromotionFactory(); \
	friend struct Z_Construct_UClass_UPaperTileMapPromotionFactory_Statics; \
public: \
	DECLARE_CLASS(UPaperTileMapPromotionFactory, UFactory, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Paper2DEditor"), NO_API) \
	DECLARE_SERIALIZER(UPaperTileMapPromotionFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperTileMapPromotionFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperTileMapPromotionFactory) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperTileMapPromotionFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperTileMapPromotionFactory); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperTileMapPromotionFactory(UPaperTileMapPromotionFactory&&); \
	NO_API UPaperTileMapPromotionFactory(const UPaperTileMapPromotionFactory&); \
public:


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UPaperTileMapPromotionFactory(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UPaperTileMapPromotionFactory(UPaperTileMapPromotionFactory&&); \
	NO_API UPaperTileMapPromotionFactory(const UPaperTileMapPromotionFactory&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UPaperTileMapPromotionFactory); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UPaperTileMapPromotionFactory); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UPaperTileMapPromotionFactory)


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_PRIVATE_PROPERTY_OFFSET
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_14_PROLOG
#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_RPC_WRAPPERS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_INCLASS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_PRIVATE_PROPERTY_OFFSET \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_SPARSE_DATA \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_RPC_WRAPPERS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_INCLASS_NO_PURE_DECLS \
	Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h_17_ENHANCED_CONSTRUCTORS \
static_assert(false, "Unknown access specifier for GENERATED_BODY() macro in class PaperTileMapPromotionFactory."); \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> PAPER2DEDITOR_API UClass* StaticClass<class UPaperTileMapPromotionFactory>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Engine_Plugins_2D_Paper2D_Source_Paper2DEditor_Classes_PaperTileMapPromotionFactory_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
