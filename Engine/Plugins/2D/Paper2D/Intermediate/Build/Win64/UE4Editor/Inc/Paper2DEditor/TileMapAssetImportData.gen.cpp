// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Paper2DEditor/Classes/TileMapAssetImportData.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTileMapAssetImportData() {}
// Cross Module References
	PAPER2DEDITOR_API UScriptStruct* Z_Construct_UScriptStruct_FTileSetImportMapping();
	UPackage* Z_Construct_UPackage__Script_Paper2DEditor();
	PAPER2D_API UClass* Z_Construct_UClass_UPaperTileSet_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UTexture_NoRegister();
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UTileMapAssetImportData_NoRegister();
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UTileMapAssetImportData();
	ENGINE_API UClass* Z_Construct_UClass_UAssetImportData();
// End Cross Module References
class UScriptStruct* FTileSetImportMapping::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern PAPER2DEDITOR_API uint32 Get_Z_Construct_UScriptStruct_FTileSetImportMapping_Hash();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FTileSetImportMapping, Z_Construct_UPackage__Script_Paper2DEditor(), TEXT("TileSetImportMapping"), sizeof(FTileSetImportMapping), Get_Z_Construct_UScriptStruct_FTileSetImportMapping_Hash());
	}
	return Singleton;
}
template<> PAPER2DEDITOR_API UScriptStruct* StaticStruct<FTileSetImportMapping>()
{
	return FTileSetImportMapping::StaticStruct();
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FTileSetImportMapping(FTileSetImportMapping::StaticStruct, TEXT("/Script/Paper2DEditor"), TEXT("TileSetImportMapping"), false, nullptr, nullptr);
static struct FScriptStruct_Paper2DEditor_StaticRegisterNativesFTileSetImportMapping
{
	FScriptStruct_Paper2DEditor_StaticRegisterNativesFTileSetImportMapping()
	{
		UScriptStruct::DeferCppStructOps<FTileSetImportMapping>(FName(TEXT("TileSetImportMapping")));
	}
} ScriptStruct_Paper2DEditor_StaticRegisterNativesFTileSetImportMapping;
	struct Z_Construct_UScriptStruct_FTileSetImportMapping_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Struct_MetaDataParams[];
#endif
		static void* NewStructOps();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SourceName_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_SourceName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportedTileSet_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_ImportedTileSet;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ImportedTexture_MetaData[];
#endif
		static const UE4CodeGen_Private::FWeakObjectPropertyParams NewProp_ImportedTexture;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FStructParams ReturnStructParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::Struct_MetaDataParams[] = {
		{ "ModuleRelativePath", "Classes/TileMapAssetImportData.h" },
	};
#endif
	void* Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::NewStructOps()
	{
		return (UScriptStruct::ICppStructOps*)new UScriptStruct::TCppStructOps<FTileSetImportMapping>();
	}
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::NewProp_SourceName_MetaData[] = {
		{ "ModuleRelativePath", "Classes/TileMapAssetImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::NewProp_SourceName = { "SourceName", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTileSetImportMapping, SourceName), METADATA_PARAMS(Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::NewProp_SourceName_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::NewProp_SourceName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::NewProp_ImportedTileSet_MetaData[] = {
		{ "ModuleRelativePath", "Classes/TileMapAssetImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::NewProp_ImportedTileSet = { "ImportedTileSet", nullptr, (EPropertyFlags)0x0014000000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTileSetImportMapping, ImportedTileSet), Z_Construct_UClass_UPaperTileSet_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::NewProp_ImportedTileSet_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::NewProp_ImportedTileSet_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::NewProp_ImportedTexture_MetaData[] = {
		{ "ModuleRelativePath", "Classes/TileMapAssetImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FWeakObjectPropertyParams Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::NewProp_ImportedTexture = { "ImportedTexture", nullptr, (EPropertyFlags)0x0014000000000000, UE4CodeGen_Private::EPropertyGenFlags::WeakObject, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(FTileSetImportMapping, ImportedTexture), Z_Construct_UClass_UTexture_NoRegister, METADATA_PARAMS(Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::NewProp_ImportedTexture_MetaData, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::NewProp_ImportedTexture_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::NewProp_SourceName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::NewProp_ImportedTileSet,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::NewProp_ImportedTexture,
	};
	const UE4CodeGen_Private::FStructParams Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::ReturnStructParams = {
		(UObject* (*)())Z_Construct_UPackage__Script_Paper2DEditor,
		nullptr,
		&NewStructOps,
		"TileSetImportMapping",
		sizeof(FTileSetImportMapping),
		alignof(FTileSetImportMapping),
		Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::PropPointers,
		UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::PropPointers),
		RF_Public|RF_Transient|RF_MarkAsNative,
		EStructFlags(0x00000001),
		METADATA_PARAMS(Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::Struct_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::Struct_MetaDataParams))
	};
	UScriptStruct* Z_Construct_UScriptStruct_FTileSetImportMapping()
	{
#if WITH_HOT_RELOAD
		extern uint32 Get_Z_Construct_UScriptStruct_FTileSetImportMapping_Hash();
		UPackage* Outer = Z_Construct_UPackage__Script_Paper2DEditor();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("TileSetImportMapping"), sizeof(FTileSetImportMapping), Get_Z_Construct_UScriptStruct_FTileSetImportMapping_Hash(), false);
#else
		static UScriptStruct* ReturnStruct = nullptr;
#endif
		if (!ReturnStruct)
		{
			UE4CodeGen_Private::ConstructUScriptStruct(ReturnStruct, Z_Construct_UScriptStruct_FTileSetImportMapping_Statics::ReturnStructParams);
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FTileSetImportMapping_Hash() { return 1745613423U; }
	void UTileMapAssetImportData::StaticRegisterNativesUTileMapAssetImportData()
	{
	}
	UClass* Z_Construct_UClass_UTileMapAssetImportData_NoRegister()
	{
		return UTileMapAssetImportData::StaticClass();
	}
	struct Z_Construct_UClass_UTileMapAssetImportData_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_TileSetMap_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TileSetMap_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_TileSetMap;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTileMapAssetImportData_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UAssetImportData,
		(UObject* (*)())Z_Construct_UPackage__Script_Paper2DEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileMapAssetImportData_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * Base class for import data and options used when importing a tile map\n */" },
		{ "IncludePath", "TileMapAssetImportData.h" },
		{ "ModuleRelativePath", "Classes/TileMapAssetImportData.h" },
		{ "ToolTip", "Base class for import data and options used when importing a tile map" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTileMapAssetImportData_Statics::NewProp_TileSetMap_Inner = { "TileSetMap", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UScriptStruct_FTileSetImportMapping, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileMapAssetImportData_Statics::NewProp_TileSetMap_MetaData[] = {
		{ "ModuleRelativePath", "Classes/TileMapAssetImportData.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_UTileMapAssetImportData_Statics::NewProp_TileSetMap = { "TileSetMap", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTileMapAssetImportData, TileSetMap), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_UTileMapAssetImportData_Statics::NewProp_TileSetMap_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTileMapAssetImportData_Statics::NewProp_TileSetMap_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTileMapAssetImportData_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTileMapAssetImportData_Statics::NewProp_TileSetMap_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTileMapAssetImportData_Statics::NewProp_TileSetMap,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTileMapAssetImportData_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTileMapAssetImportData>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTileMapAssetImportData_Statics::ClassParams = {
		&UTileMapAssetImportData::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTileMapAssetImportData_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTileMapAssetImportData_Statics::PropPointers),
		0,
		0x001010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UTileMapAssetImportData_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTileMapAssetImportData_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTileMapAssetImportData()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTileMapAssetImportData_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTileMapAssetImportData, 4281038835);
	template<> PAPER2DEDITOR_API UClass* StaticClass<UTileMapAssetImportData>()
	{
		return UTileMapAssetImportData::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTileMapAssetImportData(Z_Construct_UClass_UTileMapAssetImportData, &UTileMapAssetImportData::StaticClass, TEXT("/Script/Paper2DEditor"), TEXT("UTileMapAssetImportData"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTileMapAssetImportData);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
