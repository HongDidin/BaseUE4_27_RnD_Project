// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Paper2DEditor/Private/ExtractSprites/PaperExtractSpritesSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodePaperExtractSpritesSettings() {}
// Cross Module References
	PAPER2DEDITOR_API UEnum* Z_Construct_UEnum_Paper2DEditor_ESpriteExtractMode();
	UPackage* Z_Construct_UPackage__Script_Paper2DEditor();
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UPaperExtractSpritesSettings_NoRegister();
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UPaperExtractSpritesSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FLinearColor();
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UPaperExtractSpriteGridSettings_NoRegister();
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UPaperExtractSpriteGridSettings();
// End Cross Module References
	static UEnum* ESpriteExtractMode_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Paper2DEditor_ESpriteExtractMode, Z_Construct_UPackage__Script_Paper2DEditor(), TEXT("ESpriteExtractMode"));
		}
		return Singleton;
	}
	template<> PAPER2DEDITOR_API UEnum* StaticEnum<ESpriteExtractMode>()
	{
		return ESpriteExtractMode_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_ESpriteExtractMode(ESpriteExtractMode_StaticEnum, TEXT("/Script/Paper2DEditor"), TEXT("ESpriteExtractMode"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Paper2DEditor_ESpriteExtractMode_Hash() { return 3189332702U; }
	UEnum* Z_Construct_UEnum_Paper2DEditor_ESpriteExtractMode()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Paper2DEditor();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("ESpriteExtractMode"), 0, Get_Z_Construct_UEnum_Paper2DEditor_ESpriteExtractMode_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "ESpriteExtractMode::Auto", (int64)ESpriteExtractMode::Auto },
				{ "ESpriteExtractMode::Grid", (int64)ESpriteExtractMode::Grid },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "Auto.Comment", "/** Automatically extract sprites by detecting using alpha */" },
				{ "Auto.Name", "ESpriteExtractMode::Auto" },
				{ "Auto.ToolTip", "Automatically extract sprites by detecting using alpha" },
				{ "Grid.Comment", "/** Extract sprites in a grid defined in the properties below */" },
				{ "Grid.Name", "ESpriteExtractMode::Grid" },
				{ "Grid.ToolTip", "Extract sprites in a grid defined in the properties below" },
				{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Paper2DEditor,
				nullptr,
				"ESpriteExtractMode",
				"ESpriteExtractMode",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::EnumClass,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
	void UPaperExtractSpritesSettings::StaticRegisterNativesUPaperExtractSpritesSettings()
	{
	}
	UClass* Z_Construct_UClass_UPaperExtractSpritesSettings_NoRegister()
	{
		return UPaperExtractSpritesSettings::StaticClass();
	}
	struct Z_Construct_UClass_UPaperExtractSpritesSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_SpriteExtractMode_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpriteExtractMode_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_SpriteExtractMode;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_OutlineColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutlineColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ViewportTextureTint_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_ViewportTextureTint;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_BackgroundColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_BackgroundColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NamingTemplate_MetaData[];
#endif
		static const UE4CodeGen_Private::FStrPropertyParams NewProp_NamingTemplate;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NamingStartIndex_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NamingStartIndex;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Paper2DEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n*\n*/" },
		{ "IncludePath", "ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_SpriteExtractMode_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_SpriteExtractMode_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// Sprite extract mode\n" },
		{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ToolTip", "Sprite extract mode" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_SpriteExtractMode = { "SpriteExtractMode", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperExtractSpritesSettings, SpriteExtractMode), Z_Construct_UEnum_Paper2DEditor_ESpriteExtractMode, METADATA_PARAMS(Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_SpriteExtractMode_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_SpriteExtractMode_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_OutlineColor_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// The color of the sprite boundary outlines\n" },
		{ "HideAlphaChannel", "" },
		{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ToolTip", "The color of the sprite boundary outlines" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_OutlineColor = { "OutlineColor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperExtractSpritesSettings, OutlineColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_OutlineColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_OutlineColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_ViewportTextureTint_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// Apply a tint to the texture in the viewport to improve outline visibility in this editor\n" },
		{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ToolTip", "Apply a tint to the texture in the viewport to improve outline visibility in this editor" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_ViewportTextureTint = { "ViewportTextureTint", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperExtractSpritesSettings, ViewportTextureTint), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_ViewportTextureTint_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_ViewportTextureTint_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_BackgroundColor_MetaData[] = {
		{ "Category", "Settings" },
		{ "Comment", "// The viewport background color\n" },
		{ "HideAlphaChannel", "" },
		{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ToolTip", "The viewport background color" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_BackgroundColor = { "BackgroundColor", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperExtractSpritesSettings, BackgroundColor), Z_Construct_UScriptStruct_FLinearColor, METADATA_PARAMS(Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_BackgroundColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_BackgroundColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_NamingTemplate_MetaData[] = {
		{ "Category", "Naming" },
		{ "Comment", "// The name of the sprite that will be created. {0} will get replaced by the sprite number.\n" },
		{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ToolTip", "The name of the sprite that will be created. {0} will get replaced by the sprite number." },
	};
#endif
	const UE4CodeGen_Private::FStrPropertyParams Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_NamingTemplate = { "NamingTemplate", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Str, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperExtractSpritesSettings, NamingTemplate), METADATA_PARAMS(Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_NamingTemplate_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_NamingTemplate_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_NamingStartIndex_MetaData[] = {
		{ "Category", "Naming" },
		{ "Comment", "// The number to start naming with\n" },
		{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ToolTip", "The number to start naming with" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_NamingStartIndex = { "NamingStartIndex", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperExtractSpritesSettings, NamingStartIndex), METADATA_PARAMS(Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_NamingStartIndex_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_NamingStartIndex_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_SpriteExtractMode_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_SpriteExtractMode,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_OutlineColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_ViewportTextureTint,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_BackgroundColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_NamingTemplate,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::NewProp_NamingStartIndex,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPaperExtractSpritesSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::ClassParams = {
		&UPaperExtractSpritesSettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPaperExtractSpritesSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPaperExtractSpritesSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPaperExtractSpritesSettings, 2079181783);
	template<> PAPER2DEDITOR_API UClass* StaticClass<UPaperExtractSpritesSettings>()
	{
		return UPaperExtractSpritesSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPaperExtractSpritesSettings(Z_Construct_UClass_UPaperExtractSpritesSettings, &UPaperExtractSpritesSettings::StaticClass, TEXT("/Script/Paper2DEditor"), TEXT("UPaperExtractSpritesSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPaperExtractSpritesSettings);
	void UPaperExtractSpriteGridSettings::StaticRegisterNativesUPaperExtractSpriteGridSettings()
	{
	}
	UClass* Z_Construct_UClass_UPaperExtractSpriteGridSettings_NoRegister()
	{
		return UPaperExtractSpriteGridSettings::StaticClass();
	}
	struct Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CellWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CellWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CellHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_CellHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumCellsX_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumCellsX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_NumCellsY_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_NumCellsY;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MarginX_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MarginX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MarginY_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_MarginY;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpacingX_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SpacingX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SpacingY_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_SpacingY;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Paper2DEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ObjectInitializerConstructorDeclared", "" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_CellWidth_MetaData[] = {
		{ "Category", "Grid" },
		{ "ClampMin", "1" },
		{ "Comment", "// The width of each sprite in grid mode\n" },
		{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ToolTip", "The width of each sprite in grid mode" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_CellWidth = { "CellWidth", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperExtractSpriteGridSettings, CellWidth), METADATA_PARAMS(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_CellWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_CellWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_CellHeight_MetaData[] = {
		{ "Category", "Grid" },
		{ "ClampMin", "1" },
		{ "Comment", "// The height of each sprite in grid mode\n" },
		{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ToolTip", "The height of each sprite in grid mode" },
		{ "UIMin", "1" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_CellHeight = { "CellHeight", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperExtractSpriteGridSettings, CellHeight), METADATA_PARAMS(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_CellHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_CellHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_NumCellsX_MetaData[] = {
		{ "Category", "Grid" },
		{ "ClampMin", "0" },
		{ "Comment", "// Number of cells extracted horizontally. Can be used to limit the number of sprites extracted. Set to 0 to extract all sprites\n" },
		{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ToolTip", "Number of cells extracted horizontally. Can be used to limit the number of sprites extracted. Set to 0 to extract all sprites" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_NumCellsX = { "NumCellsX", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperExtractSpriteGridSettings, NumCellsX), METADATA_PARAMS(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_NumCellsX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_NumCellsX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_NumCellsY_MetaData[] = {
		{ "Category", "Grid" },
		{ "ClampMin", "0" },
		{ "Comment", "// Number of cells extracted vertically. Can be used to limit the number of sprites extracted. Set to 0 to extract all sprites\n" },
		{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ToolTip", "Number of cells extracted vertically. Can be used to limit the number of sprites extracted. Set to 0 to extract all sprites" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_NumCellsY = { "NumCellsY", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperExtractSpriteGridSettings, NumCellsY), METADATA_PARAMS(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_NumCellsY_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_NumCellsY_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_MarginX_MetaData[] = {
		{ "Category", "Grid" },
		{ "ClampMin", "0" },
		{ "Comment", "// Margin from the left of the texture to the first sprite\n" },
		{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ToolTip", "Margin from the left of the texture to the first sprite" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_MarginX = { "MarginX", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperExtractSpriteGridSettings, MarginX), METADATA_PARAMS(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_MarginX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_MarginX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_MarginY_MetaData[] = {
		{ "Category", "Grid" },
		{ "ClampMin", "0" },
		{ "Comment", "// Margin from the top of the texture to the first sprite\n" },
		{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ToolTip", "Margin from the top of the texture to the first sprite" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_MarginY = { "MarginY", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperExtractSpriteGridSettings, MarginY), METADATA_PARAMS(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_MarginY_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_MarginY_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_SpacingX_MetaData[] = {
		{ "Category", "Grid" },
		{ "ClampMin", "0" },
		{ "Comment", "// Horizontal spacing between sprites\n" },
		{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ToolTip", "Horizontal spacing between sprites" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_SpacingX = { "SpacingX", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperExtractSpriteGridSettings, SpacingX), METADATA_PARAMS(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_SpacingX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_SpacingX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_SpacingY_MetaData[] = {
		{ "Category", "Grid" },
		{ "ClampMin", "0" },
		{ "Comment", "// Vertical spacing between sprites\n" },
		{ "ModuleRelativePath", "Private/ExtractSprites/PaperExtractSpritesSettings.h" },
		{ "ToolTip", "Vertical spacing between sprites" },
		{ "UIMin", "0" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_SpacingY = { "SpacingY", nullptr, (EPropertyFlags)0x0010000000000001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UPaperExtractSpriteGridSettings, SpacingY), METADATA_PARAMS(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_SpacingY_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_SpacingY_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_CellWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_CellHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_NumCellsX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_NumCellsY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_MarginX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_MarginY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_SpacingX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::NewProp_SpacingY,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UPaperExtractSpriteGridSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::ClassParams = {
		&UPaperExtractSpriteGridSettings::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::PropPointers),
		0,
		0x000000A0u,
		METADATA_PARAMS(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UPaperExtractSpriteGridSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UPaperExtractSpriteGridSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UPaperExtractSpriteGridSettings, 1651245299);
	template<> PAPER2DEDITOR_API UClass* StaticClass<UPaperExtractSpriteGridSettings>()
	{
		return UPaperExtractSpriteGridSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UPaperExtractSpriteGridSettings(Z_Construct_UClass_UPaperExtractSpriteGridSettings, &UPaperExtractSpriteGridSettings::StaticClass, TEXT("/Script/Paper2DEditor"), TEXT("UPaperExtractSpriteGridSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UPaperExtractSpriteGridSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
