// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Paper2DEditor/Private/TileMapEditing/TileMapEditorSettings.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTileMapEditorSettings() {}
// Cross Module References
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UTileMapEditorSettings_NoRegister();
	PAPER2DEDITOR_API UClass* Z_Construct_UClass_UTileMapEditorSettings();
	COREUOBJECT_API UClass* Z_Construct_UClass_UObject();
	UPackage* Z_Construct_UPackage__Script_Paper2DEditor();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FColor();
// End Cross Module References
	void UTileMapEditorSettings::StaticRegisterNativesUTileMapEditorSettings()
	{
	}
	UClass* Z_Construct_UClass_UTileMapEditorSettings_NoRegister()
	{
		return UTileMapEditorSettings::StaticClass();
	}
	struct Z_Construct_UClass_UTileMapEditorSettings_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultBackgroundColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultBackgroundColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_bShowGridByDefault_MetaData[];
#endif
		static void NewProp_bShowGridByDefault_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bShowGridByDefault;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultTileGridColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultTileGridColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultMultiTileGridColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultMultiTileGridColor;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultMultiTileGridWidth_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DefaultMultiTileGridWidth;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultMultiTileGridHeight_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DefaultMultiTileGridHeight;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultMultiTileGridOffsetX_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DefaultMultiTileGridOffsetX;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultMultiTileGridOffsetY_MetaData[];
#endif
		static const UE4CodeGen_Private::FIntPropertyParams NewProp_DefaultMultiTileGridOffsetY;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DefaultLayerGridColor_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DefaultLayerGridColor;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTileMapEditorSettings_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UObject,
		(UObject* (*)())Z_Construct_UPackage__Script_Paper2DEditor,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileMapEditorSettings_Statics::Class_MetaDataParams[] = {
		{ "Comment", "// Settings for the Paper2D tile map editor\n" },
		{ "IncludePath", "TileMapEditing/TileMapEditorSettings.h" },
		{ "ModuleRelativePath", "Private/TileMapEditing/TileMapEditorSettings.h" },
		{ "ToolTip", "Settings for the Paper2D tile map editor" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultBackgroundColor_MetaData[] = {
		{ "Category", "Background" },
		{ "Comment", "/** Default background color for new tile map assets */" },
		{ "HideAlphaChannel", "" },
		{ "ModuleRelativePath", "Private/TileMapEditing/TileMapEditorSettings.h" },
		{ "ToolTip", "Default background color for new tile map assets" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultBackgroundColor = { "DefaultBackgroundColor", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTileMapEditorSettings, DefaultBackgroundColor), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultBackgroundColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultBackgroundColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_bShowGridByDefault_MetaData[] = {
		{ "Category", "Grid" },
		{ "Comment", "/** Should the grid be shown by default when the editor is opened? */" },
		{ "ModuleRelativePath", "Private/TileMapEditing/TileMapEditorSettings.h" },
		{ "ToolTip", "Should the grid be shown by default when the editor is opened?" },
	};
#endif
	void Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_bShowGridByDefault_SetBit(void* Obj)
	{
		((UTileMapEditorSettings*)Obj)->bShowGridByDefault = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_bShowGridByDefault = { "bShowGridByDefault", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(UTileMapEditorSettings), &Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_bShowGridByDefault_SetBit, METADATA_PARAMS(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_bShowGridByDefault_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_bShowGridByDefault_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultTileGridColor_MetaData[] = {
		{ "Category", "Grid" },
		{ "Comment", "/** Default per tile grid color for new tile map assets */" },
		{ "HideAlphaChannel", "" },
		{ "ModuleRelativePath", "Private/TileMapEditing/TileMapEditorSettings.h" },
		{ "ToolTip", "Default per tile grid color for new tile map assets" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultTileGridColor = { "DefaultTileGridColor", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTileMapEditorSettings, DefaultTileGridColor), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultTileGridColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultTileGridColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridColor_MetaData[] = {
		{ "Category", "Grid" },
		{ "Comment", "/** Default multi tile grid color for new tile map assets */" },
		{ "HideAlphaChannel", "" },
		{ "ModuleRelativePath", "Private/TileMapEditing/TileMapEditorSettings.h" },
		{ "ToolTip", "Default multi tile grid color for new tile map assets" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridColor = { "DefaultMultiTileGridColor", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTileMapEditorSettings, DefaultMultiTileGridColor), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridColor_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridWidth_MetaData[] = {
		{ "Category", "Grid" },
		{ "Comment", "/** Default number of tiles the multi tile grid spans horizontally for new tile map assets. 0 removes vertical lines */" },
		{ "ModuleRelativePath", "Private/TileMapEditing/TileMapEditorSettings.h" },
		{ "ToolTip", "Default number of tiles the multi tile grid spans horizontally for new tile map assets. 0 removes vertical lines" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridWidth = { "DefaultMultiTileGridWidth", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTileMapEditorSettings, DefaultMultiTileGridWidth), METADATA_PARAMS(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridWidth_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridWidth_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridHeight_MetaData[] = {
		{ "Category", "Grid" },
		{ "Comment", "/** Default number of tiles the multi tile grid spans vertically for new tile map assets. 0 removes horizontal lines */" },
		{ "ModuleRelativePath", "Private/TileMapEditing/TileMapEditorSettings.h" },
		{ "ToolTip", "Default number of tiles the multi tile grid spans vertically for new tile map assets. 0 removes horizontal lines" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridHeight = { "DefaultMultiTileGridHeight", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTileMapEditorSettings, DefaultMultiTileGridHeight), METADATA_PARAMS(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridHeight_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridHeight_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridOffsetX_MetaData[] = {
		{ "Category", "Grid" },
		{ "Comment", "/** Default number of tiles the multi tile grid is shifted to the right for new tile map assets */" },
		{ "ModuleRelativePath", "Private/TileMapEditing/TileMapEditorSettings.h" },
		{ "ToolTip", "Default number of tiles the multi tile grid is shifted to the right for new tile map assets" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridOffsetX = { "DefaultMultiTileGridOffsetX", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTileMapEditorSettings, DefaultMultiTileGridOffsetX), METADATA_PARAMS(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridOffsetX_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridOffsetX_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridOffsetY_MetaData[] = {
		{ "Category", "Grid" },
		{ "Comment", "/** Default number of tiles the multi tile grid is shifted downwards for new tile map assets */" },
		{ "ModuleRelativePath", "Private/TileMapEditing/TileMapEditorSettings.h" },
		{ "ToolTip", "Default number of tiles the multi tile grid is shifted downwards for new tile map assets" },
	};
#endif
	const UE4CodeGen_Private::FIntPropertyParams Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridOffsetY = { "DefaultMultiTileGridOffsetY", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Int, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTileMapEditorSettings, DefaultMultiTileGridOffsetY), METADATA_PARAMS(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridOffsetY_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridOffsetY_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultLayerGridColor_MetaData[] = {
		{ "Category", "Grid" },
		{ "Comment", "/** Default layer grid color for new tile map assets */" },
		{ "HideAlphaChannel", "" },
		{ "ModuleRelativePath", "Private/TileMapEditing/TileMapEditorSettings.h" },
		{ "ToolTip", "Default layer grid color for new tile map assets" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultLayerGridColor = { "DefaultLayerGridColor", nullptr, (EPropertyFlags)0x0010000000004001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTileMapEditorSettings, DefaultLayerGridColor), Z_Construct_UScriptStruct_FColor, METADATA_PARAMS(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultLayerGridColor_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultLayerGridColor_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTileMapEditorSettings_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultBackgroundColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_bShowGridByDefault,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultTileGridColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridColor,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridWidth,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridHeight,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridOffsetX,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultMultiTileGridOffsetY,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTileMapEditorSettings_Statics::NewProp_DefaultLayerGridColor,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTileMapEditorSettings_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTileMapEditorSettings>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTileMapEditorSettings_Statics::ClassParams = {
		&UTileMapEditorSettings::StaticClass,
		"EditorPerProjectUserSettings",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UTileMapEditorSettings_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UTileMapEditorSettings_Statics::PropPointers),
		0,
		0x000000A4u,
		METADATA_PARAMS(Z_Construct_UClass_UTileMapEditorSettings_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTileMapEditorSettings_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTileMapEditorSettings()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTileMapEditorSettings_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTileMapEditorSettings, 3052575975);
	template<> PAPER2DEDITOR_API UClass* StaticClass<UTileMapEditorSettings>()
	{
		return UTileMapEditorSettings::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTileMapEditorSettings(Z_Construct_UClass_UTileMapEditorSettings, &UTileMapEditorSettings::StaticClass, TEXT("/Script/Paper2DEditor"), TEXT("UTileMapEditorSettings"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTileMapEditorSettings);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
