; Copyright 2011-2019 Molecular Matters GmbH, all rights reserved.

PUBLIC ?JumpToSelf@@YAXXZ

_TEXT SEGMENT

?JumpToSelf@@YAXXZ PROC

Self:
    jmp Self

?JumpToSelf@@YAXXZ ENDP

_TEXT ENDS

END
